## eFormats ##

## Installation ##

* git clone git@bitbucket.org:gwanchi/eformats.git
* cd projectname
* composer install
* copy .env.example to .env , and edit some posible data connection.
* Open the application via a web browser
* Follow the set up and application configuration process to create a new database and super user
* Create folder storage/app/public/file, storage/app/public/bulk, storage/app/public/profile  with folder permission 777 recursively
* chmod 777 -R storage, bootstrap/cache, .env
* php artisan vendor:publish --tag=datatables
* php artisan storage:link

* (Database laravel tables will need to be updated as versions changes)
  php artisan queue:table
  php artisan queue:failed-table

* php artisan migrate --path=database/migrations/version1.0.0  (Including all other version folders if exist) 
* php artisan queue:work database --sleep=3 --tries=4 &
* 

* PHP version should be 5.6.4 or higher

Supporting Operating System Plugins

1. For managing queues
***** May not be necessary on more advanced server which there is no electricity problems *****
* Install supervisor in Linux i.e. sudo apt-get install supervisor
* create /etc/supervisor/conf.d/laravel-worker.conf and paste the following codes

[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /var/www/html/eformats/artisan queue:work database --sleep=3 --tries=4
autostart=true
autorestart=true
user=root
numprocs=8
redirect_stderr=true
stdout_logfile=/var/log/supervisor/worker.log

* create the following file using the command : touch /var/log/supervisor/worker.log

2. For generating PDF from html pages
* sudo apt-get install wkhtmltopdf
* sudo apt-get install xvfb


composer dump-autoload


### Features ###
