<?php

return [  
    /*
     * Name of the company developed the system
     */
    'company' => 'INETS Co limited',   
    /*
     * Name of the this system, with the icon decorated
     */
    'app_name_icon' => 'e<i class="fa fa-paperclip"></i></i>Formats',  
    /*
     * Name of the this system
     */
    'app_name' => 'eFormats',
    /**
     * @description return the mail of the the system
     */
    'app_mail' => 'eformats@inetstz.com',
    
    'meta_content' => 'eFormats Record Management System suite with registry automation (centralised file movement and file tracking automation) using RFID and BARCODE technology',
    
];

