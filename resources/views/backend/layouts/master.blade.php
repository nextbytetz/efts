<?php
/**
 * Master view page of the main page shown when user is logged in
 */
?>
<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js" lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->
    <head>
	<title>{{ app_name() }} - {{ $title }}</title>
	<!-- start: META -->
	<meta charset="utf-8" />
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="{{ meta_content() }}" name="description" />
	<meta name="_token" content="{{ csrf_token() }}" />
	<meta content="Erick Chrysostom <gwanchi@gmail.com>" name="author" />
	<!-- end: META -->

	<!-- start: MAIN CSS -->
	<!--		<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>-->
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/bootstrap/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/jquery-ui/css/jquery-ui-1.10.2.min.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/iCheck/skins/all.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/animate.css/animate.min.css">
	<!-- end: MAIN CSS -->

	<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/owl-carousel/owl-carousel/owl.carousel.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/owl-carousel/owl-carousel/owl.theme.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/owl-carousel/owl-carousel/owl.transitions.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/fullcalendar/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/toastr/toastr.min.css">
	<!--	<link rel="stylesheet" href="{{ asset_url() }}/plugins/bootstrap-select/bootstrap-select.min.css">-->
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
	<link rel="stylesheet" href="{{ asset_url() }}/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
	<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->

	<!-- start: CORE CSS -->
	<link rel="stylesheet" href="{{ asset_url() }}/css/styles.css">
	<link rel="stylesheet" href="{{ asset_url() }}/css/styles-responsive.css">
	<link rel="stylesheet" href="{{ asset_url() }}/css/themes/theme-style8.css" type="text/css" id="skin_color">
	<link rel="stylesheet" href="{{ asset_url() }}/css/print.css" type="text/css" media="print"/>
	<!-- end: CORE CSS -->
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	@yield('after-styles-end')
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

	<!--	Start : My custom css file-->
	<link rel="stylesheet" href="{{ asset_url() }}/css/custom.css">
	<!--	End : My custom css file-->
	<link rel="shortcut icon" href="{{ public_url() }}/favicon.ico" />
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>

	<!-- start: SLIDING BAR (SB) -->
	@include('backend.includes.sliding_bar')
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">
	    <!-- start: TOPBAR -->
	    <header class="topbar navbar navbar-inverse navbar-fixed-top inner">
		<!-- start: TOPBAR CONTAINER -->
		@include('backend.includes.top_bar')
		<!-- end: TOPBAR CONTAINER -->

	    </header>
	    <!-- end: TOPBAR -->

	    <!-- start: PAGESLIDE LEFT -->
	    @include('backend.includes.pageslide_left')
	    <!-- end: PAGESLIDE LEFT -->

	    <!-- start: PAGESLIDE RIGHT -->
	    @include('backend.includes.pageslide_right')
	    <!-- end: PAGESLIDE RIGHT -->

	    <!-- start: MAIN CONTAINER -->
	    <div class="main-container inner">
		<!-- start: PAGE -->
		<div class="main-content">
		    <!-- start: PANEL CONFIGURATION MODAL FORM -->
		    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
			    <div class="modal-content">
				<div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				    </button>
				    <h4 class="modal-title">Panel Configuration</h4>
				</div>
				<div class="modal-body">
				    Here will be a configuration form
				</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-default" data-dismiss="modal">
					Close
				    </button>
				    <button type="button" class="btn btn-primary">
					Save changes
				    </button>
				</div>
			    </div>
			    <!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		    </div>
		    <!-- /.modal -->
		    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
		    <div class="container">

			<!-- start: PAGE HEADER -->
			@include('backend.includes.page_header', ['header_title' => $header_title , 'header_sub_title' => $header_sub_title ])
			<!-- end: PAGE HEADER -->

			{{-- Change to Breadcrumbs::render() if you want it to error to remind you to create the breadcrumbs for the given route --}}
			{!! Breadcrumbs::renderIfExists() !!}

			<!--			start: Start of response messages from the server after a request call -->
			@include('includes.partials.messages')
			<!--			end: End of response messages from the server after a request call -->
			<!-- start: PAGE CONTENT -->
			<div class="row">
			    <div class="col-md-12">
				<div class="panel panel-white">
				    <div class="panel-body">
					<!--Main content goes here-->
					@yield('content')
				    </div>
				</div>
			    </div>
			</div>
			@yield('history')
			<!-- end: PAGE CONTENT-->
		    </div>
		    <div class="subviews">
			<div class="subviews-container"></div>
		    </div>
		</div>
		<!-- end: PAGE -->
	    </div>
	    <!-- end: MAIN CONTAINER -->
	    <!-- start: FOOTER -->
	    <footer class="inner">
		<div class="footer-inner">
		    <div class="pull-left">
			<script>
                            document.write(new Date().getFullYear());
			</script> &copy; {{ config('app.company') }}
		    </div>
		    <div class="pull-right">
			<span class="go-top"><i class="fa fa-chevron-up"></i></span>
		    </div>
		</div>
	    </footer>
	    <!-- end: FOOTER -->

	    <!-- start: SUBVIEW SAMPLE CONTENTS -->
	    @include('backend.includes.subview_content')
	    <!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>
	<!-- start: MAIN JAVASCRIPTS -->
	<!--[if lt IE 9]>
	<script src="assets/plugins/respond.min.js"></script>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
	<script src="{{ asset_url() }}/plugins/jQuery/jquery-2.1.1.min.js"></script>
	<!--<![endif]-->
	<script>
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                }
                            });
                            var public_url = "{{ public_url() }}";
                            var url = "{{ url('/') }}";
	</script>
	<script src="{{ asset_url() }}/plugins/jquery-ui/jquery-ui-1.10.2.min.js"></script>
	<script src="{{ asset_url() }}/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="{{ asset_url() }}/plugins/blockUI/jquery.blockUI.js"></script>
	<script src="{{ asset_url() }}/plugins/iCheck/jquery.icheck.min.js"></script>
	<script src="{{ asset_url() }}/plugins/moment/min/moment.min.js"></script>
	<script src="{{ asset_url() }}/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
	<script src="{{ asset_url() }}/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
	<script src="{{ asset_url() }}/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
	<script src="{{ asset_url() }}/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
	<script src="{{ asset_url() }}/plugins/jquery.appear/jquery.appear.js"></script>
	<script src="{{ asset_url() }}/plugins/jquery-cookie/jquery.cookie.js"></script>
	<script src="{{ asset_url() }}/plugins/velocity/jquery.velocity.min.js"></script>
	<script src="{{ asset_url() }}/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
	<!-- end: MAIN JAVASCRIPTS -->
	<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
	<script src="{{ asset_url() }}/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
	<script src="{{ asset_url() }}/plugins/jquery-mockjax/jquery.mockjax.js"></script>
	<script src="{{ asset_url() }}/plugins/toastr/toastr.js"></script>
	<script src="{{ asset_url() }}/js/subview.js"></script>
	<script src="{{ asset_url() }}/js/subview-examples.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
	<!-- start: CORE JAVASCRIPTS  -->
	<script src="{{ asset_url() }}/js/main.js"></script>
	<!-- end: CORE JAVASCRIPTS  -->
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

	@yield('after-scripts-end')
	@yield('history-scripts')
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

	<script>
                            jQuery(document).ready(function () {
                                Main.init();
                                /* SVExamples.init(); */
                                /* start : Detecting Barcodes */
                                var pressed = false;
                                var chars = [];
                                var form_press = false;
                                $(window).keypress(function (e) {
                                    var el = document.activeElement;
                                    if (el && ((el.tagName.toLowerCase() == 'input' || el.tagName.toLowerCase() == 'textarea'))) {
					/* Additional Condition in the above if condition Removed :  && el.type == 'text' */
                                        // focused element is a text input or textarea
                                        form_press = true;
                                    } else {
                                        form_press = false;
                                    }
                                    if (e.which >= 48 && e.which <= 57) {
                                        chars.push(String.fromCharCode(e.which));
                                    }
                                    /* console.log(e.which + ":" + chars.join("|")); */
                                    if (pressed == false) {
                                        setTimeout(function () {
                                            if (chars.length >= 1) {
                                                var barcode = chars.join("");
                                                /* console.log("Input Scanned: " + barcode); */
                                                /* Open the file here using barcode detected */
                                                if (!form_press) {
                                                    window.location.href = url + "/file/register/" + barcode + "/search";
                                                }
                                            }
                                            chars = [];
                                            pressed = false;
                                        }, 500);
                                    }
                                    pressed = true;
                                    /* Prevent form submit using carriage return(enter key in application forms) */
                                    if (!form_press) {
                                        if (e.which === 13) {
                                            /* console.log("Prevent form submit."); */
                                            e.preventDefault();
                                        }
                                    }
                                });

                                /* end : End detecting Barcodes */
                            });
	</script>

    </body>
    <!-- end: BODY -->

</html>

