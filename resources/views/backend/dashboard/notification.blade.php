@extends('backend.layouts.master', ['title' => trans('labels.general.dashboard'), 'header_title' => trans('labels.general.dashboard'), 'header_sub_title' => trans('labels.general.notification.title')])

@section('after-styles-end')
@stop

@section('content')
<div class="notifications">
    {!! notification()->renderAll() !!}
</div>
@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
@stop
@section('history-scripts')
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
@stop