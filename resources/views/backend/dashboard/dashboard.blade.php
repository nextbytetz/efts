@extends('backend.layouts.master', ['title' => trans('labels.general.dashboard'), 'header_title' => trans('labels.general.dashboard'), 'header_sub_title' => trans('labels.general.dashboard')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/css/datatables/buttons.dataTables.min.css") }}
<!-- start : css for generating graphs -->
{{ Html::style(asset_url() . "/plugins/nvd3/nv.d3.min.css") }}
<!-- start : css for generating graphs -->
@stop

@section('content')

<div class='row'>
    <div class="col-md-2 col-lg-2 col-sm-2">
	<div class="panel panel-default panel-white core-box">
	    <div class="panel-body no-padding">		
		<div class="partition-light-grey padding-20 text-center core-icon">
		    <i class="fa fa-inbox fa-2x icon-big"></i>
		</div>
		<div class="padding-20 core-content">
		    <h3 class="title block no-margin"> @lang('labels.backend.file_process.myfiles')</h3>
		    <span class="subtitle"> @lang('labels.backend.file_process.myfiles_helper') </span>
		</div>
	    </div>
	    <span class="badge badge-info partition-red files_desk"> 4 </span>
	    <div class="panel-footer clearfix no-padding">
		<a href="{!! route('file.process.list') . '#my_files' !!}" class="col-xs-12 padding-10 text-center text-white tooltips partition-grey" data-toggle="tooltip" data-placement="top" title="{!! trans('labels.general.more') !!}"><i class="fa fa-chevron-right"></i></a>
	    </div>
	</div>
    </div>

    <div class="col-md-2 col-lg-2 col-sm-2">
	<div class="panel panel-default panel-white core-box">
	    <div class="panel-body no-padding">		
		<div class="partition-light-grey padding-20 text-center core-icon">
		    <i class="fa fa-level-down fa-2x icon-big"></i>
		</div>
		<div class="padding-20 core-content">
		    <h3 class="title block no-margin"> @lang('labels.backend.file_process.received')</h3>
		    <span class="subtitle"> @lang('labels.backend.file_process.received_helper') </span>
		</div>
	    </div>
	    <span class="badge badge-info partition-red files_received"> 4 </span>
	    <div class="panel-footer clearfix no-padding">
		<a href="{!! route('file.process.list') . '#received_files' !!}" class="col-xs-12 padding-10 text-center text-white tooltips partition-grey" data-toggle="tooltip" data-placement="top" title="{!! trans('labels.general.more') !!}"><i class="fa fa-chevron-right"></i></a>
	    </div>
	</div>
    </div>

    <div class="col-md-2 col-lg-2 col-sm-2" style="padding-right:20px; border-right: 1px solid #ddd;">
	<div class="panel panel-default panel-white core-box">
	    <div class="panel-body no-padding">		
		<div class="partition-light-grey padding-20 text-center core-icon">
		    <i class="fa fa-retweet fa-2x icon-big"></i>
		</div>
		<div class="padding-20 core-content">
		    <h3 class="title block no-margin"> @lang('labels.backend.file_process.requests')</h3>
		    <span class="subtitle"> @lang('labels.backend.file_process.requests_helper') </span>
		</div>
	    </div>
	    <span class="badge badge-info partition-red files_requests"> 4 </span>
	    <div class="panel-footer clearfix no-padding">
		<a href="{!! route('file.process.list') . '#file_requests' !!}" class="col-xs-12 padding-10 text-center text-white tooltips partition-grey" data-toggle="tooltip" data-placement="top" title="{!! trans('labels.general.more') !!}"><i class="fa fa-chevron-right"></i></a>
	    </div>
	</div>
    </div>

    <div class="col-md-6 col-lg-6 col-sm-6">
	<div class="panel panel-white">
	    <div class="panel-heading border-light">
		<h4 class="panel-title">@lang('labels.backend.file_process.graph.overview')</h4>
	    </div>
	    <div class="panel-body partition-green">
		<div class="col-md-12">
		    <div>
			<div id="chart4" class='with-3d-shadow with-transitions'>
			    <svg></svg>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>

</div>

<hr/>
<div class="row">
    <div class="col-md-12">
	<div class="panel panel-info">
	    <div class="panel-heading">
		<h4 class="panel-title"><span class="text-bold"> @lang('labels.backend.file_process.monitoring') </span></h4>                                    
	    </div>
	    <div class="panel-body">
		{!! $dataTable->table(['class' => 'table'], true) !!}
	    </div>
	</div>
    </div>
</div>

@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
<!--<div class="row">
    <div class="col-md-12">
	<div class="panel panel-info">
	    <div class="panel-heading">
		<h4 class="panel-title"><span class="text-bold">{{ trans('history.backend.recent_history') }}</span></h4>                                    
	    </div>
	    <div class="panel-body">
		Main content goes here
		<div class="table-responsive">
		    <table id="history-table" class="table table-condensed table-hover">
			<thead>
			    <tr>
				<th>{{ trans('labels.backend.access.users.table.history') }}</th>
				<th>{{ trans('labels.general.table.created') }}</th>
			    </tr>
			</thead>
		    </table>
		</div>table-responsive
	    </div>
	</div>
    </div>
</div>-->
@stop
@section('history-scripts')
<script>
    /*
     $(function () {
     $('#history-table').DataTable({
     processing: true,
     serverSide: true,
     ajax: {
     url: '{{ route("backend.history", "file_process") }}',
     type: 'get',
     data: {column: 'active', value: 1}
     },
     columns: [
     {data: 'history', name: 'history'},
     {data: 'created_at', name: 'created_at'}
     ],
     order: [[0, "asc"]],
     searchDelay: 500
     });
     });
     **/
</script>
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/dataTables.buttons.min.js") }}
{{ Html::script(public_url(). "/vendor/datatables/buttons.server-side.js") }}
<!-- start : script for generating graphs -->
{{ Html::script(asset_url(). "/plugins/nvd3/lib/d3.v3.js") }}
{{ Html::script(asset_url() . "/plugins/nvd3/nv.d3.min.js") }}
<!-- end : script for generating graphs -->
{!! $dataTable->scripts() !!}
<script>
    $(document).ready(function () {
        runChart4();
    });
    var runChart4 = function () {
        function randValue() {
            return (Math.floor(Math.random() * (100 + 4000 - 2000))) + 2000;
        }
        ;
        function createSeries() {
            var myFileHistory = <?= $myFileHistory ?>;
            var dispatchedFileHistory = <?= $dispatchedFileHistory ?>;
            for (var key_file in myFileHistory) {
                series1.push([new Date(myFileHistory[key_file].created_at), myFileHistory[key_file].total]);
            }
            for (var key_dispatched in dispatchedFileHistory) {
                series2.push([new Date(dispatchedFileHistory[key_dispatched].disposed_at), dispatchedFileHistory[key_dispatched].total]);
            }
        }
        if ($("#chart4 > svg").length) {
            var date = new Date();
            var series1 = [];
            var series2 = [];
            var firstDay, lastDay, fifthDay, tenthDay, fifteenthDay, twentiethDay, twentyfifthDay;
            createSeries();
            var data = [{
                    "key": "{!! trans('labels.backend.file_process.graph.desk') !!}",
                    "values": series1
                }, {
                    "key": "{!! trans('labels.backend.file_process.graph.dispatched') !!}",
                    "values": series2
                }];
            nv.addGraph(function () {
                var chart = nv.models.lineChart().margin({
                    top: 30,
                    right: 0,
                    bottom: 20,
                    left: 35
                }).x(function (d) {
                    return d[0];
                }).y(function (d) {
                    return d[1];
                }).useInteractiveGuideline(true).color(['#D9534F', '#ffffff']).clipEdge(true);
                var options = {
                    showControls: false,
                    showLegend: true
                };
                chart.options(options);
                chart.xAxis.tickFormat(function (d) {
                    return d3.time.format('%x')(new Date(d));
                }).showMaxMin(false);
                chart.yAxis.tickFormat(d3.format(',f'));
                d3.select('#chart4 svg').datum(data).call(chart);
                nv.utils.windowResize(chart.update);
                return chart;
            });
        }
    };
</script>
@stop