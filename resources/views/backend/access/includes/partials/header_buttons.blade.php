<div class="pull-left mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.access.users.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('access.user.index', trans('menus.backend.access.users.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('access.user.deactivated', trans('menus.backend.access.users.deactivated')) }}</li>
        </ul>
    </div><!--btn group-->

    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.access.roles.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('access.role.index', trans('menus.backend.access.roles.all')) }}</li>
	    <li class="divider"></li>
            @permission('create_role')
            <li>{{ link_to_route('access.role.create', trans('menus.backend.access.roles.create')) }}</li>
            @endauth
        </ul>
    </div>

    <!--    <div class="btn-group">
	    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		{{ trans('menus.backend.access.users.super_user') }} <span class="caret"></span>
	    </button>
	    <ul class="dropdown-menu" role="menu">
		@permission('replace_super')
		<li><a href="#" data-method="get" data-trans-button-cancel="{{ trans('buttons.general.cancel') }}" data-trans-button-confirm="{{ trans('buttons.general.crud.update') }}"  data-trans-title="{{ trans('strings.backend.general.are_you_sure') }}">
			{{ trans('menus.backend.access.users.replace_super') }}
		    </a>
		</li>
		@endauth
		<li>{{ link_to_route('access.user.super_user_history', trans('menus.backend.access.users.super_user_history')) }}</li>
	    </ul>
	</div>-->

    <!--btn group-->
</div><!--pull right-->


<div class="clearfix"></div>
<hr/>