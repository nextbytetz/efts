<div class="pull-left mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.access.users.group.option') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('access.user.group.approve', trans('buttons.general.approve'), [], ['data-method' => 'confirm', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('strings.backend.access.users.group.approve'), 'data-choice' => '1']) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('access.user.group.delete', trans('buttons.general.crud.delete'), [], ['data-method' => 'delete', 'data-trans-button-cancel' => trans('buttons.general.cancel'), 'data-trans-button-confirm' => trans('buttons.general.confirm'), 'data-trans-title' => trans('labels.general.warning'), 'data-trans-text' => trans('strings.backend.access.users.group.delete'), 'data-choice' => '1']) }}</li>
        </ul>
    </div><!--btn group-->
</div><!--pull right-->
<div class="clearfix"></div>
