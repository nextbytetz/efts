
@extends('backend.layouts.master', ['title' => trans('labels.backend.access.users.profile.management'), 'header_title' => trans('labels.backend.access.users.profile.management'), 'header_sub_title' => trans('labels.backend.access.users.profile.active')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.css") }}
@stop

@section('content')
<div class='row'>
    <div class="col-sm-5 col-md-4">
	<legend>{!! trans('labels.backend.access.users.profile.overview') !!}</legend>
	<div class="user-left">
	    <div class="center">
		<h4>{!! access()->user()->name() !!}</h4>
		<div class="fileupload fileupload-new" data-provides="fileupload">
		    <div class="user-image">
			<div class="fileupload-new thumbnail"><img src="{!! access()->user()->pic !!}"  alt=""  height="140" width="140">
			</div>
			<!--			<div class="fileupload-preview fileupload-exists thumbnail"></div>
						<div class="user-image-buttons">
						    <span class="btn btn-azure btn-file btn-sm">
							<span class="fileupload-new"><i class="fa fa-pencil"></i></span>
							<span class="fileupload-exists"><i class="fa fa-pencil"></i></span>
							<input type="file">
						    </span>
						    <a href="#" class="btn fileupload-exists btn-red btn-sm" data-dismiss="fileupload">
							<i class="fa fa-times"></i>
						    </a>
						</div>-->
		    </div>
		</div>
		<hr>
	    </div>
	    <table class="table table-condensed table-hover">
		<thead>
		    <tr>
			<th colspan="3">{!! trans('labels.backend.access.users.profile.general') !!}</th>
		    </tr>
		</thead>
		<tbody>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.username') !!}</td>
			<td>{!! access()->user()->username !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.email') !!}</td>
			<td>{!! access()->user()->email !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.gender') !!}</td>
			<td>{!! access()->user()->gender_label !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.phone') !!}</td>
			<td>{!! access()->user()->phone !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.admin') !!}</td>
			<td>
			    {!! access()->user()->admin !!}
			</td>
		    </tr>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.last') !!}</td>
			<td><span class="label label-sm label-info">{!! access()->user()->lastlogin !!}</span></td>
		    </tr>
		</tbody>
	    </table>
	    <table class="table table-condensed table-hover">
		<thead>
		    <tr>
			<th colspan="3">{!! trans('labels.backend.access.users.profile.additional') !!}</th>
		    </tr>
		</thead>
		<tbody>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.role') !!}</td>
			<td>{!! access()->user()->role_label !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans('labels.backend.access.users.profile.department') !!}</td>
			<td>{!! access()->user()->department->name !!}</td>
		    </tr>
		</tbody>
	    </table>
	</div>
    </div>
    <div class="col-sm-7 col-md-8">
	<legend>{!! trans('labels.backend.access.users.profile.edit') !!}</legend>

	{!! Form::model($user, ['url' => 'access/user/profile', 'role' => 'form', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
	<div class='row'> 

	    <div class='col-sm-6 col-md-6'>
		<div class="form-group">
		    {!! Form::label('firstname', trans('validation.attributes.backend.access.users.firstname'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::input('text', 'firstname', null, ['class' => 'form-control']) !!}
		    {!! $errors->first('firstname', '<span class="help-block label label-danger">:message</span>') !!}
		</div>

		<div class="form-group">
		    {!! Form::label('othernames', trans('validation.attributes.backend.access.users.othernames'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::input('text', 'othernames', null, ['class' => 'form-control']) !!}
		    {!! $errors->first('othernames', '<span class="help-block label label-danger">:message</span>') !!}
		</div>

		<div class="form-group">
		    {!! Form::label('username', trans('validation.attributes.backend.access.users.username'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::input('text', 'username', null, ['class' => 'form-control']) !!}
		    {!! $errors->first('username', '<span class="help-block label label-danger">:message</span>') !!}
		</div>

		<div class="form-group">
		    {!! Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::input('text', 'email', null, ['class' => 'form-control']) !!}
		    {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
		</div>

		<div class="form-group">
		    {!! Form::label('phone', trans('validation.attributes.backend.access.users.phone'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::input('text', 'phone', null, ['class' => 'form-control']) !!}
		    {!! $errors->first('phone', '<span class="help-block label label-danger">:message</span>') !!}
		</div>

		<div class="form-group">
		    {!! Form::label('country_id', trans('validation.attributes.backend.sysdef.location.country.name'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::select('country_id', $country , (count($user->department) ? $user->department->branch->region->country->country_id : []), ['class' => 'form-control search-select', 'id' => 'country_select', 'placeholder' => '']) !!}
		    <i class="fa fa-spinner fa-spin" id = "spin" style='display: none'></i>
		    {!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
		</div>
<!--		<div class="form-group">
		    {!! Form::label('region_id', trans('validation.attributes.backend.sysdef.location.region.name'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::select('region_id', $regions , (count($user->department) ? $user->department->branch->region->region_id : []), ['class' => 'form-control search-select', 'id' => 'region_select', 'placeholder' => '']) !!}
		    <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
		    {!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
		</div>
		<div class="form-group">
		    {!! Form::label('branch_id', trans('validation.attributes.backend.sysdef.location.branch.name'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::select('branch_id', $branches , (count($user->department) ? $user->department->branch->branch_id : []), ['class' => 'form-control search-select', 'id' => 'branch_select', 'placeholder' => '']) !!}
		    <i class="fa fa-spinner fa-spin" id = "spin3" style='display: none'></i>
		    {!! $errors->first('branch_id', '<span class="help-block label label-danger">:message</span>') !!}
		</div>
		<div class="form-group">
		    {!! Form::label('department_id', trans('validation.attributes.backend.sysdef.rfid.zone.departments'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::select('department_id', $departments, (count($user->department) ? $user->department->department_id : []), ['class' => 'form-control search-select', 'id' => 'department_select']); !!}
		    {!! $errors->first('department_id', '<span class="help-block label label-danger">:message</span>') !!}
		</div>-->
		
		
	    </div>
	    <div class='col-sm-6 col-md-6'>
		<div class="form-group">
		    {!! Form::label('gender', trans('validation.attributes.frontend.gender'), ['class' => 'control-label symbol required']) !!}
		    <div>
			<label class="radio-inline">
			    {!! Form::radio('gender', 'F', (($user->gender == 'F') ? true : null), ['class' => 'grey']) !!}
			    {{ trans('labels.general.female') }}
			</label>		
			<label class="radio-inline">
			    {!! Form::radio('gender', 'M', (($user->gender == 'M') ? true : null), ['class' => 'grey']) !!}
			    {{ trans('labels.general.male') }}
			</label>
		    </div>
		    {!! $errors->first('gender', '<span class="help-block label label-danger">:message</span>') !!}
		</div>
		<div class="form-group">
		    {!! Form::label('pic', trans('validation.attributes.backend.access.users.pic'), ['class' => 'control-label symbol']) !!}
		    {!! Form::input('file', 'pic', null, ['class' => 'form-control', 'id' => 'file_upload', 'data-allowed-file-extensions' => '["png","jpeg","jpg"]']) !!}
		</div>
	    </div>

	</div>
	<hr/>
	<div class="pull-right">
	    <input type="submit" class="btn btn-sm btn-info" value="{{ trans('buttons.general.crud.update') }}" />
	</div>
	{!! Form::close() !!}
    </div>
</div>

@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
@stop
@section('history-scripts')
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.js") }}
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({});
/*
        $('#country_select').on('change', function (e) {
            $("#spin").show();
            var country_id = e.target.value;
            $.get("{{ url('/') }}/getregion?country_id=" + country_id, function (data) {
                $('#region_select').empty();
                $("#region_select").select2("val", "");
                $('#region_select').html(data);
                $("#spin").hide();
            });
        });

        $('#region_select').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getbranch?region_id=" + region_id, function (data) {
                $('#branch_select').empty();
                $("#branch_select").select2("val", "");
                $('#branch_select').html(data);
                $("#spin2").hide();
            });
        });

        $('#branch_select').on('change', function (e) {
            $("#spin3").show();
            var branch_id = e.target.value;
            $.get("{{ url('/') }}/getdepartment?branch_id=" + branch_id, function (data) {
                $('#department_select').empty();
                $("#department_select").select2("val", "");
                $('#department_select').html(data);
                $("#spin3").hide();
            });
        });
	**/
        /************************** start: Document Upload (Drag & Drop) JS ***************************/
        $("#file_upload").fileinput({
            /*showPreview: false,*/
            showUpload: false,
	    showCaption: false,
            maxFileCount: 1
        });
        /************************** end: Document Upload (Drag & Drop) JS ***************************/
    });
</script>


@stop