@extends('backend.layouts.master', ['title' => trans('labels.backend.access.roles.management'), 'header_title' => trans('labels.backend.access.roles.management'), 'header_sub_title' => trans('labels.backend.access.roles.create')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/jstree/themes/default/style.min.css") }}
@stop

@section('content')
@include('backend.access.includes.partials.header_buttons')
<br/>

{!! Form::open(['route' => 'access.role.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create_role']) !!}
<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.access.roles.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.roles.name')]) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
    
</div><!--form control-->
<div class="form-group">
    <div class="col-lg-10 col-lg-offset-2">
	<div class="row">
	    <div class="col-lg-12">
		<div class="alert alert-info">
		    <i class="fa fa-info-circle"></i>
		    {!! getLanguageBlock('backend.lang.access.roles.associated-permissions-explanation') !!}
		</div><!--alert-->
	    </div><!--col-lg-12-->
	    <div class="col-lg-6">
		<p><strong>{{ trans('labels.backend.access.permissions.grouped_permissions') }}</strong></p>
		@if ($groups->count())
		<div id="permission-tree">
		    <ul>
			@foreach ($groups as $group)
			<li>{!! $group->name !!}
			    @if ($group->permissions->count())
			    <ul>
				@foreach ($group->permissions as $permission)
				<li id="{!! $permission->permission_id !!}" data-dependencies="{!! json_encode($permission->dependencies->pluck('dependency_id')->all()) !!}" data-backdependancies="{!! json_encode($permission->backDependencies->pluck('permission_id')->all()) !!}">

				    @if ($permission->dependencies->count())
				    <?php
				    //Get the dependency list for the tooltip
				    $dependency_list = [];
				    foreach ($permission->dependencies as $dependency)
					array_push($dependency_list, $dependency->permission->display_name);
				    $dependency_list = implode(", ", $dependency_list);
				    ?>
				    <a data-toggle="tooltip" data-html="true" title="{{ trans('labels.backend.access.permissions.dependencies') }}: {!! $dependency_list !!}">{!! $permission->display_name !!} <small><strong>(D)</strong></small></a>
				    @else
				    {!! $permission->display_name !!}
				    @endif

				</li>
				@endforeach
			    </ul>
			    @endif
			</li>
			@endforeach
		    </ul>
		</div>
		@else
		<p>{{ trans('labels.backend.access.permissions.no_groups') }}</p>
		@endif
	    </div>
	</div>
    </div>
</div>
{!! Form::hidden('permissions') !!}

<hr/>
<div class="pull-left">
    <a href="{{route('access.role.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.create') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}

@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/js/plugins/jstree/jstree.min.js") }}
{{ Html::script(asset_url(). "/js/backend/access/roles/script.js") }}
@stop