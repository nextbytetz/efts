@extends('backend.layouts.master', ['title' => trans('labels.backend.access.roles.management'), 'header_title' => trans('labels.backend.access.roles.management'), 'header_sub_title' => trans('labels.backend.access.roles.management')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
@stop

@section('content')
@include('backend.access.includes.partials.header_buttons')
<br/>
<div class="table-responsive">
    <table id="roles-table" class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>{{ trans('labels.backend.access.roles.table.role') }}</th>
                <th>{{ trans('labels.backend.access.roles.table.permissions') }}</th>
                <th>{{ trans('labels.backend.access.roles.table.number_of_users') }}</th>
                <th>{{ trans('labels.general.actions') }}</th>
            </tr>
        </thead>
    </table>
</div><!--table-responsive-->
@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title"><span class="text-bold">{{ trans('history.backend.recent_history') }}</span></h4>                                    
            </div>
            <div class="panel-body">
                <!--Main content goes here-->
                <div class="table-responsive">
                    <table id="history-table" class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('labels.backend.access.users.table.history') }}</th>
                                <th>{{ trans('labels.general.table.created') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div><!--table-responsive-->
            </div>
        </div>
    </div>
</div>
@stop
@section('history-scripts')
<script>
    $(function () {
        $('#history-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("backend.history", "role_management") }}',
                type: 'get',
                data: {column: 'active', value: 1}
            },
            columns: [
                {data: 'history', name: 'history'},
                {data: 'created_at', name: 'created_at'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    });
</script>
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}

<script>
    $(function () {
        $('#roles-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("access.role.get") }}',
                type: 'get',
                data: {}
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'permissions', name: 'permissions'},
                {data: 'users', name: 'users'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    });
</script>
@stop