@extends('backend.layouts.master', ['title' => trans('labels.backend.access.users.management'), 'header_title' => trans('labels.backend.access.users.management'), 'header_sub_title' => trans('labels.backend.access.users.edit')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.access.includes.partials.header_buttons')
<br/>
{!! Form::model($user, ['route' => ['access.user.update', $user->officer_id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) !!}
<div class="form-group">
    {!! Form::label('firstname', trans('validation.attributes.backend.access.users.firstname'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('firstname', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.firstname')]) !!}
	{!! $errors->first('firstname', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('othernames', trans('validation.attributes.backend.access.users.othernames'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('othernames', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.othernames')]) !!}
	{!! $errors->first('othernames', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.access.users.email')]) !!}
	{!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('officeid', trans('validation.attributes.frontend.office_id'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('officeid', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.office_id')]) !!}
	{!! $errors->first('officeid', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('country_id', trans('validation.attributes.backend.sysdef.location.country.name'), ['class' => 'col-lg-2 control-label symbol required']) !!}
    <div class="col-lg-10">
	{!! Form::select('country_id', $country , (count($user->department) ? $user->department->branch->region->country->country_id : []), ['class' => 'form-control search-select', 'id' => 'country_select', 'placeholder' => '']) !!}
	<i class="fa fa-spinner fa-spin" id = "spin" style='display: none'></i>
	{!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->
<div class="form-group">
    {!! Form::label('region_id', trans('validation.attributes.backend.sysdef.location.region.name'), ['class' => 'col-lg-2 control-label symbol required']) !!}
    <div class="col-lg-10">
	{!! Form::select('region_id', $regions , (count($user->department) ? $user->department->branch->region->region_id : []), ['class' => 'form-control search-select', 'id' => 'region_select', 'placeholder' => '']) !!}
	<i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
	{!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->
<div class="form-group">
    {!! Form::label('branch_id', trans('validation.attributes.backend.sysdef.location.branch.name'), ['class' => 'col-lg-2 control-label symbol required']) !!}
    <div class="col-lg-10">
	{!! Form::select('branch_id', $branches , (count($user->department) ? $user->department->branch->branch_id : []), ['class' => 'form-control search-select', 'id' => 'branch_select', 'placeholder' => '']) !!}
	<i class="fa fa-spinner fa-spin" id = "spin3" style='display: none'></i>
	{!! $errors->first('branch_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->
<div class="form-group">
    {!! Form::label('department_id', trans('validation.attributes.backend.sysdef.rfid.zone.departments'), ['class' => 'col-lg-2 control-label symbol required']) !!}
    <div class="col-lg-10">
	{!! Form::select('department_id', $departments, (count($user->department) ? $user->department->department_id : []), ['class' => 'form-control search-select', 'id' => 'department_select']); !!}
	{!! $errors->first('department_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->
<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.active') }}</label>
    <div class="col-lg-1">
	{!!  $user->active_label !!}

    </div>
</div><!--form control-->

<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.confirmed') }}</label>
    <div class="col-lg-1">
	{!!  $user->confirmed_label !!}
    </div>
</div><!--form control-->
<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.available') }}</label>
    <div class="col-lg-1">
	{!!  $user->available_label !!}
    </div>
</div><!--form control-->


@if (!$user->hasRole('super') Or (access()->hasRole('super') And access()->id() != $user->officer_id))
<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.associated_roles') }}</label>
    <div class="col-lg-3">
	@if (count($roles) > 0)
	@foreach($roles as $role)
	<input type="checkbox" value="{{$role->role_id}}" name="assignees_roles[]" {{in_array($role->role_id, $user_roles) ? 'checked' : ''}} id="role-{{$role->role_id}}" /> <label for="role-{{$role->role_id}}">{!! $role->name !!}</label>
	<a href="#" data-role="role_{{$role->role_id}}" class="show-permissions small">
	    (
	    <span class="show-text">{{ trans('labels.general.show') }}</span>
	    <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
	    {{ trans('labels.backend.access.users.permissions') }}
	    )
	</a>
	<br/>
	<div class="permission-list hidden" data-role="role_{{$role->role_id}}">
	    @if (count($role->permissions) > 0)
	    <blockquote class="small">@foreach ($role->permissions as $perm) {{$perm->display_name}} <br/>
		@endforeach
	    </blockquote>
	    @else
	    {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
	    @endif
	</div><!--permission list-->
	@endforeach
	@else
	{{ trans('labels.backend.access.users.no_roles') }}
	@endif
    </div>
</div><!--form control-->

<div class="form-group">
    <label class="col-lg-2 control-label">{{ trans('validation.attributes.backend.access.users.other_permissions') }}</label>
    <div class="col-lg-10">
	<div class="alert alert-info">
	    <i class="fa fa-info-circle"></i> {{ trans('labels.backend.access.users.permission_check') }}
	</div><!--alert-->

	@if (count($permissions))
	@foreach (array_chunk($permissions->toArray(), 10) as $perm)
	<div class="col-lg-3">
	    <ul style="margin:0;padding:0;list-style:none;">
		@foreach ($perm as $p)
		<?php
		//Since we are using array format to nicely display the permissions in rows
		//we will just manually create an array of dependencies since we do not have
		//access to the relationship to use the pluck() function of eloquent
		//but the relationships are eager loaded in array format now
		$dependencies = [];
		$backdependencies = [];
		$dependency_list = [];
		if (count($p['dependencies'])) {
		    foreach ($p['dependencies'] as $dependency) {
			array_push($dependencies, $dependency['dependency_id']);
			array_push($dependency_list, $dependency['permission']['display_name']);
		    }
		}
		if (count($p['back_dependencies'])) {
		    foreach ($p['back_dependencies'] as $backdependency) {
			array_push($backdependencies, $backdependency['back_permission']['permission_id']);
		    }
		}
		$dependencies = json_encode($dependencies);
		$backdependencies = json_encode($backdependencies);
		$dependency_list = implode(", ", $dependency_list);
		?>

		<li><input type="checkbox" value="{{$p['permission_id']}}" name="permission_user[]" data-dependencies="{!! $dependencies !!}" data-backdependencies="{!! $backdependencies !!}" {{in_array($p['permission_id'], $user_permissions) ? 'checked' : ""}} id="permission-{{$p['permission_id']}}" /> <label for="permission-{{$p['permission_id']}}">

			@if ($p['dependencies'])
			<a style="color:black;text-decoration:none;" data-toggle="tooltip" data-html="true" title="{{ trans('labels.backend.access.users.dependencies') }}: {!! $dependency_list !!}">{!! $p['display_name'] !!} <small><strong>(D)</strong></small></a>
			@else
			{!! $p['display_name'] !!}
			@endif

		    </label></li>
		@endforeach
	    </ul>
	</div>
	@endforeach
	@else
	{{ trans('labels.backend.access.users.no_other_permissions') }}
	@endif
    </div><!--col 3-->
</div><!--form control-->
@endif


<hr/>
<div class="pull-left">
    <a href="{{route('access.user.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.update') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url(). "/js/backend/access/permissions/script.js") }}
{{ Html::script(asset_url(). "/js/backend/access/users/script.js") }}
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({});

        $('#country_select').on('change', function (e) {
            $("#spin").show();
            var country_id = e.target.value;
            $.get("{{ url('/') }}/getregion?country_id=" + country_id, function (data) {
                $('#region_select').empty();
                $("#region_select").select2("val", "");
                $('#region_select').html(data);
                $("#spin").hide();
            });
        });

        $('#region_select').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getbranch?region_id=" + region_id, function (data) {
                $('#branch_select').empty();
                $("#branch_select").select2("val", "");
                $('#branch_select').html(data);
                $("#spin2").hide();
            });
        });

        $('#branch_select').on('change', function (e) {
            $("#spin3").show();
            var branch_id = e.target.value;
            $.get("{{ url('/') }}/getdepartment?branch_id=" + branch_id, function (data) {
                $('#department_select').empty();
                $("#department_select").select2("val", "");
                $('#department_select').html(data);
                $("#spin3").hide();
            });
        });
    });
</script>
@stop
