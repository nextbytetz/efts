@extends('backend.layouts.master', ['title' => trans('labels.backend.access.users.management'), 'header_title' => trans('labels.backend.access.users.management'), 'header_sub_title' => trans('labels.backend.access.users.deactivated')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
@stop

@section('content')
@include('backend.access.includes.partials.header_buttons')
<br/>
<div class="table-responsive">
    <table id="users-table" class="table table-condensed table-hover">
	<thead>
	    <tr>
		<th>{{ trans('labels.backend.access.users.table.firstname') }}</th>
		<th>{{ trans('labels.backend.access.users.table.othernames') }}</th>
		<th>{{ trans('labels.backend.access.users.table.username') }}</th>
		<th>{{ trans('labels.backend.access.users.table.department') }}</th>
		<th>{{ trans('labels.backend.access.users.table.branch_name') }}</th>
		<th>{{ trans('labels.backend.access.users.table.region') }}</th>
		<th>{{ trans('labels.backend.access.users.table.confirmed') }}</th>
		<th>{{ trans('labels.backend.access.users.table.available') }}</th>
		<th>{{ trans('labels.backend.access.users.table.roles') }}</th>
		<th>{{ trans('labels.general.table.created') }}</th>
		<th>{{ trans('labels.general.table.last_updated') }}</th>
		<th>{{ trans('labels.general.actions') }}</th>
	    </tr>
	</thead>
    </table>
</div><!--table-responsive-->
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}

<script>
    $(function () {
	$('#users-table').DataTable({
	    processing: true,
	    serverSide: true,
	    ajax: {
		url: '{{ route("access.user.get") }}',
		type: 'get',
		data: {column: 'active', value: 0}
	    },
	    columns: [
		{data: 'firstname', name: 'firstname'},
		{data: 'othernames', name: 'othernames'},
		{data: 'username', name: 'username'},
		{data: 'department', name: 'department'},
		{data: 'branch_name', name: 'branch_name'},
		{data: 'region', name: 'region'},
		{data: 'checked', name: 'checked'},
		{data: 'available', name: 'available'},
		{data: 'roles', name: 'roles'},
		{data: 'created_at', name: 'created_at'},
		{data: 'updated_at', name: 'updated_at'},
		{data: 'actions', name: 'actions'}
	    ],
	    order: [[0, "asc"]],
	    searchDelay: 500
	});
    });
</script>
@stop