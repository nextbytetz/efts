@extends('backend.layouts.master', ['title' => trans('labels.backend.access.users.profile.management'), 'header_title' => trans('labels.backend.access.users.profile.management'), 'header_sub_title' => trans('labels.backend.access.users.profile.password.title')])

@section('content')
<br/>
<p>
    <b>{{ trans('labels.backend.access.users.profile.password.create') }}</b>
</p>
<hr/>
{!! Form::open(['url' => ['access/user/password'], 'role' => 'form', 'method' => 'post']) !!}
<div class="row">
    <div class="col-md-12">
	<div class="form-group">
	    {!! Form::label('old_password', trans('validation.attributes.backend.access.users.old_password'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'old_password', null, ['class' => 'form-control']) !!}
		<i class="fa fa-lock"></i>
	    </span>
	    {!! $errors->first('old_password', '<span class="help-block label label-danger">:message</span>') !!}
	</div>	
	<div class="form-group">
	    {!! Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
		<i class="fa fa-lock"></i>
	    </span>
	    {!! $errors->first('password', '<span class="help-block label label-danger">:message</span>') !!}
	</div>
	<div class="form-group">
	    {!! Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}
		<i class="fa fa-lock"></i>
	    </span>
	    {!! $errors->first('password_confirmation', '<span class="help-block label label-danger">:message</span>') !!}
	</div>
    </div>
</div>

<br/>
<div class="pull-left">
    <a href="{{route('backend.dashboard')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.update') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop