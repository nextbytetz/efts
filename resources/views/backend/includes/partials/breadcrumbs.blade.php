<!-- start: BREADCRUMB -->
@if ($breadcrumbs)
<div class="row">
    <div class="col-md-12">	
	<ol class="breadcrumb">
	    @foreach ($breadcrumbs as $breadcrumb)
	    @if (!$breadcrumb->last)
	    <li>
		<a href="{{ $breadcrumb->url }}">
		    {{ $breadcrumb->title }}
		</a>
	    </li>
	    @else
	    <li class="active">
		{{ $breadcrumb->title }}
	    </li>	    
	    @endif
	    @endforeach
	</ol>	
    </div>
</div>
@endif
<!-- end: BREADCRUMB -->