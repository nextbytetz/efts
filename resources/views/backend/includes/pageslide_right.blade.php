<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- start: PAGESLIDE RIGHT -->
<div id="pageslide-right" class="pageslide slide-fixed inner">
    <div class="right-wrapper">
	<ul class="nav nav-tabs nav-justified" id="sidebar-tab">
<!--	    Chat disabled for now-->
<!--	    <li class="active">
		<a href="#users" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a>
	    </li>-->
	    <li class="active">
		<a href="#notifications" role="tab" data-toggle="tab"><i class="fa fa-bookmark "></i></a>
	    </li>
	    <li>
		<a href="#settings" role="tab" data-toggle="tab"><i class="fa fa-gear"></i></a>
	    </li>
	</ul>
	<div class="tab-content">
<!--	    Chat Disabled for now-->
<!--	    <div class="tab-pane active" id="users">
		<div class="users-list">
		    <h5 class="sidebar-title">On-line</h5>
		    <ul class="media-list">
			<li class="media">
			    <a href="#">
				<i class="fa fa-circle status-online"></i>
				<img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
				<div class="media-body">
				    <h4 class="media-heading">Nicole Bell</h4>
				    <span> Content Designer </span>
				</div>
			    </a>
			</li>
		    </ul>
		    <h5 class="sidebar-title">Off-line</h5>
		    <ul class="media-list">
			<li class="media">
			    <a href="#">
				<img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
				<div class="media-body">
				    <h4 class="media-heading">Nicole Bell</h4>
				    <span> Content Designer </span>
				</div>
			    </a>
			</li>
		    </ul>
		</div>
		<div class="user-chat">
		    <div class="sidebar-content">
			<a class="sidebar-back" href="#"><i class="fa fa-chevron-circle-left"></i> Back</a>
		    </div>
		    <div class="user-chat-form sidebar-content">
			<div class="input-group">
			    <input type="text" placeholder="Type a message here..." class="form-control">
			    <div class="input-group-btn">
				<button class="btn btn-blue no-radius" type="button">
				    <i class="fa fa-chevron-right"></i>
				</button>
			    </div>
			</div>
		    </div>
		    <ol class="discussion sidebar-content">
			<li class="other">
			    <div class="avatar">
				<img src="assets/images/avatar-4.jpg" alt="">
			    </div>
			    <div class="messages">
				<p>
				    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
				</p>
				<span class="time"> 51 min </span>
			    </div>
			</li>
		    </ol>
		</div>
	    </div>-->
	    <div class="tab-pane active" id="notifications">
		<div class="notifications">
		    <div class="pageslide-title">
			{!! notification()->count() !!}
		    </div>
		    {!! notification()->render() !!}
		    <div class="view-all">
			<a href="{!! route('backend.notification') !!}">
			    {!! trans('labels.general.notification.see_all') !!} <i class="fa fa-arrow-circle-o-right"></i>
			</a>
		    </div>
		</div>
	    </div>
	    <div class="tab-pane" id="settings">
		<h5 class="sidebar-title">{!! trans('labels.general.setting.general') !!}</h5>
		<ul class="media-list">
		    <li class="media">
			<div class="checkbox sidebar-content">
			    <label>
				<input type="checkbox" value="" class="green" checked="checked">
				{!! trans('labels.general.notification.enable') !!}
			    </label>
			</div>
		    </li>
		</ul>
<!--		<div class="sidebar-content">
		    <button class="btn btn-success">
			<i class="icon-settings"></i> Save Changes
		    </button>
		</div>-->
	    </div>
	</div>
	
    </div>
</div>
<!-- end: PAGESLIDE RIGHT -->
