<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- start: SLIDING BAR (SB) -->
<div id="slidingbar-area">
    <div id="slidingbar">
	<div class="row">
	    <!-- start: SLIDING BAR FIRST COLUMN -->
	    <div class="col-md-8 col-sm-8">
		<h2>{{ trans('labels.backend.file_process.file_list') }}</h2>
		<div class="row">
		    <div class="col-xs-4 col-lg-2">
			<a href="{!! route('file.process.list') . '#my_files' !!}" class="btn btn-icon btn-block space5">
			    <i class="fa fa-inbox"></i>
			    {!! trans('labels.backend.file_process.myfiles') !!} <span class="badge badge-info partition-red files_desk"> 4 </span>
			</a>
		    </div>
		    <div class="col-xs-4 col-lg-2">
			<a href="{!! route('file.process.list') . '#received_files' !!}"  class="btn btn-icon btn-block space5">
			    <i class="fa fa-level-down"></i>
			    {!! trans('labels.backend.file_process.received') !!} <span class="badge badge-info partition-red files_received"> 4 </span>
			</a>
		    </div>
		    <div class="col-xs-4 col-lg-2">
			<a href="{!! route('file.process.list') . '#file_requests' !!}"  class="btn btn-icon btn-block space5">
			    <i class="fa fa-retweet"></i>
			    {!! trans('labels.backend.file_process.requests') !!} <span class="badge badge-info partition-red files_requests"> 4 </span>
			</a>
		    </div>
		</div>
	    </div>
	    <!-- end: SLIDING BAR FIRST COLUMN -->
	    <!-- start: SLIDING BAR SECOND COLUMN -->
<!--	    <div class="col-md-4 col-sm-4">
		<h2>My Recent Works</h2>
		<div class="blog-photo-stream margin-bottom-30">
		    <ul class="list-unstyled">
			<li>
			    <a href="#"><img alt="" src="assets/images/image01_th.jpg"></a>
			</li>
		    </ul>
		</div>
	    </div>-->
	    <!-- end: SLIDING BAR SECOND COLUMN -->
	    <!-- start: SLIDING BAR THIRD COLUMN -->
	    <div class="col-md-4 col-sm-4">
		<h2>@lang('labels.backend.access.users.profile.info')</h2>
		<address class="margin-bottom-40">
		    {!! access()->user()->name() !!}
		    <br>
<!--		    12345 Street Name, City Name, United States
		    <br>-->
		    {!! access()->user()->phone !!}
		    <br>
		    @lang('labels.backend.access.users.profile.email'):
		    <a href="#">
			{!! access()->user()->email !!}
		    </a>
		</address>
		<a class="btn btn-transparent-white" href="{!! route('access.user.profile') !!}">
		    <i class="fa fa-pencil"></i> @lang('labels.backend.access.users.profile.edit'):
		</a>
	    </div>
	    <!-- end: SLIDING BAR THIRD COLUMN -->
	</div>
	<div class="row">
	    <!-- start: SLIDING BAR TOGGLE BUTTON -->
	    <div class="col-md-12 text-center">
		<a href="#" class="sb_toggle"><i class="fa fa-chevron-up"></i></a>
	    </div>
	    <!-- end: SLIDING BAR TOGGLE BUTTON -->
	</div>
    </div>
</div>
<!-- end: SLIDING BAR -->
