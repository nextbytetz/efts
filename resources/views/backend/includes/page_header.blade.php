<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- start: PAGE HEADER -->
<!-- start: TOOLBAR -->
<div class="toolbar row">
    <div class="col-sm-6 hidden-xs">
        <div class="page-header">
            <h1> {{ $header_title }} <small> {{ $header_sub_title }} </small></h1>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <a href="#" class="back-subviews">
            <i class="fa fa-chevron-left"></i> BACK
        </a>
        <a href="#" class="close-subviews">
            <i class="fa fa-times"></i> CLOSE
        </a>
        <div class="toolbar-tools pull-right">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">
                @permission('search_file')
                <li class="menu-search">
                    <a href="#">
                        <i class="fa fa-search"></i> {{ trans('labels.backend.page_header.search_file') }}
                    </a>
                    <!-- start: SEARCH POPOVER -->
                    <div class="popover bottom search-box transition-all">
                        <div class="arrow"></div>
                        <div class="popover-content">
                            <!-- start: SEARCH FORM -->
                            {!! Form::open(['route' => ['file.register.search'],  'id' => 'searchform', 'method' => 'get']) !!} 
                            <div class="input-group wide-tip">
                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                    <i class="fa fa-2x fa-barcode addIcon"></i></div>
                                {!! Form::input('text', 'search_item', null, ['class' => 'form-control input-lg', 'placeholder' => trans('labels.general.search'), 'autocomplete' => 'off', 'tableindex' => '1']) !!}
                            </div>
                            {!! Form::close() !!}
                            <!-- end: SEARCH FORM -->
                        </div>
                    </div>
                    <!-- end: SEARCH POPOVER -->
                </li>
                @endauth
                <!-- start: TO-DO DROPDOWN -->
                <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                        <i class="fa fa-plus"></i> {{ trans('labels.general.shortcut') }}
                        <div class="tooltip-notification hide">
                            <div class="tooltip-notification-arrow"></div>
                            <div class="tooltip-notification-inner">
                                <div>
                                    <div class="semi-bold">
                                        {{ trans('labels.general.greeting') }}
                                    </div>
                                    <div class="message">
                                        {{ trans('labels.general.try_shortcut') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-light dropdown-subview">
			<li class="dropdown-header">
                            {{ trans('menus.backend.file_register.main') }}
                        </li>
                        @permission('print_barcode')
                        <li>
                            <a href="{!! route('file.process.barcodes') !!}" ><span class="fa-stack"> <i class="fa fa-print fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_register.print_barcode') }} </a>
                        </li>			
                        @endauth
			<li>
                            <a href="{!! route('file.register.all') !!}" ><span class="fa-stack"> <i class="fa fa-list-alt fa-stack-1x fa-lg"></i> <i class="fa fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('menus.backend.file_register.all') }} </a>
                        </li>
                        @permission('create_edit_file')
                        <li class="dropdown-header">
                            {{ trans('labels.backend.file_register.title') }}
                        </li>
                        <li>
                            <a href="{!! route('file.register.index') !!}" ><span class="fa-stack"> <i class="fa fa-file fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_register.title') }} </a>
                        </li>			
                        @permission('register_bulk')
                        <li>
                            <a href="{!! route('file.register.bulk') !!}" ><span class="fa-stack"> <i class="fa fa-files-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_register.bulk_register') }} </a>
                        </li>
                        @endauth

                        @endauth
                        <li class="dropdown-header">
                            {{ trans('labels.backend.file_process.title') }}
                        </li>
                        <li>
                            <a href="{!! route('file.process.list') !!}" ><span class="fa-stack"> <i class="fa fa-list fa-stack-1x fa-lg"></i> <i class="fa fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_process.file_list') }} </a>
                        </li>
                        <li>
                            <a href="{!! route('file.process.search') !!}" ><span class="fa-stack"> <i class="fa fa-search-plus fa-stack-1x fa-lg"></i> <i class="fa fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_process.search_file') }} </a>
                        </li>
                        <!--			<li>
                                                    <a href="#" ><span class="fa-stack"> <i class="fa fa-location-arrow fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_process.transfer_file') }} </a>
                                                </li>-->
                        @permission('transfer_document')
<!--                        <li>
                            <a href="{!! route('storage.document.transfer') !!}" ><span class="fa-stack"> <i class="fa fa-location-arrow fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_process.transfer_document') }} </a>
                        </li>-->
                        @endauth
                        <!--			@permission('pick_file')
                                                <li>
                                                    <a href="#" ><span class="fa-stack"> <i class="fa fa-folder-open fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.file_process.pick_file') }} </a>
                                                </li>
                                                @endauth-->
                        @permission('perfomance_report')
                        <li class="dropdown-header">
                            {{ trans('labels.backend.report.title') }}
                        </li>
                        <li>
                            <a href="{!! route('report.backend.basic') !!}" ><span class="fa-stack"> <i class="fa fa-table fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.report.basic.title') }} </a>
                        </li>
                        <li>
                            <a href="{!! route('report.backend.user') !!}" ><span class="fa-stack"> <i class="fa fa-table fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.report.user.title') }} </a>
                        </li>
                        <li>
                            <a href="{!! route('report.backend.tracking') !!}" ><span class="fa-stack"> <i class="fa fa-table fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.report.tracking.title') }} </a>
                        </li>
                        @endauth
                    </ul>
                </li>
                <!-- start: TO-DO DROPDOWN [[This is office suite to be added in the next version of this application]]-->
		
<!--                <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                        <i class="fa fa-plus"></i> {{ trans('labels.general.office_suite') }}
                        <div class="tooltip-notification hide">
                            <div class="tooltip-notification-arrow"></div>
                            <div class="tooltip-notification-inner">
                                <div>
                                    <div class="semi-bold">
                                        {{ trans('labels.general.office_suite') }}
                                    </div>
                                    <div class="message">
                                        {{ trans('labels.general.try_shortcut') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-light dropdown-subview">
                        <li class="dropdown-header">
                            Notes
                        </li>
                        <li>
                            <a href="#newNote" class="new-note"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.office_suite.new_note') }} </a>
                        </li>
                        <li>
                            <a href="#readNote" class="read-all-notes"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.office_suite.read_note') }} </a>
                        </li>
                        <li class="dropdown-header">
                            {{ trans('labels.general.calender') }}
                        </li>
                        <li>
                            <a href="#newEvent" class="new-event"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.backend.office_suite.new_event') }} </a>
                        </li>
                        <li>
                            <a href="#showCalendar" class="show-calendar"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> {{ trans('labels.general.calender') }} </a>
                        </li>
                    </ul>
                </li>-->

                <!--		<li class="dropdown">
                                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                        <span class="messages-count badge badge-default hide">3</span> <i class="fa fa-envelope"></i> MESSAGES
                                    </a>
                                    <ul class="dropdown-menu dropdown-light dropdown-messages">
                                        <li>
                                            <span class="dropdown-header"> You have 9 messages</span>
                                        </li>
                                        <li>
                                            <div class="drop-down-wrapper ps-container">
                                                <ul>
                                                    <li class="unread">
                                                        <a href="javascript:;" class="unread">
                                                            <div class="clearfix">
                                                                <div class="thread-image">
                                                                    <img src="assets/images/avatar-2.jpg" alt="">
                                                                </div>
                                                                <div class="thread-content">
                                                                    <span class="author">Nicole Bell</span>
                                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                                    <span class="time"> Just Now</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" class="unread">
                                                            <div class="clearfix">
                                                                <div class="thread-image">
                                                                    <img src="assets/images/avatar-3.jpg" alt="">
                                                                </div>
                                                                <div class="thread-content">
                                                                    <span class="author">Steven Thompson</span>
                                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                                    <span class="time">8 hrs</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <div class="clearfix">
                                                                <div class="thread-image">
                                                                    <img src="assets/images/avatar-5.jpg" alt="">
                                                                </div>
                                                                <div class="thread-content">
                                                                    <span class="author">Kenneth Ross</span>
                                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                                    <span class="time">14 hrs</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="view-all">
                                            <a href="pages_messages.html">
                                                See All
                                            </a>
                                        </li>
                                    </ul>
                                </li>-->
            </ul>
            <!-- end: TOP NAVIGATION MENU -->
        </div>

    </div>

</div>
<!-- end: TOOLBAR -->
<!-- end: PAGE HEADER -->
