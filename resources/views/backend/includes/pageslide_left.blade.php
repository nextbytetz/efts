<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- start: PAGESLIDE LEFT -->
<a class="closedbar inner hidden-sm hidden-xs" href="#">
</a>
<nav id="pageslide-left" class="pageslide inner">
    <div class="navbar-content">
	<!-- start: SIDEBAR -->
	<div class="main-navigation left-wrapper transition-left">
	    <div class="navigation-toggler hidden-sm hidden-xs">
		<a href="#main-navbar" class="sb-toggle-left">
		</a>
	    </div>
	    <div class="user-profile border-top padding-horizontal-10 block">
		<div class="inline-block">
		    <img src="{{ Auth::user()->pic }}" alt=""  height="50" width="50">
		</div>
		<div class="inline-block">
		    <h5 class="no-margin"> {{ trans('labels.general.welcome') }} </h5>
		    <h4 class="no-margin"> {{ Auth::user()->firstname }}&nbsp;{{ Auth::user()->othernames }}  </h4>
		    <a class="btn user-options sb_toggle">
			<i class="fa fa-cog"></i>
		    </a>
		</div>
	    </div>
	    <!-- start: MAIN NAVIGATION MENU -->
	    <ul class="main-navigation-menu">
		<li>
		    <a href="{!! route('backend.dashboard') !!}"><i class="fa fa-home"></i><span class="title"> {{ trans('labels.general.dashboard') }} </span><span class="label label-default pull-right "></span> </a>
		</li>
		@permission('data_registration')
		<li>
		    <a href="{!! route('sysdef.country.index') !!}"><i class="fa fa-plus-square-o"></i><span class="title"> {{ trans('labels.backend.data_register.title') }} </span></a>
		</li>
		@endauth
		@permission('view_access')
		<li>
		    <a href="{!! route('access.user.index') !!}"> <i class="fa fa-users"></i><span class="title"> {{ trans('labels.backend.access_manager.title') }} </span></a>
		</li>
		@endauth

		<li>
		    <a href="#"><i class="fa fa-folder"></i> <span class="title"> {{ trans('labels.backend.file_register.title') }} </span><i class="icon-arrow"></i> </a>
		    <ul class="sub-menu">

			<li>
			    <a href="{!! route('file.register.index') !!}">
				<i class="fa fa-file"></i> {{ trans('labels.backend.file_register.title') }}
			    </a>
			</li>						
			@permission('register_bulk')
			<li>
			    <a href="{!! route('file.register.bulk') !!}">
				<i class="fa fa-files-o"></i> {{ trans('labels.backend.file_register.bulk_register') }}
			    </a>
			</li>
			@endauth
		    </ul>
		</li>

		@permission('print_barcode')
		<li>
		    <a href="{!! route('file.process.barcodes') !!}">
			<i class="fa fa-print"></i> <span class="title"> {{ trans('labels.backend.file_register.print_barcode') }} </span>
		    </a>
		</li>
		@endauth
		<li>
		    <a href="#"><i class="fa fa-folder-open-o"></i> <span class="title"> {{ trans('labels.backend.file_process.title') }} </span><i class="icon-arrow"></i> </a>
		    <ul class="sub-menu">
			<li>
			    <a href="{!! route('file.process.list') !!}">
				<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span> {{ trans('labels.backend.file_process.file_list') }}
			    </a>
			</li>
			@permission('search_file')
			<li>
			    <a href="{!! route('file.process.search') !!}">
				<span class="fa-stack"><i class="fa fa-search-plus fa-stack-1x "></i></span> {{ trans('labels.backend.file_process.search_file') }}
			    </a>
			</li>
			@endauth
			<!--			<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-folder-open fa-stack-1x "></i><i class="fa fa-location-arrow fa-stack-1x stack-right-bottom text-info"></i></span>  {{ trans('labels.backend.file_process.transfer_file') }}
						    </a>
						</li>-->
			@permission('transfer_document')
			<!--			<li>
						    <a href="{!! route('storage.document.transfer') !!}">
							<span class="fa-stack"><i class="fa fa-file fa-stack-1x "></i><i class="fa fa-location-arrow fa-stack-1x stack-right-bottom text-info"></i></span>  {{ trans('labels.backend.file_process.transfer_document') }}
						    </a>
						</li>-->
			@endauth
			<!--			@permission('pick_file')
						<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-tag fa-stack-1x "></i></span> {{ trans('labels.backend.file_process.pick_file') }}
						    </a>
						</li>
						@endauth
						<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-external-link fa-stack-1x "></i></span> {{ trans('labels.backend.file_process.dispose_file') }}
						    </a>
						</li>-->
		    </ul>
		</li>
		@permission('perfomance_report')
		<li>
		    <a href="#"><i class="fa fa-list-alt"></i> <span class="title"> {{ trans('labels.backend.report.title') }} </span><i class="icon-arrow"></i> </a>
		    <ul class="sub-menu">
			<li>
			    <a href="#">
				{{ trans('labels.backend.file.normal_file') }} <i class="icon-arrow"></i>
			    </a>
			    <ul class="sub-menu">
				<li>
				    <a href="{!! route('report.backend.basic') !!}">
					<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.basic.title') }}
				    </a>
				</li>
				<li>
				    <a href="{!! route('report.backend.user') !!}">
					<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.user.title') }}
				    </a>
				</li>
				<li>
				    <a href="{!! route('report.backend.closed') !!}">
					<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.closed.title') }}
				    </a>
				</li>
				<li>
				    <a href="{!! route('report.backend.tracking') !!}">
					<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.tracking.title') }}
				    </a>
				</li>
			    </ul>
			</li>
			<li>
			    <a href="#">
				{{ trans('labels.backend.file.notice_file') }} <i class="icon-arrow"></i>
			    </a>
			    <ul class="sub-menu">
				<li>
				    <a href="{!! route('report.backend.basic_notice') !!}">
					<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.basic.title') }}
				    </a>
				</li>
			    </ul>
			</li>
			<!--			<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.detailed_file_movement') }}
						    </a>
						</li>
						<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.file_receipt_register') }}
						    </a>
						</li>
						<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.file_dispatch_register') }}
						    </a>
						</li>
						<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.list_of_file') }}
						    </a>
						</li>
						<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.pending_file_register') }}
						    </a>
						</li>
						<li>
						    <a href="#">
							<span class="fa-stack"><i class="fa fa-list fa-stack-1x "></i></span>  {{ trans('labels.backend.report.department_list') }}
						    </a>
						</li>-->
		    </ul>
		</li>
		@endauth
		<!--		<li>
				    <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Layouts </span><i class="icon-arrow"></i> </a>
				    <ul class="sub-menu">
					<li>
					    <a href="javascript:;">
						Horizontal Menu <i class="icon-arrow"></i>
					    </a>
					    <ul class="sub-menu">
						<li>
						    <a href="layouts_horizontal_menu.html">
							Horizontal Menu
						    </a>
						</li>
					    </ul>
					</li>
					<li>
					    <a href="layouts_sidebar_closed.html">
						<span class="title"> Sidebar Closed </span>
					    </a>
					</li>
				    </ul>
				</li>-->
	    </ul>
	    <!-- end: MAIN NAVIGATION MENU -->
	</div>
	<!-- end: SIDEBAR -->
    </div>
    <div class="slide-tools">
<!--	<div class="col-xs-6 text-left no-padding">
	    <a class="btn btn-sm status" href="#">
		{{ trans('labels.general.status') }} <i class="fa fa-dot-circle-o text-green"></i> <span>{{ trans('labels.general.online') }} </span>
	    </a>
	</div>-->
	<div class="col-xs-offset-6 col-xs-6 text-right no-padding">
	    <a class="btn btn-sm log-out text-right" href="{!! route('auth.logout') !!}">
		<i class="fa fa-power-off"></i> {{ trans('labels.general.logout') }}
	    </a>
	</div>
    </div>
</nav>
<!-- end: PAGESLIDE LEFT -->

