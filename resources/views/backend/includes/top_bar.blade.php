<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<!-- start: TOPBAR CONTAINER -->
<div class="container">
    <div class="navbar-header">
	<a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar">
	    <i class="fa fa-bars"></i>
	</a>
	<!-- start: LOGO -->
	<!--	<a class="navbar-brand" href="index-2.html">
		    <img src="assets/images/logo.png" alt="MyOffice"/>
		</a>-->
	<a class="navbar-brand" href="#">
	    {!! app_name() !!} 
	</a>
	<!-- end: LOGO -->
    </div>
    <div class="topbar-tools">
	<!-- start: TOP NAVIGATION MENU -->
	<ul class="nav navbar-right">
	    <!--	    start: LANGUAGE DROPDOWN-->
	    @if (config('locale.status') && count(config('locale.languages')) > 1)
	    <li class="dropdown current-user">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
		    {{ trans('menus.language-picker.language') }}
		    <span class="caret"></span>
		</a>
		@include('includes.partials.lang')
	    </li>
	    @endif
	    <!--	    END: LANGUAGE DROPDOWN-->
	    <!-- start: USER DROPDOWN -->
	    <li class="dropdown current-user">
		<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
		    <img src="{{ Auth::user()->pic }}" class="img-circle" alt=""  height="25" width="25"> <span class="username hidden-xs">{{ Auth::user()->firstname }}&nbsp;{{ Auth::user()->othernames }}</span> <i class="fa fa-caret-down "></i>
		</a>
		<ul class="dropdown-menu">
<!--		     this class was used in the ul above : dropdown-dark-->
		    <li>
			<a href="{!! route('access.user.profile') !!}">
			    {{ trans('labels.general.profile') }}
			</a>
		    </li>
<!--		    <li>
			<a href="">
			    {{ trans('labels.general.calender') }}
			</a>
		    </li>-->
		    <li>
			<a href="{!! route('access.user.password') !!}">
			    {{ trans('labels.backend.access.users.change_password') }}
			</a>
		    </li>
		    <!--		    <li>
					    <a href="pages_messages.html">
						My Messages (3)
					    </a>
					</li>-->
		    <!--		    <li>
					    <a href="login_lock_screen.html">
						Lock Screen
					    </a>
					</li>-->
		    <li>
			<a href="{!! route('auth.logout') !!}">
			    {{ trans('labels.general.logout') }}
			</a>
		    </li>
		</ul>
	    </li>
	    <!-- end: USER DROPDOWN -->
	    <li class="right-menu-toggle">
		<a href="#" class="sb-toggle-right">
		    <i class="fa fa-globe toggle-icon"></i> <i class="fa fa-caret-right"></i> <span class="notifications-count badge badge-default hide"> {!! notification()->count() !!} </span>
		</a>
	    </li>
	</ul>
	<!-- end: TOP NAVIGATION MENU -->
    </div>
</div>
<!-- end: TOPBAR CONTAINER -->