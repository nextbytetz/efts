
@extends('backend.layouts.master', ['title' => trans('labels.backend.file_process.barcodes.title'), 'header_title' => trans('labels.backend.file_process.barcodes.title'), 'header_sub_title' => trans('labels.backend.file_process.barcodes.title')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/js/plugins/barcode/print.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/plugins/jquery-ui/css/jquery.ui.autocomplete.min.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal.css") }}
@stop

@section('content')
<div class='row'>
    <div class="col-sm-12 col-md-12 col-lg-12">
	<div class="well well-sm no-print">
	    <div class="form-group">
		{!! Form::label('reference', trans('labels.backend.file_process.barcodes.add'), ['class' => 'control-label symbol required']) !!}
		{!! Form::input('text', 'add_file', null, ['class' => 'form-control', 'id' => 'add_file', 'placeholder' => trans('labels.backend.file_process.barcodes.add')]) !!}
	    </div>
	    {!! Form::open(['route' => ['file.process.barcodes'], 'id' => 'barcode_print_form']) !!} 
	    <div class="controls table-controls">
		<table id="bcTable" class="table items table-striped table-bordered table-condensed table-hover">
		    <thead>
			<tr>
			    <th class="col-xs-4"><b>{!! trans('labels.backend.file_process.barcodes.file') !!}</b></th>
			    <th class="col-xs-8"><b>{!! trans('labels.backend.file_process.barcodes.quantity') !!}</b></th>
			    <th class="text-center" style="width:30px;">
				<i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
			    </th>
			</tr>
		    </thead>
		    <tbody></tbody>
		</table>
		{!! $errors->first('file', '<span class="help-block label label-danger">:message </span><br/><br/>') !!}
	    </div>
	    <div class="form-group">
		{!! Form::label('reference', trans('labels.backend.file_process.barcodes.style'), ['class' => 'control-label symbol required']) !!}
		{!! Form::select('style', $styles, null, ['class' => 'form-control search-select', 'id' => 'style']) !!}
		<!--		 display: none;-->
		<div class="row cf-con" style="margin-top: 10px;display: none;">
		    <div class="col-xs-4">
			<div class="form-group">
			    <div class="input-group">
				{!! Form::input('text', 'cf_width', null, ['class' => 'form-control', 'id' => 'cf_width', 'placeholder' => trans('labels.backend.file_process.barcodes.width')]) !!}
				<span class="input-group-addon" style="padding-left:10px;padding-right:10px;">{!! trans('labels.backend.file_process.barcodes.inches') !!}</span>
			    </div>
			    {!! $errors->first('cf_width', '<span class="help-block label label-danger">:message</span>') !!}
			</div>
		    </div>
		    <div class="col-xs-4">
			<div class="form-group">
			    <div class="input-group">
				{!! Form::input('text', 'cf_height', null, ['class' => 'form-control', 'id' => 'cf_height', 'placeholder' => trans('labels.backend.file_process.barcodes.height')]) !!}
				<span class="input-group-addon" style="padding-left:10px;padding-right:10px;">{!! trans('labels.backend.file_process.barcodes.inches') !!}</span>
			    </div>
			    {!! $errors->first('cf_height', '<span class="help-block label label-danger">:message</span>') !!}
			</div>
		    </div>
		    <div class="col-xs-4">
			<div class="form-group">
			    {!! Form::select('cf_orientation', [0 => trans('labels.backend.file_process.barcodes.potrait'), 1 => trans('labels.backend.file_process.barcodes.landscape')], 0, ['class' => 'form-control search-select']) !!}
			</div>
		    </div>
		</div>
		<span class="help-block label label-info">{!! trans('labels.backend.file_process.barcodes.style_helper') !!}</span>
	    </div>
	    <div class="form-group">
		<span style="font-weight: bold; margin-right: 15px;">{!! trans('labels.backend.file_process.barcodes.print') !!}:</span>
		<label for="file_name" class="checkbox-inline">
		    {!! Form::checkbox("print_option[file_name]", '1', (isset($input['file_name']) ? true : false), ['class' => 'grey']) !!}	
		    {!! trans('labels.backend.file_process.barcodes.file_name')!!}
		</label>
		<label for="reference" class="checkbox-inline">
		    {!! Form::checkbox("print_option[reference]", '1', (isset($input['reference']) == 'reference' ? true : false), ['class' => 'grey']) !!}	
		    {!! trans('labels.backend.file_process.barcodes.reference') !!}
		</label>
		{!! $errors->first('print_option', '<span class="help-block label label-danger">:message</span>') !!}
	    </div>
	    <div class="form-group">
		{!! Form::input('submit', 'print', trans('buttons.general.crud.update'), ['class' => 'btn btn-sm btn-success']) !!}
		<a href="{!! route('file.process.barcodes') !!}" class="btn btn-sm btn-danger">{!! trans('buttons.general.reset') !!}</a>
	    </div>
	    {!! Form::close() !!}
	    <div class="clearfix"></div>
	</div>
	<div id="barcode-con">
            @if (isset($barcodes))
	    <button type="button" onclick="window.print();return false;" class="btn btn-primary btn-block tip no-print" title="{!! trans('labels.backend.file_process.barcodes.print') !!}">
		<i class="icon fa fa-print"></i> {!! trans('labels.backend.file_process.barcodes.print') !!}
	    </button>
	    <?php $c = 1; ?>

	    @if ($style == 12 || $style == 18 || $style == 24 || $style == 40)
	    <div class="barcodea4">
		@elseif ($style != 50)
		<div class="barcode">
		    @endif

		    @foreach ($barcodes as $item)

		    @for ($r = 1; $r <= $item['quantity']; $r++)
                    <div class="item style{!! $style !!}" <?= (($style == 50) ? 'style="width:' . $style50['cf_width'] . 'in;height:' . $style50['cf_height'] . 'in;border:0;"' : '') ?>>
			@if ($style == 50)

			@if ($style50['cf_orientation'] == 1) 
			<?php
			$ty = (($style50['cf_height'] / $style50['cf_width']) * 100) . '%';
			$landscape = '
			-webkit-transform-origin: 0 0;
			-moz-transform-origin:    0 0;
			-ms-transform-origin:     0 0;
			transform-origin:         0 0;
			-webkit-transform: translateY(' . $ty . ') rotate(-90deg);
			-moz-transform:    translateY(' . $ty . ') rotate(-90deg);
			-ms-transform:     translateY(' . $ty . ') rotate(-90deg);
			transform:         translateY(' . $ty . ') rotate(-90deg);
			';
			?>
			<div class="div50" style="width:{!! $style50['cf_height'] !!}in;height:{!! $style50['cf_width'] !!}in;border: 1px dotted #CCC;{!! $landscape !!}">
			    @else 
			    <div class="div50" style="width:{!! $style50['cf_width'] !!}in;height:{!! $style50['cf_height'] !!}in;border: 1px dotted #CCC;padding-top:0.025in;">
				@endif			

				@endif

				<span class="barcode_name"> {!! app_name() !!}</span>
				@if (isset($input['file_name']) ) 
				<span class="barcode_name"> {!! $item['name'] !!}</span>
				@endif

				@if (isset($input['reference']) ) 
				<span class="barcode_price">{!! $item['reference'] !!}</span>
				@endif

				<span class="barcode_image">{!! $item['barcode'] !!}</span>

				@if ($style == 50)
			    </div>
			    @endif			
			</div>

			@if ($style == 40) 
			@if ($c % 40 == 0) 
		    </div><div class="clearfix"></div><div class="barcodea4">
			@endif
			@elseif ($style == 30) 
			@if ($c % 30 == 0) 
		    </div><div class="clearfix"></div><div class="barcode">
			@endif
			@elseif ($style == 24) 
			@if ($c % 24 == 0) 
		    </div><div class="clearfix"></div><div class="barcodea4">
			@endif
			@elseif ($style == 20) 
			@if ($c % 20 == 0) 
		    </div><div class="clearfix"></div><div class="barcode">
			@endif
			@elseif ($style == 18) 
			@if ($c % 18 == 0) 
		    </div><div class="clearfix"></div><div class="barcodea4">
			@endif
			@elseif ($style == 14)
			@if ($c % 14 == 0)
		    </div><div class="clearfix"></div><div class="barcode">
			@endif
			@elseif ($style == 12) 
			@if ($c % 12 == 0)
		    </div><div class="clearfix"></div><div class="barcodea4">
			@endif
			@elseif ($style == 10)
			@if ($c % 10 == 0)
		    </div><div class="clearfix"></div><div class="barcode">
			@endif
			@endif

			<?php $c++ ?>

			@endfor

			@endforeach

			@if ($style != 50)
		    </div>
		    @endif


		    <button type="button" onclick="window.print();return false;" class="btn btn-primary btn-block tip no-print" title="{!! trans('labels.backend.file_process.barcodes.print') !!}">
			<i class="icon fa fa-print"></i> {!! trans('labels.backend.file_process.barcodes.print') !!}
		    </button>
		    @else
		    {!! trans('labels.backend.file_process.barcodes.no_file_selected') !!}
		    @endif
		</div>
	    </div>
	</div>
<!--    </div>
</div>-->

@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
@stop
@section('history-scripts')
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url() . "/plugins/bootbox/bootbox.min.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modal.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modalmanager.js") }}
{{ Html::script(asset_url() . "/js/helpers.js") }}
<script type="text/javascript">
    var ac = false;
    bcitems = {};
    $(document).ready(function () {
        $("#style").select2({});
        $('#style').change(function (e) {
            localStorage.setItem('bcstyle', $(this).val());
            if ($(this).val() == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        });
        if (style = localStorage.getItem('bcstyle')) {
            //$('#style').val(style);
            //$('#style').select2("val", style);
            $("#style").val(style).trigger("change");
            if (style == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        } else {
            //$('#style').val(40);
            $("#style").val(40).trigger("change");
        }
        if (localStorage.getItem('bcitems')) {
            loadItems();
        }
        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete bcitems[id];
            localStorage.setItem('bcitems', JSON.stringify(bcitems));
            $(this).closest('#row_' + id).remove();
        });

        var old_row_qty;
        $(document).on("focus", '.quantity', function () {
            old_row_qty = $(this).val();
        }).on("change", '.quantity', function () {
            var row = $(this).closest('tr');
            if (!is_numeric($(this).val())) {
                $(this).val(old_row_qty);
                bootbox.alert("{!! trans('labels.backend.file_process.barcodes.unexpected_value') !!}");
                return;
            }
            var new_qty = parseFloat($(this).val()),
                    item_id = row.attr('data-item-id');
            bcitems[item_id].qty = new_qty;
            localStorage.setItem('bcitems', JSON.stringify(bcitems));
        });

        $("#add_file").autocomplete({
            source: "{!! route('file.register.searchall') !!}",
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert("{!! trans('labels.backend.file_process.barcodes.no_file_found') !!}", function () {
                        $('#add_file').focus();
                    });
                    $(this).val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                } else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert("{!! trans('labels.backend.file_process.barcodes.no_file_found') !!}", function () {
                        $('#add_file').focus();
                    });
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_file_item(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert("{!! trans('labels.backend.file_process.barcodes.no_file_found') !!}");
                }
            }
        });
    });

    function add_file_item(item) {
        ac = true;
        if (item == null) {
            return false;
        }
        var item_id = item.id;
        bcitems[item_id] = item;
        localStorage.setItem('bcitems', JSON.stringify(bcitems));
        loadItems();
        return true;
    }

    function loadItems() {
        if (localStorage.getItem('bcitems')) {
            $("#bcTable tbody").empty();
            bcitems = JSON.parse(localStorage.getItem('bcitems'));
            $.each(bcitems, function () {
                var item = this;
                var row_no = item.id;
                var vd = '';
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item.id + '" data-item-id="' + item.id + '"></tr>');
                tr_html = '<td><input name="file[]" type="hidden" value="' + item.id + '"><input name="name[]" type="hidden" value="' + item.name + '"><input name="reference[]" type="hidden" value="' + item.reference + '"><span id="name_' + row_no + '">' + item.name + ' (' + item.reference + ')</span></td>';
                tr_html += '<td><input class="form-control quantity text-center" name="quantity[]" type="text" value="' + formatDecimal(item.qty) + '" data-id="' + row_no + '" data-item="' + item.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                newTr.html(tr_html);
                newTr.appendTo("#bcTable");
            });
            return true;
        }
    }

</script>

@stop