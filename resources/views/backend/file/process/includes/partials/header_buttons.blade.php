<div class="pull-left mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.file_register.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
	    <li>{{ link_to_route('file.process.list', trans('labels.backend.file_process.file_list')) }}</li>
	    <li class="divider"></li>
	    <li>{{ link_to_route('file.process.search', trans('labels.backend.file_process.search_file')) }}</li>   
	    @permission('register_bulk')
            <li class="divider"></li>
            <li>{{ link_to_route('storage.document.transfer', trans('labels.backend.file_process.transfer_document')) }}</li>
	    @endauth
        </ul>
    </div><!--btn group-->

</div><!--pull right-->


<div class="clearfix"></div>
<hr/>