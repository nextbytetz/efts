@extends('backend.layouts.master', ['title' => trans('labels.backend.file_process.title'), 'header_title' => trans('labels.backend.file_process.title'), 'header_sub_title' => trans('labels.backend.file_process.file_list')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal.css") }}
@stop

@section('content')
@include('backend.file.process.includes.partials.header_buttons')

<div class="row">
    <div class="col-md-12">
	<ul class="nav nav-pills">
	    <li role="presentation" class="active"><a href="#my_files" data-toggle="tab" data-target="#my_files">{!! trans('labels.backend.file_process.myfiles') !!}</a></li>

	    <li role="presentation"><a href="#received_files" data-toggle="tab" data-target="#received_files">{!! trans('labels.backend.file_process.received') !!}</a></li>
	    <li role="presentation"><a href="#file_requests" data-toggle="tab" data-target="#file_requests">{!! trans('labels.backend.file_process.requests') !!}</a></li>
	</ul>
	<div class="tab-content">
	    <div class="tab-pane active" id="my_files">
		<div class="table-responsive">
		    <table id="my_files_table" class="table table-condensed table-hover" style="width:100%">
			<thead>
			    <tr>
				<th></th>
				<th>{{ trans('labels.backend.file.table.name') }}</th>
				<th>{{ trans('labels.backend.file.table.priority') }}</th>
				<th>{{ trans('labels.backend.file.table.status') }}</th>
				<th>{{ trans('labels.general.actions') }}</th>
			    </tr>
			</thead>
		    </table>
		</div><!--table-responsive-->
	    </div>
	    <div class="tab-pane" id="received_files">
		<div class="table-responsive">
		    <table id="received_files_table" class="table table-condensed table-hover" style="width:100%">
			<thead>
			    <tr>
				<th></th>
				<th>{{ trans('labels.backend.file.table.name') }}</th>
				<th>{{ trans('labels.backend.file.table.priority') }}</th>
				<th>{{ trans('labels.backend.file.table.status') }}</th>
				<th>{{ trans('labels.general.actions') }}</th>
			    </tr>
			</thead>
		    </table>
		</div><!--table-responsive-->
	    </div>
	    <div class="tab-pane" id="file_requests">
		<div class="table-responsive">
		    <table id="file_requests_table" class="table table-condensed table-hover" style="width:100%">
			<thead>
			    <tr>
				<th></th>
				<th>{{ trans('labels.backend.file.table.name') }}</th>
				<th>{{ trans('labels.backend.file.table.priority') }}</th>
				<th>{{ trans('labels.backend.file.table.status') }}</th>
				<th>{{ trans('labels.general.actions') }}</th>
			    </tr>
			</thead>
		    </table>
		</div><!--table-responsive-->
	    </div>
	</div>
    </div>
</div>


<hr/>
<!-- start: BOOTSTRAP EXTENDED FILE CASE MODALS -->
@include('includes.partials.file_case_modal')
<!-- end: BOOTSTRAP EXTENDED FILE CASE MODALS -->
@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
@stop
@section('history-scripts')
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modal.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modalmanager.js") }}
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
<script>
    $(function () {
        $("#file_case_select").select2({
            multiple: true
        });
        $('#file_case_form').submit(function (event) {
            event.preventDefault();
            if (!$("#file_case_select").val()) {
                alert("{{ trans('labels.backend.file_process.case.select') }}");
            } else {
                this.submit();
            }
        });
        $('#my_files_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("file.process.get_list") }}',
                type: 'get',
                data: {'state': 4}
            },
            columns: [
                {data: 'icon'},
                {data: 'name', name: 'name'},
                {data: 'priority', name: 'priority'},
                {data: 'status', name: 'status'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
        $('#received_files_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("file.process.get_list") }}',
                type: 'get',
                data: {'state': 6}
            },
            columns: [
                {data: 'icon'},
                {data: 'name', name: 'name'},
                {data: 'priority', name: 'priority'},
                {data: 'status', name: 'status'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
        $('#file_requests_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("file.process.get_list") }}',
                type: 'get',
                data: {'state': 7}
            },
            columns: [
                {data: 'icon'},
                {data: 'name', name: 'name'},
                {data: 'priority', name: 'priority'},
                {data: 'status', name: 'status'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
        /************************** start: Control hash for tabs ***************************/
        // show active tab on reload
        if (location.hash !== '')
            $('a[href="' + location.hash + '"]').tab('show');

        // remember the hash in the URL without jumping
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if (history.pushState) {
                history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
            } else {
                location.hash = '#' + $(e.target).attr('href').substr(1);
            }
        });
        /************************** end: Control hash for tabs ***************************/
    });
</script>
@stop