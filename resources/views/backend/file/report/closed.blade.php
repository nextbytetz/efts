@extends('backend.layouts.master', ['title' => trans('labels.backend.report.title'), 'header_title' => trans('labels.backend.report.title'), 'header_sub_title' => trans('labels.backend.report.closed.title')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/css/datatables/buttons.dataTables.min.css") }}
@stop

@section('content')
{!! $dataTable->table(['class' => 'table'], true) !!}
@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
@stop
@section('history-scripts')
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/dataTables.buttons.min.js") }}
{{ Html::script(public_url(). "/vendor/datatables/buttons.server-side.js") }}
{!! $dataTable->scripts() !!}
@stop