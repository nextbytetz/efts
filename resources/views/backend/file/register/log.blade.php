@extends('backend.layouts.master', ['title' => trans('labels.backend.file_process.title'), 'header_title' => trans('labels.backend.file_process.title'), 'header_sub_title' => trans('labels.backend.file.register_log')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/js/plugins/mentiony_facebook/jquery.mentiony.css") }}
<style> 
</style>
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')

@include('includes.partials.file_comment', ['file' => $file_id, 'file_category_id' => $file_category_id, 'workflow' => $workflow])

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title"><span class="text-bold">{{ trans('labels.backend.file.log') }}</span></h4>                                    
            </div>
            <div class="panel-body">
                <!--Main content goes here-->
                <div class="table-responsive">
                    <table id="log-table" class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>{{ trans('labels.backend.access.users.table.log') }}</th>
                                <th>{{ trans('labels.general.table.updated') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div><!--table-responsive-->
            </div>
        </div>
    </div>
</div>

@stop

@section('after-scripts-end')
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
<!--{{ Html::script(asset_url() . "/js/plugins/textcomplete/jquery.textcomplete.min.js") }}-->
{{ Html::script(asset_url() . "/js/plugins/mentiony_facebook/jquery.mentiony.min.js") }}
{{ Html::script(asset_url(). "/plugins/autosize/jquery.autosize.min.js") }}
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
<script>
    $(function () {
        $("textarea.autosize").autosize();
<?php if ($file_category_id == 2) { ?>
            $(".search-select").select2();
            /************************** start: Officer Lazy Search ***************************/
            $(".officer-select").select2({
                minimumInputLength: 2,
                multiple: true,
                ajax: {
                    url: "{!! route('file.process.dispatch.officers', $file_id) !!}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.officer_id
                                };
                            }),
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
            /************************** end: Officer Lazy Search ***************************/
            /************************** start: Control notify user drop down ***************************/
            $('#notify').change(function () {
                var choice = $("#notify").val();
                switch (choice) {
                    case '0':
                        $('#officers').hide();
                        break;
                    case '1':
                        $('#officers').hide();
                        break;
                    case '2':
                        $('#officers').show();
                        break;
                }
            });
            $('#officers').hide();
            $('#notify').trigger('change');
            /************************** end: Control notify user drop down ***************************/
<?php } ?>

        /************************** start: Comment Auto complete for loading document Script using facebook mentiony plugin ***************************/
	var html = '{!! $workflow->comment or '' !!}';
        $('#comment_field').mentiony({
            onDataRequest: function (mode, keyword, onDataRequestCompleteCallback) {
                $.ajax({
                    method: "GET",
                    url: "{!! route('storage.document.search') !!}?q=" + keyword + "&file_id={!! $file_id !!}",
                    dataType: "json",
                    success: function (response) {
                        var data = response;
                        /**
                         *  NOTE: Assuming this filter process was done on server-side
                         data = jQuery.grep(data, function (item) {
                         return item.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                         });
                         // End server-side **/
                        // Call this to populate mention.
                        onDataRequestCompleteCallback.call(this, data);
                    }
                });
            },
            timeOut: 400,
            debug: 0,
            triggerChar: '@'
        });
	$(".mentiony-content").html(html);
        /************************** end: Comment Auto complete for loading document Script using facebook mentiony plugin***************************/

        /************************** start: Comment Auto complete for loading document Script using textcomplete plugin ***************************/
        /*
         $('#comment_field').textcomplete([{
         match: /\B@([\-+\w]*)$/,
         replace: function (value) {
         return '@("' + value + '") ';
         },
         search: function (term, callback) {
         //callback(cache[term], true);
         $.getJSON("{!! route('storage.document.search') !!}", {'q': term, 'file_id': {!! $file_id !!}})
         .done(function (resp) {
         callback(resp);
         })
         .fail(function () {
         callback([]);
         });
         },
         template: function (value) {
         return '<i class="fa fa-file-text-o"></i> &nbsp;' + value;
         },
         index: 1
         }], {
         onKeydown: function (e, commands) {
         if (e.ctrlKey && e.keyCode === 74) { // CTRL-J
         return commands.KEY_ENTER;
         }
         }
         });
         */
        /************************** end: Comment Auto complete for loading document Script using text complete plugin***************************/
        /************************** start: Post Comment Script ***************************/
        $('#comment_form').submit(function (event) {
            $comment = $('#comment_field').val();
            if ($comment !== "") {
                $("#spin").show();
                $.ajax({
                    type: $(this).attr('method'),
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    encode: true
                })
                        .done(function (data) {
                            $("#spin").hide();
                            $('#comment_field').val("");
                            $('#log-table').DataTable().ajax.reload();
                            //console.log(data);
                        });
            }
            event.preventDefault();
        });
        /************************** end: Post Comment Script ***************************/
        /************************** start: File Register Log Datatable ***************************/
        $('#log-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("file.process.get_log") }}',
                type: 'get',
                data: {file_id: "{!! $file_id !!}"}
            },
            columns: [
                {data: 'log', name: 'log', orderable : false},
                {data: 'updated_at', name: 'updated_at', orderable : false}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
        /************************** end: File Register Log Datatable ***************************/
    });
</script>
@stop