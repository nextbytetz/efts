@extends('backend.layouts.master', ['title' => trans('labels.backend.file_register.title'), 'header_title' => trans('labels.backend.file_register.title'), 'header_sub_title' => trans('labels.backend.file_register.create_bulk')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/css/jstree/themes/default/style.min.css") }}
{{ Html::style(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')

<div class="well">
    {{ $file_nature == 1 ? trans('labels.general.electronic') : trans('labels.general.physical') }}
</div>

<div class="row">
    <div class="col-md-12">
	<ul class="nav nav-pills">
	    <li role="presentation" class="active"><a href="#upload_new" data-toggle="tab" data-target="#upload_new">{!! trans('labels.backend.file.upload_new') !!}</a></li>

	    <li role="presentation"><a href="#recent_import" data-toggle="tab" data-target="#recent_import">{!! trans('labels.backend.file.recent_import') !!}</a></li>
	</ul>
	<div class="tab-content">
	    <div class="tab-pane active" id="upload_new">
		{!! Form::open(['route' => ['file.register.store.bulk'],  'id' => 'create_bulk_file', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!} 
		<div class="row">
		    <div class="col-md-12">
			<div>
			    <span class="symbol required"></span>{{ trans('labels.general.required') }}
			</div>
		    </div>
		</div>
		<div class="row">

		    <div class="col-md-6 col-sm-6" style="padding-right:20px; border-right: 1px solid #ddd;">
			<legend>{!! trans('validation.attributes.backend.file.general') !!}</legend>
			{!! Form::hidden('file_nature_id', $file_nature) !!}
			{!! Form::hidden('officer_opened', access()->id()) !!}
			{!! Form::hidden('officer_edited', access()->id()) !!}

			<div class="form-group">
			    {!! Form::label('name', trans('validation.attributes.backend.file.group_name'), ['class' => 'control-label symbol required']) !!}
			    {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}
			    <span class="help-block">
				<p>@lang('validation.attributes.backend.file.group_name_helper')</p>
				{!! $errors->first('name', '<p class="label label-danger">:message</p>') !!}
			    </span>
			</div>
			<div class="form-group">
			    {!! Form::label('priority_id', trans('validation.attributes.backend.file.priority'), ['class' => 'control-label symbol required']) !!}
			    {!! Form::select('priority_id', $priorities, null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
			    {!! $errors->first('priority_id', '<span class="help-block label label-danger">:message</span>') !!}
			</div>
			<div class="form-group">
			    {!! Form::label('file_subject_id', trans('validation.attributes.backend.file.subject'), ['class' => 'control-label symbol required']) !!}
			    {!! Form::select('file_subject_id', $file_subjects, null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
			    <i class="fa fa-spinner fa-spin" id = "subject_spin" style='display: none'></i>
			    <span class="help-block">
				<p>{{ trans_choice('validation.attributes.backend.file.subject_helper', 1) }}</p>
				{!! $errors->first('file_subject_id', '<p class="label label-danger">:message</p>') !!}
			    </span>	    
			</div>
			<div class="form-group">
			    {!! Form::label('files', trans('validation.attributes.backend.file.csv'), ['class' => 'control-label symbol required']) !!}
			    {!! Form::input('file', 'files', null, ['class' => 'form-control', 'id' => 'file_upload', 'data-allowed-file-extensions' => '["xlsx","xls"]']) !!}
			    <span class="help-block">
				<p>@lang('validation.attributes.backend.file.csv_helper')</p>
				{!! $errors->first('files', '<p class="label label-danger">:message</p>') !!}
			    </span>	    
			</div>
			<div class="panel panel-white space20">
			    <div class="panel-heading">
				<i class="clip-menu"></i>
				@lang('labels.backend.file.instruction')
			    </div>
			    <div class="panel-body">

				<div class="row">
				    <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-pink"></i> 
					    <i class="fa fa-info fa-stack-1x fa-inverse"></i> 
					</span>
				    </div>
				    <div class='col-md-11'>
					<h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.file')</u></h4>
					Microsoft Excel file xlsx, xls <br/>
					<!--					@lang('labels.backend.file.format.file_helper')-->
				    </div>
				</div>
				<hr/>
				<div class="row">
				    <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-orange"></i> 
					    <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i> 
					</span>
				    </div>
				    <div class='col-md-11'>
					<h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.content')</u></h4>
					@lang('labels.backend.file.format.content_helper') <br/>
					@lang('labels.backend.file.format.column')  &nbsp;<span class="label label-success">name</span>&nbsp;<span class="label label-success">reference</span>
				    </div>
				</div>
				<hr/>
				<div class="row">
				    <div class='col-md-1'>
					<span class="fa-stack fa-2x">
					    <i class="fa fa-square fa-stack-2x text-yellow"></i> 
					    <i class="fa fa-file-excel-o fa-stack-1x fa-inverse"></i> 
					</span>
				    </div>
				    <div class='col-md-11'>
					<h4><u style="border-bottom: 1px dashed #999;text-decoration: none;">@lang('labels.backend.file.format.example')</u></h4>
					@lang('labels.backend.file.format.example_helper') <br/><br/>
					<img src="{{ asset_url() . '/images/excel_sample.png' }}">					
				    </div>
				</div>

			    </div>
			</div>
		    </div>

		    <div class="col-md-6 col-sm-6">
			<legend>{!! trans('validation.attributes.backend.file.accessibility') !!}</legend>
			<div class="form-group">
			    {!! Form::label('file_type_id', trans('validation.attributes.backend.file.type'), ['class' => 'control-label symbol required']) !!}
			    {!! Form::select('file_type_id', $file_types, null, ['class' => 'form-control search-select', 'placeholder' => '', 'id'=>'file_type_id']) !!}
			    {!! $errors->first('file_type_id', '<span class="help-block label label-danger">:message</span>') !!}
			</div>

			<div class="row">
			    <div class="col-md-5 col-sm-5">
				<div id="departments">
				    <div class="form-group">
					{!! Form::label(null, trans('validation.attributes.backend.file.department'), ['class' => 'control-label symbol required']) !!}
					@if ($countries->count())
					<div id="department-tree">
					    <ul>
						@foreach ($countries as $country)
						@if ($country->regions->count())
						<li>{!! $country->name !!}
						    <ul>
							@foreach ($country->regions as $region)
							@if ($region->departments->count())
							<li>{!! $region->name !!}
							    <ul>
								@foreach ($region->branches as $branch)	
								@if ($branch->departments->count())
								<li>{!! $branch->name !!}
								    <ul>
									@foreach ($branch->departments as $department)
									<li id="{!! $department->department_id !!}" >{!! $department->name !!}
									</li>
									@endforeach
								    </ul>
								</li>
								@endif
								@endforeach
							    </ul>
							</li>
							@endif
							@endforeach
						    </ul>
						</li>
						@endif
						@endforeach
					    </ul>
					</div>
					@else
					<p>{{ trans('labels.backend.file_register.no_departments') }}</p>
					@endif
				    </div>
				    {!! Form::hidden('departments') !!}
				    {!! $errors->first('departments', '<span class="help-block label label-danger">:message</span>') !!}
				</div>
			    </div>
			    <div class="col-md-7 col-sm-7">
				<div id="officers">
				    <div class="form-group">
					{!! Form::label(null, trans('validation.attributes.backend.file.officer'), ['class' => 'control-label symbol required']) !!}
					<i class="fa fa-spinner fa-spin" id = "officer_spin" style='display: none'></i>
					{!! Form::select('officers[]', [], null, ['class' => 'form-control officer-select']); !!}
					<span class="help-block">
					    <p>{{ trans('validation.attributes.backend.file.officer_helper') }}</p>
					    {!! $errors->first('officers[]', '<p class="help-block label label-danger">:message</p>') !!}
					</span>
				    </div>
				</div>
			    </div>
			</div>
		    </div>
		    {!! Form::hidden('anchor', '#upload_new') !!}

		</div>

		<hr/>

		<div class="pull-left">
		    <a href="{{route('file.register.index')}}" class="btn btn-sm btn-danger">{{ trans('buttons.general.cancel') }}</a>
		</div>

		<div class="pull-right">
		    <input type="submit" class="btn btn-sm btn-success" value="{{ trans('buttons.general.crud.create') }}" />
		</div>

		{!! Form::close() !!}

	    </div>

	    <div class="tab-pane" id="recent_import">
		<!--	start : table-responsive-->
		<div class="table-responsive">
		    <table id="bulk_upload-table" class="table table-condensed table-hover" style="width:100%">
			<thead>
			    <tr>
				<th>{{ trans('labels.backend.file.table.name') }}</th>
				<th>{{ trans('labels.backend.file.table.status') }}</th>
				<th>{{ trans('labels.general.table.created') }}</th>
				<th>{{ trans('labels.general.actions') }}</th>
			    </tr>
			</thead>
		    </table>
		</div>
		<!--	end : table-responsive-->
	    </div>

	</div>
    </div>
</div>



@stop


@section('after-scripts-end')
{{ Html::script(asset_url() . "/js/plugins/jstree/jstree.min.js") }}
{{ Html::script(asset_url(). "/plugins/autosize/jquery.autosize.min.js") }}
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.js") }}
{{ Html::script(asset_url(). "/js/backend/file/register/script.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}

<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({});
        $('#file_type_id').change(function () {
            var choice = $("#file_type_id").val();
            switch (choice) {
                case '1':
                    $('#departments').show();
                    $('#officers').hide();
                    $("#file_referenceid").children().prop('disabled', false);
                    break;
                case '2':
                    $('#departments').show();
                    $('#officers').show();
                    $("#file_referenceid").children().prop('disabled', false);
                    break;
                case '3':
                    $('#departments').hide();
                    $('#officers').hide();
                    $("#file_referenceid").children().prop('disabled', true);
                    break;
            }
        });
        $(".officer-select").select2({
            minimumInputLength: 2,
            multiple: true,
            ajax: {
                url: "{!! route('file.register.officers') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        departments: function () {
                            var departments = $('#department-tree').jstree("get_checked", false);
                            return (departments == "") ? -1 : departments;
                        },
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.officer_id
                            };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        $('#departments').hide();
        $('#officers').hide();
        $('#file_type_id').trigger('change');

        /************************** start: Control hash for tabs ***************************/
        // show active tab on reload
        if (location.hash !== '')
            $('a[href="' + location.hash + '"]').tab('show');

        // remember the hash in the URL without jumping
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if (history.pushState) {
                history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
            } else {
                location.hash = '#' + $(e.target).attr('href').substr(1);
            }
        });
        /************************** end: Control hash for tabs ***************************/
        /************************** start: Document Upload (Drag & Drop) JS ***************************/
        $("#file_upload").fileinput({
            showPreview: false,
            showUpload: false,
            maxFileCount: 1
        });
        /************************** end: Document Upload (Drag & Drop) JS ***************************/
        /************************** start: Recently uploaded Datatable ***************************/

        $('#bulk_upload-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("file.register.get_bulk") }}',
                type: 'get',
                data: function () {
                    return {};
                }
            },
            columns: [
                {data: 'general', name: 'general'},
                {data: 'status', name: 'status'},
                {data: 'created_at', name: 'created_at'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
        /************************** end: recently uploaded Datatable ***************************/
    });
</script>
@stop