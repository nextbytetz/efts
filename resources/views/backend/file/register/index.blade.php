@extends('backend.layouts.master', ['title' => trans('labels.backend.file_register.title'), 'header_title' => trans('labels.backend.file_register.title'), 'header_sub_title' => trans('labels.backend.file_register.title')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')
{!! Form::open(['route' => ['file.register.create'], 'method' => 'GET']) !!} 
<div class="row-fluid">
    <div class="col-md-6 col-sm-6">
	<div class="form-group">	    
	    <h4><span class="symbol required"></span> &nbsp;&nbsp;{{ trans('validation.attributes.backend.file.nature_select') }}</h4>
	    <div>
		<label class="radio-inline">
		    {!! Form::input('radio', 'file_nature', '1', ['class' => 'grey']) !!}
		    {{ trans('labels.general.electronic') }}
		</label>
		<label class="radio-inline">
		    {!! Form::input('radio', 'file_nature', '2', ['class' => 'grey']) !!}
		    {{ trans('labels.general.physical') }}
		</label>
	    </div>
	    {!! $errors->first('file_nature', '<span class="help-block label label-danger">:message</span>') !!}
	</div>
	<div class="form-actions">	    
	    {!! Form::button(trans('labels.backend.file_register.create_file') .' <i class="fa fa-arrow-circle-right"></i>', ['class' => 'btn btn-info pull-left', 'type'=>'submit']) !!}
	</div>
    </div>
    <div class="col-md-6 col-sm-6" style="padding-left:20px; border-left: 1px solid #ddd;">
        <div class="well">
	    <p><b>{{ trans('labels.general.electronic') }}&nbsp;:</b></p>
	    <p>{{ trans('strings.backend.file_register.electronic') }}</p>
	    <p><b>{{ trans('labels.general.physical') }}&nbsp;:</b></p>
	    <p>{{ trans('strings.backend.file_register.physical') }}</p>
	</div>
    </div>
</div>
{!! Form::close() !!}
<hr/>
@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
<div class="row">
    <div class="col-md-12">
	<div class="panel panel-info">
	    <div class="panel-heading">
		<h4 class="panel-title"><span class="text-bold">{{ trans('history.backend.recent_history') }}</span></h4>                                    
	    </div>
	    <div class="panel-body">
		<!--Main content goes here-->
		<div class="table-responsive">
		    <table id="history-table" class="table table-condensed table-hover">
			<thead>
			    <tr>
				<th>{{ trans('labels.backend.access.users.table.history') }}</th>
				<th>{{ trans('labels.general.table.created') }}</th>
			    </tr>
			</thead>
		    </table>
		</div><!--table-responsive-->
	    </div>
	</div>
    </div>
</div>
@stop
@section('history-scripts')
<script>
    $(function () {
	$('#history-table').DataTable({
	    processing: true,
	    serverSide: true,
	    ajax: {
		url: '{{ route("backend.history", "file_register") }}',
		type: 'get',
		data: {column: 'active', value: 1}
	    },
	    columns: [
		{data: 'history', name: 'history'},
		{data: 'created_at', name: 'created_at'}
	    ],
	    //order: [[0, "asc"]],
	    searchDelay: 500
	});
    });
</script>
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}


<script>

</script>
@stop