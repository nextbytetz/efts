@extends('backend.layouts.master', ['title' => trans('labels.backend.file_register.title'), 'header_title' => trans('labels.backend.file_register.title'), 'header_sub_title' => trans('labels.backend.file_register.all')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal.css") }}
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')
<div class="table-responsive">
    <table id="files-all-table" class="table table-condensed table-hover">
        <thead>
            <tr>
                <th></th>
                <th>{{ trans('labels.backend.file.table.name') }}</th>
                <th>{{ trans('labels.backend.file.table.priority') }}</th>
                <th>{{ trans('labels.backend.file.table.status') }}</th>
                <th>{{ trans('labels.general.actions') }}</th>
            </tr>
        </thead>
    </table>
</div><!--table-responsive-->
<hr/>
<!-- start: BOOTSTRAP EXTENDED FILE CASE MODALS -->
@include('includes.partials.file_case_modal')
<!-- end: BOOTSTRAP EXTENDED FILE CASE MODALS -->
@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
@stop
@section('history-scripts')
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modal.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modalmanager.js") }}
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
<script>
    $(function () {
        $("#file_case_select").select2({
	    multiple: true
	});
        $('#file_case_form').submit(function (event) {
            event.preventDefault();
            if (!$("#file_case_select").val()) {
                alert("{{ trans('labels.backend.file_process.case.select') }}");
            } else {
                this.submit();
            }
        });
        $('#files-all-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("file.register.get") }}',
                type: 'get',
                data: {}
            },
            columns: [
                {data: 'icon'},
                {data: 'name', name: 'name'},
                {data: 'priority', name: 'priority'},
                {data: 'status', name: 'status'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    });
</script>
@stop