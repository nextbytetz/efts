@extends('backend.layouts.master', ['title' => trans('labels.backend.file_register.title'), 'header_title' => trans('labels.backend.file_register.title'), 'header_sub_title' => trans('labels.backend.file_register.history')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
<style> 
</style>
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')

<div class="row">
    <div class="col-md-12">
	<div class="panel panel-info">
	    <div class="panel-heading">
		<h4 class="panel-title"><span class="text-bold">{{ trans('history.backend.recent_history') }}</span></h4>                                    
	    </div>
	    <div class="panel-body">
		<!--Main content goes here-->
		<div class="table-responsive">
		    <table id="history-table" class="table table-condensed table-hover">
			<thead>
			    <tr>
				<th>{{ trans('labels.backend.access.users.table.log') }}</th>
				<th>{{ trans('labels.general.table.created') }}</th>
			    </tr>
			</thead>
		    </table>
		</div><!--table-responsive-->
	    </div>
	</div>
    </div>
</div>

@stop

@section('after-scripts-end')
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
<script>
    $(function () {
	$('#history-table').DataTable({
	    processing: true,
	    serverSide: true,
	    ajax: {
		url: '{{ route("file.register.get_history") }}',
		type: 'get',
		data: {history_type: 'file_register', file_id: "{!! $file_id !!}"}
	    },
	    columns: [
		{data: 'history', name: 'history'},
		{data: 'created_at', name: 'created_at'}
	    ],
	    order: [[0, "asc"]],
	    searchDelay: 500
	});
    });
</script>
@stop