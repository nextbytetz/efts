@extends('backend.layouts.master', ['title' => trans('labels.backend.file_register.title'), 'header_title' => trans('labels.backend.file_register.title'), 'header_sub_title' => trans('labels.backend.file_register.create_file')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/jstree/themes/default/style.min.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')
{!! Form::open(['route' => ['file.register.store'],  'id' => 'create_file']) !!} 
<div class="well">
    {{ $file_nature == 1 ? trans('labels.general.electronic') : trans('labels.general.physical') }}
</div>
<div class="row">
    <div class="col-md-12">
	<div>
	    <span class="symbol required"></span>{{ trans('labels.general.required') }}
	</div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6" style="padding-right:20px; border-right: 1px solid #ddd;">
	<legend>{!! trans('validation.attributes.backend.file.general') !!}</legend>
	{!! Form::hidden('file_nature_id', $file_nature) !!}
	{!! Form::hidden('officer_opened', access()->id()) !!}
        {!! Form::hidden('officer_edited', access()->id()) !!}
	<div class="form-group">
	    {!! Form::label('name', trans('validation.attributes.backend.file.name'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}
	    {!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
	</div>
	<div class="form-group">
	    {!! Form::label('reference', trans('validation.attributes.backend.file.reference'), ['class' => 'control-label symbol required']) !!}
	    <div id="file_referenceid" class="input-group">
		{!! Form::input('text', 'reference', null, ['class' => 'form-control']) !!}
		<span class="input-group-addon pointer" id="random_reference" style="padding: 1px 10px;">
		    <i class="fa fa-random"></i>
		</span>
	    </div>
	    {!! $errors->first('reference', '<span class="help-block label label-danger">:message</span>') !!}
	</div>
	<div class="form-group">
	    {!! Form::label('priority_id', trans('validation.attributes.backend.file.priority'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::select('priority_id', $priorities, null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
	    {!! $errors->first('priority_id', '<span class="help-block label label-danger">:message</span>') !!}
	</div>
	<div class="form-group">
	    {!! Form::label('file_subject_id', trans('validation.attributes.backend.file.subject'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::select('file_subject_id', $file_subjects, null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
	    <i class="fa fa-spinner fa-spin" id = "subject_spin" style='display: none'></i>
	    <span class="help-block">
		<p>{{ trans_choice('validation.attributes.backend.file.subject_helper', 1) }}</p>
		{!! $errors->first('file_subject_id', '<p class="label label-danger">:message</p>') !!}
	    </span>	    
	</div>
	<div class="form-group">
	    {!! Form::label('description', trans('validation.attributes.backend.file.description'), ['class' => 'control-label']) !!}
	    {!! Form::textarea('description', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;']) !!}
	    <span class="help-block">
		<p>{{ trans('validation.attributes.backend.file.description_helper') }}</p>
		{!! $errors->first('description', '<p class="label label-danger">:message</p>') !!}
	    </span>
	</div>
	@if ($file_nature == 2)
	<div class="form-group">
	    {!! Form::label('epc', trans('validation.attributes.backend.file.epc'), ['class' => 'control-label']) !!}
	    {!! Form::input('text', 'epc', null, ['class' => 'form-control']) !!}
	    <span class="help-block">
		<p>{{ trans('validation.attributes.backend.file.epc_helper') }}</p>
		{!! $errors->first('epc', '<p class="label label-danger">:message</p>') !!}
	    </span>
	</div>	
	<div class="form-group">
	    {!! Form::label('shelf_number', trans('validation.attributes.backend.file.shelf_number'), ['class' => 'control-label']) !!}
	    {!! Form::input('text', 'shelf_number', null, ['class' => 'form-control']) !!}
	    <span class="help-block">
		<p>{{ trans('validation.attributes.backend.file.shelf_number_helper') }}</p>
		{!! $errors->first('shelf_number', '<p class="help-block label label-danger">:message</p>') !!}
	    </span>
	</div>
	@endif
	<div class="form-group">
	    {!! Form::label('status', trans('validation.attributes.backend.file.status'), ['class' => 'control-label']) !!}
	    <div>
		<label class="radio-inline">
		    {!! Form::input('radio', 'status', '1', ['class' => 'grey', 'checked' => 'checked']) !!}
		    {{ trans('labels.general.open') }}
		</label>
		<label class="radio-inline">
		    {!! Form::input('radio', 'status', '2', ['class' => 'grey']) !!}
		    {{ trans('labels.general.closed') }}
		</label>
	    </div>
	    {!! $errors->first('status', '<p class="label label-danger">:message</p>') !!}
	</div>
    </div>
    <div class="col-md-6 col-sm-6">
	<legend>{!! trans('validation.attributes.backend.file.accessibility') !!}</legend>
	@if ($file_nature == 1)
	<div class="form-group">
	    {!! Form::label('file_category_id', trans('validation.attributes.backend.file.category'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::select('file_category_id', $file_categories, null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
	    {!! $errors->first('file_category_id', '<span class="help-block label label-danger">:message</span>') !!}
	</div>
	@else
	    {!! Form::hidden('file_category_id', 1) !!}
	@endif
	<div class="form-group">
	    {!! Form::label('file_type_id', trans('validation.attributes.backend.file.type'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::select('file_type_id', $file_types, null, ['class' => 'form-control search-select', 'placeholder' => '', 'id'=>'file_type_id']) !!}
	    {!! $errors->first('file_type_id', '<span class="help-block label label-danger">:message</span>') !!}
	</div>

	<div class="row">
	    <div class="col-md-5 col-sm-5">
		<div id="departments">
		    <div class="form-group">
			{!! Form::label(null, trans('validation.attributes.backend.file.department'), ['class' => 'control-label symbol required']) !!}
			@if ($countries->count())
			<div id="department-tree">
			    <ul>
				@foreach ($countries as $country)
				@if ($country->regions->count())
				<li>{!! $country->name !!}
				    <ul>
					@foreach ($country->regions as $region)
					@if ($region->departments->count())
					<li>{!! $region->name !!}
					    <ul>
						@foreach ($region->branches as $branch)	
                                                @if ($branch->departments->count())
						<li>{!! $branch->name !!}
						    <ul>
							@foreach ($branch->departments as $department)
							<li id="{!! $department->department_id !!}" >{!! $department->name !!}
							</li>
							@endforeach
						    </ul>
						</li>
                                                @endif
						@endforeach
					    </ul>
					</li>
					@endif
					@endforeach
				    </ul>
				</li>
				@endif
				@endforeach
			    </ul>
			</div>
			@else
			<p>{{ trans('labels.backend.file_register.no_departments') }}</p>
			@endif
		    </div>
		    {!! Form::hidden('departments') !!}
		    {!! $errors->first('departments', '<span class="help-block label label-danger">:message</span>') !!}
		</div>
	    </div>
	    <div class="col-md-7 col-sm-7">
		<div id="officers">
		    <div class="form-group">
			{!! Form::label(null, trans('validation.attributes.backend.file.officer'), ['class' => 'control-label symbol required']) !!}
			<i class="fa fa-spinner fa-spin" id = "officer_spin" style='display: none'></i>
			{!! Form::select('officers[]', [], null, ['class' => 'form-control officer-select']); !!}
			<span class="help-block">
			    <p>{{ trans('validation.attributes.backend.file.officer_helper') }}</p>
			    {!! $errors->first('officers[]', '<p class="help-block label label-danger">:message</p>') !!}
			</span>
		    </div>
		</div>
	    </div>
	</div>

    </div>
</div>

<hr/>

<div class="pull-left">
    <a href="{{route('file.register.index')}}" class="btn btn-sm btn-danger">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-sm btn-success" value="{{ trans('buttons.general.crud.create') }}" />
</div>

{!! Form::close() !!}

@stop


@section('after-scripts-end')
{{ Html::script(asset_url() . "/js/plugins/jstree/jstree.min.js") }}
{{ Html::script(asset_url(). "/plugins/autosize/jquery.autosize.min.js") }}
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url(). "/js/backend/file/register/script.js") }}

<script>
    jQuery(document).ready(function () {
	$("textarea.autosize").autosize();
	$(".search-select").select2({});
	$('#file_type_id').change(function () {
	    var choice = $("#file_type_id").val();
	    switch (choice) {
		case '1':
		    $('#departments').show();
		    $('#officers').hide();
		    $("#file_referenceid").children().prop('disabled', false);
		    break;
		case '2':
		    $('#departments').show();
		    $('#officers').show();
		    $("#file_referenceid").children().prop('disabled', false);
		    break;
		case '3':
		    $('#departments').hide();
		    $('#officers').hide();
		    $("#file_referenceid").children().prop('disabled', true);
		    break;
	    }
	});
	$(".officer-select").select2({
	    minimumInputLength: 2,
	    multiple: true,
	    ajax: {
		url: "{!! route('file.register.officers') !!}",
		dataType: 'json',
		delay: 250,
		data: function (params) {
		    return {
			q: params.term,
			departments: function () {
			    var departments = $('#department-tree').jstree("get_checked", false);
			    return (departments == "") ? -1 : departments;
			},
			page: params.page
		    };
		},
		processResults: function (data, params) {
		    params.page = params.page || 1;
		    return {
			results: $.map(data.items, function (item) {
			    return {
				text: item.name,
				id: item.officer_id
			    };
			}),
			pagination: {
			    more: (params.page * 30) < data.total_count
			}
		    };
		},
		cache: true
	    },
	    escapeMarkup: function (markup) {
		return markup;
	    }
	});
	$('#departments').hide();
	$('#officers').hide();
	$('#file_type_id').trigger('change');
    });
</script>
@stop