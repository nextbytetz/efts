@extends('backend.layouts.master', ['title' => trans('labels.backend.file_register.title'), 'header_title' => trans('labels.backend.file_register.title'), 'header_sub_title' => trans('labels.backend.file_register.show')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/css/jstree/themes/default/style.min.css") }}
{{ Html::style(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css") }}
{{ Html::style(asset_url() . "/plugins/bootstrap-modal/css/bootstrap-modal.css") }}
{{ Html::style(asset_url() . "/css/file_format.css") }}
{{ Html::style(asset_url() . "/js/plugins/mentiony_facebook/jquery.mentiony.css") }}

<style>
    #folder-tree .folder { background:url('{!! asset_url() !!}/images/file_sprite.png') right bottom no-repeat; }
    #folder-tree .file { background:url('{!! asset_url() !!}/images/file_sprite.png') 0 0 no-repeat; }   
    #folder-tree .file-pdf { background-position: -32px 0 }
    #folder-tree .file-as { background-position: -36px 0 }
    #folder-tree .file-c { background-position: -72px -0px }
    #folder-tree .file-iso { background-position: -108px -0px }
    #folder-tree .file-htm, #folder-tree .file-html, #folder-tree .file-xml, #folder-tree .file-xsl { background-position: -126px -0px }
    #folder-tree .file-cf { background-position: -162px -0px }
    #folder-tree .file-cpp { background-position: -216px -0px }
    #folder-tree .file-cs { background-position: -236px -0px }
    #folder-tree .file-sql { background-position: -272px -0px }
    #folder-tree .file-xls, #folder-tree .file-xlsx { background-position: -362px -0px }
    #folder-tree .file-h { background-position: -488px -0px }
    #folder-tree .file-crt, #folder-tree .file-pem, #folder-tree .file-cer { background-position: -452px -18px }
    #folder-tree .file-php { background-position: -108px -18px }
    #folder-tree .file-jpg, #folder-tree .file-jpeg, #folder-tree .file-png, #folder-tree .file-gif, #folder-tree .file-bmp { background-position: -126px -18px }
    #folder-tree .file-ppt, #folder-tree .file-pptx { background-position: -144px -18px }
    #folder-tree .file-rb { background-position: -180px -18px }
    #folder-tree .file-text, #folder-tree .file-txt, #folder-tree .file-md, #folder-tree .file-log, #folder-tree .file-htaccess { background-position: -254px -18px }
    #folder-tree .file-doc, #folder-tree .file-docx { background-position: -362px -18px }
    #folder-tree .file-zip, #folder-tree .file-gz, #folder-tree .file-tar, #folder-tree .file-rar { background-position: -416px -18px }
    #folder-tree .file-js { background-position: -434px -18px }
    #folder-tree .file-css { background-position: -144px -0px }
    #folder-tree .file-fla { background-position: -398px -0px }   

</style>
@stop

@section('content')
<!-- start: BOOTSTRAP EXTENDED FILE CASE MODALS -->
@include('includes.partials.file_case_modal')
<!-- end: BOOTSTRAP EXTENDED FILE CASE MODALS -->

@include('backend.file.register.includes.partials.header_buttons')

<nav class="navbar-default">
    <!--     style="background-color: whitesmoke !important"-->
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">{!! trans('labels.general.file') !!}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <!--		<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{!! trans('labels.backend.file.option') !!}<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">{!! trans('labels.backend.file.add_comment') !!}</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="#">{!! trans('labels.backend.file.forward') !!}</a></li>
                                    </ul>
                                </li>-->
		@if ($file->file_category_id != 2 And $file->file_type_id != 3)
		{{-- Check if the file is not a notice file or not a confidential file --}}
                <li><a href="{!! route('file.process.dispatch', $file->file_id) !!}">{!! trans('labels.backend.file.dispatch.file') !!}</a></li>  
		@endif
                <!--		<li><a href="#">{!! trans('labels.backend.file.add_subfolder') !!}<span class="sr-only">(current)</span></a></li>
                                <li><a href="#">{!! trans('labels.backend.file.edit_folder') !!}</a></li>
                                <li><a href="#">{!! trans('labels.backend.file.delete_folder') !!}</a></li>-->
                <!--		<li><a href="#">{!! trans('labels.backend.file.add_document') !!}</a></li>-->
                <li><a href="{!! route('file.process.log', $file->file_id) !!}">{!! trans('labels.backend.file.register_log') !!}</a></li>
                <!--		<li><a href="#">{!! trans('labels.backend.file.change_log') !!}</a></li>
                                <li><a href="#">{!! trans('labels.backend.file.status_log') !!}</a></li>-->
                <li><a href="{!! route('file.register.history', $file->file_id) !!}">{!! trans('labels.backend.file.history') !!}</a></li>
                <li><a href="{!! route('file.process.release', $file->file_id) !!}">{!! trans('labels.backend.file_process.release') !!}</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<!--START: BREADCRUMBS-->
<div class="breadcrumb_show" style="background-color: aliceblue;border-radius: 5px;padding-left: 5px;">
    @include('includes.partials.folders', ['folder_id' => $folder_id, 'file' => $file])
</div>
<div style="background-color: whitesmoke;border-radius: 5px;padding: 6px;margin-bottom: 2px">
    @lang('labels.backend.file_process.log.recent') <b style="border-bottom: 1px dashed #999;text-decoration: none;">{!! $file->recentLog() !!}</b>
</div>
<div style="background-color: lavender;border-radius: 5px;padding: 6px;">
    @lang('labels.backend.file_process.case.current') <i><b id="active_case_bold">{!! $file->active_case !!}</b></i> &nbsp;&nbsp; {!! $file->getEditFileCaseLink() !!}
</div>
<!--END: BREADCRUMBS-->
<hr/>

<div class="row">
    <div class="col-sm-8">
        <div class="well">
            <div class = "row">
                <div class="col-sm-6">
                    <legend>{!! trans('validation.attributes.backend.file.general') !!}</legend>		    
                    <dl class="dl-horizontal">
                        <dt>
                            {!! trans('labels.backend.file.name') !!} :
                        </dt>
                        <dd>
                            @if (access()->allow('create_edit_file'))
                            <a href="{!! route('file.register.edit', $file->file_id) !!}">
                                <b>
                                    {!! $file->name !!}
                                </b>
                            </a>
                            @else
                            <b>
                                {!! $file->name !!}
                            </b>
                            @endif
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.open_date') !!} :
                        </dt>
                        <dd>
                            {!! date("l F j, Y, g:i a", strtotime($file->created_at)) !!}
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.officer_opened') !!} :
                        </dt>
                        <dd>
                            {!! ($file->officer_opened == access()->user()->officer_id) ? 'Me' : $file->useropened->name() !!}
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.category') !!} :
                        </dt>
                        <dd>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="{!! trans('labels.backend.file.category_helper') !!}">{!! $file->category->name !!}</a>
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.type') !!} :
                        </dt>
                        <dd>
                            {!! $file->type->name !!}
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.nature') !!} :
                        </dt>
                        <dd>
                            {!! $file->nature->name !!}
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.reference') !!} :
                        </dt>
                        <dd>
                            {!! $file->reference !!}
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.status') !!} :
                        </dt>
                        <dd>
                            {!! (($file->status == '1')) ? 'OPEN' : 'CLOSED' !!}
                        </dd>
                        <dt>
                            {!! trans('labels.backend.file.shelf_number') !!} :
                        </dt>
                        <dd>
                            {!! $file->shelf_number !!}
                        </dd>
                        <dt>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="{!! trans('labels.backend.file.epc_helper') !!}">{!! trans('labels.backend.file.epc') !!}</a> :
                        </dt>
                        <dd>
                            {!! $file->epc !!} 
                        </dd>
                        <dt>
                            {!! trans('validation.attributes.backend.file.subject') !!} :
                        </dt>
                        <dd>
                            {!! (count($file->subject)) ? $file->subject->name : '' !!}
                        </dd>
                        <dt>
                            {!! trans('validation.attributes.backend.file.description') !!} :
                        </dt>
                        <dd>
                            {!! !(empty($file->description)) ? $file->description : trans('labels.general.none') !!}	    
                        </dd>
                    </dl>
                </div>
                <div class="col-sm-6" style="padding-left:20px; border-left: 1px solid #ddd;">
                    <legend>{!! trans('validation.attributes.backend.file.accessibility') !!}</legend>
                    @if (count($file->departments))
                    <div id="department-show-tree">
                        <ul>
                            <li data-jstree='{"icon":"{!! asset_url() . "/images/department_list.png" !!}"}'>{!! trans('labels.general.departments') !!}
                                <ul>
                                    @foreach ($file->departments as $department)
                                    <li data-jstree='{"icon":"{!! asset_url() . "/images/department.png" !!}"}'><b>D:</b> &nbsp; {!! $department->name !!}, &nbsp; <b>B:</b> &nbsp; {!! $department->branch->name !!}, &nbsp; <b>R:</b> &nbsp; {!! $department->branch->region->name !!}
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                    @endif
                    @if (count($file->users))
                    <div id="officer-show-tree">
                        <ul>
                            <li data-jstree='{"icon":"{!! asset_url() . "/images/users.png" !!}"}'>{!! trans('labels.general.officers') !!}
                                <ul>
                                    @foreach ($file->users as $user)
                                    <li data-jstree='{"icon":"{!! asset_url() . "/images/user.png" !!}"}'>{!! $user->name() !!}
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>  
    <div class='col-sm-4'>
        <legend>{!! trans('labels.backend.file.fast_upload') !!}</legend>
        <input id="document_upload" name="documents[]" type="file" data-preview-file-type="text" multiple class="file-loading">
    </div>
</div>

<div class='row'>
    <div class="col-md-4">
        <legend>{!! trans('labels.backend.file.library') !!}</legend>
        <div id="folder-tree" class="nopadding"></div>
    </div>
    <div class='col-md-8'>
        <legend>
            {!! trans('labels.backend.file.document') !!}
        </legend>
        <!--START: BREADCRUMBS-->
        <div class="breadcrumb_show" style="background-color: aliceblue;border-radius: 5px;padding-left: 5px;">
            @include('includes.partials.folders', ['folder_id' => $folder_id, 'file' => $file])
        </div>
        <!--END: BREADCRUMBS-->
        <div class="table-responsive">
            <table id="documents-table" class="table table-condensed table-hover">
                <thead>
                    <tr>
                        <th></th>
                        <th>{{ trans('labels.backend.file.table.name') }}</th>
                        <th>{{ trans('labels.backend.file.table.priority') }}</th>
                        <th>{{ trans('labels.backend.file.table.status') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
            </table>
        </div>
        <!--	table-responsive-->
        <!--	<br/>
                <legend>
                    {!! trans('labels.backend.file.document') !!}
                </legend>
                <div class="table-responsive">
                    <table id="folders-table" class="table table-condensed table-hover">
                        <thead>
                            <tr>
                                <th></th>
                                <th>{{ trans('labels.backend.file.table.name') }}</th>
                                <th>{{ trans('labels.backend.file.table.status') }}</th>
                                <th>{{ trans('labels.general.actions') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>-->

    </div>
</div>

<div style="margin-top: 100px"></div>
{{ Form::hidden('folder_id', $folder_id) }}
<hr/>
@stop

<!-- start: PAGE HISTORY CONTENT -->
@section('history')
@stop
@section('history-scripts')
@stop
<!-- end: PAGE HISTORY CONTENT -->

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modal.js") }}
{{ Html::script(asset_url() . "/plugins/bootstrap-modal/js/bootstrap-modalmanager.js") }}
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
{{ Html::script(asset_url() . "/js/plugins/jstree/jstree.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}

<script>
    $(function () {
        var department_show = $('#department-show-tree');
        var officer_show = $('#officer-show-tree');
        var folder_show = $('#folder-tree');
        department_show.jstree();
        officer_show.jstree();

        /************************** start: Change file case modal ***************************/
        $("#file_case_select").select2({
            multiple: true
        });
        $('#file_case_form').submit(function (event) {
            event.preventDefault();
            if (!$("#file_case_select").val()) {
                alert("{{ trans('labels.backend.file_process.case.select') }}");
            } else {
                $.ajax({
                    type: $(this).attr('method'),
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    dataType: 'json',
                    encode: true
                })
                        .done(function (data) {
                            $("#active_case_bold").text(data.active_cases);
                        });
                $('#pick_reason').modal('hide');
            }
        });
        /************************** end: Change file case modal ***************************/

        /************************** start: Document & Folders Datatable ***************************/

        $('#documents-table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("storage.document.get") }}',
                type: 'get',
                data: function () {
                    return {'folder_id': $('input[name="folder_id"]').val(), 'file_id': "{!! $file->file_id !!}"};
                }
            },
            columns: [
                {data: 'icon'},
                {data: 'name', name: 'name'},
                {data: 'priority', name: 'priority'},
                {data: 'status', name: 'status'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
        /************************** end: Document & Folders Datatable ***************************/

        /************************** start: Document Upload (Drag & Drop) JS ***************************/
        $("#document_upload").fileinput({
            uploadUrl: "{!! route('storage.document.store') !!}", // server upload action
            uploadAsync: false,
            maxFileCount: 5,
            uploadExtraData: function () {
                return {'folder_id': $('input[name="folder_id"]').val(), 'file_id': "{!! $file->file_id !!}", 'status': 'store'};
            }
        }).on('fileuploaded', function (event, data, previewId, index) {
            var form = data.form, files = data.files, extra = data.extra,
                    response = data.response, reader = data.reader;
            folder_show.jstree("refresh");
        });
        /************************** end: Document Upload (Drag & Drop) JS ***************************/

        /************************** start: Document Library JS ***************************/
        folder_show.jstree({
            'core': {
                'data': {
                    'url': "{!! route('storage.file.show', $file->file_id) !!}",
                    'data': function (node) {
                        return {'id': node.id};
                    }
                },
                'check_callback': function (o, n, p, i, m) {
                    if (m && m.dnd && m.pos !== 'i') {
                        return false;
                    }
                    if (o === "move_node" || o === "copy_node") {
                        if (this.get_node(n).parent === this.get_node(p).id) {
                            return false;
                        }
                    }
                    return true;
                },
                'force_text': true,
                'themes': {
                    'responsive': true,
                    'variant': 'small',
                    'stripes': true
                }
            },
            'sort': function (a, b) {
                return this.get_type(a) === this.get_type(b) ? (this.get_text(a) > this.get_text(b) ? 1 : -1) : (this.get_type(a) >= this.get_type(b) ? 1 : -1);
            },
            'contextmenu': {
                'items': function (node) {
                    var tmp = $.jstree.defaults.contextmenu.items();
                    delete tmp.create.action;
                    delete tmp.remove.action;
                    tmp.remove.action = function (data) {
                        var inst = $.jstree.reference(data.reference),
                                obj = inst.get_node(data.reference);
                        confirmDelete("{!! trans('labels.general.warning') !!}", "{!! trans('strings.backend.general.delete_message') !!}", "{!! trans('buttons.general.cancel') !!}", "{!! trans('buttons.general.confirm') !!}", function (data) {
                            if (data) {
                                inst.delete_node(obj);
                            }
                        });
                    };
                    tmp.create.label = "{!! trans('labels.general.new') !!}";
                    tmp.create.submenu = {
                        "create_folder": {
                            "label": "{!! trans('labels.general.folder') !!}",
                            "action": function (data) {
                                var inst = $.jstree.reference(data.reference),
                                        obj = inst.get_node(data.reference);
                                inst.create_node(obj, {type: "default"}, "last", function (new_node) {
                                    setTimeout(function () {
                                        inst.edit(new_node);
                                    }, 0);
                                });
                            }
                        }
                    };
                    if (this.get_type(node) === "file") {
                        delete tmp.create;
                        delete tmp.remove;
                    }
                    ;
                    return tmp;
                }
            },
            'types': {
                'default': {'icon': 'folder'},
                'file': {'valid_children': [], 'icon': 'file'}
            },
            'unique': {
                'duplicate': function (name, counter) {
                    return name + ' ' + counter;
                }
            },
            'plugins': ['state', 'dnd', 'sort', 'types', 'contextmenu', 'unique']
        })
                .on('delete_node.jstree', function (e, data) {
                    $.ajax({url: "{!! route('storage.file.destroy', $file->file_id) !!}", type: 'POST', data: {'id': data.node.id}})
                            .done(function (d) {
                                if (!d.success) {
                                    data.instance.refresh();
                                    sweetAlert(d.message);
                                }
                            })
                            .fail(function () {
                                data.instance.refresh();
                            });
                })
                .on('create_node.jstree', function (e, data) {
                    $.get("{!! route('storage.file.create') !!}", {'type': data.node.type, 'id': data.node.parent, 'text': data.node.text, 'file_id': "{!! $file->file_id !!}"})
                            .done(function (d) {
                                data.instance.set_id(data.node, d.id);
                            })
                            .fail(function () {
                                data.instance.refresh();
                            });
                })
                .on('rename_node.jstree', function (e, data) {
                    $.get("{!! route('storage.file.edit', $file->file_id) !!}", {'id': data.node.id, 'text': data.text, 'file_id': "{!! $file->file_id !!}"})
                            .done(function (d) {
                                data.instance.set_id(data.node, d.id);
                            })
                            .fail(function () {
                                data.instance.refresh();
                            });
                })
                .on('move_node.jstree', function (e, data) {
                    $.get("{!! route('storage.file.move') !!}", {'id': data.node.id, 'parent': data.parent, 'file_id': "{!! $file->file_id !!}"})
                            .done(function (d) {
                                //data.instance.load_node(data.parent);
                                data.instance.refresh();
                            })
                            .fail(function () {
                                data.instance.refresh();
                            });
                })
                .on('copy_node.jstree', function (e, data) {
                    sweetAlert("{!! trans('alerts.backend.file_register.no_duplicate') !!}");
                    data.instance.refresh();
                });
        folder_show.bind("click.jstree", function (event, data) {
            var node = $(event.target).closest("li").attr("id");
            var type = folder_show.jstree().get_selected(true)[0].type;
            if (type === 'default') {
                /*should only work on folders*/
                node = node.split("{!! DIRECTORY_SEPARATOR !!}");
                node = node[node.length - 1];
                if (node.length > 0) {
                    $('input[name="folder_id"]').val(node);
                    /*window.location = "{!! route('file.register.show', $file->file_id) !!}?folder_id=" + node;*/
                    $('#documents-table').DataTable().ajax.reload();
                    getFolders(node, "{!! $file->name !!}");
                } else {
                    $('input[name="folder_id"]').val(0);
                    $('#documents-table').DataTable().ajax.reload();
                    getFolders(0, "{!! $file->name !!}");
                    /*window.location = "{!! route('file.register.show', $file->file_id) !!}";*/
                }
            }
        });
        /************************** end: Document Library JS ***************************/
    });
    function getFolders(folder_id, file_name) {
        $.get("{{ url('/') }}/getfolders?folder_id=" + folder_id + "&name=" + file_name, function (data) {
            $('.breadcrumb_show').empty();
            $('.breadcrumb_show').html(data);
        });
    }
</script>
@stop