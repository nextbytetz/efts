@extends('backend.layouts.master', ['title' => trans('labels.backend.file_register.title'), 'header_title' => trans('labels.backend.file_register.title'), 'header_sub_title' => trans('labels.backend.file_register.document.show')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.css") }}
{{ Html::style(asset_url() . "/js/plugins/timeline/vis.min.css") }}
{{ Html::style(asset_url() . "/css/file_format.css") }}
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')

<!--START: BREADCRUMBS-->
<div style="background-color: aliceblue;border-radius: 5px;padding-left: 5px;">
    @if (count($document->folder))
    @include('includes.partials.folders', ['folder_id' => $document->folder->folder_id, 'file' => $document->file])
    @else
    @include('includes.partials.folders', ['folder_id' => 0, 'file' => $document->file])
    @endif
</div>
<!--END: BREADCRUMBS-->


<div class="row">
    <div class="col-md-3">
	<legend> {!! trans('labels.backend.file.document_info') !!}</legend>
	<div class="well">
	    <table class="table-condensed">
		<tbody>
		    <tr>
			<td>ID:</td>
			<td>{!! $document->document_id !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans('labels.backend.file.table.name') !!}:</td>
			<td>{!! $document->name !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans("labels.backend.file.created_by") !!}:</td>
			<td>{!! $document->user->name() !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans("labels.backend.file_register.document.space") !!}:</td>
			<td>{!! formatBytes((float) $document->size) !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans("labels.backend.file.created") !!}:</td>
			<td>{!! date("l F j, Y, g:i a", strtotime($document->created_at)) !!}</td>
		    </tr>
		    <tr>
			<td>{!! trans("labels.general.table.last_updated") !!}:</td>
			<td>{!! (!is_null($document->updated_at)) ? $document->updated_at->diffForHumans() : '' !!}</td>
		    </tr>
		</tbody>
	    </table>
	</div>
    </div>
    <div class="col-md-9" style="padding-left:20px; border-left: 1px solid #ddd;">
	<div class="row">
	    <div class="col-md-12">
		<ul class="nav nav-pills">
		    <li role="presentation" class="{!! ($action == 'show') ? 'active' : '' !!}"><a href="#current_version" data-toggle="tab" data-target="#current_version">{!! trans('labels.backend.file.current_version') !!}</a></li>

		    <li role="presentation"><a href="#prev_version" data-toggle="tab" data-target="#prev_version">{!! trans('labels.backend.file.prev_version') !!}</a></li>
		    <li role="presentation" class="{!! ($action == 'edit') ? 'active' : '' !!}"><a href="#edit_document" data-toggle="tab" data-target="#edit_document">{!! trans('labels.backend.file_register.document.edit') !!}</a></li>
		    <li role="presentation""><a href="#change_status" data-toggle="tab" data-target="#change_status">{!! trans('labels.backend.file_register.document.change_status') !!}</a></li>
		</ul>
		<div class="tab-content">
		    <div class="tab-pane {!! ($action == 'show') ? 'active' : '' !!}" id="current_version">
			<div class="well">
			    <table class="table">
				<thead>
				    <tr>
					<th width="*"></th>
					<th width="*">{!! trans('labels.backend.file_register.document.title') !!}</th>
					<th width="*">{!! trans('labels.general.status') !!}</th>
					<th width="*"></th>
				    </tr>
				</thead>
				<tbody>
				    <tr>
					<td>{!! $document->icon !!}</td>
					<td>
					    <ul class="list-group">
						<li class="list-group-item">{!! $document->name !!}</li>
						<li class="list-group-item">{!! trans("labels.backend.file.version") !!} &nbsp;:&nbsp; {!! $document->version !!}</li>
						<li class="list-group-item">{!! formatBytes((float) $document->size) !!} &nbsp;,&nbsp; {!! $document->mime !!}</li>
						<li class="list-group-item">{!! trans("labels.backend.file.created_by") !!} &nbsp;:&nbsp; {!! $document->user->name() !!}</li>
						<li class="list-group-item">{!! $document->created_at->diffForHumans() !!}</li>
						<li class="list-group-item">{!! trans("labels.backend.file.table.priority") !!} &nbsp;:&nbsp; {!! $document->priority->name !!}</li>
					    </ul>
					</td>
					<td>{!! $document->status_info !!}</td>
					<td>
					    {!! $document->download !!}
					</td>
				    </tr>
				</tbody>
			    </table>
			    <hr/>
			    <h5>{!! trans('labels.backend.file.description') !!}</h5>
			    <div>
				{!! $document->description !!}
			    </div>
			</div>
		    </div>
		    <div class="tab-pane {!! ($action == 'edit') ? 'active' : '' !!}" id="edit_document">

			{!! Form::open(['route' => ['storage.document.store'], 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}
			<legend>{!! trans('labels.backend.file_register.document.new') !!}</legend>
			<input id="document_upload" name="documents[]" type="file" data-preview-file-type="text" multiple class="file-loading">

			<br/>
			<div class="form-group">
			    {!! Form::label('description', trans('validation.attributes.backend.file.document.description'), ['class' => 'control-label']) !!}
			    {!! Form::textarea('description', $document->description, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;']) !!}
			    <span class="help-block">
				<p>{{ trans('validation.attributes.backend.file.document.description_helper') }}</p>
				{!! $errors->first('description', '<p class="label label-danger">:message</p>') !!}
			    </span>
			</div>

			{!! Form::hidden('folder_id', $folder_id) !!}
			{!! Form::hidden('file_id', $document->file->file_id) !!}
			{!! Form::hidden('status', 'update') !!}
			{!! Form::hidden('document_id', $document->document_id) !!}

			<hr/>
			<div class="pull-right">
			    <input type="submit" class="btn btn-sm btn-success" value="{{ trans('buttons.general.crud.update') }}" />
			</div>

			{!! Form::close() !!}

		    </div>
		    <div class="tab-pane" id="prev_version">

		    </div>
		    <div class="tab-pane" id="change_status">
			<div class="well">
			    <div class="row">
				<div class="col-md-12">
				    {!! Form::open(['route' => ['storage.document.status']]) !!} 

				    <div class="form-group">
					{!! Form::label('comment', trans('validation.attributes.backend.file.document.comment'), ['class' => 'control-label symbol required']) !!}
					{!! Form::textarea('comment', null, ['class' => 'form-control autosize',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;']) !!}
					<span class="help-block">
					    {!! $errors->first('comment', '<p class="label label-danger">:message</p>') !!}
					</span>
				    </div>
				    <div class="form-group">
					{!! Form::label('priority_id', trans('validation.attributes.backend.file.document.priority'), ['class' => 'control-label symbol required']) !!}
					{!! Form::select('priority_id', $priority, $document->priority_id, ['class' => 'form-control search-select', 'placeholder' => '', 'style' => 'width : 100%']) !!}
					{!! $errors->first('priority_id', '<span class="help-block label label-danger">:message</span>') !!}
				    </div>
				    <div class="form-group">
					{!! Form::label('status', trans('validation.attributes.backend.file.document.status'), ['class' => 'control-label symbol required']) !!}
					{!! Form::select('status', ['1' => trans('labels.general.open'), '0' => trans('labels.general.closed')], $document->status, ['class' => 'form-control search-select', 'style' => 'width : 100%']) !!}
					{!! $errors->first('status', '<span class="help-block label label-danger">:message</span>') !!}
				    </div>
				    @if ($document->file_type_id != 3)
				    <div class="form-group">
					{!! Form::label('notify', trans('validation.attributes.backend.file.document.notify'), ['class' => 'control-label']) !!}
					{!! Form::select('notify', ['0' => trans('labels.general.none'), '1' => trans('labels.general.all'), '2' => trans('labels.general.custom')], null, ['class' => 'form-control search-select', 'style' => 'width : 100%', 'id' => 'notify']) !!}
					{!! $errors->first('notify', '<span class="help-block label label-danger">:message</span>') !!}
				    </div>
				    <div id="officers">
					<div class="form-group">
					    {!! Form::label(null, trans('labels.general.officers'), ['class' => 'control-label symbol required']) !!}
					    {!! Form::select('officers[]', [], null, ['class' => 'form-control officer-select', 'style' => 'width : 100%']); !!}
					    <span class="help-block">
						<p>{{ trans('validation.attributes.backend.file.file_officer') }}</p>
						{!! $errors->first('officers', '<p class="help-block label label-danger">:message</p>') !!}
					    </span>
					</div>
				    </div>
				    @endif
				    {!! Form::hidden('document_id', $document->document_id) !!}
				    {!! Form::hidden('anchor', '#change_status') !!}
				    <hr/>
				    <div class="pull-right">
					<input type="submit" class="btn btn-sm btn-success" value="{{ trans('buttons.general.crud.update') }}" />
				    </div>
				    {!! Form::close() !!}
				</div>
			    </div>
			</div>
		    </div>
		</div>

	    </div>
	</div>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
	<legend>{!! trans('labels.backend.file.status') !!}</legend>
	<div class="well">
	    <div class="table-responsive">
		<table id="document_status_table" class="table table-condensed table-hover">
		    <thead>
			<tr>
			    <th>{{ trans('labels.backend.file.table.date') }}</th>
			    <th>{{ trans('labels.backend.file.table.priority') }}</th>
			    <th>{{ trans('labels.backend.file.table.user') }}</th>
			    <th>{{ trans('labels.backend.file.table.comment') }}</th>
			    <th>{{ trans('labels.general.actions') }}</th>
			</tr>
		    </thead>
		</table>
	    </div>
	</div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
	<legend>{!! trans('labels.backend.file.timeline') !!}</legend>
	<div id="timeline_visualisation"></div>
    </div>
</div>

<hr/>

@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/krajee/fileinput/fileinput.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/timeline/vis.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url(). "/plugins/autosize/jquery.autosize.min.js") }}

<script>
    $(function () {
        $("textarea.autosize").autosize();
        $(".search-select").select2();

        /************************** start: Datatable for status ************************************/
        $('#document_status_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("storage.document.status") }}',
                type: 'get',
                data: {'document_id': "{!! $document->document_id !!}"}
            },
            columns: [
                {data: 'date', name: 'date'},
                {data: 'priority', name: 'priority'},
                {data: 'user', name: 'user'},
                {data: 'comment', name: 'comment'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "desc"]],
            searchDelay: 500
        });
        /************************** end: Datatable for status ************************************/

        /************************** start: Control hash for tabs ***************************/
        // show active tab on reload
        if (location.hash !== '')
            $('a[href="' + location.hash + '"]').tab('show');

        // remember the hash in the URL without jumping
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if (history.pushState) {
                history.pushState(null, null, '#' + $(e.target).attr('href').substr(1));
            } else {
                location.hash = '#' + $(e.target).attr('href').substr(1);
            }
        });
        /************************** end: Control hash for tabs ***************************/
        /************************** start: Control notify user drop down ***************************/
        $('#notify').change(function () {
            var choice = $("#notify").val();
            switch (choice) {
                case '0':
                    $('#officers').hide();
                    break;
                case '1':
                    $('#officers').hide();
                    break;
                case '2':
                    $('#officers').show();
                    break;
            }
        });
        $('#officers').hide();
        $('#notify').trigger('change');
        /************************** end: Control notify user drop down ***************************/

        /************************** start: Officer Lazy Search ***************************/
        $(".officer-select").select2({
            minimumInputLength: 2,
            multiple: true,
            ajax: {
                url: "{!! route('file.process.dispatch.officers', $document->file->file_id) !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.officer_id
                            };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        /************************** end: Officer Lazy Search ***************************/
        /************************** start: Document Upload (Drag & Drop) JS ***************************/
        /*
         $("#document_upload").fileinput({
         uploadUrl: "{!! route('storage.document.store') !!}", // server upload action
         uploadAsync: false,
         maxFileCount: 1,
         showUpload: false,
         uploadExtraData: function () {
         return {'folder_id': "{!! $folder_id !!}", 'file_id': "{!! $document->file->file_id !!}", 'status' : 'update', 'document_id' : "{!! $document->document_id !!}"};
         }
         }).on('fileuploaded', function (event, data, previewId, index) {
         var form = data.form, files = data.files, extra = data.extra,
         response = data.response, reader = data.reader;
         window.location = "{!! route('storage.document.show', $document->document_id) !!}";
         });
         */
        $("#document_upload").fileinput({
            maxFileCount: 1,
	    /* showCaption: false, */
            showUpload: false
        });
        /************************** end: Document Upload (Drag & Drop) JS ***************************/
        /************************** start: Timeline JS ***************************/
        var container = document.getElementById('timeline_visualisation');
        // Create a DataSet (allows two way data-binding)
        var items = new vis.DataSet({!! $doc_status !!});
                // Configuration for the Timeline
                var options = {};
        // Create a Timeline
        var timeline = new vis.Timeline(container, items, options);
        /************************** start: Timeline JS ***************************/
    });
</script>
@stop