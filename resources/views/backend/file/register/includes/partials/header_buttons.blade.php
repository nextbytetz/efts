<div class="pull-left mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.file_register.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
	    <li>{{ link_to_route('file.register.all', trans('menus.backend.file_register.all')) }}</li>
	    <li class="divider"></li>
	    <li>{{ link_to_route('file.register.index', trans('menus.backend.file_register.title')) }}</li>   
	    @permission('register_bulk')
            <li class="divider"></li>
            <li>{{ link_to_route('file.register.bulk', trans('menus.backend.file_register.create_bulk')) }}</li>
	    @endauth
        </ul>
    </div><!--btn group-->

</div><!--pull right-->


<div class="clearfix"></div>
<hr/>