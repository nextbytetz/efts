@extends('backend.layouts.master', ['title' => trans('labels.backend.file_process.title'), 'header_title' => trans('labels.backend.file_process.title'), 'header_sub_title' => trans('labels.backend.file.dispatch.file')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
{{ Html::style(asset_url() . "/js/plugins/mentiony_facebook/jquery.mentiony.css") }}
<style> 
</style>
@stop

@section('content')
@include('backend.file.register.includes.partials.header_buttons')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h4 class="panel-title"><span class="text-bold">{{ trans('labels.backend.file.dispatch.file') }} &nbsp;: <span style="border-bottom: 1px dashed #999;text-decoration: none;">{!! $file->name !!}</span> , {!! trans('validation.attributes.backend.file.category') !!} &nbsp;&nbsp;:&nbsp;&nbsp;{!! $file->category->name !!}</span></h4>                                    
            </div>
            <div class="panel-body">
                <!--Main content goes here-->
                {!! Form::open(['url' => ["/file/process/{$file->file_id}/dispatch"], 'role' => 'form', 'id' => 'dispatch_form']) !!}
                <legend>
                    {!! trans('labels.backend.file.request_active') !!}
                </legend>
                <p>{!! $file->requestUsers() !!}</p>
                <hr/>
                <div class="form-group">
                    {!! Form::label('priority_id', trans('validation.attributes.backend.file.priority'), ['class' => 'control-label symbol required']) !!}
                    {!! Form::select('priority_id', $priorities, $file->priority_id, ['class' => 'form-control search-select']) !!}
                    {!! $errors->first('priority_id', '<span class="help-block label label-danger">:message</span>') !!}
                </div>

                <div class="form-group">
                    {!! Form::label(null, trans('validation.attributes.backend.file.dispatch.officer'), ['class' => 'control-label symbol required']) !!}
                    <i class="fa fa-spinner fa-spin" id = "officer_spin" style='display: none'></i>
                    {!! Form::select('officers[]', [], null, ['class' => 'form-control officer-select']); !!}
                    <span class="help-block">
                        <p>{{ trans('validation.attributes.backend.file.dispatch.officer_helper') }}</p>
                        {!! $errors->first('officers', '<p class="help-block label label-danger">:message</p>') !!}
                    </span>
                </div>

                <legend>
                    {!! trans('validation.attributes.backend.file.comment.title') !!}
                </legend>
                <div class="form-group">
                    {!! Form::label('comment', trans('validation.attributes.backend.file.comment.main'), ['class' => 'control-label symbol required']) !!}
                    {!! Form::textarea('comment', null, ['class' => 'form-control autosize', 'id' => 'comment_field',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;']) !!}
                    <i class="fa fa-spinner fa-spin" id = "spin" style='display: none'></i>
                    <span class="help-block">
                        <p>{{ trans('validation.attributes.backend.file.comment.helper') }}</p>
                        {!! $errors->first('comment', '<p class="label label-danger">:message</p>') !!}
                    </span>
                </div>
                <hr/>

                <div class="pull-left">
                    <a href="{{route('file.register.all')}}" class="btn btn-sm btn-danger">{{ trans('buttons.general.cancel') }}</a>
                </div>
                <div class="pull-right">
                    <input type="submit" class="btn btn-sm btn-success" value="{{ trans('buttons.general.dispatch') }}" />
                </div>
                <br/>
                <br/>
                <hr/>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@stop

@section('after-scripts-end')
<!--{{ Html::script(asset_url() . "/js/plugins/textcomplete/jquery.textcomplete.min.js") }}-->
{{ Html::script(asset_url() . "/js/plugins/mentiony_facebook/jquery.mentiony.min.js") }}
{{ Html::script(asset_url(). "/plugins/autosize/jquery.autosize.min.js") }}
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}
<script>
    $(function () {
        $("textarea.autosize").autosize();
        $(".search-select").select2({});
	
        /************************** start: submit dispatch_form Script ***************************/
        $('#dispatch_form').submit(function (event) {
            event.preventDefault();
            var self = this;
            swal({
                title: "{!! trans('labels.general.confirm') !!}",
                text: "{!! trans('strings.backend.file_process.confirm_forward') !!}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{!! trans('buttons.general.cancel') !!}",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "{!! trans('buttons.general.confirm') !!}",
                closeOnConfirm: true
            }, function (confirmed) {
                if (confirmed)
                    self.submit();
            });
        });
        /************************** end: submit dispatch_form Script ***************************/
	/************************** start: Comment Auto complete for loading document Script using facebook mentiony plugin ***************************/
	var html = '{!! $workflow->comment or '' !!}';
        $('#comment_field').mentiony({
            onDataRequest: function (mode, keyword, onDataRequestCompleteCallback) {
                $.ajax({
                    method: "GET",
                    url: "{!! route('storage.document.search') !!}?q=" + keyword + "&file_id={!! $file->file_id !!}",
                    dataType: "json",
                    success: function (response) {
                        var data = response;
                        /**
                         *  NOTE: Assuming this filter process was done on server-side
                         data = jQuery.grep(data, function (item) {
                         return item.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                         });
                         // End server-side **/
                        // Call this to populate mention.
                        onDataRequestCompleteCallback.call(this, data);
                    }
                });
            },
            timeOut: 400,
            debug: 0,
            triggerChar: '@'
        });	
	$(".mentiony-content").html(html);
        /************************** end: Comment Auto complete for loading document Script using facebook mentiony plugin***************************/
	
        /************************** start: Comment Auto complete for loading document Script ***************************/
	/*
        $('#comment_field').textcomplete([{
                match: /\B@([\-+\w]*)$/,
                replace: function (value) {
                    return '@("' + value + '") ';
                },
                search: function (term, callback) {
                    $.getJSON("{!! route('storage.document.search') !!}", {'q': term, 'file_id': "{!! $file->file_id !!}"})
                            .done(function (resp) {
                                callback(resp);
                            })
                            .fail(function () {
                                callback([]);
                            });
                },
                template: function (value) {
                    return '<i class="fa fa-file-text-o"></i> &nbsp;' + value;
                },
                index: 1
            }], {
            onKeydown: function (e, commands) {
                if (e.ctrlKey && e.keyCode === 74) {
                    return commands.KEY_ENTER;
                }
            }}
        );
	*/
        /************************** end: Comment Auto complete for loading document Script ***************************/
        /************************** start: Select officer for forwarding Script ***************************/
        $(".officer-select").select2({
            minimumInputLength: 2,
            maximumSelectionLength: "{{ ($file->file_category_id == '1') ? 1 : -1 }}",
            multiple: true,
            ajax: {
                url: "{!! route('file.process.dispatch.officers', $file->file_id) !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.officer_id
                            };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        /************************** end: Select officer for forwarding Script ***************************/
    });
</script>
@stop