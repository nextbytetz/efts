@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.location.branch.management'), 'header_title' => trans('labels.backend.sysdef.location.branch.management'), 'header_sub_title' => trans('labels.backend.sysdef.location.branch.create')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
{!! Form::open(['url' => 'sysdef/branch/store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<div class="form-group">
    {!! Form::label('country_id', trans('validation.attributes.backend.sysdef.location.country.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('country_id', $country , null, ['class' => 'form-control search-select', 'id' => 'country_select', 'placeholder' => '']) !!}
	<i class="fa fa-spinner fa-spin" id = "spin" style='display: none'></i>
	{!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('region_id', trans('validation.attributes.backend.sysdef.location.region.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('region_id', [] , null, ['class' => 'form-control search-select', 'id' => 'region_select', 'placeholder' => '']) !!}	
	{!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.location.branch.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{{-- Form::text('name', null, ['class' => 'form-control', 'placeholder' => '']) --}}
	{!! Form::select('name[]', [] , null, ['class' => 'form-control branch-select', 'multiple' => 'multiple']) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.branch.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.create') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
<script>
    jQuery(document).ready(function () {
	$(".search-select").select2({});
	$(".branch-select").select2({
	    tags: true
	});
	$('#country_select').on('change', function (e) {
	    $("#spin").show();
	    var country_id = e.target.value;
	    $.get("{{ url('/') }}/getregion?country_id=" + country_id, function (data) {
		$('#region_select').empty();
		$("#region_select").select2("val", "");
		$('#region_select').html(data);
		$("#spin").hide();
	    });
	});
	if ($("#country_select").val() !== "") {
	    $("#country_select").trigger("change");
	}
    });
</script>
@stop