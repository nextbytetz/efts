@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.location.region.management'), 'header_title' => trans('labels.backend.sysdef.location.region.management'), 'header_sub_title' => trans('labels.backend.sysdef.location.region.edit')])

@section('after-styles-end')
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
<div class="row">
    <p class="col-lg-offset-2 col-lg-10">
	{{ trans('validation.attributes.backend.sysdef.location.country.name') }} &nbsp;:&nbsp; <b> {{ $branch->region->country->name }} </b>
    </p>
</div>
<div class="row">
    <p class="col-lg-offset-2 col-lg-10">
	{{ trans('validation.attributes.backend.sysdef.location.region.name') }} &nbsp;:&nbsp; <b> {{ $branch->region->name }} </b>
    </p>
</div>
<br/>
{!! Form::model($branch, ['url' => ['sysdef/branch/update', $branch->branch_id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.location.branch.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '']) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->
{!! Form::hidden('region_id', $branch->region_id) !!}
<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.branch.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.update') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
@stop