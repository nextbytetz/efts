@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.location.country.management'), 'header_title' => trans('labels.backend.sysdef.location.country.management'), 'header_sub_title' => trans('labels.backend.sysdef.location.country.edit')])

@section('after-styles-end')

@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
{!! Form::model($country, ['url' => ['sysdef/country/update', $country->country_id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.location.country.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.sysdef.location.country.name')]) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.country.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.create') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')

@stop