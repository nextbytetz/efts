@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.location.country.management'), 'header_title' => trans('labels.backend.sysdef.location.country.management'), 'header_sub_title' => trans('labels.backend.sysdef.location.country.create')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
{!! Form::open(['url' => 'sysdef/country/store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<div class="form-group">
    {!! Form::label('country_id', trans('validation.attributes.backend.sysdef.location.country.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('country_id', $country , null, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
	{!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.country.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.create') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
<script>
    jQuery(document).ready(function () {
	$(".search-select").select2({});
    });
</script>
@stop