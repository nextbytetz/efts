@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.location.country.management'), 'header_title' => trans('labels.backend.sysdef.location.country.management'), 'header_sub_title' => trans('labels.backend.sysdef.location.country.active')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
<div class="table-responsive">
    <table id="countries-table" class="table table-condensed table-hover">
	<thead>
	    <tr>
		<th>{{ trans('labels.backend.sysdef.location.country.table.name') }}</th>
		<th>{{ trans('labels.backend.sysdef.location.country.table.number_of_regions') }}</th>
		<th>{{ trans('labels.general.table.created') }}</th>
		<th>{{ trans('labels.general.table.last_updated') }}</th>
		<th>{{ trans('labels.general.actions') }}</th>
	    </tr>
	</thead>
    </table>
</div><!--table-responsive-->
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}

<script>
    $(function () {
	$('#countries-table').DataTable({
	    processing: true,
	    serverSide: true,
	    ajax: {
		url: '{{ route("sysdef.country.get") }}',
		type: 'get',
		data: {}
	    },
	    columns: [
		{data: 'name', name: 'name'},
		{data: 'regions', name: 'regions'},
		{data: 'created_at', name: 'created_at'},
		{data: 'updated_at', name: 'updated_at'},
		{data: 'actions', name: 'actions'}
	    ],
	    order: [[0, "asc"]],
	    searchDelay: 500
	});
    });
</script>
@stop