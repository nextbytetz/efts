@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.rfid.zone.management'), 'header_title' => trans('labels.backend.sysdef.rfid.zone.management'), 'header_sub_title' => trans('labels.backend.sysdef.rfid.zone.create')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
{!! Form::model($zone, ['route' => ['sysdef.rfid.zone.update', $zone->zone_id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) !!}

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.rfid.zone.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}	
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('reader', trans('validation.attributes.backend.sysdef.rfid.zone.reader'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::input('text', 'reader', null, ['class' => 'form-control']) !!}
	{!! $errors->first('reader', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('antenna', trans('validation.attributes.backend.sysdef.rfid.zone.antenna'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::input('text', 'antenna', null, ['class' => 'form-control']) !!}
	{!! $errors->first('antenna', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('country_id', trans('validation.attributes.backend.sysdef.location.country.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('country_id', $country , $zone->departments()->first()->branch->region->country->country_id, ['class' => 'form-control search-select', 'id' => 'country_select', 'placeholder' => '']) !!}
	<i class="fa fa-spinner fa-spin" id = "spin" style='display: none'></i>
	{!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('region_id', trans('validation.attributes.backend.sysdef.location.region.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('region_id', $regions , $zone->departments()->first()->branch->region->region_id, ['class' => 'form-control search-select', 'id' => 'region_select', 'placeholder' => '']) !!}
	<i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
	{!! $errors->first('region_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('branch_id', trans('validation.attributes.backend.sysdef.location.branch.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('branch_id', $branches , $zone->departments()->first()->branch->branch_id, ['class' => 'form-control search-select', 'id' => 'branch_select', 'placeholder' => '']) !!}	
	{!! $errors->first('branch_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('departments', trans('validation.attributes.backend.sysdef.rfid.zone.departments'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('departments[]', $zone->departments->pluck('name', 'department_id')->all(), null, ['class' => 'form-control department-select', 'id' => 'department_select']); !!}
	{!! $errors->first('departments', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->


<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.rfid.zone.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.create') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({});

        $('#country_select').on('change', function (e) {
            $("#spin").show();
            var country_id = e.target.value;
            $.get("{{ url('/') }}/getregion?country_id=" + country_id, function (data) {
                $('#region_select').empty();
                $("#region_select").select2("val", "");
                $('#region_select').html(data);
                $("#spin").hide();
            });
        });

        $('#region_select').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getbranch?region_id=" + region_id, function (data) {
                $('#branch_select').empty();
                $("#branch_select").select2("val", "");
                $('#branch_select').html(data);
                $("#spin2").hide();
            });
        });

        /**** start: Select department based on the branch which has been selected  *****/
        $(".department-select").select2({
            minimumInputLength: 2,
            multiple: true,
            ajax: {
                url: "{!! route('sysdef.rfid.zone.departments') !!}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        branch_id: function () {
                            var branch_id = $('#branch_select').val();
                            return (branch_id == "") ? -1 : branch_id;
                        },
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.items, function (item) {
                            return {
                                text: item.name,
                                id: item.department_id
                            };
                        }),
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            }
        });
        /**** end: Select department based on the branch which has been selected *****/
	$('#department_select').val({!! json_encode($zone->departments->pluck('department_id')->all()) !!});
	$('#department_select').trigger('change.select2');
    });
</script>
@stop