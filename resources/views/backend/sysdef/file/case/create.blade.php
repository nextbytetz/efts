@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.file.case.management'), 'header_title' => trans('labels.backend.sysdef.file.case.management'), 'header_sub_title' => trans('labels.backend.sysdef.file.case.create')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
{!! Form::open(['url' => 'sysdef/file/case', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<div class="form-group">
    {!! Form::label('file_subject_id', trans('validation.attributes.backend.sysdef.file.subject.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('file_subject_id', $subjects , $file_subject_id, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
	{!! $errors->first('file_subject_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.file.case.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{{-- Form::text('name', null, ['class' => 'form-control', 'placeholder' => '']) --}}
	{!! Form::select('name[]', [] , null, ['class' => 'form-control case-select', 'multiple' => 'multiple']) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.file.case.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.create') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
<script>
    jQuery(document).ready(function () {
	$(".search-select").select2({});
	$(".case-select").select2({
	    tags: true
	});
    });
</script>
@stop