@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.file.case.management'), 'header_title' => trans('labels.backend.sysdef.file.case.management'), 'header_sub_title' => trans('labels.backend.sysdef.file.case.edit')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')

<br/>
{!! Form::model($case, ['url' => ['sysdef/file/case', $case->file_case_id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) !!}

<div class="form-group">
    {!! Form::label('file_subject_id', trans('validation.attributes.backend.sysdef.file.subject.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::select('file_subject_id', $subjects , $case->subject->file_subject_id, ['class' => 'form-control search-select', 'placeholder' => '']) !!}
	{!! $errors->first('file_subject_id', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.file.case.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '']) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.file.case.index')}}" class="btn btn-danger btn-xs">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-xs" value="{{ trans('buttons.general.crud.update') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({});
    });
</script>
@stop