@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.file.subject.management'), 'header_title' => trans('labels.backend.sysdef.file.subject.management'), 'header_sub_title' => trans('labels.backend.sysdef.file.subject.create')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/plugins/select2/select2.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
{!! Form::open(['url' => 'sysdef/file/subject', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.file.subject.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{{-- Form::text('name', null, ['class' => 'form-control', 'placeholder' => '']) --}}
	{!! Form::select('name[]', [] , null, ['class' => 'form-control subject-select', 'multiple' => 'multiple']) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->

<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.file.subject.index')}}" class="btn btn-danger btn-sm">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-sm" value="{{ trans('buttons.general.crud.create') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/plugins/select2/select2.min.js") }}
<script>
    jQuery(document).ready(function () {
	$(".subject-select").select2({
	    tags: true
	});
    });
</script>
@stop