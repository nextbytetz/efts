@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.file.subject.management'), 'header_title' => trans('labels.backend.sysdef.file.subject.management'), 'header_sub_title' => trans('labels.backend.sysdef.file.subject.active')])

@section('after-styles-end')
{{ Html::style(asset_url() . "/css/datatables/dataTables.bootstrap.min.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/sweet-alert.css") }}
{{ Html::style(asset_url() . "/plugins/sweetalert/lib/google.css") }}
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>
<div class="table-responsive">
    <table id="file_subject_table" class="table table-condensed table-hover">
        <thead>
            <tr>
                <th>{{ trans('labels.backend.sysdef.file.subject.table.name') }}</th>
                <th>{{ trans('labels.backend.sysdef.file.subject.table.number_of_cases') }}</th>
                <th>{{ trans('labels.backend.sysdef.file.subject.table.number_of_files') }}</th>
                <th>{{ trans('labels.general.table.created') }}</th>
                <th>{{ trans('labels.general.table.last_updated') }}</th>
                <th>{{ trans('labels.general.actions') }}</th>
            </tr>
        </thead>
    </table>
</div><!--table-responsive-->
@stop

@section('after-scripts-end')
{{ Html::script(asset_url() . "/js/backend/backend.js") }}
{{ Html::script(asset_url(). "/js/plugins/datatables/jquery.dataTables.min.js") }}
{{ Html::script(asset_url() . "/js/plugins/datatables/dataTables.bootstrap.min.js") }}
{{ Html::script(asset_url() . "/plugins/sweetalert/lib/sweet-alert.min.js") }}

<script>
    $(function () {
        $('#file_subject_table').DataTable({
            processing: true,
            serverSide: true,
            stateSave: true,
            stateSaveCallback: function (settings, data) {
                localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
            },
            ajax: {
                url: '{{ route("sysdef.file.subject.get") }}',
                type: 'get',
                data: {}
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'cases', name: 'cases'},
                {data: 'files', name: 'files'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'actions', name: 'actions'}
            ],
            order: [[0, "asc"]],
            searchDelay: 500
        });
    });
</script>
@stop