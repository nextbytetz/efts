@extends('backend.layouts.master', ['title' => trans('labels.backend.sysdef.file.subject.management'), 'header_title' => trans('labels.backend.sysdef.file.subject.management'), 'header_sub_title' => trans('labels.backend.sysdef.file.subject.edit')])

@section('after-styles-end')
@stop

@section('content')
@include('backend.sysdef.includes.partials.header_buttons')
<br/>

{!! Form::model($subject, ['url' => ['sysdef/file/subject', $subject->file_subject_id], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) !!}

<div class="form-group">
    {!! Form::label('name', trans('validation.attributes.backend.sysdef.file.subject.name'), ['class' => 'col-lg-2 control-label']) !!}
    <div class="col-lg-10">
	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '']) !!}
	{!! $errors->first('name', '<span class="help-block label label-danger">:message</span>') !!}
    </div>
</div><!--form control-->
{!! Form::hidden('file_subject_id', $subject->file_subject_id) !!}
<hr/>
<div class="pull-left">
    <a href="{{route('sysdef.file.subject.index')}}" class="btn btn-danger btn-xs">{{ trans('buttons.general.cancel') }}</a>
</div>

<div class="pull-right">
    <input type="submit" class="btn btn-success btn-xs" value="{{ trans('buttons.general.crud.update') }}" />
</div>
<div class="clearfix"></div>

{!! Form::close() !!}
@stop

@section('after-scripts-end')
@stop