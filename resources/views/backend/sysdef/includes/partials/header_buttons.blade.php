<div class="pull-left mb-10">
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.sysdef.location.country.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('sysdef.country.index', trans('menus.backend.sysdef.location.country.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('sysdef.country.create', trans('menus.backend.sysdef.location.country.create')) }}</li>
        </ul>
    </div><!--btn group-->
    
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.sysdef.location.region.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('sysdef.region.index', trans('menus.backend.sysdef.location.region.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('sysdef.region.create', trans('menus.backend.sysdef.location.region.create')) }}</li>
        </ul>
    </div><!--btn group-->
    
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.sysdef.location.branch.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('sysdef.branch.index', trans('menus.backend.sysdef.location.branch.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('sysdef.branch.create', trans('menus.backend.sysdef.location.branch.create')) }}</li>
        </ul>
    </div><!--btn group-->
    
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.sysdef.location.department.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('sysdef.department.index', trans('menus.backend.sysdef.location.department.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('sysdef.department.create', trans('menus.backend.sysdef.location.department.create')) }}</li>
        </ul>
    </div><!--btn group-->
    
    <div class="btn-group">
        <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.sysdef.file.subject.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('sysdef.file.subject.index', trans('menus.backend.sysdef.file.subject.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('sysdef.file.subject.create', trans('menus.backend.sysdef.file.subject.create')) }}</li>
        </ul>
    </div><!--btn group-->
    
    <div class="btn-group">
        <button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.sysdef.file.case.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('sysdef.file.case.index', trans('menus.backend.sysdef.file.case.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('sysdef.file.case.create', trans('menus.backend.sysdef.file.case.create')) }}</li>
        </ul>
    </div><!--btn group-->
    
    <div class="btn-group">
        <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            {{ trans('menus.backend.sysdef.rfid.main') }} <span class="caret"></span>
        </button>

        <ul class="dropdown-menu" role="menu">
            <li>{{ link_to_route('sysdef.rfid.zone.index', trans('menus.backend.sysdef.rfid.all')) }}</li>
            <li class="divider"></li>
            <li>{{ link_to_route('sysdef.rfid.zone.create', trans('menus.backend.sysdef.rfid.create')) }}</li>
        </ul>
    </div><!--btn group-->

    
    <!--btn group-->
</div><!--pull right-->


<div class="clearfix"></div>
<hr/>