@extends('frontend.layouts.master')
@section('content')

<!-- start: FORGOT BOX -->
<div class="box-forgot">
    <h3>{{ trans('labels.frontend.auth.forgot_password') }}</h3>
    <p>	
	{{ trans('labels.frontend.auth.forgot_password_helper') }}
    </p>
    @include('includes.partials.messages')
    {!! Form::open(['url' => 'password/email', 'class' => 'form-forgot']) !!}
    <fieldset>
	<div class="form-group">
	    <span class="input-icon">
		{!! Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder'=> trans('validation.attributes.frontend.email')]) !!}	    
		<i class="fa fa-envelope"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('email') }}</span>
	</div>
	<div class="form-actions">
	    {!! link_to('/', trans('labels.frontend.auth.goto_login'), ['class' => 'btn btn-light-grey go-back'], false) !!}
	    {!! Form::button(trans('buttons.general.submit') .' <i class="fa fa-arrow-circle-right"></i>', ['class' => 'btn btn-info pull-right', 'type'=>'submit']) !!}
	</div>
    </fieldset>
    {!! Form::close() !!}
</div>
<!-- end: FORGOT BOX -->

@stop

