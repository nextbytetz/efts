@extends('frontend.layouts.master')
@section('content')

<!-- start: RESET PASSWORD -->
<div class="box-reset-password">
    <h4>{{ trans('labels.frontend.passwords.reset_password_box_title') }}</h4>
    @include('includes.partials.messages')

    {!! Form::open(['url' => 'password/reset', 'class' => 'form-reset-password']) !!}
    <input type="hidden" name="token" value="{{ $token }}">
    <fieldset>
	<div class="form-group">
	    {!! Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('text', 'email', null, ['class' => 'form-control']) !!}
		<i class="fa fa-envelope"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('email') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'password', null, ['class' => 'pass form-control']) !!}
		<i class="fa fa-lock"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('password') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}	
		<i class="fa fa-lock"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('password_confirmation') }}</span>
	</div>

	<div class="form-actions">
	    {!! link_to('/', trans('labels.frontend.auth.goto_login'), ['class' => 'btn btn-light-grey go-back'], false) !!}
	    {!! Form::button(trans('buttons.general.submit') .' <i class="fa fa-arrow-circle-right"></i>', ['class' => 'btn btn-info pull-right', 'type'=>'submit']) !!}
	</div>
    </fieldset>
    {!! Form::close() !!}
</div>
<!-- end: RESET PASSWORD -->

@stop