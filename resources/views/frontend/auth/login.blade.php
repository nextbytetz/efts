@extends('frontend.layouts.master')
@section('content')

<!-- start: LOGIN BOX -->
<div class="box-login">
    <h3>{{ trans('labels.frontend.auth.login_box_title') }}</h3>
    <p>
	{{ trans('labels.frontend.auth.login_box_helper') }}
    </p>
    @include('includes.partials.messages')

    {!! Form::open(['url' => 'login', 'class' => 'form-login']) !!}

    <fieldset>
	<div class="form-group">
	    <span class="input-icon">
		{!! Form::input('text', 'username', null, ['class' => 'form-control',  'placeholder' => trans('validation.attributes.frontend.username')]) !!}
		<i class="fa fa-user"></i>
		{!! $errors->first('username', '<span class="help-block label label-danger">:message</span>') !!}
	    </span>
	</div>
	<div class="form-group form-actions">
	    <span class="input-icon">
		{!! Form::input('password', 'password', null, ['class' => 'form-control password', 'placeholder' => trans('validation.attributes.frontend.password')]) !!}
		<i class="fa fa-lock"></i>
		{!! $errors->first('password', '<span class="help-block label label-danger">:message</span>') !!}
		{!! link_to('password/reset', trans('labels.frontend.passwords.forgot_password'), ['class' => 'forgot'], false) !!}
	    </span>
	</div>
	<div class="form-actions">
	    <label for="remember" class="checkbox-inline">
		{!! Form::input('checkbox', 'remember', null, ['class' => 'grey remember']) !!}		
		{{ trans('labels.frontend.auth.remember_me') }}
	    </label>
	    {!! Form::button(trans('labels.frontend.auth.login_button') .' <i class="fa fa-arrow-circle-right"></i>', ['class' => 'btn btn-info pull-right', 'type'=>'submit']) !!}	   
	</div>
	<div class="new-account">
	    {{ trans('labels.frontend.auth.new_account') }}
	    {!! link_to('register', trans('labels.frontend.auth.create_account'), ['class' => 'register'], false) !!}
	    {!! link_to('help', trans('labels.frontend.auth.help'), ['class' => 'pull-right'], false) !!}
	</div>

    </fieldset>
    <!-- start: COPYRIGHT -->
    <div class="copyright">
	<script>
	    document.write(new Date().getFullYear());
	</script> &copy; <?= company(); ?>
    </div>
    <!-- end: COPYRIGHT -->
    {!! Form::close() !!}
</div>
<!-- end: LOGIN BOX -->
@stop