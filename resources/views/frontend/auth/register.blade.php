
@extends('frontend.layouts.master')
@section('content')

<!-- start: REGISTER BOX -->
<div class="box-register">
    <h3>{{ trans('labels.frontend.auth.sign_up_title') }}</h3>
    <p>
        {{ trans('labels.frontend.auth.sign_up_helper') }}
    </p>

    @include('includes.partials.messages')
    {!! Form::open(['url' => 'register', 'class' => 'form-register']) !!}
    <fieldset>
        <div class="form-actions">	    
            {{ trans('labels.frontend.auth.has_account') }}
            {!! link_to('/', trans('labels.frontend.auth.goto_login'), ['class' => 'register'], false) !!}
        </div>
        <div class="form-group">
            {!! Form::label('firstname', trans('validation.attributes.frontend.firstname'), ['class' => 'control-label symbol required']) !!}
            {!! Form::input('text', 'firstname', null, ['class' => 'form-control']) !!}
            {!! $errors->first('firstname', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('othernames', trans('validation.attributes.frontend.othernames'), ['class' => 'control-label symbol required']) !!}
            {!! Form::input('text', 'othernames', null, ['class' => 'form-control']) !!}
            {!! $errors->first('othernames', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div id = "username" class="form-group">
            {!! Form::label('username', trans('validation.attributes.frontend.username'), ['class' => 'control-label symbol required']) !!}
            {!! Form::input('text', 'username', null, ['class' => 'form-control']) !!}
            {!! $errors->first('username', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('country_id', trans('validation.attributes.backend.sysdef.location.country.name'), ['class' => 'control-label symbol required']) !!}
            {!! Form::select('country_id', $country , null, ['class' => 'form-control search-select', 'id' => 'country_select']) !!}
            <i class="fa fa-spinner fa-spin" id = "spin" style='display: none'></i>
            {!! $errors->first('country_id', '<span class="help-block label label-danger">:message</span>') !!}
        </div><!--form control-->
        <div class="form-group">
            {!! Form::label('region', trans('validation.attributes.frontend.region'), ['class' => 'control-label symbol required']) !!}
            {!! Form::select('region', [], null, ['class' => 'form-control search-select', 'id' => 'region_select']); !!}
            <i class="fa fa-spinner fa-spin" id = "spin2" style='display: none'></i>
            {!! $errors->first('region', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('branch_id', trans('validation.attributes.backend.sysdef.location.branch.name'), ['class' => 'control-label symbol required']) !!}
            {!! Form::select('branch_id', [] , null, ['class' => 'form-control search-select', 'id' => 'branch_select']) !!}
            <i class="fa fa-spinner fa-spin" id = "spin3" style='display: none'></i>
            {!! $errors->first('branch_id', '<span class="help-block label label-danger">:message</span>') !!}
        </div><!--form control-->
        <div class="form-group">
            {!! Form::label('department_id', trans('validation.attributes.frontend.department'), ['class' => 'control-label symbol required']) !!}
            {!! Form::select('department_id', [], null, ['class' => 'form-control search-select', 'id' => 'department_select']); !!}
            {!! $errors->first('department_id', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('officeid', trans('validation.attributes.frontend.office_id'), ['class' => 'control-label']) !!}
            {!! Form::input('text', 'officeid', null, ['class' => 'form-control']) !!}
            {!! $errors->first('officeid', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('gender', trans('validation.attributes.frontend.gender'), ['class' => 'control-label symbol required']) !!}
            <div>
                <label class="radio-inline">
                    {!! Form::input('radio', 'gender', 'F', ['class' => 'grey']) !!}
                    {{ trans('labels.general.female') }}
                </label>		
                <label class="radio-inline">
                    {!! Form::input('radio', 'gender', 'M', ['class' => 'grey']) !!}
                    {{ trans('labels.general.male') }}
                </label>
            </div>
            {!! $errors->first('gender', '<span class="help-block label label-danger">:message</span>') !!}
        </div>

        <div class="form-group">
            {!! Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'control-label']) !!}
            <span class="input-icon">
                {!! Form::input('text', 'email', null, ['class' => 'form-control']) !!}
                <i class="fa fa-envelope"></i>
            </span>
            {!! $errors->first('email', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'control-label symbol required']) !!}
            <span class="input-icon">
                {!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
                <i class="fa fa-lock"></i>
            </span>
            {!! $errors->first('password', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'control-label symbol required']) !!}
            <span class="input-icon">
                {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}
                <i class="fa fa-lock"></i>
            </span>
            {!! $errors->first('password_confirmation', '<span class="help-block label label-danger">:message</span>') !!}
        </div>
        <div class="form-group">
            <div>
                <label for="agree" class="checkbox-inline">
                    {!! Form::input('checkbox', 'agree', null, ['class' => 'grey agree']) !!}	
                    {!! link_to('terms', trans('labels.frontend.auth.terms'), [], false) !!}
                </label>
            </div>
            <span class="help-block label label-danger">{{ $errors->first('agree') }}</span>
        </div>
        <div class="form-actions">	    
            {!! Form::button(trans('labels.frontend.auth.register_button') .' <i class="fa fa-arrow-circle-right"></i>', ['class' => 'btn btn-info pull-right', 'type'=>'submit']) !!}
        </div>
    </fieldset>
    {!! Form::close() !!}
</div>
<!-- end: REGISTER BOX -->

@stop

@section('after-scripts-end')
<script>
    jQuery(document).ready(function () {
        $(".search-select").select2({});
        $('#country_select').on('change', function (e) {
            $("#spin").show();
            var country_id = e.target.value;
	    if (country_id === "") {
		$("#spin").hide();
		return false;
	    }
            $.get("{{ url('/') }}/getregion?country_id=" + country_id, function (data) {
                $('#region_select').empty();
                $("#region_select").select2("val", "");
                $('#region_select').html(data);
                $("#spin").hide();
            });
        });

        $('#region_select').on('change', function (e) {
            $("#spin2").show();
            var region_id = e.target.value;
            $.get("{{ url('/') }}/getbranch?region_id=" + region_id, function (data) {
                $('#branch_select').empty();
                $("#branch_select").select2("val", "");
                $('#branch_select').html(data);
                $("#spin2").hide();
            });
        });

        $('#branch_select').on('change', function (e) {
            $("#spin3").show();
            var branch_id = e.target.value;
            $.get("{{ url('/') }}/getdepartment?branch_id=" + branch_id, function (data) {
                $('#department_select').empty();
                $("#department_select").select2("val", "");
                $('#department_select').html(data);
                $("#spin3").hide();
            });
        });
        $("#country_select").trigger("change");
    });
</script>
@stop