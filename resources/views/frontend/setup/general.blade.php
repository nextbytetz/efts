<?php
/* *
 * Description : View Application set up
 */
?>
@extends('frontend.layouts.master')
@section('content')

<!-- start: SETUP BOX -->
<div class="box-setup">

    <h3>{{ trans('labels.frontend.setup.setup_title') }}</h3>

    {!! Form::open(['url' => 'setup/general', 'class' => 'form-db-setup']) !!}

    @include('includes.partials.messages')	    

    <fieldset>
	<p>
	    <span style="color:red;">**</span>&nbsp; &nbsp; {{ trans('labels.frontend.setup.is_new_instalation') }} <u>{!! link_to('setup/dbnew', trans('labels.general.click_here'), [], false) !!}</u> &nbsp; &nbsp; <span style="color:red;">**</span>
	</p>
	<p>
	    {{ trans('labels.frontend.setup.setup_helper') }}
	</p>
	<p>{{ trans('labels.frontend.setup.pg_connection') }}</p>
	<hr/>
	<div class="form-group">
	    {!! Form::label('DB_HOST', trans('validation.attributes.setup.db_host'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_HOST', $DB_HOST, ['class' => 'form-control']) !!}
	    <span class="help-block">e.g localhost / 127.0.0.1 / 192.168.0.27  etc</span>
	    <span class="help-block label label-danger">{{ $errors->first('DB_HOST') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_PORT', trans('validation.attributes.setup.db_port'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_PORT', $DB_PORT, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_PORT') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_DATABASE', trans('validation.attributes.setup.db_database'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_DATABASE', $DB_DATABASE, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_DATABASE') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_USERNAME', trans('validation.attributes.setup.db_username'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_USERNAME', $DB_USERNAME, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_USERNAME') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_PASSWORD', trans('validation.attributes.setup.db_password'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_PASSWORD', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_PASSWORD') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_SCHEMA', trans('validation.attributes.setup.db_schema'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_SCHEMA', $DB_SCHEMA, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_SCHEMA') }}</span>
	</div>

	<div class="form-actions">
	    {!! link_to('/', trans('buttons.general.cancel'), ['class' => 'btn btn-info'], false) !!}
	    {!! Form::button(trans('buttons.general.crud.update') .' <i class="fa fa-arrow-circle-right"></i>', ['class' => 'btn btn-info pull-right', 'type'=>'submit']) !!}
	</div>
    </fieldset>
    {!! Form::close() !!}


</div>
<!-- end: SETUP BOX -->

@stop