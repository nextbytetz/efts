<?php
/* *
 * @Description : View Application set up
 */
?>

@extends('frontend.layouts.master')
@section('content')


<!-- start: SETUP BOX -->
<div class="box-setup">

    <h4>{{ trans('labels.frontend.setup.setup_helper') }}</h4>

    @include('includes.partials.messages')

    @if ($context == 'dbupdate') 
    <div class="alert alert-info">
	<button data-dismiss="alert" class="close">
	    ×
	</button>
	<p>

	    {{ trans('labels.frontend.setup.dbupdate') }}
	</p>
    </div>
    @elseif ($context == 'dbnew') 
    <div class="alert alert-info">
	<button data-dismiss="alert" class="close">
	    ×
	</button>
	<p>

	    {{ trans('labels.frontend.setup.dbnew') }}
	</p>
    </div>
    @endif

    {!! Form::open(['url' => 'setup/database', 'class' => 'form-db-config']) !!}
    <fieldset>
	@if ($context == 'dbnew') 
	<!--		set database configuration form-->
	<br/>
	<p><b>Database Properties</b></p>
	<hr/>
	<!--	<div class="form-group">		
		    {!! Form::label('CURRENT_DB_USERNAME', trans('validation.attributes.setup.current_db_username'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::input('text', 'CURRENT_DB_USERNAME', null, ['class' => 'form-control']) !!}
		    <span class="help-block label label-danger">{{ $errors->first('CURRENT_DB_USERNAME') }}</span>
		</div>
		<div class="form-group">
		    {!! Form::label('CURRENT_DB_PASSWORD', trans('validation.attributes.setup.current_db_password'), ['class' => 'control-label symbol required']) !!}
		    {!! Form::input('password', 'CURRENT_DB_PASSWORD', null, ['class' => 'form-control']) !!}
		    <span class="help-block label label-danger">{{ $errors->first('CURRENT_DB_PASSWORD') }}</span>
		</div>-->
	<!--			<div class="form-group">
				    <label class="control-label">
					Existing Sample Database <span class="symbol required"></span>
				    </label>
				    <input type="text" class="form-control" name="CURRENT_DATABASE" value="postgres" data-rule-required="true">
				</div> 
				May not be used in future  -->
	<div class="form-group">
	    {!! Form::label('DB_HOST', trans('validation.attributes.setup.db_host'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_HOST', null, ['class' => 'form-control']) !!}
	    <span class="help-block">e.g localhost / 127.0.0.1 / 192.168.0.27  etc</span>
	    <span class="help-block label label-danger">{{ $errors->first('DB_HOST') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_PORT', trans('validation.attributes.setup.db_port'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_PORT', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_PORT') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_DATABASE', trans('validation.attributes.setup.db_database'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_DATABASE', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_DATABASE') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_SCHEMA', trans('validation.attributes.setup.db_schema'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_SCHEMA', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_SCHEMA') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_USERNAME', trans('validation.attributes.setup.db_username'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'DB_USERNAME', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_USERNAME') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('DB_PASSWORD', trans('validation.attributes.setup.db_password'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('password', 'DB_PASSWORD', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('DB_PASSWORD') }}</span>
	</div>
	<br/>
	@endif

	<p><b>{{ trans('labels.frontend.setup.super_properties') }}</b></p>
	<hr/>
	<div class="form-group">
	    {!! Form::label('firstname', trans('validation.attributes.frontend.firstname'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'firstname', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('firstname') }}</span>		    
	</div>
	<div class="form-group">
	    {!! Form::label('othernames', trans('validation.attributes.frontend.othernames'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'othernames', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('othernames') }}</span>		    
	</div>
	<div class="form-group">
	    {!! Form::label('username', trans('validation.attributes.frontend.username'), ['class' => 'control-label symbol required']) !!}
	    {!! Form::input('text', 'username', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('username') }}</span>		    
	</div>
	<div class="form-group">
	    {!! Form::label('officeid', trans('validation.attributes.frontend.office_id'), ['class' => 'control-label']) !!}
	    {!! Form::input('text', 'officeid', null, ['class' => 'form-control']) !!}
	    <span class="help-block label label-danger">{{ $errors->first('officeid') }}</span>		    
	</div>
	<div class="form-group">
	    {!! Form::label('gender', trans('validation.attributes.frontend.gender'), ['class' => 'control-label symbol required']) !!}
	    <div>
		<label class="radio-inline">
		    {!! Form::input('radio', 'gender', 'F', ['class' => 'grey']) !!}
		    {{ trans('labels.general.female') }}
		</label>		
		<label class="radio-inline">
		    {!! Form::input('radio', 'gender', 'M', ['class' => 'grey']) !!}
		    {{ trans('labels.general.male') }}
		</label>
	    </div>
	    <span class="help-block label label-danger">{{ $errors->first('gender') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'control-label']) !!}
	    <span class="input-icon">
		{!! Form::input('text', 'email', null, ['class' => 'form-control']) !!}
		<i class="fa fa-envelope"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('email') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
		<i class="fa fa-lock"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('password') }}</span>
	</div>
	<div class="form-group">
	    {!! Form::label('password_confirmation', trans('validation.attributes.frontend.password_confirmation'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}
		<i class="fa fa-lock"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('password_confirmation') }}</span>
	</div>
	@if  ($context == 'dbupdate') 
	<!--		This field is for confirming password of the current super user who has the mandate to recreate and reconfigure the database and system-->
	<p><b>{{ trans('labels.frontend.setup.super_properties') }}</b></p>
	<hr/>
	<div class="form-group">		
	    {!! Form::label('password_super_user', trans('validation.attributes.frontend.password'), ['class' => 'control-label symbol required']) !!}
	    <span class="input-icon">
		{!! Form::input('password', 'password_super_user', null, ['class' => 'form-control']) !!}
		<i class="fa fa-lock"></i>
	    </span>
	    <span class="help-block label label-danger">{{ $errors->first('password_super_user') }}</span>
	</div>
	@endif
	<input type="hidden" name="context" value="<?= $context; ?>">
	<div class="form-actions">
	    {!! Form::button(trans('labels.frontend.auth.register_button') .' <i class="fa fa-arrow-circle-right"></i>', ['class' => 'btn btn-info pull-right', 'type'=>'submit']) !!}
	</div>
    </fieldset>
    {!! Form::close() !!}

</div>
<!-- end: SETUP BOX -->


@stop