<?php
/**
 * @author Erick Chrysostom 
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- start: HEAD -->

    <head>
	<title><?= app_name(); ?> - Home</title>
	<!-- start: META -->
	<meta charset="utf-8" />
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta content="{{ meta_content() }}" name="description" />
	<meta content="Erick Chrysostom <gwanchi@gmail.com>" name="author" />
	<!-- Add security to verify http request that come from this application -->
        <meta name="_token" content="{{ csrf_token() }}" />
	<!-- start: MAIN CSS -->
	<link rel="stylesheet" href="<?= asset_url() ?>/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= asset_url() ?>/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= asset_url() ?>/plugins/animate.css/animate.min.css">
	<link rel="stylesheet" href="<?= asset_url() ?>/plugins/iCheck/skins/all.css">
	<link rel="stylesheet" href="<?= asset_url() ?>/plugins/sweetalert/lib/sweet-alert.css">
	<link rel="stylesheet" href="<?= asset_url() ?>/css/styles.css">
	<!--	Start : My custom css file-->
	<link rel="stylesheet" href="<?= asset_url() ?>/css/custom.css">
	<!--	End : My custom css file-->
	<link rel="stylesheet" href="<?= asset_url() ?>/css/styles-responsive.css">
	<link rel="stylesheet" href="<?= asset_url() ?>/plugins/iCheck/skins/all.css">
	<!--[if IE 7]>
	<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
	<![endif]-->
	<!-- end: MAIN CSS -->

	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link href="<?= asset_url() ?>/plugins/select2/select2.css" rel="stylesheet" />
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?= asset_url() ?>/plugins/jQuery/jquery-2.1.1.min.js"></script>
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body class="login">

	<!-- start: TOPBAR -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
	    <div class="container">
		<div class="navbar-header">
		    <!-- Collapsed Hamburger -->
		    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#frontend-navbar-collapse">
			<span class="sr-only"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		    </button>
		    <!-- Branding Image -->
		    <a class="navbar-brand" href="#">
			{!! app_name() !!} &nbsp; (<b>{{ trans('labels.general.app_title') }}</b>)
		    </a>
		</div><!--navbar-header-->

		<div class="collapse navbar-collapse" id="frontend-navbar-collapse">
		    <!-- Left Side Of Navbar -->
		    <ul class="nav navbar-nav">
		    </ul>
		    <!-- Right Side Of Navbar -->
		    <ul class="nav navbar-nav navbar-right">
			@if (config('locale.status') && count(config('locale.languages')) > 1)
			<li class="dropdown">
			    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
				{{ trans('menus.language-picker.language') }}
				<span class="caret"></span>
			    </a>
			    @include('includes.partials.lang')
			</li>
			@endif
		    </ul>
		</div><!--navbar-collapse-->
	    </div><!--container-->
	</nav>
	<!-- end: TOPBAR -->


	<div class="main-login col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
	    <div class="logo">
		<?= app_name_icon(); ?>
	    </div>
	    <!--Main content goes here-->
	    @yield('content')
	</div>

	<!-- start: MAIN JAVASCRIPTS -->
	<!--[if lt IE 9]>
	<script src="assets/plugins/respond.min.js"></script>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
	<!--<![endif]-->
	<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});
var public_url = "<?= public_url() ?>";
	</script>
	<script src="<?= asset_url() ?>/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
	<script src="<?= asset_url() ?>/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?= asset_url() ?>/plugins/iCheck/jquery.icheck.min.js"></script>
	<script src="<?= asset_url() ?>/plugins/jquery.transit/jquery.transit.js"></script>
	<script src="<?= asset_url() ?>/plugins/bootbox/bootbox.min.js"></script>
	<script src="<?= asset_url() ?>/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!--	<script src="<?php //echo asset_url()    ?>/plugins/sweetalert/lib/sweet-alert.min.js"></script>	-->
	<script src="<?= asset_url() ?>/js/main.js"></script>
	<!-- end: MAIN JAVASCRIPTS -->
	<!-- start: JAVASCRIPT REQUIRED FOR BOOTSTRAP MODALS ONLY -->
	<script src="<?= asset_url() ?>/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
	<script src="<?= asset_url() ?>/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
	<!-- end: JAVASCRIPT REQUIRED FOR BOOTSTRAP MODALS ONLY -->
	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?= asset_url() ?>/plugins/select2/select2.min.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	@yield('after-scripts-end')
	<script>
jQuery(document).ready(function () {
    Main.init();
    //Home.init();   
});
	</script>
    </body>
    <!-- end: BODY -->

</html>
