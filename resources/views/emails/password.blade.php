{{ trans('labels.general.dear') }}  <?= $user->firstname ?> <br/>
{{ trans('strings.emails.auth.reset_password') }} : {{ url('password/reset/'.$token) }}