<div id="pick_reason" class="modal extended-modal fade no-display" tabindex="-1" data-width="760">
    <div class="modal-dialog">
	<div class="modal-content">
	    {!! Form::open(['url' => '#', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'get', 'id' => 'file_case_form']) !!}
	    {!! Form::token() !!}
	    {!! Form::hidden('_method', null, ['id' => 'method']) !!}
	    <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
		    &times;
		</button>
		<h4 class="modal-title">@lang('labels.backend.file_process.pick_reason') &nbsp;&nbsp; <b><span id='file_name' style="border-bottom: 1px dashed #999;text-decoration: none;"></span></b></h4>
		<a id='add_case_url' href=''>@lang('labels.backend.file_process.case.add')</a>
	    </div>
	    <div class="modal-body">
		<div class="form-group">
		    {!! Form::label('region_id', trans('labels.backend.file_process.case.title'), ['class' => 'col-sm-2 control-label']) !!}
		    <div class="col-sm-10">
			{!! Form::select('file_case[]', [] , null, ['class' => 'form-control', 'id' => 'file_case_select', 'placeholder' => '', 'style' => 'width : 100%']) !!}
		    </div>
		</div><!--form control-->
	    </div>
	    <div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-light-grey">
		    @lang('labels.general.buttons.close')
		</button>
		<button type="submit" class="btn btn-blue">
		    @lang('labels.general.buttons.continue')
		</button>
	    </div>
	    {!! Form::close() !!}
	</div>
    </div>
</div>