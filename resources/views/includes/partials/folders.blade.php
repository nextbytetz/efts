<?php

use App\Models\File\Folder;

$folder = Folder::find($folder_id);
$loop = true;
$stack = array();
if (count($folder)) {
    while ($loop) {
	array_push($stack, $folder->name);
	if (count($folder->upfolder)) {
	    $folder = Folder::where(['folder_id' => $folder->parent_id])->first();
	} else {
	    $loop = false;
	}
    }
    array_push($stack, $folder->file->name);
} else {
    array_push($stack, $file->name);
}
$reversed = array_reverse($stack);
?>
<!--START: BREADCRUMBS-->
<ol class="breadcrumb">
    @foreach ($reversed as $value)
    <li>
	{!! $value !!}	 	
    </li>
    @endforeach
</ol>
<!--END: BREADCRUMBS-->