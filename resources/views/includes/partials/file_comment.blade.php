{!! Form::open(['url' => ["/file/process/{$file_id}/log"], 'role' => 'form', 'id' => 'comment_form']) !!} 
<legend>
    {!! trans('validation.attributes.backend.file.comment.title') !!}
</legend>
<div class="form-group">
    {!! Form::label('comment', trans('validation.attributes.backend.file.comment.main'), ['class' => 'control-label']) !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control autosize', 'id' => 'comment_field',  'style' => 'overflow: hidden; word-wrap: break-word; resize: horizontal;']) !!}
    <i class="fa fa-spinner fa-spin" id = "spin" style='display: none'></i>
    <span class="help-block">
	<p>{{ trans('validation.attributes.backend.file.comment.helper') }}</p>
	{!! $errors->first('comment', '<p class="label label-danger">:message</p>') !!}
    </span>
</div>
@if ($file_category_id == 2)
<div class="form-group">
    {!! Form::label('notify', trans('validation.attributes.backend.file.document.notify'), ['class' => 'control-label']) !!}
    {!! Form::select('notify', ['0' => trans('labels.general.none'), '1' => trans('labels.general.all'), '2' => trans('labels.general.custom')], null, ['class' => 'form-control search-select', 'style' => 'width : 100%', 'id' => 'notify']) !!}
    {!! $errors->first('notify', '<span class="help-block label label-danger">:message</span>') !!}
</div>
<div id="officers">
    <div class="form-group">
	{!! Form::label(null, trans('labels.general.officers'), ['class' => 'control-label symbol required']) !!}
	{!! Form::select('officers[]', [], null, ['class' => 'form-control officer-select', 'style' => 'width : 100%']); !!}
	<span class="help-block">
	    <p>{{ trans('validation.attributes.backend.file.file_officer') }}</p>
	    {!! $errors->first('officers', '<p class="help-block label label-danger">:message</p>') !!}
	</span>
    </div>
</div>
@endif

<div class="pull-right">
    <input type="submit" class="btn btn-sm btn-success" value="{{ trans('buttons.general.crud.update') }}" />
</div>
<br/>
<br/>
<hr/>
{!! Form::close() !!}