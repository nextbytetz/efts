<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Menus Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in menu items throughout the system.
      | Regardless where it is placed, a menu item can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
	'access' => [
	    'title' => 'Access Management',
	    'roles' => [
		'all' => 'All Roles',
		'create' => 'Create Role',
		'edit' => 'Edit Role',
		'management' => 'Role Management',
		'main' => 'Roles',
	    ],
	    'users' => [
		'all' => 'All Users',
		'change_password' => 'Change Password',
		'deactivated' => 'Deactivated Users',
		'main' => 'Users',
		'out' => 'Out of Office',
		'replace_super' => 'Replace Super',
		'super_user_history' => 'Super User History',
		'super_user' => 'Super User',
		'edit' => 'Edit User',
		'profile' => 'User Profile',
		'password' => 'Change Password',
                'group' => [
                    'option' => 'Group Options',
                ],
	    ],
	],
	'sysdef' => [
	    'location' => [
		'country' => [
		    'main' => 'Countries',
		    'all' => 'All Countries',
		    'create' => 'Add Country',
		],
		'region' => [
		    'main' => 'Regions',
		    'all' => 'All Regions',
		    'create' => 'Add Region',
		],
		'branch' => [
		    'main' => 'Branches',
		    'all' => 'All Branches',
		    'create' => 'Add Branch',
		],
		'department' => [
		    'main' => 'Departments',
		    'all' => 'All Departments',
		    'create' => 'Add Department',
		],
	    ],
	    'rfid' => [
		'main' => 'RFID Zones',
		'all' => 'All Zones',
		'create' => 'Add RFID Zone',
	    ],
	    'file' => [
		'subject' => [
		    'main' => 'File Subjects',
		    'all' => 'All Subjects',
		    'create' => 'Add File Subjects',
		],
		'case' => [
		    'main' => 'File Cases',
		    'all' => 'All Cases',
		    'create' => 'Add File Cases',
		],
	    ],
	],
	'file_register' => [
	    'main' => 'File',
	    'title' => 'File Register',
	    'create_bulk' => 'Bulk Register',
	    'all' => 'All Registered',
	],
	'file_process' => [
	    
	],
    ],
    'language-picker' => [
	'language' => 'Language',
	/**
	 * Add the new language to this array.
	 * The key should have the same language code as the folder name.
	 * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
	 * Be sure to add the new language in alphabetical order.
	 */
	'langs' => [
	    'en' => 'English',
	    'sw' => 'Swahili',
	],
    ],
];
