<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => 'The :attribute must be accepted.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'alpha' => 'The :attribute may only contain letters.',
    'alpha_dash' => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => 'The :attribute may only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'before' => 'The :attribute must be a date before :date.',
    'between' => [
	'numeric' => 'The :attribute must be between :min and :max.',
	'file' => 'The :attribute must be between :min and :max kilobytes.',
	'string' => 'The :attribute must be between :min and :max characters.',
	'array' => 'The :attribute must have between :min and :max items.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'date' => 'The :attribute is not a valid date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'email' => 'The :attribute must be a valid email address.',
    'exists' => 'The selected :attribute is invalid.',
    'filled' => 'The :attribute field is required.',
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'max' => [
	'numeric' => 'The :attribute may not be greater than :max.',
	'file' => 'The :attribute may not be greater than :max kilobytes.',
	'string' => 'The :attribute may not be greater than :max characters.',
	'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'min' => [
	'numeric' => 'The :attribute must be at least :min.',
	'file' => 'The :attribute must be at least :min kilobytes.',
	'string' => 'The :attribute must be at least :min characters.',
	'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'The selected :attribute is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'present' => 'The :attribute field must be present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values is present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
	'numeric' => 'The :attribute must be :size.',
	'file' => 'The :attribute must be :size kilobytes.',
	'string' => 'The :attribute must be :size characters.',
	'array' => 'The :attribute must contain :size items.',
    ],
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'The :attribute has already been taken.',
    'url' => 'The :attribute format is invalid.',
    "phone" => "The :attribute field contains an invalid number.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
	'attribute-name' => [
	    'rule-name' => 'custom-message',
	],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [

	'backend' => [
	    'access' => [
		'permissions' => [
		    'associated_roles' => 'Associated Roles',
		    'dependencies' => 'Dependencies',
		    'display_name' => 'Display Name',
		    'group' => 'Group',
		    'group_sort' => 'Group Sort',
		    'groups' => [
			'name' => 'Group Name',
		    ],
		    'name' => 'Name',
		    'system' => 'System?',
		],
		'roles' => [
		    'associated_permissions' => 'Associated Permissions',
		    'name' => 'Name',
		    'sort' => 'Sort',
		],
		'users' => [
		    'active' => 'Active',
		    'associated_roles' => 'Associated Roles',
		    'confirmed' => 'Confirmed',
		    'available' => 'Available',
		    'email' => 'Email',
		    'username' => 'Username',
		    'gender' => 'Gender',
		    'pic' => 'Image Upload',
		    'phone' => 'Phone',
		    'firstname' => 'Firstname',
		    'othernames' => 'Othenames',
		    'other_permissions' => 'Other Permissions',
		    'password' => 'Password',
		    'password_confirmation' => 'Password Confirmation',
		    'send_confirmation_email' => 'Send Confirmation E-mail',
		    'old_password' => 'Old Password',
		    'new_password' => 'New Password',
		    'password' => 'Password',
		    'password_confirmation' => 'Confirm Password',
		],
	    ],
	    'sysdef' => [
		'location' => [
		    'country' => [
			'name' => 'Country Name',
		    ],
		    'region' => [
			'name' => 'Region Name',
		    ],
		    'branch' => [
			'name' => 'Branch Name',
		    ],
		    'department' => [
			'name' => 'Department Name',
		    ],
		],
		'rfid' => [
		    'zone' => [
			'name' => 'Name',
			'reader' => 'Reader',
			'antenna' => 'Antenna Port Number',
			'departments' => 'Departments',
		    ],
		],
		'file' => [
		    'subject' => [
			'name' => 'File Subject',
		    ],
		    'case' => [
			'name' => 'File Case',
		    ],
		],
	    ],
	    'file' => [
		'nature_select' => 'Select nature of your file',
		'name' => 'File Name',
		'reference' => 'Reference Number',
		'subject' => 'File Subject',
		'description' => 'File Description',
		'subject_helper' => 'Topic about this file if any, e.g House Loans, Customer Queries, Employee Clearance, Employee file etc. Previously entered entries will be auto suggested as you type to help you selecting the same file subjects|Topic about these files if any, e.g House Loans, Customer Queries, Employee Clearance, Employee file etc. Previously entered entries will be auto suggested as you type to help you selecting the same file subjects',
		'description_helper' => 'Further description about this file based on the file subject if any',
		'category' => 'File Category',
		'type' => 'File Type',
		'epc' => 'EPC',
		'shelf_number' => 'Shelf Number',
		'shelf_number_helper' => 'Permanent storage shelf number in the file store room',
		'status' => 'File Status',
		'general' => 'General Information',
		'accessibility' => 'File Accessibility',
		'epc_helper' => 'Electronic Product Code for RFID tag placed on a file',
		'department' => 'Department Accessibility',
		'officer' => 'Officer Accessibility',
		'officer_helper' => 'Please check the department in order to search the respective officer',
		'file_officer' => 'Only officers given access to this file will be available',
                'dispatch' => [
                    'officer' => 'Choose officer',
                    'officer_helper' => 'Search for officer to dispatch the file, if the file category is normal you will be limited to dispatch to only one officer and if the file category is notice, you can dispatch the file to more than one officer. All officers to recive the file should have been given access to this file',
                ],
		'storage' => [
		    'remove_error' => 'Can not remove file in this context',
		    'rename_error' => 'Can not rename file in this context',
		    'name_error' => 'Invalid name : :name',
		    'path_error' => 'Path already exists : :new',
		    'list_error' => 'Could not list path : :dir',
		    'name_exist' => 'Folder :name already exists',
		    'path_exist' => 'Path does not exist: :path',
		    'base_error' => 'Base directory does not exist',
		],
		'document' => [
		    'description' => 'Document Description',
		    'description_helper' => 'Explain the content of this document',
		    'comment' => 'Comments / Reviews',
		    'priority' => 'Priority',
		    'status' => 'Status',
		    'notify' => 'Notify User',
		],
		//'comment' => 'Comment',
		//'comment_helper' => 'Add your comment to the file for the next officer',
		'comment' => [
		    'main' => 'Comment (To refer to any document in this file start with the key @)',
		    'title' => 'Post Comment',
		    'helper' => 'Add your comment to the file for the next officer',
		],
		'priority' => 'Priority',
		'csv' => 'Upload Microsoft Excel xlsx, xls file',
		'csv_helper' => 'Make sure that your data are on the first sheet of excel document',
		'group_name' => 'Group name',
		'group_name_helper' => 'By specifying a group name, it will give you flexibility to change any information related to this bulk import in the future',
	    ],
	],
	'frontend' => [
	    'username' => 'Username',
	    'firstname' => 'First Name',
	    'password' => 'Password',
	    'othernames' => 'Other Names(s)',
	    'region' => 'Region',
	    'select' => 'Select',
	    'department' => 'Department',
	    'office_id' => 'Office ID',
	    'gender' => 'Gender',
	    'email' => 'Email',
	    'password_confirmation' => 'Confirm Password',
	    'old_password' => 'Old Password',
	    'new_password' => 'New Password',
	    'new_password_confirmation' => 'New Password Confirmation',
	],
	'setup' => [
	    'db_host' => 'Database Host',
	    'db_port' => 'Database Port',
	    'db_database' => 'Database Name',
	    'db_username' => 'Database User',
	    'db_password' => 'Database Password',
	    'db_schema' => 'Database Schema',
	    'current_db_username' => 'Current Privileged Database User Name',
	    'current_db_password' => 'Current Privileged Database User Password',
	],
    ],
];
