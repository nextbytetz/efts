<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Buttons Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in buttons throughout the system.
      | Regardless where it is placed, a button can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
	'access' => [
	    'users' => [
		'activate' => 'Activate',
		'change_password' => 'Change Password',
		'deactivate' => 'Deactivate',
		'delete_permanently' => 'Delete Permanently',
		'resend_email' => 'Resend Confirmation E-mail',
		'restore_user' => 'Restore User',
		'out_office' => 'Out of Office',
		'in_office' => 'In Office',
	    ],
	],
    ],
    'general' => [
	'cancel' => 'Cancel',
	'confirm' => 'Yes',
	'reset' => 'Reset',
	'crud' => [
	    'create' => 'Create',
	    'delete' => 'Delete',
	    'edit' => 'Edit',
	    'update' => 'Update',
	],
	'file' => [
	    'open' => 'Open file',
	],
	'save' => 'Save',
	'view' => 'View',
	'approve' => 'Approve',
	'submit' => 'Submit',
	'copy' => 'Add to clipboard',
        'dispatch' => 'Dispatch File',
    ],
];
