<?php

/*
  |--------------------------------------------------------------------------
  | Labels Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used in labels throughout the system.
  | Regardless where it is placed, a label can be listed here so it is easily
  | found in a intuitive way.
  |
 */

return [

    'general' => [
	'all' => 'All',
	'yes' => 'Yes',
	'no' => 'No',
	'custom' => 'Custom',
	'actions' => 'Actions',
	'action' => 'Action',
	'buttons' => [
	    'save' => 'Save',
	    'update' => 'Update',
	    'close' => 'Close',
	    'continue' => 'Continue',
	],
	'table' => [
	    'created' => 'Created',
	    'updated' => 'Updated',
	    'last_updated' => 'Last Updated',
	],
	'hide' => 'Hide',
	'none' => 'None',
	'show' => 'Show',
	'toggle_navigation' => 'Toggle Navigation',
	'female' => 'Female',
	'male' => 'Male',
	'dear' => 'Dear',
	'click_here' => 'Click here',
	'app_title' => 'RFID & Bar Code Enabled File Tracking, Store and Monitoring System',
	'search' => 'Scan/Search file by name, reference/code',
	'shortcut' => 'SHORTCUT',
	'try_shortcut' => 'Try the application shortcut here',
	'office_suite' => 'OFFICE SUITE',
	'greeting' => 'Hi there',
	'profile' => 'Profile',
	'calender' => 'Calendar',
	'logout' => 'Logout',
	'welcome' => 'Welcome',
	'note' => 'Note',
	'dashboard' => 'Dashboard',
	'status' => 'Status',
	'online' => 'Online',
	'offline' => 'Offline',
	'warning' => 'Warning',
	'confirm' => 'Confirm',
	'electronic' => 'Electronic File',
	'physical' => 'Physical File',
	'open' => 'OPEN',
	'closed' => 'CLOSED',
	'required' => 'Required Fields',
	'departments' => 'Departments',
	'officers' => 'Officers',
	'officer' => 'Officer',
	'file' => 'File',
	'new' => 'New',
	'folder' => 'Folder',
	'download' => 'Download',
	'notification' => [
	    'title' => 'Notification',
	    'see_all' => 'See all notifications',
	    'enable' => 'Enable Notifications',
	],
	'setting' => [
	    'general' => 'General Settings'
	],
	'message' => 'Message',
	'others' => 'and others',
	'pending' => 'Pending',
	'progress' => 'In progress',
	'success' => 'Success',
	'fail' => 'Fail',
	'untracked' => 'Not yet tracked',
	'more' => 'View more',
    ],
    'frontend' => [

	'auth' => [
	    'login_box_title' => 'Sign in to your account',
	    'login_box_helper' => 'Please enter your name and password to log in.',
	    'login_button' => 'Login',
	    'login_with' => 'Login with :social_media',
	    'register_box_title' => 'Register',
	    'register_button' => 'Register',
	    'remember_me' => 'Keep me signed in',
	    'new_account' => 'Don\'t have an account yet?',
	    'create_account' => 'Create an account',
	    'help' => 'Help?',
	    'sign_up_title' => 'Signup',
	    'sign_up_helper' => "Enter your personal details below, fields marked with * are mandatory",
	    'terms' => 'I agree to the Terms of Service and Privacy Policy',
	    'has_account' => 'Already have an account?',
	    'goto_login' => 'Go to login',
	    'forgot_password_helper' => 'Enter your e-mail address below to reset your password.',
	    'forgot_password' => 'Forget Password?',
	],
	'passwords' => [
	    'forgot_password' => 'I forgot my password?',
	    'reset_password_box_title' => 'Reset Password',
	    'reset_password_button' => 'Reset Password',
	    'send_password_reset_link_button' => 'Send Password Reset Link',
	],
	'setup' => [
	    'setup_title' => 'APPLICATION SETUP',
	    'setup_helper' => 'Configure your application in the form below.',
	    'pg_connection' => 'Postgres Database Connection',
	    'is_new_instalation' => 'If this is the new installation',
	    'dbupdate' => 'In this configuration set up, application will backup the current database, clear all the data and create a new super user ready for the newly application usage.',
	    'dbnew' => 'In this application set up, application will create the new database, add the system metadata and create a new super user ready for the newly application usage.',
	    'super_properties' => 'New Super User Properties ',
	    'current_super_properties' => 'Current Super User Properties ',
	],
    ],
    'backend' => [
	'page_header' => [
	    'search_file' => 'SEARCH FILE',
	],
	'data_register' => [
	    'title' => 'Data Register',
	],
	'access_manager' => [
	    'title' => 'Access Manager',
	],
	'file_register' => [
	    'title' => 'File Register',
	    'create_file' => 'Create File',
	    'create_bulk' => 'Physical file bulk import',
	    'all' => 'All Registered',
	    'edit' => 'Edit File',
	    'print_barcode' => 'Print Barcode',
	    'bulk_register' => 'Bulk Register',
	    'no_departments' => 'No departments',
	    'show' => 'File Dashboard',
	    'history' => 'File History',
	    'document' => [
		'show' => 'Document Dashboard',
		'edit' => 'Edit Document',
		'space' => 'Used dick space',
		'title' => 'Document',
		'change_status' => 'Update Status',
		'new' => 'Upload new document version',
	    ],
	    'search' => 'Search',
	],
	'file' => [
	    'open_date' => 'Open Date',
	    'officer_opened' => 'Officer Opened',
	    'category' => 'File Category',
	    'category_helper' => 'Normal: file can only be sent to one user, Notice: file can be sent to multiple users',
	    'type' => 'File Type',
	    'nature' => 'File Nature',
	    'reference' => 'Reference Number',
	    'status' => 'Status',
	    'shelf_number' => 'Shelf Number',
	    'epc' => 'EPC',
	    'normal_file' => 'Normal files',
	    'notice_file' => 'Notice files',
	    'epc_helper' => 'Electronic Product Code for a tag attached to a file used for identification by RFID antenna fixed at entry points',
	    'created_by' => 'Created by',
	    'created' => 'Created',
	    'subject' => 'File Subject',
	    'folder' => 'Folders',
	    'document' => 'Documents',
	    'table' => [
		'name' => 'Name',
		'status' => 'Status',
		'priority' => 'Priority',
		'date' => 'Date',
		'user' => 'User',
		'comment' => 'Comment',
		'file' => 'File',
		'size' => 'Size',
	    ],
	    'fast_upload' => 'Fast Upload',
	    'drop_here' => 'Drop documents here!',
	    'library' => 'Document Library',
	    'name' => 'File Name',
	    'option' => 'File Option',
	    'register_log' => 'File Register Log',
	    'change_log' => 'File Change Log',
	    'status_log' => 'File Status Log',
	    'add_subfolder' => 'Add Subfolder',
	    'edit_folder' => 'Edit Folder',
	    'delete_folder' => 'Delete Folder',
	    'add_document' => 'Add Document',
	    'add_comment' => 'Add Comment',
	    'forward' => 'Forward File',
	    'history' => 'File History',
	    'version' => 'Version',
	    'directory' => 'Directory',
	    'document_info' => 'Document Information',
	    'current_version' => 'Current Version',
	    'prev_version' => 'Previous Version',
	    'timeline' => 'Timeline',
	    'description' => 'Description',
	    'dispatch' => [
		'file' => 'Dispatch file',
		'to' => 'Dispatched to',
	    ],
	    'log' => 'File Log',
	    'state' => [
		'title' => 'State',
		'disposed' => 'Released',
		'picked' => 'Picked up by',
		'pending' => 'Pending receipt to',
	    ],
	    'request' => 'Request',
	    'request_active' => 'Active File Request(s)',
	    'location' => 'Last Tracked Location',
	    'upload_new' => 'New file Import',
	    'recent_import' => 'Recent Imported',
	    'instruction' => 'Important Instructions',
	    'format' => [
		'file' => 'Supported File Format',
		'file_helper' => 'If you have other than csv file e.g Excel file, you need to change it by saving it as comma separated values file in document save options.',
		'content' => 'File Content Format',
		'content_helper' => 'The first row of the file should contain names indicating contents available in their column.',
		'column' => 'Supported columns : ',
		'example' => 'Example',
		'example_helper' => 'File with contents to be uploaded',
	    ],
	    'notice' => 'Notice',
	],
	'file_process' => [
	    'title' => 'File Process',
	    'monitoring' => 'Live Monitoring',
	    'search_file' => 'Advanced Search',
	    'file_list' => 'My file List',
	    'transfer_file' => 'Dispatch File',
	    'transfer_document' => 'Transfer Document',
	    'pick_file' => 'Pick File',
	    'pick_forced' => 'Forced Pick File',
	    'dispose_file' => 'Dispatch File',
	    'request' => [
		'make' => 'Request File',
		'cancel' => 'Cancel Request',
	    ],
	    'release' => 'Release file',
	    'receive' => 'Receive',
	    'pick_reason' => 'Choose reason for picking this file',
	    'case' => [
		'title' => 'File Case',
		'add' => 'Click here to add a new file case',
		'select' => 'Please select file case to proceed',
		'current' => 'Current file case : ',
		'edit' => 'Edit',
	    ],
	    'barcodes' => [
		'title' => 'Print Barcodes',
		'add' => 'Add File',
		'file' => 'File Name (Reference Number)',
		'quantity' => 'Quantity',
		'style' => 'Style',
		'style_helper' => 'Please don\'t forget to set correct page size and margin for your printer. You can set right and bottom to 0 while left and top margin can be adjusted according to your need.',
		'40_per_sheet' => '40 per sheet (a4) (1.799" x 1.003")',
		'30_per_sheet' => '30 per sheet (2.625" x 1")',
		'24_per_sheet' => '24 per sheet (a4) (2.48" x 1.334")',
		'20_per_sheet' => '20 per sheet (4" x 1")',
		'18_per_sheet' => '18 per sheet (a4) (2.5" x 1.835")',
		'14_per_sheet' => '14 per sheet (4" x 1.33")',
		'12_per_sheet' => '12 per sheet (a4) (2.5" x 2.834")',
		'10_per_sheet' => '10 per sheet (4" x 2")',
		'continuous_feed' => 'Continuous feed',
		'width' => 'Width',
		'height' => 'Height',
		'inches' => 'Inches',
		'potrait' => 'Potrait',
		'landscape' => 'Landscape',
		'print' => 'Print',
		'file_name' => 'File Name',
		'reference' => 'Reference',
		'no_file_found' => 'No file found',
		'no_file_selected' => 'No file selected',
		'unexpected_value' => 'Unexpected value, not valid input',
	    ],
	    'myfiles' => 'My Files',
	    'received' => 'Received Files',
	    'requests' => 'File Requests',
	    'myfiles_helper' => 'Files current at your desk',
	    'received_helper' => 'Files dispatched to your desk, but yet to be acknowledged in the system',
	    'requests_helper' => 'Files at your desk requested by other officer',
	    'log' => [
		'recent' => 'Recent Log : ',
	    ],
	    'graph' => [
		'overview' => 'General files statistics in last 10 days',
		'desk' => 'Files at my desk',
		'dispatched' => 'Files Dispatched/Released',
	    ],
	],
	'report' => [
	    'title' => 'Report',
	    'summary_report' => 'Summary Report',
	    'file_detail_report' => 'File Detailed Report',
	    'pending_clear_file' => 'Pending & Clear files',
	    'detailed_file_movement' => 'Detailed File Movement',
	    'pendency_summary' => 'Pendency Summary',
	    'file_receipt_register' => 'File Receipt Register',
	    'file_dispatch_register' => 'File Dispatch Register',
	    'list_of_file' => 'List of File(s)',
	    'pending_file_register' => 'Pending File Register',
	    'department_list' => 'Department List',
	    'basic' => [
		'title' => 'Basic Info',
		'notice' => 'Basic Info (Notice File)',
		'file' => 'File',
		'reference' => 'Reference',
		'desk' => 'At the desk of',
		'days' => 'Period on desk',
		'days_notice' => 'Period since picked',
		'state' => 'State',
		'status' => 'Status',
		'subject' => 'Subject',
		'created' => 'Created At',
	    ],
	    'user' => [
		'title' => 'By User',
		'officer' => 'Officer',
		'file' => 'File',
		'reference' => 'Reference',
		'received' => 'Pick Date',
		'released' => 'Released Date',
		'period' => 'Period on desk',
		'receiver' => 'Dispatched to',
		'case' => 'File Case',
		'desk' => 'Current at desk of',
	    ],
	    'closed' => [
		'title' => 'Closed Files',
		'file' => 'File',
		'reference' => 'Reference',
		'shelf' => 'Shelf Number',
		'subject' => 'Subject',
		'created' => 'Created At',
		'officer_opened' => 'Officer Opened',
		'closed' => 'Closed At',
		'officer_closed' => 'Officer Closed',
	    ],
	    'tracking' => [
		'title' => 'Tracking (RFID Read)',
		'file' => 'File',
		'reference' => 'Reference',
		'zone' => 'Zone',
		'last_read' => 'Last Read time',
		'duration' => 'Duration',
	    ],
	],
	'office_suite' => [
	    'new_note' => 'Add new note',
	    'read_note' => 'Read all notes',
	    'new_event' => 'Add new event',
	],
	'access' => [
	    'roles' => [
		'create' => 'Create Role',
		'edit' => 'Edit Role',
		'management' => 'Role Management',
		'table' => [
		    'number_of_users' => 'Number of Users',
		    'permissions' => 'Permissions',
		    'role' => 'Role',
		    'sort' => 'Sort',
		    'total' => 'role total|roles total',
		],
	    ],
	    'permissions' => [
		'grouped_permissions' => 'Grouped Permissions',
		'no_groups' => 'No groups',
		'dependencies' => 'Dependency',
	    ],
	    'users' => [
		'active' => 'Active Users',
		'all_permissions' => 'All Permissions',
		'change_password' => 'Change Password',
		'change_password_for' => 'Change Password for :user',
		'create' => 'Create User',
		'deactivated' => 'Deactivated Users',
		'deleted' => 'Deleted Users',
		'edit' => 'Edit User',
		'management' => 'User Management',
		'no_permissions' => 'No Permissions',
		'no_roles' => 'No Roles to set.',
		'permissions' => 'Permissions',
		'permission_check' => 'Checking a permission will also check its dependencies, if any.',
		'dependencies' => 'Dependencies',
		'table' => [
		    'firstname' => 'Name',
		    'othernames' => 'Othername',
		    'username' => 'Username',
		    'department' => 'Department',
		    'branch_name' => 'Branch',
		    'region' => 'Region',
		    'confirmed' => 'Confirmed',
		    'available' => 'Available',
		    'roles' => 'Roles',
		    'no_deleted' => 'No Deleted Users',
		    'history' => 'History',
		    'total' => 'user total|users total',
		    'log' => 'Log',
		],
		'profile' => [
		    'management' => 'User Profile',
		    'active' => 'user profile page',
		    'username' => 'Username',
		    'email' => 'Email',
		    'gender' => 'Gender',
		    'phone' => 'Phone',
		    'last' => 'Last logged in',
		    'role' => 'Roles',
		    'department' => 'Department',
		    'firstname' => 'Firstname',
		    'othernames' => 'Othername(s)',
		    'pic' => 'Image Upload',
		    'general' => 'General Information',
		    'additional' => 'Addition Information',
		    'edit' => 'Edit Account',
		    'overview' => 'Overview',
		    'info' => 'My Info',
		    'admin' => 'Administrator',
		    'password' => [
			'title' => 'Change Password',
			'create' => 'Create a new Password'
		    ],
		],
	    ],
	],
	'sysdef' => [
	    'location' => [
		'country' => [
		    'management' => 'Country Register',
		    'active' => 'All Registered',
		    'create' => 'Register Country',
		    'edit' => 'Edit Country',
		    'table' => [
			'name' => 'Country',
			'number_of_regions' => 'Number of Regions',
		    ],
		],
		'region' => [
		    'management' => 'Region Register',
		    'active' => 'All Registered',
		    'create' => 'Register Region',
		    'edit' => 'Edit Region',
		    'table' => [
			'name' => 'Region',
			'country' => 'Country',
			'number_of_branches' => 'Number of Branches'
		    ],
		],
		'branch' => [
		    'management' => 'Branch Register',
		    'active' => 'All Registered',
		    'create' => 'Register Branch',
		    'edit' => 'Edit Branch',
		    'table' => [
			'name' => 'Branch',
			'region' => 'Region',
			'country' => 'Country',
			'number_of_departments' => 'Number of Departments'
		    ],
		],
		'department' => [
		    'management' => 'Department Register',
		    'active' => 'All Registered',
		    'create' => 'Register Department',
		    'edit' => 'Edit Department',
		    'table' => [
			'name' => 'Department',
			'branch' => 'Branch',
			'region' => 'Region',
			'country' => 'Country',
			'number_of_users' => 'Number of Users'
		    ],
		],
	    ],
	    'rfid' => [
		'zone' => [
		    'management' => 'RFID Zones Register',
		    'active' => 'All Registered',
		    'create' => 'Register Zone',
		    'edit' => 'Edit Zone',
		    'table' => [
			'name' => 'Name',
			'reader' => 'Reader',
			'antenna' => 'Antenna',
			'departments' => 'Departments'
		    ],
		],
	    ],
	    'file' => [
		'subject' => [
		    'management' => 'File Subject Register',
		    'active' => 'All Registered',
		    'create' => 'Register Subject',
		    'edit' => 'Edit Subject',
		    'table' => [
			'name' => 'Subject',
			'number_of_cases' => 'Number of Cases',
			'number_of_files' => 'Number of files',
		    ],
		],
		'case' => [
		    'management' => 'File Case Register',
		    'active' => 'All Registered',
		    'create' => 'Register Case',
		    'edit' => 'Edit Case',
		    'table' => [
			'name' => 'Case',
			'subject' => 'Subject',
		    ],
		],
	    ],
	],
    ],
];
