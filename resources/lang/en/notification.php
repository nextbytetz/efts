<?php

return [
    'backend' => [
	'none' => 'There is no Notification',
	'count' => 'You have :count notifications',
	'document' => [
	    'status_updated' => 'updated status for document',
	],
        'file' => [
            'dispatched' => 'has dipatched file to you, ',
	    'register' => [
		'updated' => 'has updated the file register,',
	    ],
        ],
    ],
];