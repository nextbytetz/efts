<?php

return [

    /*
      |--------------------------------------------------------------------------
      | History Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain strings associated to the
      | system adding lines to the history table.
      |
     */

    'backend' => [
	'none' => 'There is no recent history.',
	'none_for_type' => 'There is no history for this type.',
	'none_for_entity' => "There is no history for this :entity.",
	'recent_history' => 'Recent History',
	'roles' => [
	    'created' => 'created role',
	    'deleted' => 'deleted role',
	    'updated' => 'updated role',
	],
	'users' => [
	    'changed_password' => 'changed password for user',
	    'deactivated' => 'deactivated user',
	    'edited' => 'edited user',
	    'reactivated' => 'reactivated user',
	    'approved' => 'approved user',
	    'activated' => 'activated user',
	    'deactivated' => 'dectivated user',
	    'out' => 'marked out of office user',
	    'in' => 'marked in office user',
	    'changed_password' => 'changed password for user',
	    'deleted' => 'deleted user',
	],
	'roles' => [
	    'created' => 'created role',
	    'edited' => 'edited role',
	    'deleted' => 'deleted role',
	],
	'file_register' => [
	    'created' => 'created file',
	    'updated' => 'updated file',
	    'deleted' => 'deleted a file',
	    'folder' => [
		'created' => 'created folder',
		'renamed' => 'renamed folder',
		'deleted' => 'deleted folder',
	    ],
	    'bulk' => [
		'uploaded' => 'imported new bulk file registration named ',
		'success' => 'successfully imported new bulk file registration named',
	    ],
	],
	'document' => [
	    'deleted' => 'deleted a document',
	],
    ],
];
