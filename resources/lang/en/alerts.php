<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Alert Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain alert messages for various scenarios
      | during CRUD operations. You are free to modify these language lines
      | according to your application's requirements.
      |
     */

    'backend' => [
	'permissions' => [
	    'created' => 'Permission successfully created.',
	    'deleted' => 'Permission successfully deleted.',
	    'groups' => [
		'created' => 'Permission group successfully created.',
		'deleted' => 'Permission group successfully deleted.',
		'updated' => 'Permission group successfully updated.',
	    ],
	    'updated' => 'Permission successfully updated.',
	],
	'roles' => [
	    'created' => 'The role was successfully created.',
	    'deleted' => 'The role was successfully deleted.',
	    'updated' => 'The role was successfully updated.',
	],
	'users' => [
	    'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
	    'created' => 'The user was successfully created.',
	    'deleted' => 'The user was successfully deleted.',
	    'deleted_permanently' => 'The user was deleted permanently.',
	    'restored' => 'The user was successfully restored.',
	    'updated' => 'The user was successfully updated.',
	    'updated_password' => "Success, Password Updated",
	    'approved' => 'Success, User have been approved',
	    'changed_status' => 'Success, user status has been updated',
	    'group' => [
		'approved' => 'Success, selected users have been approved',
		'deleted' => 'Success, selected users have been deleted',
	    ],
	    'profile' => [
		'updated' => 'Success, Profile has been updated',
		'password' => 'Success, Your password has been changed',
	    ],
	],
	'sysdef' => [
	    'location' => [
		'country' => [
		    'created' => 'Success, Country added',
		    'deleted' => 'Success, Country deleted',
		],
		'region' => [
		    'created' => 'Success, Region(s) added',
		    'deleted' => 'Success, Region deleted',
		    'updated' => 'Success, Region updated',
		],
		'branch' => [
		    'created' => 'Success, Branch added',
		    'deleted' => 'Success, Branch deleted',
		    'updated' => 'Success, Branch updated',
		],
		'department' => [
		    'created' => 'Success, Department(s) added',
		    'deleted' => 'Success, Department deleted',
		    'updated' => 'Success, Department updated',
		],
	    ],
	    'rfid' => [
		'zone' => [
		    'created' => 'Success, RFID zone added',
		    'updated' => 'Success, RFID zone updated',
		    'deleted' => 'Success, RFID zone deleted',
		],
	    ],
	    'file' => [
		'subject' => [
		    'created' => 'Success, File subject(s) added',
		    'updated' => 'Success, file subject has been updated',
		    'deleted' => 'Success, file subject has been deleted',
		],
		'case' => [
		    'created' => 'Success, file subject case has been added',
		    'updated' => 'Success, file subject case has been updated',
		    'deleted' => 'Success, file subject case has been deleted',
		],		
	    ],
	],
	'file_register' => [
	    'invalid_create' => 'Invalid create file request',
	    'created' => 'Success, File added',
	    'updated' => 'Success, File Updated',
	    'no_departments' => 'You need to select departments which this file will be accessible|You need to select departments which these files will be accessible',
	    'no_officers' => 'You need to select officers who will be able to access this file|You need to select officers who will be able to access these files',
	    'no_duplicate' => 'No information duplication is allowed, use Cut option instead to move the file(s)',
	    'bulk_uploaded' => 'Success, excel file has been uploaded and scheduled to register file(s) in background. Upon completion of registering all files from the uploaded document you will receive notification. For more details and status about this upload can be viewed in Recent Imported tab on this page. If you don\'t see a success message you will need to correct the mistakes from the file and upload again.',
	    'deleted' => 'Success, File deleted',
	],
	'file_process' => [
	    'picked' => 'Success, you have picked the file. To release the file click on release option',
	    'released' => 'Success, you have released the file :name',
	    'dispatched' => 'Success, you have dispatched the file :name',
	    'requested' => 'Success, your request has been created for file :name',
	    'requested_cancelled' => 'Success, your request for the file :name has been cancelled',
	    'received' => 'Success, you have received the file :name',
	],
	'document' => [
	    'status' => [
		'updated' => 'Success, Document status updated.',
		'deleted' => 'Success, Document status has been deleted.'
	    ],
	    'deleted' => 'Success, Document deleted',
	    'updated' => 'Success, Document updated',
	],
    ],
];
