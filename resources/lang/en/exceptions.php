<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Exception Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in Exceptions thrown throughout the system.
      | Regardless where it is placed, a button can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
	'access' => [
	    'permissions' => [
		'create_error' => 'There was a problem creating this permission. Please try again.',
		'delete_error' => 'There was a problem deleting this permission. Please try again.',
		'groups' => [
		    'associated_permissions' => 'You can not delete this group because it has associated permissions.',
		    'has_children' => 'You can not delete this group because it has child groups.',
		    'name_taken' => 'There is already a group with that name',
		],
		'not_found' => 'That permission does not exist.',
		'system_delete_error' => 'You can not delete a system permission.',
		'update_error' => 'There was a problem updating this permission. Please try again.',
	    ],
	    'roles' => [
		'already_exists' => 'That role already exists. Please choose a different name.',
		'cant_delete_admin' => 'You can not delete the Administrator role.',
		'create_error' => 'There was a problem creating this role. Please try again.',
		'delete_error' => 'There was a problem deleting this role. Please try again.',
		'has_users' => 'You can not delete a role with associated users.',
		'needs_permission' => 'You must select at least one permission for this role.',
		'not_found' => 'That role does not exist.',
		'update_error' => 'There was a problem updating this role. Please try again.',
	    ],
	    'users' => [
		'cant_deactivate_self' => 'You can not do that to yourself.',
		'cant_delete_self' => 'You can not delete yourself.',
		'create_error' => 'There was a problem creating this user. Please try again.',
		'delete_error' => 'There was a problem deleting this user. Please try again.',
		'email_error' => 'That email address belongs to a different user.',
		'current_password_error' => 'The current password is not corrent',
		'username_error' => 'That username belongs to a different user.',
		'phone_error' => 'That phone number belongs to a different user.',
		'mark_error' => 'There was a problem updating this user. Please try again.',
		'not_found' => 'That user does not exist.',
		'restore_error' => 'There was a problem restoring this user. Please try again.',
		'role_needed_create' => 'You must choose at lease one role. User has been created but deactivated.',
		'role_needed' => 'You must choose at least one role.',
		'update_error' => 'There was a problem updating this user. Please try again.',
		'update_password_error' => 'There was a problem changing this users password. Please try again.',
	    ],
	],
	'sysdef' => [
	    'location' => [
		'country' => [
		    'not_found' => 'That country does not exist',
		    'has_regions' => 'You can not delete a country with associated regions',
		],
		'region' => [
		    'not_found' => 'That region does not exist',
		    'has_branches' => 'You can not delete a region with associated branches',
		    'exist' => 'Region name is already taken',
		],
		'branch' => [
		    'not_found' => 'That branch does not exist',
		    'has_department' => 'You can not delete a branch with associated departments',
		    'exist' => 'Branch name is already taken',
		],
		'department' => [
		    'not_found' => 'That department does not exist',
		    'has_users' => 'You can not delete a department with associated users',
		    'has_files' => 'You can not delete a department with associated files',
		    'exist' => 'Department name is already taken',
		],
	    ],
	    'rfid' => [
		'zone' => [
		    'not_found' => 'That zone does not exist',
		    'exist' => 'Zone name is already taken',
		    'reader_exist' => 'Reader with the given port number is already registered',
		],
	    ],
	    'file' => [
		'subject' => [
		    'exist' => 'File subject name is already taken',
		    'has_cases' => 'You can not delete a subject with associated cases',
		],
		'case' => [
		    'exist' => 'File Case name is already taken',
		],
	    ],
	],
	'file' => [
	    'not_found' => 'The file requested does not exist or not available for you',
	    'exist' => 'File with the name :name already exist',
            'not_picked' => 'File not yet picked, you need to pick the file before working on it. Click the following link for more info :link',
	],
	'document' => [
	    'not_found' => 'That document does not exist',
	    'exist' => 'Document with the name :name already exist',
	],
	'general' => [
	    'error' => 'There was the problem on performing the requested action, please try again',
	],
    ],
    'frontend' => [
	'auth' => [
	    'confirmation' => 'Dear :firstname your account need to be verified, please contact your company system administrator to verify and activate your account.',
	    'deactivated' => 'Sorry, your account has been deactivated.',
	    'token_mismatch' => 'Sorry, we could not verify your request. Please try again or refresh your browser.',
	    'session_expired' => 'Session expired, please log in to cotinue',
	    'email_taken' => 'That e-mail address is already taken.',
	    'created' => 'Sucess, your account has been registered. You will be able to login once your account has been activated by an administrator.',
	    'password' => [
		'change_mismatch' => 'That is not your old password.',
	    ],
	],
	'invalid_request' => 'Invalid page request',
	'config_app_message' => 'Application backend has not been properly configured, please proceed to set up your application in the form below.',
	'setup' => [
	    'missing_command' => "<b>Note</b> ; Make sure that Postgres Database has been installed and the following commands <b>psql , pg_dump, pg_restore</b> location directory is included in the enironmental variable PATH of your operating system or are found in one of the following directories \n\n <b> :currpath </b>",
	    'incompatible_os' => "Set up can not be done on this device ; OS Vers. :os",
	    'setup_success' => 'Success, application has been configured properly',
	    'invalid_super_pass' => 'Invalid password for the current super user',
	],
    ],
    'general' => [
	'error' => 'Please correct the errors below.',
    ],
];
