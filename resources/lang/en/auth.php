<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Authentication Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */

    'failed' => 'Login Failure! Incorrect username or password. Type the correct username and password, and try again.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'general_error' => 'Insufficient permission to perform the requested action',
];
