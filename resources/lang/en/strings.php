<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Strings Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in strings throughout the system.
      | Regardless where it is placed, a string can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
	'access' => [
	    'permissions' => [
		'edit_explanation' => 'If you performed operations in the hierarchy section without refreshing this page, you will need to refresh to reflect the changes here.',
		'groups' => [
		    'hierarchy_saved' => 'Hierarchy successfully saved.',
		],
		'sort_explanation' => 'This section allows you to organize your permissions into groups to stay organized. Regardless of the group, the permissions are still individually assigned to each role.',
	    ],
	    'users' => [
		'delete_user_confirm' => 'Are you sure you want to delete this user permanently? Anywhere in the application that references this user\'s id will most likely error. Proceed at your own risk. This can not be un-done.',
		'if_confirmed_off' => '(If confirmed is off)',
		'restore_user_confirm' => 'Restore this user to its original state?',
                'group' => [
                    'delete' => 'Are you sure you want to delete all selected user(s)',
                    'approve' => 'Are you sure you want to approve all seleted user(s)',
                ],
	    ],
	],
	'dashboard' => [
	    'title' => 'Administrative Dashboard',
	    'welcome' => 'Welcome',
	],
	'general' => [
	    'all_rights_reserved' => 'All Rights Reserved.',
	    'are_you_sure' => 'Are you sure?',
	    'boilerplate_link' => 'Laravel 5 Boilerplate',
	    'continue' => 'Continue',
	    'member_since' => 'Member since',
	    'search_placeholder' => 'Search...',
	    'see_all' => [
		'messages' => 'See all messages',
		'notifications' => 'View all',
		'tasks' => 'View all tasks',
	    ],
	    'status' => [
		'online' => 'Online',
		'offline' => 'Offline',
	    ],
	    'you_have' => [
		'messages' => '{0} You don\'t have messages|{1} You have 1 message|[2,Inf] You have :number messages',
		'notifications' => '{0} You don\'t have notifications|{1} You have 1 notification|[2,Inf] You have :number notifications',
		'tasks' => '{0} You don\'t have tasks|{1} You have 1 task|[2,Inf] You have :number tasks',
	    ],
	    'delete_message' => 'Are you sure you want to delete this item?',
	],
	'file_register' => [
	    'electronic' => 'File and it\'s associated documents  will be stored in the digital format. Later submission, transfer, workflow and accessing of structured digital documents or forms will be done via computer system only. Electronic file can not be touched but can be printed onto a paper to produce a physical file.',
	    'physical' => 'File and it\'s associated documents exists and can be touched, the records of this file is also stored in the computer/electronic system for easy submission, transfer and accessing of records and workflow tracking. Storing copy of physical file records in electronic format help to recover the possible lost documents in the future.',
            'search' => [
                'text' => 'Search text',
            ],
	],
        'file_process' => [
            'forced_pick' => 'Are you sure you want to pick this file?, the current user holding the file might not have finished working on it. It is recommended to use this option when the user holding the file is completely out of office or not available to work on the file at the moment',
            'confirm_forward' => 'You are about to dispatch the file to the selected officer(s), please confirm?',
            'pick_warned' => 'You are not the target receipient for this file, Are you sure you want to pick this file instead of the targeted recipient?',
        ],
    ],
    'emails' => [
	'auth' => [
	    'password_reset_subject' => 'Your Password Reset Link',
	    'reset_password' => 'Click here to reset your password',
	],
    ],
    'frontend' => [
	'email' => [
	    'confirm_account' => 'Click here to confirm your account:',
	],
	'test' => 'Test',
	'tests' => [
	    'based_on' => [
		'permission' => 'Permission Based - ',
		'role' => 'Role Based - ',
	    ],
	    'js_injected_from_controller' => 'Javascript Injected from a Controller',
	    'using_blade_extensions' => 'Using Blade Extensions',
	    'using_access_helper' => [
		'array_permissions' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does have to possess all.',
		'array_permissions_not' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does not have to possess all.',
		'array_roles' => 'Using Access Helper with Array of Role Names or ID\'s where the user does have to possess all.',
		'array_roles_not' => 'Using Access Helper with Array of Role Names or ID\'s where the user does not have to possess all.',
		'permission_id' => 'Using Access Helper with Permission ID',
		'permission_name' => 'Using Access Helper with Permission Name',
		'role_id' => 'Using Access Helper with Role ID',
		'role_name' => 'Using Access Helper with Role Name',
	    ],
	    'view_console_it_works' => 'View console, you should see \'it works!\' which is coming from FrontendController@index',
	    'you_can_see_because' => 'You can see this because you have the role of \':role\'!',
	    'you_can_see_because_permission' => 'You can see this because you have the permission of \':permission\'!',
	],
	'user' => [
	    'profile_updated' => 'Profile successfully updated.',
	    'password_updated' => 'Password successfully updated.',
	],
	'welcome_to' => 'Welcome to :place',
    ],
    'meta_info' => [
	'description' => app_name() . ' application suite with registry automation (centralised file movement and file tracking automation) for private and public institution, office chat module',
    ],
];
