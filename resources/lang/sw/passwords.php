<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Nywila ni lazima iwe na herufi zisizopungua sita na lazima zifanane na nywila hakikisha.',
    'reset' => 'Nywila yako imedadilishwa!',
    'sent' => 'Tumekutumia barua pepe yenye kiungo tovuti cha kubadilisha nywila yako.',
    'token' => 'Nywila ishara hii si sahihi.',
    'user' => "Hakuna mtumiaji mfumo mwenye hii barua pepe.",

];
