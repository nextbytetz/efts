<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Menus Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in menu items throughout the system.
      | Regardless where it is placed, a menu item can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
	'access' => [
	    'title' => 'Upatikanaji Huduma',
	    'roles' => [
		'all' => 'Majukumu yote',
		'create' => 'Tengeneza jukumu',
		'edit' => 'Hariri jukumu',
		'management' => 'Usimamizi wa Majukumu',
		'main' => 'Majukumu',
	    ],
	    'users' => [
		'all' => 'Wote',
		'change_password' => 'Badilisha nywila',
		'deactivated' => 'Waliositishwa',
		'main' => 'Watumiaji',
		'out' => 'Walio nje ya ofisi',
		'replace_super' => 'Badilisha Super',
		'super_user_history' => 'Historia ya Super',
		'super_user' => 'Mtumiaji Super',
		'edit' => 'Hariri Mtumiaji',
		'profile' => 'Wasifu Wangu',
		'password' => 'Badili Nywila',
		'group' => [
		    'option' => 'Chaguo kundi',
		],
	    ],
	],
	'sysdef' => [
	    'location' => [
		'country' => [
		    'main' => 'Nchi',
		    'all' => 'Nchi zote',
		    'create' => 'Ongeza Nchi',
		],
		'region' => [
		    'main' => 'Mikoa',
		    'all' => 'Mikoa yote',
		    'create' => 'Ongeza Mkoa',
		],
		'branch' => [
		    'main' => 'Matawi',
		    'all' => 'Matawi yote',
		    'create' => 'Ongeza Tawi',
		],
		'department' => [
		    'main' => 'Idara',
		    'all' => 'Idara Zote',
		    'create' => 'Ongeza Idara',
		],
	    ],
	    'rfid' => [
		'main' => 'Kanda za RFID',
		'all' => 'Kanda Zote',
		'create' => 'Ongeza Kanda',
	    ],
	    'file' => [
		'subject' => [
		    'main' => 'Mada za Faili',
		    'all' => 'Mada zote',
		    'create' => 'Ongeza Mada ya Faili',
		],
		'case' => [
		    'main' => 'Kesi za Faili',
		    'all' => 'Kesi zote',
		    'create' => 'Ongeza kesi ya Faili',
		],
	    ],
	],
	'file_register' => [
	    'main' => 'Faili',
	    'title' => 'Usajili wa Faili',
	    'create_bulk' => 'Sajili Nyingi',
	    'all' => 'Mafaili Yote',
	],
	'file_process' => [
	],
    ],
    'language-picker' => [
	'language' => 'Lugha',
	/**
	 * Add the new language to this array.
	 * The key should have the same language code as the folder name.
	 * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
	 * Be sure to add the new language in alphabetical order.
	 */
	'langs' => [
	    'en' => 'Kiingereza',
	    'sw' => 'Kiswahili',
	],
    ],
];
