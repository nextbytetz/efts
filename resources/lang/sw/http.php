<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the views/errors files.
    |
    */

    '404' => [
        'title' => 'Ukurasa haipo',
        'description' => 'Samahani, ukurasa unaojaribu kuufungua haupo.',
    ],

    '503' => [
        'title' => 'Tutarudi.',
        'description' => 'Tutarudi',
    ],

];