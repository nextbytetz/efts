<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Alert Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain alert messages for various scenarios
      | during CRUD operations. You are free to modify these language lines
      | according to your application's requirements.
      |
     */

    'backend' => [
	'permissions' => [
	    'created' => 'Permission successfully created.',
	    'deleted' => 'Permission successfully deleted.',
	    'groups' => [
		'created' => 'Permission group successfully created.',
		'deleted' => 'Permission group successfully deleted.',
		'updated' => 'Permission group successfully updated.',
	    ],
	    'updated' => 'Permission successfully updated.',
	],
	'roles' => [
	    'created' => 'Jukumu limetengenezwa',
	    'deleted' => 'Jukumu limefutwa',
	    'updated' => 'Jukumu limesahihishwa',
	],
	'users' => [
	    'confirmation_email' => 'A new confirmation e-mail has been sent to the address on file.',
	    'created' => 'The user was successfully created.',
	    'deleted' => 'The user was successfully deleted.',
	    'deleted_permanently' => 'The user was deleted permanently.',
	    'restored' => 'The user was successfully restored.',
	    'updated' => 'Imefanikiwa kuhifadhi taharifa za mtumiaji.',
	    'updated_password' => "Imefanikiwa, Nywila imebadilishwa",
	    'approved' => 'Imefanikiwa, mtumiaji ameidhinishwa',
	    'changed_status' => 'Imefanikiwa, hali ya mtumiaji imedadilishwa.',
	    'group' => [
		'approved' => 'Imefanikiwa, atumiaji waliochaguliwa wamepitishwa',
		'deleted' => 'Imefanikiwa, watumiaji waliochaguliwa wamefutwa',
	    ],
	    'profile' => [
		'updated' => 'Imefanikiwa, wasifu wako umesahihishwa',
		'password' => 'Imefanikiwa, nywila yako imebadilishwa',
	    ],
	],
	'sysdef' => [
	    'location' => [
		'country' => [
		    'created' => 'Imefanikiwa, Nchi imeongezwa',
		    'deleted' => 'Imefanikiwa, Nchi imefutwa',
		],
		'region' => [
		    'created' => 'Imefanikiwa, Mkoa umeongezwa',
		    'deleted' => 'Imefanikiwa, Mkoa umefutwa',
		    'updated' => 'Imefanikiwa, Mkoa umesahihishwa',
		],
		'branch' => [
		    'created' => 'Imefanikiwa, Tawi limeongezwa',
		    'deleted' => 'Imefanikiwa, Tawi limefutwa',
		    'updated' => 'Imefanikiwa, Tawi limesahihishwa',
		],
		'department' => [
		    'created' => 'Imefanikiwa, Idara imeongezwa',
		    'deleted' => 'Imefanikiwa, Idara imefutwa',
		    'updated' => 'Imefanikiwa, Idara imesahihishwa',
		],
	    ],
	    'rfid' => [
		'zone' => [
		    'created' => 'Imefanikiwa, Ukanda wa RFID umeongezwa',
		    'updated' => 'Imefanikiwa, Ukanda wa RFID umesahihishwa',
		    'deleted' => 'Imefanikiwa, Ukanda wa RFID umefutwa',
		],
	    ],
	    'file' => [
		'subject' => [
		    'created' => 'Imefanikiwa, maada ya faili imeongezwa',
		    'updated' => 'Imefanikiwa, maada ya faili imesahihishwa',
		    'deleted' => 'Imefanikiwa, mada ya faili imefutwa',
		],
		'case' => [
		    'created' => 'Imefanikiwa, kesi ya faili imeongezwa',
		    'updated' => 'Imefanikiwa, kesi ya faili imesahihishwa',
		    'deleted' => 'Imefanikiwa, kesi ya faili imefutwa',
		],		
	    ],
	],
	'file_register' => [
	    'invalid_create' => 'Ombi si sahihi la kutengeneza faili',
	    'created' => 'Imefanikiwa, Faili limeongezwa',
	    'updated' => 'Imefanikiwa, Faili limesahihishwa',
	    'no_departments' => 'Unatakiwa kuchagua idara ambazo faili hili litapatikana.|Unatakiwa kuchagua idara ambazo faili hizi zitapatikana.',
	    'no_officers' => 'Unatakiwa kuchagua wafanyakazi ambao wataweza kutumia faili hili.|Unatakiwa kuchagua wafanyakazi ambao wataweza kutumia faili hizi.',
	    'no_duplicate' => 'Hairuhusiwi kurudia kuhifadhi taharifa',
	    'bulk_uploaded' => 'Imefanikiwa, faili limepakiwa na litaendelea kusajili mafaili. Likimaliza utapata taharifa. Kwa taharifa zaidi na hali ya faili lililopakiwa angalia katika kichupo cha Yaliyopakiwa hivi karibuni. Usipoona ujumbe wa kufanikiwa itakuhitaji kurekebisha makosa kwenye faili na kupakia upya.',
	    'deleted' => 'Imefanikiwa, Faili limefutwa',
	],
	'file_process' => [
	    'picked' => 'Imefanikiwa, umechukua faili. kuliachia faili bofya Achia faili',
	    'released' => 'Imefanikiwa, umeachia faili :name',
	    'dispatched' => 'Imefanikiwa, umetuma faili :name',
	    'requested' => 'Imefanikiwa, ombi lako limewekwa kwa faili :name',
	    'requested_cancelled' => 'Imefanikiwa, ombi lako kwa faili :name limefutwa',
	    'received' => 'Imefanikiwa, umepokea faili :name',
	],
	'document' => [
	    'status' => 'Imefanikiwa, Hali ya hati imehaririwa',
	    'status' => [
		'updated' => 'Imefanikiwa, Hali ya hati imehaririwa',
		'deleted' => 'Imefanikiwa, Hali ya hati imefutwa'
	    ],
	    'deleted' => 'Imefanikiwa, Hati imefutwa',
	    'updated' => 'Imefanikiwa, Hati imehaririwa',
	],
    ],
];
