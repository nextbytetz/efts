<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Strings Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in strings throughout the system.
      | Regardless where it is placed, a string can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
        'access' => [
            'permissions' => [
                'edit_explanation' => 'If you performed operations in the hierarchy section without refreshing this page, you will need to refresh to reflect the changes here.',
                'groups' => [
                    'hierarchy_saved' => 'Hierarchy successfully saved.',
                ],
                'sort_explanation' => 'This section allows you to organize your permissions into groups to stay organized. Regardless of the group, the permissions are still individually assigned to each role.',
            ],
            'users' => [
                'delete_user_confirm' => 'Are you sure you want to delete this user permanently? Anywhere in the application that references this user\'s id will most likely error. Proceed at your own risk. This can not be un-done.',
                'if_confirmed_off' => '(If confirmed is off)',
                'restore_user_confirm' => 'Restore this user to its original state?',
                'group' => [
                    'delete' => 'Tafadhali hakikisha kufuta taharifa zilizochaguliwa',
                    'approve' => 'Tafadhali hakikisha kupitisha taharifa zilizochaguliwa',
                ],
            ],
        ],
        'dashboard' => [
            'title' => 'Administrative Dashboard',
            'welcome' => 'Welcome',
        ],
        'general' => [
            'all_rights_reserved' => 'All Rights Reserved.',
            'are_you_sure' => 'Una uhakika?',
            'boilerplate_link' => 'Laravel 5 Boilerplate',
            'continue' => 'Continue',
            'member_since' => 'Member since',
            'search_placeholder' => 'Search...',
            'see_all' => [
                'messages' => 'See all messages',
                'notifications' => 'View all',
                'tasks' => 'View all tasks',
            ],
            'status' => [
                'online' => 'Online',
                'offline' => 'Offline',
            ],
            'you_have' => [
                'messages' => '{0} You don\'t have messages|{1} You have 1 message|[2,Inf] You have :number messages',
                'notifications' => '{0} You don\'t have notifications|{1} You have 1 notification|[2,Inf] You have :number notifications',
                'tasks' => '{0} You don\'t have tasks|{1} You have 1 task|[2,Inf] You have :number tasks',
            ],
            'delete_message' => 'Una uhakika kufuta taharifa hii?',
        ],
        'file_register' => [
            'electronic' => 'Faili na hati zake zitahifadhiwa katika mfumo wa kidijitali. Kuwasilisha, kutuma, mgawanyiko wa kazi na kupata hati za faili zitafanyika kupitia mifumo ya kompyuta tu. Hati za kidijitali haziwezi kuguswa lakini zaweza kuprintiwa kutengeneza faili mguso.',
            'physical' => 'Faili na hati zake tayari zipo na zaweza kuguswa, taharifa za faili hili pia zitahifadhiwa katika mfumo wa kidijitali kwa urahisi wa kuwasilisha, kutuma/kuhamisha, kupata hati za faili na mgawanyiko wa kazi. Kuhifadhi kopi ya faili mguso kwa njia ya kidijitali inasaidia kurejesha faili au hati ambayo huenda itapotea hapo baadae.',
            'search' => [
                'text' => 'Nakala ya utafutaji',
            ],
        ],
        'file_process' => [
            'forced_pick' => 'Una uhakika kuchukua faili hili?, mwenye faili kwa sasa yawezekana hajamaliza kulifanyia kazi. Inapendekezwa kutumia chaguo hili kama mwenye faili hayupo kabisa ofisini au hawezi kulifanyia kazi faili kwa sasa.',
            'confirm_forward' => 'Utatuma faili hili kwa maofisa waliochaguliwa, tafadhari thibitisha?',
            'pick_warned' => 'Faili halijatumwa kwako, hakikisha kwamba unachukua faili hili badala ya mtu aliyetumiwa?',
        ],
    ],
    'emails' => [
        'auth' => [
            'password_reset_subject' => 'Kiungo cha tovuti kwa ajili ya kubadilisha nywila yako.',
            'reset_password' => 'Bofya hapa kubadilisha nywila yako.',
        ],
    ],
    'frontend' => [
        'email' => [
            'confirm_account' => 'Click here to confirm your account:',
        ],
        'test' => 'Test',
        'tests' => [
            'based_on' => [
                'permission' => 'Permission Based - ',
                'role' => 'Role Based - ',
            ],
            'js_injected_from_controller' => 'Javascript Injected from a Controller',
            'using_blade_extensions' => 'Using Blade Extensions',
            'using_access_helper' => [
                'array_permissions' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does have to possess all.',
                'array_permissions_not' => 'Using Access Helper with Array of Permission Names or ID\'s where the user does not have to possess all.',
                'array_roles' => 'Using Access Helper with Array of Role Names or ID\'s where the user does have to possess all.',
                'array_roles_not' => 'Using Access Helper with Array of Role Names or ID\'s where the user does not have to possess all.',
                'permission_id' => 'Using Access Helper with Permission ID',
                'permission_name' => 'Using Access Helper with Permission Name',
                'role_id' => 'Using Access Helper with Role ID',
                'role_name' => 'Using Access Helper with Role Name',
            ],
            'view_console_it_works' => 'View console, you should see \'it works!\' which is coming from FrontendController@index',
            'you_can_see_because' => 'You can see this because you have the role of \':role\'!',
            'you_can_see_because_permission' => 'You can see this because you have the permission of \':permission\'!',
        ],
        'user' => [
            'profile_updated' => 'Profile successfully updated.',
            'password_updated' => 'Password successfully updated.',
        ],
        'welcome_to' => 'Welcome to :place',
    ],
    'meta_info' => [
        'description' => app_name() . ' application suite with registry automation (centralised file movement and file tracking automation) for private and public institution, office chat module',
    ],
];
