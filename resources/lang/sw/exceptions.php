<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Exception Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in Exceptions thrown throughout the system.
      | Regardless where it is placed, a button can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
	'access' => [
	    'permissions' => [
		'create_error' => 'There was a problem creating this permission. Please try again.',
		'delete_error' => 'There was a problem deleting this permission. Please try again.',
		'groups' => [
		    'associated_permissions' => 'You can not delete this group because it has associated permissions.',
		    'has_children' => 'You can not delete this group because it has child groups.',
		    'name_taken' => 'There is already a group with that name',
		],
		'not_found' => 'That permission does not exist.',
		'system_delete_error' => 'You can not delete a system permission.',
		'update_error' => 'There was a problem updating this permission. Please try again.',
	    ],
	    'roles' => [
		'already_exists' => 'Jukumu lenye jina hilo lipo, tafadhali chagua jingine',
		'cant_delete_admin' => 'You can not delete the Administrator role.',
		'create_error' => 'Kumetokea tatizo wakati inahifadhi jukumu, tafadhali jaribu tena.',
		'delete_error' => 'Kumetoka tatizo wakati wa kufuta jukumu hili, tafadhali jaribu tena.',
		'has_users' => 'Huwezi kufuta jukumu hili, lina watumiaji.',
		'needs_permission' => 'Unatakiwa kuchagua angalau idhini moja kwenye jukumu hili.',
		'not_found' => 'Jukumu hili halipatikani',
		'update_error' => 'Kumetokea tatizo wakati wa kusahihisha jukumu, tafadhali jaribu tena.',
	    ],
	    'users' => [
		'cant_deactivate_self' => 'You can not do that to yourself.',
		'cant_delete_self' => 'You can not delete yourself.',
		'create_error' => 'There was a problem creating this user. Please try again.',
		'delete_error' => 'Kumetokea tatizo wakati wa kumfuta mtumiaji huyu, tafadhali jaribu baadae.',
		'email_error' => 'Barua pepe hiyo inatumika na mtu mwingine.',
		'username_error' => 'Jina la kutumia uliloweka linatumika na mtu mwingine.',
		'current_password_error' => 'Nywila ya sasa si sahihi',
		'phone_error' => 'Namba ya simu hiyo inatumika na mtu mwingine.',
		'mark_error' => 'There was a problem updating this user. Please try again.',
		'not_found' => 'Mtumiaji huyu wa mfumo hayupo',
		'restore_error' => 'There was a problem restoring this user. Please try again.',
		'role_needed_create' => 'You must choose at lease one role. User has been created but deactivated.',
		'role_needed' => 'Inatakiwa kuchagua angalau jukumu moja',
		'update_error' => 'There was a problem updating this user. Please try again.',
		'update_password_error' => 'Kumetokea tatizo wakati inabadilisha nywila, tafadhali jaribu baadae.',
	    ],
	],
	'sysdef' => [
	    'location' => [
		'country' => [
		    'not_found' => 'Nchi hii haipo',
		    'has_regions' => 'Huwezi futa nchi hii ina mikoa iliyosajiliwa',
		],
		'region' => [
		    'not_found' => 'Mkoa huu haupo',
		    'has_branches' => 'Huwezi futa mkoa huu una matawi yaliyosajiliwa',
		    'exist' => 'Jina la mkoa linatumika',
		],
		'branch' => [
		    'not_found' => 'Tawi hili halipo',
		    'has_department' => 'Huwezi futa tawi hili lina idara zilizosajiliwa',
		    'exist' => 'Jina la tawi linatumika',
		],
		'department' => [
		    'not_found' => 'Idara hii haipo',
		    'has_users' => 'Huwezi futa idara hii ina watumiaji waliosajiliwa',
		    'has_files' => 'Huwezi futa idara hii ina mafaili yaliyosajiliwa',
		    'exist' => 'Jina la idara linatumika',
		],
	    ],
	    'rfid' => [
		'zone' => [
		    'not_found' => 'Kanda hiyo haipo',
		    'exist' => 'Jina la kanda linatumika',
		    'reader_exist' => 'Reader yenye namba hiyo ya port imeshasajiliwa',
		],
	    ],
	    'file' => [
		'subject' => [
		    'exist' => 'Jina la mada ya faili linatumika',
		    'has_cases' => 'Huwezi futa mada ya faili yenye kesi zilizosajiliwa',
		],
		'case' => [
		    'exist' => 'Jina la kesi ya faili linatumika',
		],
	    ],
	],
	'file' => [
	    'not_found' => 'Faili lililoombwa halipo au huna haki ya kulitumia',
	    'exist' => 'Faili lenye :name lipo',
	    'not_picked' => 'Hujachukua faili bado, inatakiwa kuchukua faili kabla ya kulifanyia kazi. Bofya kiungo tovuti kifuatacho kwa taharifa zaidi :link',
	],
	'document' => [
	    'not_found' => 'Hati hii haipo',
	    'exist' => 'Hati yenye jina :name ipo tayari',
	],
	'general' => [
	    'error' => 'Kumetokea tatizo, tafadhali jaribu tena.',
	],
    ],
    'frontend' => [
	'auth' => [
	    'confirmation' => 'Mpendwa :firstname, akaunti yako inahitaji kuhakikiwa, wasiliana na msimamizi wa mfumo ili ahakiki akaunti yako.',
	    'deactivated' => 'Samahani, akaunti yako imesitishwa',
	    'token_mismatch' => 'Samahani, imeshindwa kutambua ombi lako, tafadhari jaribu tena au fungua upya ukurasa huu.',
	    'session_expired' => 'Mda wa kuwepo kwenye mfumo uliisha, tafadhali logia tena',
	    'email_taken' => 'That e-mail address is already taken.',
	    'created' => 'Imefanikiwa, akaunti yako imetengenezwa. Utaweza kulogia baada ya akaunti yako kupitishwa na msimamizi mfumo.',
	    'password' => [
		'change_mismatch' => 'That is not your old password.',
	    ],
	],
	'invalid_request' => 'Ukurasa uliofunguliwa si sahihi',
	'config_app_message' => 'Mfumo haujasanidiwa ipasavyo, tafadhali sanidi mfumo kwenye form hapo chini.',
	'setup' => [
	    'missing_command' => "<b>Angalizo</b> ; Hakikisha hifadhidata ya Postgres imewekwa kwenye kompyuta hii na komandi hizi <b> psql , pg_dump, pg_restore </b> zipo kwenye saraka zifuatazo \n\n <b> :currpath </b>",
	    'incompatible_os' => "Huwezi kusanidi mfumo katika kifaa hichi ; OS Vers. :os",
	    'setup_success' => 'Imefanikiwa, mfumo umesanidi ipasavyo',
	    'invalid_super_pass' => 'Nywila ya msimamizi mkuu wa mfumo wa sasa si sahihi',
	],
    ],
    'general' => [
	'error' => 'Tafadhali rekebisha makosa hapo chini.',
    ],
];
