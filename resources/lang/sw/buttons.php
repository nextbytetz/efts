<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Buttons Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in buttons throughout the system.
      | Regardless where it is placed, a button can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'backend' => [
	'access' => [
	    'users' => [
		'activate' => 'Rudisha',
		'change_password' => 'Badilisha Nywila',
		'deactivate' => 'Sitisha',
		'delete_permanently' => 'Futa Milele',
		'resend_email' => 'Tuma tena barua pepe',
		'restore_user' => 'Mrudishe Mtumiaji',
		'out_office' => 'Nje ya Ofisi',
		'in_office' => 'In Office',
	    ],
	],
    ],
    'general' => [
	'cancel' => 'Ghairi',
	'confirm' => 'Ndio',
	'reset' => 'Weka Upya',
	'crud' => [
	    'create' => 'Tengeneza',
	    'delete' => 'Futa',
	    'edit' => 'Hariri',
	    'update' => 'Hifadhi',
	],
	'file' => [
	    'open' => 'Fungua faili',
	],
	'save' => 'Hifadhi',
	'view' => 'Fungua',
	'approve' => 'Idhinisha',
	'submit' => 'Wasilisha',
	'copy' => 'Weka kwenye kumbukumbu ya mda',
        'dispatch' => 'Hamisha Faili',
    ],
];
