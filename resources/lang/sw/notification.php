<?php

return [
    'backend' => [
	'none' => 'Hakuna taarifa yoyote',
	'count' => 'Una taarifa :count',
	'document' => [
	    'status_updated' => 'amehariri hali ya hati',
	],
	'file' => [
	    'dispatched' => 'ametuma faili kwako, ',
	    'register' => [
		'updated' => 'ameongeza maelezo kwenye faili,',
	    ],
	],
    ],
];
