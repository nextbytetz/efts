<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain the default error messages used by
      | the validator class. Some of these rules have multiple versions such
      | as the size rules. Feel free to tweak each of these messages here.
      |
     */

    'accepted' => ':attribute lazima ikubaliwe.',
    'active_url' => ':attribute sio kiungo cha tovuti halali.',
    'after' => ':attribute lazima iwe tarehe baada ya :date.',
    'alpha' => ':attribute ni lazima iwe na herufi tu.',
    'alpha_dash' => ':attribute inatakiwa iwe na herufi, namba na dashi tu.',
    'alpha_num' => ':attribute inatakiwa kuwa na herufi na namba tu.',
    'array' => ':attribute lazima iwe na mkusanyiko.',
    'before' => 'The :attribute must be a date before :date.',
    'between' => [
	'numeric' => ':attribute lazima iwe kati ya :min na :max.',
	'file' => ':attribute lazima iwe kati ya :min na :max kilobytes.',
	'string' => ':attribute lazima iwe na idadi ya herufi kati ya :min na :max.',
	'array' => ' :attribute lazima iwe na idadi kati ya :min na :max.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'Uhakiki wa :attribute haufanani.',
    'date' => ':attribute si tarehe halali.',
    'date_format' => ':attribute lazima iwe sawa na mpangilio huu :format.',
    'different' => ':attribute na :other lazima ziwe tofauti.',
    'digits' => ':attribute lazima iwe na idadi ya namba :digits.',
    'digits_between' => ':attribute lazima iwe na idadi ya namba kati ya :min na :max.',
    'distinct' => ':attribute imewekwa zaidi ya mara moja.',
    'email' => ':attribute lazima iwe barua pepe halali.',
    'exists' => ':attribute iliyochaguliwa si halali.',
    'filled' => ':attribute ni lazima kujaza.',
    'image' => ':attribute lazima iwe picha.',
    'in' => ':attribute liyochaguliwa si halali.',
    'in_array' => ':attribute haipo ndani ya :other.',
    'integer' => ':attribute lazima iwe namba.',
    'ip' => ':attribute si anuwani ya IP halali.',
    'json' => ':attribute lazima iwe ni mkusanyiko wa JSON halali.',
    'max' => [
	'numeric' => ':attribute lazima iwe namba zaidi ya :max.',
	'file' => ':attribute si lazima iwe na zaidi ya :max kilobytes.',
	'string' => ':attribute si lazima iwe zaidi ya herufi :max.',
	'array' => ':attribute si lazima iwe na idadi ya zaidi ya vitu :max.',
    ],
    'mimes' => ':attribute ni lazima iwe faili la aina ya: :values.',
    'min' => [
	'numeric' => ':attribute ni lazima iwe chini ya :min.',
	'file' => ':attribute ni lazima iwe chini ya :min kilobytes.',
	'string' => ':attribute ni lazima iwe na angalau herufi :min na kuendelea.',
	'array' => ':attribute ni lazima iwe na idadi ya vitu :min na kuendelea.',
    ],
    'not_in' => ':attribute iliyochaguliwa si halali.',
    'numeric' => ':attribute lazima iwe ni namba.',
    'present' => ':attribute lazima iwepo.',
    'regex' => 'mpangilio wa :attribute si sahihi.',
    'required' => ':attribute ni lazima kujaza',
    'required_if' => ':attribute inahitajika endapo :other ni :value.',
    'required_unless' => ':attribute inahitajika endapo :other haipo ndani ya :values.',
    'required_with' => ':attribute inahitajika endapo :values ipo.',
    'required_with_all' => ':attribute inahitajika endapo :values ipo.',
    'required_without' => ':attribute inahitajika endapo :values haipo.',
    'required_without_all' => ':attribute endapo hakuna hata moja kati ya :values ikiwemo.',
    'same' => ':attribute na :other hazifanani.',
    'size' => [
	'numeric' => ':attribute lazima iwe :size.',
	'file' => ':attribute lazima iwe na :size kilobytes.',
	'string' => ':attribute lazima iwe na herufi :size.',
	'array' => ':attribute lazima iwe na idadi ya vitu :size.',
    ],
    'string' => ':attribute lazima liwe neno au maneno.',
    'timezone' => ':attribute lazima iwe ukanda wa saa halali.',
    'unique' => ':attribute imekwishatumika na mtu mwingine.',
    'url' => 'Pangilio wa :attribute si halali.',
    "phone" => ":attribute haina namba halali.",
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Language Lines
      |--------------------------------------------------------------------------
      |
      | Here you may specify custom validation messages for attributes using the
      | convention "attribute.rule" to name the lines. This makes it quick to
      | specify a specific custom language line for a given attribute rule.
      |
     */
    'custom' => [
	'attribute-name' => [
	    'rule-name' => 'custom-message',
	],
    ],
    /*
      |--------------------------------------------------------------------------
      | Custom Validation Attributes
      |--------------------------------------------------------------------------
      |
      | The following language lines are used to swap attribute place-holders
      | with something more reader friendly such as E-Mail Address instead
      | of "email". This simply helps us make messages a little cleaner.
      |
     */
    'attributes' => [

	'backend' => [
	    'access' => [
		'permissions' => [
		    'associated_roles' => 'Majukumu yake',
		    'dependencies' => 'Dependencies',
		    'display_name' => 'Display Name',
		    'group' => 'Group',
		    'group_sort' => 'Group Sort',
		    'groups' => [
			'name' => 'Group Name',
		    ],
		    'name' => 'Name',
		    'system' => 'System?',
		],
		'roles' => [
		    'associated_permissions' => 'Idhini zake',
		    'name' => 'Jina',
		    'sort' => 'Sort',
		],
		'users' => [
		    'active' => 'Mtumiaji',
		    'associated_roles' => 'Majukumu yake',
		    'confirmed' => 'Amehakikiwa',
		    'available' => 'Yupo',
		    'email' => 'Barua Pepe',
		    'username' => 'Jina la kutumia',
		    'gender' => 'Jinsia',
		    'pic' => 'Pakia Picha',
		    'phone' => 'Namba ya Simu',
		    'firstname' => 'Jina la kwanza',
		    'othernames' => 'Majina mengine',
		    'other_permissions' => 'Idhini zingine',
		    'password' => 'Password',
		    'password_confirmation' => 'Password Confirmation',
		    'send_confirmation_email' => 'Send Confirmation E-mail',
		    'old_password' => 'Old Password',
		    'new_password' => 'New Password',
		    'password' => 'Nywila',
		    'password_confirmation' => 'Hakikisha nywila',
		],
	    ],
	    'sysdef' => [
		'location' => [
		    'country' => [
			'name' => 'Jina la Nchi'
		    ],
		    'region' => [
			'name' => 'Jina la Mkoa',
		    ],
		    'branch' => [
			'name' => 'Jina la Tawi',
		    ],
		    'department' => [
			'name' => 'Jina la Idara',
		    ],
		],
		'rfid' => [
		    'zone' => [
			'name' => 'Jina',
			'reader' => 'Reader',
			'antenna' => 'Namba ya Antenna Port',
			'departments' => 'Idara',
		    ],
		],
		'file' => [
		    'subject' => [
			'name' => 'Mada ya faili',
		    ],
		    'case' => [
			'name' => 'Kesi ya faili',
		    ],
		],
	    ],
	    'file' => [
		'nature_select' => 'Chagua asili ya faili',
		'name' => 'Jina la Faili',
		'reference' => 'Kumbukumbu Namba',
		'subject' => 'Mada ya Faili',
		'description' => 'Maelezo ya faili',
		'subject_helper' => 'Mada yoyote kuhusiana na faili kama ipo mfano Mikopo ya Nyumba, Maulizo ya Wateja, Faili la mfanakazi nk. Mada zilizokwisha kuwekwa zitaonekana kadri utakavyokuwa unaandika kukusaidia kuchagua|Mada yoyote kuhusiana na mafaili haya kama ipo mfano Mikopo ya Nyumba, Maulizo ya Wateja, Faili la mfanyakazi nk. Mada zilizokwisha kuwekwa zitaonekana kadri utakavyokuwa unaandika kukusaidia kuchagua',
		'description_helper' => 'Maelezo zaidi kuhusiana na faili kwa kuzingatia maada iliyowekwa kama yapo.',
		'category' => 'Jamii ya faili',
		'type' => 'Aina ya faili',
		'epc' => 'EPC',
		'shelf_number' => 'Namba ya Rafu',
		'shelf_number_helper' => 'Namba ya Rafu katika chumba cha kuhifadhia mafaili',
		'status' => 'Hali ya faili',
		'general' => 'Taharifa za Jumla',
		'accessibility' => 'Upatikanaji wa Faili',
		'epc_helper' => 'Electronic Product Code ya RFID tag itakayowekwa kwenye faili',
		'department' => 'Upatikanaji katika idara',
		'officer' => 'Upatikanaji wa Watumiaji',
		'officer_helper' => 'Tafadhari chagua idara ili uweze kumtafuta mfanyakazi husika.',
		'file_officer' => 'Wafanyakazi pekee waliopewa ruhusa kwenye faili hili wataonekana',
		'dispatch' => [
		    'officer' => 'Chagua Afisa',
		    'officer_helper' => 'Tafuta afisa wa kumtumia faili, kama jamii ya faili ni Normal utaweza kutuma faili kwa mtu mmoja na kama jamii ya faili ni Notice, utaweza kutuma faili kwa mtu zaidi ya mmoja. Afisa wote watakaopokea faili hili lazima wawe wameshapewa haki katika hili faili',
		],
		'storage' => [
		    'remove_error' => 'Haiwezi kufuta faili kwa namna hii',
		    'rename_error' => 'Haiwezi kubadilisha jina la faili kwa namna hii',
		    'name_error' => 'Jina batili : :name',
		    'path_error' => 'Folda hili :new tayari lipo',
		    'list_error' => 'Imeshindwa kufungua mafaili :dir',
		    'name_exist' => 'Folda :name lipo',
		    'path_exist' => 'Njia ya faili hili :path si sahihi',
		    'base_error' => 'Saraka ya mwanzo haipo',
		],
		'document' => [
		    'description' => 'Maelezo ya Hati (Kuweka rejea ya hati katika faili hili anza na alama @)',
		    'description_helper' => 'Elezea kuhusu yaliomo katika hati',
		    'comment' => 'Maoni / Mapitio',
		    'priority' => 'Kipaumbele',
		    'status' => 'Hali',
		    'notify' => 'Wataharifu watumiaji',
		],
		//'comment' => 'Maoni',
		//'comment_helper' => 'Weka maoni yako kuhusu faili kwa ajili ya afisa atakayepokea',
		'comment' => [
		    'main' => 'Maoni',
		    'title' => 'Weka maoni',
		    'helper' => 'Weka maoni yako kuhusu faili kwa ajili ya afisa atakayepokea',
		],
		'priority' => 'Kipaumbele',
		'csv' => 'Pakia faili excel la Microsoft xlsx, xls',
		'csv_helper' => 'Hakikisha taarifa zipo kwenye karatasi ya kwanza kwenye hati ya excel',
		'group_name' => 'Jina la kundi',
		'group_name_helper' => 'Kwa kuandika jina la kundi, itakupa uwezo wa kufanya mabadiliko yoyote kuhusiana na taharifa hizi mda ujao',
	    ],
	],
	'frontend' => [
	    'username' => 'Jina',
	    'firstname' => 'Jina la kwanza',
	    'password' => 'Nywila',
	    'othernames' => 'Majina mengine',
	    'region' => 'Mkoa',
	    'select' => 'Chagua',
	    'department' => 'Idara',
	    'office_id' => 'Namba yako ya utambulisho ofisini',
	    'gender' => 'Jinsia',
	    'email' => 'Barua pepe',
	    'password_confirmation' => 'Hakikisha nywila',
	    'old_password' => 'Old Password',
	    'new_password' => 'New Password',
	    'new_password_confirmation' => 'New Password Confirmation',
	],
	'setup' => [
	    'db_host' => 'Database Host',
	    'db_port' => 'Database Port',
	    'db_database' => 'Database Name',
	    'db_username' => 'Database User',
	    'db_password' => 'Database Password',
	    'db_schema' => 'Database Schema',
	    'current_db_username' => 'Mtumiaji wa hifadhidata mwenye mamlaka yote kwa sasa',
	    'current_db_password' => 'Nywila ya mtumiaji wa hifadhidata mwenye mamlaka yote kwa sasa',
	],
    ],
];
