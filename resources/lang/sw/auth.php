<?php

return [

    /*
      |--------------------------------------------------------------------------
      | Authentication Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used during authentication for various
      | messages that we need to display to the user. You are free to modify
      | these language lines according to your application's requirements.
      |
     */

    'failed' => 'Imeshindwa kulogia! jina au nywila si sahihi. Weka nywila na jina sahihi, kisha jaribu tena.',
    'throttle' => 'Imejaribu mara nyingi, jaribu tena baada ya sekunde :seconds',
    'general_error' => 'Huna idhini ya kufanya kitendo ulichokiomba.',
];
