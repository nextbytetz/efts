<?php

return [

    /*
      |--------------------------------------------------------------------------
      | History Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines contain strings associated to the
      | system adding lines to the history table.
      |
     */

    'backend' => [
	'none' => 'Hakuna historia ya hivi karibuni.',
	'none_for_type' => 'Hakuna historia katika mahali hapa.',
	'none_for_entity' => "Hakuna historia ya hii :entity.",
	'recent_history' => 'Historia ya hivi karibuni',
	'roles' => [
	    'created' => 'ametengeneza jukumu',
	    'deleted' => 'amefuta jukumu',
	    'updated' => 'amehariri jukumu',
	],
	'users' => [
	    'changed_password' => 'amebadilisha nywila ya mtumiaji',
	    'deactivated' => 'amemsitisha mtumiaji',
	    'edited' => 'amemhariri mtumiaji',
	    'reactivated' => 'amemrudisha mtumiaji',
	    'approved' => 'amempitisha mtumiaji',
	    'activated' => 'amemuwezesha mtumiaji',
	    'deactivated' => 'amemsitisha mtumiaji',
	    'out' => 'amemuweka nje ya ofisi mtumiaji',
	    'in' => 'amemuweka ndani ya ofisi mtumiaji',
	    'changed_password' => 'amebadilisha nywila kwa ajili ya mtumiaji',
	    'deleted' => 'amemfuta mtumiaji',
	],
	'roles' => [
	    'created' => 'ametengeneza jukumu',
	    'edited' => 'amehariri jukumu',
	    'deleted' => 'amefuta jukumu',
	],
	'file_register' => [
	    'created' => 'ametengeneza faili',
	    'updated' => 'amehariri faili',
	    'deleted' => 'amefuta faili',
	    'folder' => [
		'created' => 'ametengeneza folda',
		'renamed' => 'amebadili jina folda',
		'deleted' => 'amefuta folder',
	    ],
	    'bulk' => [
		'uploaded' => 'amepakia usajili mpya kwa wingi wenye jina ',
		'success' => 'amefanikishaa kupakia usajili mpya kwa wingi wenye jina ',
	    ],
	],
	'document' => [
	    'deleted' => 'amefuta hati',
	],
    ],
];
