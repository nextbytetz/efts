var checkStatus = function (pg, conString, data, callback) {
    var pg_client = new pg.Client(conString);
    pg_client.connect(function (err) {
        if (err) {
            return callback(err, 'could not connect to postgres');
        }
        pg_client.query('select * from app.app_notification where staff_id = ' + data.staff_id, function (err, result) {
            if (err) {
                return callback(err, 'error running query');
            }
            pg_client.end();
            return callback(err, result.rows);           
        });
    });
};

var dbNotification = function (pg, conString, callback){
    var pg_client = new pg.Client(conString);
    /* Create a persistence database connection */
    pg_client.connect(function (err) {
        if (err) {
            return callback(err, 'Failed to connect to database to listen notification');
        }
        pg_client.query('listen app_update; listen app_update_both; listen bn_app_status; listen android_appforum', function (err) {
            if (err) {
                return callback(err, 'Failed to execute listen notification command to the database server');
            }
            return callback(err, pg_client);           
        });
    });
};

module.exports.checkStatus = checkStatus;
module.exports.dbNotification = dbNotification;