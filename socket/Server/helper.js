var gcm = require('node-gcm')
	, kue = require('kue'),
	queue = kue.createQueue({
	    prefix: 'kue',
	    redis: 'redis://localhost:6379'
	})
	, queueJob
	;
;
/*Global: send notification to the android devices */
var sendAndroid = function (msgArr, gcmID, callback) {
    /* Broadcast ID to send message to all connected clients via android */
    var broadID = (typeof gcmID === null) ? "APA91bGawSnXF9n00n00gZpkkCfNRIaOJvzjFeC7X6RgcW7wNoc83snrcigx_MpzW6SPtt6LShKu-gNnG6UwQ490z8K8ZQ35esAN_dSxSOY9hqkq3Z_CywGVdoHEQniXEY7DpjnWxlq_QIs9aNOeRKi25YygT8fd_g" : gcmID;
    /* Set up the sender with your Gooogle API key */
    var sender = new gcm.Sender('AIzaSyCEmicefa98_ECH5kFDfqADl_Sl5cQusuA');
    var message = new gcm.Message();
    for (var i in msgArr) {
	message.addData(i, msgArr[i]);
    }
    // Add the registration tokens of the devices you want to send to
    var registrationTokens = [];
    registrationTokens.push(broadID);
    // ... send retrying a specific number of times (3)
    sender.sendNoRetry(message, {registrationTokens: registrationTokens}, function (err, response) {
	if (err) {
	    return callback(err, "Android Notification Sending Failed");
	} else {
	    return callback(err, response);
	}
    });
};
/*Clobal: end send notification to the android devices */

/* Custom: this function is used to store android notification in queue to send them in a queue, the queued jobs are saved in the redis database */
var saveAndroidNotification = function (msgArr, gcm_id) {
    queueJob = queue.create('androidQueue', {
	message: msgArr,
	gcm_id: gcm_id
    }).priority('high').removeOnComplete(true).save(function (err) {
	if (!err) {
	    console.log(queueJob.id);
	} else {
	    console.log(err);
	}
    });
};
/* End Custom: this function is used to store android notification in queue to send them in a queue, the queued jobs are saved in the redis database */

module.exports.sendAndroid = sendAndroid;
module.exports.saveAndroidNotification = saveAndroidNotification;
module.exports.queue = queue;

/* module.exports.queueJob = queueJob; */
