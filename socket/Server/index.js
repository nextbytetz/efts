/*
 * 
 * @author Erick Chrysostom, Main engine for node notification server
 */
var express = require('express'),
	dbCredential = require("./config/dbCredential"),
	app = express(),
	path = require("path"),
	pg = require("pg"),
	http = require('http').Server(app),
	io = require('socket.io')(http),
	router = express.Router(),
	conString = "postgres://" + dbCredential.username + ":" + dbCredential.password + "@" + dbCredential.host + ":" + dbCredential.port + "/" + dbCredential.database + "?application_name=Socket_Notification_Server_(PSPF_WEB_and_ANDROID)",
	db = require("./db"),
	helper = require("./helper"),
	routes = require("../Routes/")(router, pg, app),
	pg_client;

app.use('/', router);
http.listen(3000, function () {
    console.log("Listening on 3000 ...");
});

/* Global: this variable is used to save all clients connected, will consist of customID and socketID */
var clients = [];

/* Custom: This is to process the jobs created by the queue */

helper.queue.process('androidQueue', function (job, done) {
    helper.sendAndroid(job.data.message, job.data.gcm_id, function (error, result) {
	if (error) {
	    done(error);
	    job.state('inactive').save();
	} else {
	    done();
	}
    });
});


helper.queue.failedCount('androidQueue', function (err, total) {
    console.log(total + ' failed androidQueue');
});

/* End Custom: This is to check the event of the jobs created by the queue */

/* Custom: Detect failed queue jobs and change their state*/

/* End Custom: Detect failed queue jobs and change their state*/

/*Global: get the socketId of the client , client is in the form staff_n, vote_n, client_n that when user log in*/
var getSocketId = function (client) {
    var socketId = [];
    for (var i = 0, len = clients.length; i < len; ++i) {
	var c = clients[i];
	if (c.customId === client) {
	    socketId.push(c.socketId);
	}
    }
    return socketId;
};
/* end get the socketId of the client , client is in the form staff_n, vote_n, client_n that when user log in*/

/*Global: send notification to all socketId of the clients specified in socketArr, socketName is the name of the notification and data is the information to be sent with the notification */
var sendNotification = function (sockectArr, sockectName, data) {
    if (sockectArr.length !== 0) {
	for (var i in sockectArr) {
	    io.sockets.connected[sockectArr[i]].emit(sockectName, data);
	}
    }
};
/*Global: end send notification to all socketId of the clients specified in socketArr, socketName is the name of the notification and data is the information to be sent with the notification */

/*Custom: get the application status specific for PSPF App  */
var getAppStatus = function (data) {
    db.checkStatus(pg, conString, data, function (error, result) {
	if (error) {
	    io.emit('error');
	    console.log(result);
	} else {
	    sendNotification(getSocketId("staff_" + data.staff_id), 'notify_app_status', result);
	}
    });
};
/*Custom: end get the application status specific for PSPF App  */

/*Custom: listen to database notification, persistent connection to the database is established. */
db.dbNotification(pg, conString, function (error, result) {
    if (error) {
	console.log(result);
    } else {
	pg_client = result;
	pg_client.on('notification', function (data) {
	    /*
	     * data.channel is the name of the notification specified in pg_notify i.e app_update
	     * data.payload is the message json specified in the pg_notify ie {"staff_id" : 30, "app_count" : 6, "table" : "vc_app", "operation" : "UPDATE"}
	     */
	    /*convert payload json string to javascript array */
	    var output = JSON.parse(data.payload);
	    if (data.channel === 'app_update') {
		getAppStatus({'staff_id': output.staff_id});
	    }
	    if (data.channel === 'app_update_both') {
		getAppStatus({'staff_id': output.old_staff_id});
		getAppStatus({'staff_id': output.new_staff_id});
	    }
	    if (data.channel === 'bn_app_status') {
		console.log(output);
		sendNotification(getSocketId("client_" + output.client_id), 'bn_app_status', {action: output.action});
	    }
	    if (data.channel === 'android_appforum') {
		var message = {'notify': {'tag': '2', 'question': output.question, 'answer': output.answer}};
		//var message = {'notify': {'tag': '1', 'message': output.answer}}; /* for general message */

		if (output.gcm_id != null) {
		    helper.saveAndroidNotification(message, output.gcm_id);

		    /*
		     helper.sendAndroid(message, output.gcm_id, function (error, result) {
		     if (error) {
		     console.log(error);
		     } else {
		     console.log(result);
		     }
		     });
		     */
		    console.log("\n" + new Date().toJSON() + ":\n" + output.gcm_id);
		}
	    }
	});
    }
});
/*Custom: end listen to database notification, persistent connection to the database is established. */

/*Global: Handle socket connection from the clients  */
io.on('connection', function (socket) {
    socket.on('storeClientInfo', function (data) {
	var clientInfo = new Object();
	clientInfo.customId = data.customId;
	clientInfo.socketId = socket.id;
	clients.push(clientInfo);
	console.log('We have user connected ! total now is ' + clients.length);
	/* console.log(clients); */
    });

    /*Custom: Handle notification from pspf_app  */
    socket.on('admin_check', function (data) {
	sendNotification(getSocketId("staff_" + data.staff_id), 'notify_admin_check', {staff_id: data.staff_id, level: data.level});
    });
    socket.on('app_status', function (data) {
	getAppStatus(data);
    });
    /*Custom: end Handle notification from pspf_app  */

    /*Global: handle socket disconnection from the clients*/
    socket.on('disconnect', function (data) {
	for (var i = 0, len = clients.length; i < len; ++i) {
	    var c = clients[i];
	    if (c.socketId === socket.id) {
		clients.splice(i, 1);
		break;
	    }
	}
	console.log('We have user disconnected! total now is ' + clients.length);
    });
    /*Global: end handle socket disconnection from the clients*/
});

