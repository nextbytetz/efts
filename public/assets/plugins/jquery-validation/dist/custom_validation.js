// Set jQuery.validate settings for bootstrap integration & custom methods
$.validator.setDefaults({
    errorElement: "span",
    errorClass: "label label-warning",
    errorPlacement: function (a, b) {
	"radio" == b.attr("type") || "checkbox" == b.attr("type") ? a.insertAfter($(b).closest(".form-group").children("div").children().last()) : "card_expiry_mm" == b.attr("name") || "card_expiry_yyyy" == b.attr("name") ? a.appendTo($(b).closest(".form-group").children("div")) : a.insertAfter(b);
    },
    ignore: ":hidden",
    highlight: function (element) {
	jQuery(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element) {
	jQuery(element).closest('.form-group').removeClass('has-error');
    }
});
$.validator.addMethod("textonly", function (value, element) {
    var filter = /^[a-zA-Z']+$/;
    if (!filter.test(value)) {
	return false;
    } else {
	return true;
    }
}, 'Only characters are allowed, remove any space or non character ');
