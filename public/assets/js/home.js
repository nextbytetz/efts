var Home = function () {
    var e = function () {
	var e = $(".box-login");
	if (o("box").length)
	    switch (o("box")) {
		case "register":
		    e = $(".box-register");
		    break;
		case "forgot":
		    e = $(".box-forgot");
		    break;
		case "reset_password":
		    e = $(".box-reset-password");
		    break;
		default:
		    e = $(".box-login")
	    }
	e.show()
    },
	    a = function () {
		var e = $(".form-login"),
			a = $(".errorHandler", e);
		e.validate({
		    submitHandler: function (e) {
			a.hide(), $("#messages", e).removeClass("alert alert-warning").empty(), $(".fa-spin").show(), $(".btn-login").prop("disabled", !0);
			var r = $(".form-login").serialize(),
				s = actionAjax(base_url + "/verify_login", r, "post", "json");
			-1 === s && ($(".pass").val(""), $(".fa-spin").hide(), $(".btn-login").prop("disabled", !1)), s.success ? s.unchecked ? ($("#messages").addClass("alert alert-info").append("Dear " + s.name + ", your account need to be verified, please contact your company system administrator to verify and activate your account."), $(".password").val("")) : s.inactive ? ($("#messages").addClass("alert alert-info").append("Dear " + s.name + ", you cannot log in to your account, please contact your company system administrator for more information"), $(".password").val("")) : window.location.reload() : ($("#messages").addClass("alert alert-warning").append(s.errors.identity), $(".password").val("")), $(".fa-spin").hide(), $(".btn-login").prop("disabled", !1)
		    },
		    invalidHandler: function (e, r) {
			a.show().fadeOut(5e3)
		    }
		})
	    },
	    r = function () {
		var e = $(".form-forgot"),
			a = $(".errorHandler", e);
		e.validate({
		    submitHandler: function (e) {
			$("#messages_forgot_password").removeClass("alert alert-info").empty(), a.hide(), $(".fa-spin").show(), $(".btn-forgot-password").prop("disabled", !0);
			var r = $(".form-forgot").serialize(),
				s = actionAjax(base_url + "/password/email", r, "post", "json");
			s && $("#messages_forgot_password").addClass("alert alert-info").append("Password reset link have been sent to your email address").fadeOut(1e4), $(".btn-forgot-password").prop("disabled", !1), $(".fa-spin").hide()
		    },
		    invalidHandler: function (e, r) {
			a.show().fadeOut(5e3)
		    }
		})
	    },
	    s = function () {
		var e = $(".form-register"),
			a = $(".errorHandler", e);
		e.validate({
		    submitHandler: function (e) {
			a.hide(), $(".fa-spin").show(), $(".btn-register").prop("disabled", !0);
			var r = $(".form-register").serialize(),
				s = actionAjax(base_url + "/signup", r, "post", "json");
			s.success && $("#messages_register").addClass("alert alert-info").append("Signup Successifully &nbsp; <i class='fa fa-check-circle'></i> <br> Contact your company system administrator to verify and activate your account <br> To login <a href='" + base_url + "/home?box=login'><b><u>Click here</b></u></a> <br/> To register another account <a href='" + base_url + "/home?box=register'><b><u>Click here</b></u></a>"), $(".fa-spin").hide()
		    },
		    invalidHandler: function (e, r) {
			a.show().fadeOut(5e3)
		    }
		})
	    },
	    o = function (e) {
		e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var a = new RegExp("[\\?&]" + e + "=([^&#]*)"),
			r = a.exec(location.search);
		return null === r ? "" : decodeURIComponent(r[1].replace(/\+/g, " "))
	    },
	    n = function () {
		$("#region_select").bind("change", function () {
		    var e = $(this).val();
		    $.post(base_url + "/get_department_region", {
			region_id: e
		    }, function (e) {
			$("#department_select").html(e)
		    }, "html")
		})
	    },
	    t = function () {
		$(".search-select").select2({})
	    },
	    i = function () {
		var e = $(".form-reset-password"),
			a = $(".errorHandler", e);
		e.validate({
		    submitHandler: function (e) {
			a.hide(), $("#messages_reset_password").removeClass("alert alert-info").empty(), a.hide(), $(".fa-spin").show(), $(".btn-reset-password").prop("disabled", !0);
			var r = $(".form-reset-password").serialize(),
				s = actionAjax(base_url + "/reset_password", r, "post", "json");
			s ? $("#messages_reset_password").addClass("alert alert-info").append("Your password has been changed successifully, To log in <a href='" + base_url + "/home?box=login'><b><u>click here</b></u></a>") : $("#messages_reset_password").addClass("alert alert-info").append("Invalid request or you have used an outdated password reset link. To reset your password <a href='" + base_url + "/home?box=forgot'><b><u>click here</b></u></a>"), $(".fa-spin").hide()
		    },
		    invalidHandler: function (e, r) {
			a.show().fadeOut(5e3)
		    }
		})
	    },
	    b = function () {
		var e = $(".form-db-setup");
		a = $(".errorHandler", e);
		e.validate({
		    submitHandler: function (e) {
			a.hide(), $(".fa-spin").show(), $(".btn-db-setup").prop("disabled", !0);
			var r = $(".form-db-setup").serialize(),
				s = actionAjax(base_url + "/save_db_param", r, "post", "json");
			if (s.success) {
			    $("#messages_setup").removeClass("alert alert-danger").empty().addClass("alert alert-info").append("Application has been properly configured ...<br/>Current Super User : <b>" + s.super.firstname + "</b> <br/> Super User Email : <b>" + s.super.email + "</b> <br/> If application reconfiguration is necessary and you need to clear and recreate the current database <b><u><a href='" + base_url + "/setup_db_configure?context=update'>Click here</a></u></b> <br/> To login <a href='" + base_url + "/home?box=login'><b><u>Click here</b></u></a> ");
			} else {
			    $("#messages_setup").empty();
			    $("#messages_setup").addClass("alert alert-danger").append(s.message + " <br/> If this is the new installation, application reconfiguration may be necessary to create and configure the new database, to proceed <b><u><a href='" + base_url + "/setup_db_configure?context=new'>Click here</a></u></b> <br/>");
			}
			$(".fa-spin").hide(), $(".btn-db-setup").prop("disabled", !1);
		    }
		});
	    },
	    c = function () {
		var e = $(".form-db-config");
		a = $(".errorHandler", e);
		e.validate({
		    submitHandler: function (e) {
			bootbox.confirm("Confirm your action?", function (result) {
			    if (result) {
				a.hide(), $(".fa-spin").show(), $(".btn-db-config").prop("disabled", !0);
				var r = $(".form-db-config").serialize(),
					s = actionAjax(base_url + "/save_db_config", r, "post", "json");
				if (s.success) {
				    $("#messages_db_config").removeClass("alert alert-danger").empty().addClass("alert alert-info").append(s.message + " <br/>To login <a href='" + base_url + "/home?box=login'><b><u>Click here</b></u></a> ");				    
				} else {
				    $("#messages_db_config").empty();
				    $("#messages_db_config").addClass("alert alert-danger").append(s.message);
				    $(".btn-db-config").prop("disabled", !1);
				}
				$(".fa-spin").hide();
			    }
			});
		    }
		});
	    };
    return {
	init: function () {
	    a(), e(), r(), s(), n(), t(), $(".fa-spin").hide(), i(), b(), c();
	}
    }
}();