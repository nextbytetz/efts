/**
 * 
 * @author Erick Chrysostom <gwanchi@gmail.com>
 * Description
 * Contain reusable javascript functions
 */

var is_reload = false;

function gritter(title, text) {
    $.gritter.add({
	// (string | mandatory) the heading of the notification
	title: title,
	// (string | mandatory) the text inside the notification
	text: text
    });
}

/*Global function to load page on the content*/
function call_page(path, reload) {
    is_reload = (typeof reload === "undefined") ? false : reload;
    if (window.location.hash.substring(1) === path) {
	$(window).hashchange();
	/*window.location.reload(); */
    } else {
	window.location.hash = path;
    }
}

function loadajax(path, div) {
    if (is_reload === false) {
	var div = $(((typeof div === "undefined") ? '#content' : div));
	div.mask('<i class="fa fa-refresh fa-spin"></i> Loading...');
	div.load(path, function (response, status, xhr) { /* new implementation of the function using jquery load */
	    if (status === "error") {
		var msg = "Sorry but there was an error: ";
		swal("Failed", xhr.status + " " + xhr.statusText, "error");
	    }
	    div.unmask();
	});
    } else {
	window.location.reload();
    }
}
/*end of Global function to load page on the content*/
/* function to display errors */
function on_error(jqXHR, errorThrown) {
    var mesg = (jqXHR.responseText === undefined) ? 'No response from the server, this may be due to the lost internet connection or something went wrong on the server. If you have a good internet connection, then there must be a problem on the server' : 'status code: ' + jqXHR.status + ', errorThrown: ' + errorThrown;
    swal("Failed", mesg, "error");
}
/* end of function to display errors */

/* load jqx table */
/*
 * @table_id id of the div where the jqx table is to be placed
 * @url url to the controller for fetching data from the database
 * @dataFields (database columns) data fields to take from the json array
 * @root name of the array with the datafields to be used in the table
 * @columns the column definition array
 * @pageSize the size of pagenation, number of records per page
 * @width width of the table
 */
function generate_jqx_table(table_id, url, root, dataFields, columns, filterable, total, width, pageSize) {

    /*$.jqx.theme = "bootstrap";*/
    width = (typeof width === "undefined") ? 1040 : width;
    pageSize = (typeof pageSize === "undefined") ? 12 : pageSize;
    total = (typeof total === "undefined") ? 100 : total;
    filterable = (typeof filterable === "undefined") ? true : false;

    var source =
	    {
		dataType: "json",
		dataFields: dataFields,
		root: root,
		url: url
	    };

    var filterChanged = false;
    var dataAdapter = new $.jqx.dataAdapter(source,
	    {
		formatData: function (data) {
		    if (data.sortdatafield && data.sortorder) {
			data.$orderby = data.sortdatafield + " " + data.sortorder;
		    }
		    if (source.totalRecords) {
			data.$skip = data.pagenum * data.pagesize;
			data.$top = data.pagesize;
		    }
		    return data;
		},
		downloadComplete: function (data, status, xhr) {
		    source.totalRecords = data.value.total;
		},
		loadError: function (jqXHR, errorThrown) {
		    on_error(jqXHR, errorThrown);
		}
	    }
    );

    $(table_id).jqxDataTable(
	    {
		width: '100%',
		pageable: true,
		enableHover: true,
		pagerButtonsCount: 10,
		serverProcessing: true,
		selectionMode: "singleRow",
		pagerMode: "advanced",
		source: dataAdapter,
		altRows: true,
		filterable: filterable,
		filterMode: 'advanced',
		pageSize: pageSize,
		exportSettings:  { columnsHeader: true, hiddenColumns: false, serverURL: null, characterSet: null, recordsInView: false, fileName: table_id},
		pageSizeOptions: ['5', '15', '20', '30', total],
		theme: 'bootstrap',
		showAggregates: true,
		aggregatesHeight: 70,
		sortable: true,
		columnsResize: true,
		columns: columns
	    });

    $(table_id + '_print').jqxButton({width: 80});
    $(table_id + '_excelExport').jqxButton({width: 120});

    $(table_id + '_print').click(function () {
	var gridContent = $(table_id).jqxDataTable('exportData', 'html');
	var newWindow = window.open('', '', 'width=800, height=500'),
		document = newWindow.document.open(),
		pageContent =
		'<!DOCTYPE html>' +
		'<html>' +
		'<head>' +
		'<meta charset="utf-8" />' +
		'<title></title>' +
		'</head>' +
		'<body>' + gridContent + '</body></html>';
	document.write(pageContent);
	document.close();
	newWindow.print();
    });

    $(table_id + '_excelExport').click(function () {
	$(table_id).jqxDataTable('exportData', 'xls');
    });
}

/*
 * 
 * @combo_id id of the div to generate the combobox
 * @url is the remote location where list of combobox values will be fetched
 * @datafields is the array with the list of database columns to be used in combobox
 * @displayMember is the database column which will be displayed in the combobox list
 * @valueMember is the value of the dispayed member in the combobox list
 * @data array of parameters to be sent to the server for searching purposes
 * @returns {String|Boolean} 
 * 
 * */
function generate_jqx_combo(combo_id, url, datafields, displayMember, valueMember, multiSelect, data) {

    if (typeof data === "undefined") {
	data = {
	    maxRows: 12,
	    username: "jqwidgets"
	};
    }

    multiSelect = (typeof multiSelect === "undefined") ? true : multiSelect;

    var source =
	    {
		datatype: "jsonp",
		datafields: datafields,
		url: url,
		data: data
	    };
    var dataAdapter = new $.jqx.dataAdapter(source,
	    {
		formatData: function (data) {
		    if ($(combo_id).jqxComboBox('searchString') !== "undefined") {
			data.name_startsWith = $(combo_id).jqxComboBox('searchString');
			return data;
		    }
		}
	    }
    );
    $(combo_id).jqxComboBox(
	    {
		width: '100%',
		height: 25,
		source: dataAdapter,
		remoteAutoComplete: true,
		autoDropDownHeight: true,
		enableBrowserBoundsDetection: true,
		placeHolder: "Type to search ...",
		minLength: 1,
		multiSelect: multiSelect,
		/*theme: 'office',*/
		selectedIndex: 0,
		displayMember: displayMember,
		valueMember: valueMember,
		search: function (searchString) {
		    dataAdapter.dataBind();
		}
	    });
}

function get_jqx_combo_value(combo_id, multiSelect) {
    multiSelect = (typeof multiSelect === "undefined") ? true : multiSelect;
    var file_list = "";

    if (multiSelect) {
	var items = $(combo_id).jqxComboBox('getSelectedItems');
	$.each(items, function (index) {
	    file_list += this.value;
	    if (items.length - 1 !== index) {
		file_list += ",";
	    }
	});
    } else {
	var items = $(combo_id).jqxComboBox('getSelectedItem');
	(typeof items === "undefined") ? file_list = "" : file_list = items.value;
    }
    return file_list;
}

function get_jqx_listbox(combo_id) {
    var items = $(combo_id).jqxListBox('getSelectedItems');
    var app_list = "";
    $.each(items, function (index) {
	app_list += this.value;
	if (items.length - 1 !== index) {
	    app_list += ",";
	}
    });
    return app_list;
}

/*
 * 
 * @list_id id of the div to generate the listbox
 * @url is the remote location where list of combobox values will be fetched
 * @datafields is the array with the list of database columns to be used in combobox
 * @displayMember is the database column which will be displayed in the combobox list
 * @valueMember is the value of the dispayed member in the combobox list
 * @data array of parameters to be sent to the server for searching purposes
 * @returns {String|Boolean} 
 * 
 * */
function generate_jqx_list(list_id, url, datafields, displayMember, valueMember, search_input_id, custome_filter_id, data) {
    if (typeof data === "undefined") {
	data = {
	    maxRows: 12,
	    username: "jqwidgets"
	};
    }
    var source =
	    {
		datatype: "jsonp",
		datafields: datafields,
		url: url,
		data: data
	    };
    var dataAdapter = new $.jqx.dataAdapter(source,
	    {
		formatData: function (data) {
		    data.name_startsWith = $(search_input_id).val();
		    data.custom_filter = $(custome_filter_id).val();
		    return data;
		}
	    }
    );
    $(list_id).jqxListBox(
	    {
		width: '100%',
		height: 350,
		source: dataAdapter,
		selectedIndex: 0,
		multipleextended: true,
		/*theme: 'bootstrap',*/
		displayMember: displayMember,
		valueMember: valueMember
	    });
    $(search_input_id).change(function (event) {
	dataAdapter.dataBind();
    });
    var me = this;
    $(custome_filter_id).on('keyup', function (event) {
	if (me.timer)
	    clearTimeout(me.timer);
	me.timer = setTimeout(function () {
	    dataAdapter.dataBind();
	}, 300);
    });
}
/*
 * Display notifications on the browser sent by socket-notification
 * @param {type} user , name of the user to be notified
 * @param {type} message , message to be displayed
 * @returns {undefined}
 */
function notifyMe(user, message) {
    // Let's check if the browser supports notifications
    if (!("Notification" in window)) {
	alert("This browser does not support desktop notification");
    }
    // Let's check if the user is okay to get some notification
    else if (Notification.permission === "granted") {
	// If it's okay let's create a notification
	var options = {
	    body: message,
	    dir: "ltr",
	    icon: "admin_assets/assets/images/example.png"
	};
	var notification = new Notification("Dear " + user, options);
	document.getElementById('sound').play();

    }
    // Otherwise, we need to ask the user for permission
    // Note, Chrome does not implement the permission static property
    // So we have to check for NOT 'denied' instead of 'default'
    else if (Notification.permission !== 'denied') {
	Notification.requestPermission(function (permission) {
	    // Whatever the user answers, we make sure we store the information
	    if (!('permission' in Notification)) {
		Notification.permission = permission;
	    }
	    // If the user is okay, let's create a notification
	    if (permission === "granted") {
		var options = {
		    body: message,
		    dir: "ltr",
		    icon: "admin_assets/assets/images/example.png"
		};
		var notification = new Notification("Dear " + user, options);
		document.getElementById('sound').play();
	    }
	});
    }
    // At last, if the user already denied any notification, and you
    // want to be respectful there is no need to bother them any more.
}

/*
 * Function for generating notification
 * @param {type} layout
 * @param {type} type
 * @returns {undefined}
 */

function generate_noty(layout, type, app_name) {
    var n = noty({
	text: 'You have new ' + app_name + ' application',
	type: type,
	dismissQueue: true,
	layout: layout,
	maxVisible: 3,
	killer: false,
	timeout: 5000,
	theme: 'defaultTheme'
    });
}

/**
 * Description : function to execute ajax calls for processing action requests
 * @param {type} path
 * @param {type} data_
 * @param {type} method_
 * @param {type} data_type
 * @param {type} div
 * @returns {data}
 */
function actionAjax(path, data_, method_, data_type) {
    var output;
    data_ = (typeof data_ === "undefined") ? {} : data_;
    method_ = (typeof method_ === "undefined") ? "get" : method_;
    data_type = (typeof data_type === "undefined") ? "json" : data_type;
    $.ajax({
	method: method_,
	url: path,
	dataType: data_type,
	data: data_,
	cache: true,
	async: false,
	success: function (data, textStatus, XMLHttpRequest) {
	    output = data;
	},
	error: function (jqXHR, errorThrown) {
	    output = -1;
	    on_error(jqXHR, errorThrown);
	}
    });
    return output;
}

/**
 * Description : function to execute ajax calls for processing action requests asynchronisly
 * @param {type} path
 * @param {type} data_
 * @param {type} callback
 * @param {type} method_
 * @param {type} data_type
 * @returns {undefined}
 */
function actionAjaxAsync(path, data_, callback, method_, data_type) {
    data_ = (typeof data_ === "undefined") ? {} : data_;
    method_ = (typeof method_ === "undefined") ? "get" : method_;
    data_type = (typeof data_type === "undefined") ? "json" : data_type;
    $.ajax({
	method: method_,
	url: path,
	dataType: data_type,
	data: data_,
	cache: true,
	async: true,
	success: function (data) {
	    return callback(data);
	},
	error: function (jqXHR, errorThrown) {
	    on_error(jqXHR, errorThrown)
	}
    });
}