var department = $('#department-tree');
var checked_department;
/**
 * Initiate the tree and open all items
 * When a node is changed, loop through all of its dependencies
 * and search through the tree to check/uncheck them
 */
department.jstree({
    "checkbox": {
	"keep_selected_style": true
    },
    "plugins": ["checkbox"],
}).on('ready.jstree', function () {
    /*tree.jstree('open_all');*/
    department.jstree('hide_icons');
});

/**
 * 
 * Prepare a folder view js
 */

/**
 * Get list of the checked items and send them to the serer
 */
$("#create_file, #edit_file, #create_bulk_file").submit(function () {
    checked_department = department.jstree("get_checked", false);
    $("input[name='departments']").val(checked_department);
});
