/**
 * Sample program that check if there is a connection to the server and send
 *  all offline notification to the server for a fixed period of time (1000ms)
 * .
 * @file notification.c
 * @author Erick Chrysostom <gwanchi@gmail.com>
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */
#include <libpq-fe.h>  /* Postgres supports library and connection */



int main() {

    /* start: Parameters of where we are going to send the data */
    int portno = 90;
    char *host = "10.42.0.1";
    char *reader = "529fc4";
    PGresult *res;
    PGconn *conn = PQconnectdb("user=postgres dbname=postgres host=127.0.0.1");
    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total, message_size, rows, loop, notification_id;
    char message[240], response[4096], parameters[180], query[200];
   /* end: Parameters of where we are going to send the data */
    
    /* start : connecting to the database */
    if (PQstatus(conn) == CONNECTION_BAD) {

        fprintf(stderr, "Connection to database failed: %s\n",
            PQerrorMessage(conn));

        PQfinish(conn);
        return 0;
    }
    /* end : connecting to the database */

    while(1) {
        
        sprintf(query, "select notification_id, epc, frequency, antenna, read_count, rssi, created_at from notification limit %d", 3);
        res = PQexec(conn, query);
        
        if (PQresultStatus(res) == PGRES_TUPLES_OK) {
            if (PQntuples(res) > 0) {
                rows = PQntuples(res);
                for( loop = 0; loop < rows; loop = loop + 1 ){
                    /* create the socket */
                    sockfd = socket(AF_INET, SOCK_STREAM, 0);
                    if (sockfd < 0) printf("ERROR opening socket");
                    /* lookup the ip address */
                    server = gethostbyname(host);
                    if (server == NULL) printf("ERROR, no such host");
                    /* fill in the structure */
                    memset(&serv_addr,0,sizeof(serv_addr));
                    serv_addr.sin_family = AF_INET;
                    serv_addr.sin_port = htons(portno);
                    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

                    /* connect the socket */
                    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) >= 0) {
                        
                        sprintf(parameters, "reader=%s&epc=%s&rssi=%s&antenna=%s&count=%s&frequency=%s&time=%s", reader, PQgetvalue(res, loop, 1), PQgetvalue(res, loop, 5), PQgetvalue(res, loop, 3), PQgetvalue(res, loop, 4), PQgetvalue(res, loop, 2), PQgetvalue(res, loop, 6) );
                        message_size=0;
                        message_size+=strlen("%s %s HTTP/1.0\r\n");
                        message_size+=strlen("GET");
                        message_size+=strlen("/api/reader");
                        message_size+=strlen("Content-Length: %d\r\n")+10; /* content length */
                        message_size+=strlen("\r\n");
                        message_size+=  strlen(parameters);
                        
                        /* fill in the parameters */
                        sprintf(message,"%s %s%s%s HTTP/1.0\r\n", "GET", "/api/reader", "?", parameters);
                        strcat(message,"Content-Type: application/x-www-form-urlencoded");
                        strcat(message,"\r\n");
                        strcat(message,"\r\n"); 
                        
                        /* send the request */
                        total = strlen(message);
                        sent = 0;
                        do {
                        bytes = write(sockfd,message+sent,total-sent);
                        if (bytes < 0)
                            printf("ERROR writing message to socket");
                        if (bytes == 0)
                            break;
                        sent+=bytes;
                        } while (sent < total);

                        /* receive the response */
                        memset(response,0,sizeof(response));
                        total = sizeof(response)-1;
                        received = 0;
                        do {
                            bytes = read(sockfd,response+received,total-received);
                            if (bytes < 0)
                                printf("ERROR reading response from socket");
                            if (bytes == 0)
                                break;
                            received+=bytes;
                        } while (received < total);

                        if (received == total)
                            printf("ERROR storing complete response from socket");
                        
                        /* process response */
                        printf("Response:\n%s\n\n",response);

                        /* end : Send information to the server */
                        
                        notification_id = atoi (PQgetvalue(res, loop, 0));
                        
                        sprintf(query, "delete from notification where notification_id = %d", notification_id);
                        PQexec(conn, query);
                        
                        sleep(2);
                        
                    }
                    
                    /* close the socket */
                    close(sockfd);
                }
            }
        }
        PQclear(res);
        sleep(4);
        
    }
    return 0;
}