/**
 * Sample program that reads tags for a fixed period of time (500ms)
 * and prints the tags found.
 * @file read.c
 * Modified by Erick Chrysostom <gwanchi@gmail.com>
 * Added support to send notification to the server via http post, detect tags 
 * by considering aging, saving offline notifications to the local postgres database
 * so that can be sent later.
 */

#include <stdio.h>
#include <stdlib.h>
#include <tm_reader.h>
#include <time.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h> /* read, write, close */
#include <sys/socket.h> /* socket, connect */
#include <netinet/in.h> /* struct sockaddr_in, struct sockaddr */
#include <netdb.h> /* struct hostent, gethostbyname */
#include <libpq-fe.h>  /* Postgres supports library and connection */

#ifndef BARE_METAL
#if WIN32
#define snprintf sprintf_s
#endif 

/* Enable this to use transportListener */
#ifndef USE_TRANSPORT_LISTENER
#define USE_TRANSPORT_LISTENER 0
#endif

#define usage() {errx(1, "Please provide reader URL, such as:\n"\
                         "tmr:///com4 or tmr:///com4 --ant 1,2\n"\
                         "tmr://my-reader.example.com or tmr://my-reader.example.com --ant 1,2\n");}

void errx(int exitval, const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);

  exit(exitval);
}

void checkerr(TMR_Reader* rp, TMR_Status ret, int exitval, const char *msg)
{
  if (TMR_SUCCESS != ret)
  {
    errx(exitval, "Error %s: %s\n", msg, TMR_strerr(rp, ret));
  }
}

void serialPrinter(bool tx, uint32_t dataLen, const uint8_t data[],
                   uint32_t timeout, void *cookie)
{
  FILE *out = cookie;
  uint32_t i;

  fprintf(out, "%s", tx ? "Sending: " : "Received:");
  for (i = 0; i < dataLen; i++)
  {
    if (i > 0 && (i & 15) == 0)
    {
      fprintf(out, "\n         ");
    }
    fprintf(out, " %02x", data[i]);
  }
  fprintf(out, "\n");
}

void stringPrinter(bool tx,uint32_t dataLen, const uint8_t data[],uint32_t timeout, void *cookie)
{
  FILE *out = cookie;

  fprintf(out, "%s", tx ? "Sending: " : "Received:");
  fprintf(out, "%s\n", data);
}

void parseAntennaList(uint8_t *antenna, uint8_t *antennaCount, char *args)
{
  char *token = NULL;
  char *str = ",";
  uint8_t i = 0x00;
  int scans;

  /* get the first token */
  if (NULL == args)
  {
    fprintf(stdout, "Missing argument\n");
    usage();
  }

  token = strtok(args, str);
  if (NULL == token)
  {
    fprintf(stdout, "Missing argument after %s\n", args);
    usage();
  }

  while(NULL != token)
  {
    scans = sscanf(token, "%"SCNu8, &antenna[i]);
    if (1 != scans)
    {
      fprintf(stdout, "Can't parse '%s' as an 8-bit unsigned integer value\n", token);
      usage();
    }
    i++;
    token = strtok(NULL, str);
  }
  *antennaCount = i;
}
#endif

int main(int argc, char *argv[])
{
  TMR_Reader r, *rp;
  TMR_Status ret;
  TMR_ReadPlan plan;
  TMR_Region region;
  uint8_t *antennaList = NULL;
  uint8_t buffer[20];
  uint8_t i;
  uint8_t antennaCount = 0x0;
  TMR_String model;
  char str[64];
  
  /* start: Parameters of where we are going to send the data */
    int portno = 90;
    char *host = "10.42.0.1";
    char *reader = "529fc4";
    PGresult *res;
    PGconn *conn = PQconnectdb("user=postgres dbname=postgres host=127.0.0.1");
    struct hostent *server;
    struct sockaddr_in serv_addr;
    int sockfd, bytes, sent, received, total, message_size, db_age;
    char message[240], response[4096], parameters[180], query[200];
    int age = 3;
  /* end: Parameters of where we are going to send the data */
    
    /* start : connecting to the database */
    if (PQstatus(conn) == CONNECTION_BAD) {

        fprintf(stderr, "Connection to database failed: %s\n",
            PQerrorMessage(conn));

        PQfinish(conn);
        return 0;
    }
    /* end : connecting to the database */


#ifndef BARE_METAL
#if USE_TRANSPORT_LISTENER
  TMR_TransportListenerBlock tb;
#endif
 
  if (argc < 2)
  {
    usage(); 
  }

  for (i = 2; i < argc; i+=2)
  {
    if(0x00 == strcmp("--ant", argv[i]))
    {
      if (NULL != antennaList)
      {
        fprintf(stdout, "Duplicate argument: --ant specified more than once\n");
        usage();
      }
      parseAntennaList(buffer, &antennaCount, argv[i+1]);
      antennaList = buffer;
    }
    else
    {
      fprintf(stdout, "Argument %s is not recognized\n", argv[i]);
      usage();
    }
  }
#endif
  
  rp = &r;
#ifndef BARE_METAL
  ret = TMR_create(rp, argv[1]);
#else
  ret = TMR_create(rp, "tmr:///com1");
  buffer[0] = 1;
  antennaList = buffer;
  antennaCount = 0x01;
#endif
	
#ifndef BARE_METAL
  checkerr(rp, ret, 1, "creating reader");

#if USE_TRANSPORT_LISTENER

  if (TMR_READER_TYPE_SERIAL == rp->readerType)
  {
    tb.listener = serialPrinter;
  }
  else
  {
    tb.listener = stringPrinter;
  }
  tb.cookie = stdout;

  TMR_addTransportListener(rp, &tb);
#endif
#endif

  ret = TMR_connect(rp);

#ifndef BARE_METAL
  checkerr(rp, ret, 1, "connecting reader");
#endif
  region = TMR_REGION_NONE;
  ret = TMR_paramGet(rp, TMR_PARAM_REGION_ID, &region);
#ifndef BARE_METAL
  checkerr(rp, ret, 1, "getting region");
#endif
  
  if (TMR_REGION_NONE == region)
  {
    TMR_RegionList regions;
    TMR_Region _regionStore[32];
    regions.list = _regionStore;
    regions.max = sizeof(_regionStore)/sizeof(_regionStore[0]);
    regions.len = 0;

    ret = TMR_paramGet(rp, TMR_PARAM_REGION_SUPPORTEDREGIONS, &regions);
#ifndef BARE_METAL
    checkerr(rp, ret, __LINE__, "getting supported regions");

    if (regions.len < 1)
    {
      checkerr(rp, TMR_ERROR_INVALID_REGION, __LINE__, "Reader doesn't support any regions");
    }
#endif  
    region = regions.list[0];
    ret = TMR_paramSet(rp, TMR_PARAM_REGION_ID, &region);
#ifndef BARE_METAL    
	checkerr(rp, ret, 1, "setting region");  
#endif 
  }

  model.value = str;
  model.max = 64;
  TMR_paramGet(rp, TMR_PARAM_VERSION_MODEL, &model);
  if (((0 == strcmp("Sargas", model.value)) || (0 == strcmp("M6e Micro", model.value)) ||(0 == strcmp("M6e Nano", model.value)))
    && (NULL == antennaList))
  {
#ifndef BARE_METAL	  
    fprintf(stdout, "Module doesn't has antenna detection support please provide antenna list\n");
    usage();
#endif	
  }

  /**
  * for antenna configuration we need two parameters
  * 1. antennaCount : specifies the no of antennas should
  *    be included in the read plan, out of the provided antenna list.
  * 2. antennaList  : specifies  a list of antennas for the read plan.
  **/ 

  // initialize the read plan 
  ret = TMR_RP_init_simple(&plan, antennaCount, antennaList, TMR_TAG_PROTOCOL_GEN2, 1000);
#ifndef BARE_METAL
  checkerr(rp, ret, 1, "initializing the  read plan");
#endif
  
  /* Commit read plan */
  ret = TMR_paramSet(rp, TMR_PARAM_READ_PLAN, &plan);
#ifndef BARE_METAL
  checkerr(rp, ret, 1, "setting read plan");
#endif
  ret = TMR_read(rp, 500, NULL);
  
#ifndef BARE_METAL
if (TMR_ERROR_TAG_ID_BUFFER_FULL == ret)
  {
    /* In case of TAG ID Buffer Full, extract the tags present
    * in buffer.
    */
    fprintf(stdout, "reading tags:%s\n", TMR_strerr(rp, ret));
  }
  else
  {
    checkerr(rp, ret, 1, "reading tags");
  }
#endif
  while (true) {
  
  while (TMR_SUCCESS == TMR_hasMoreTags(rp))
  {
    
    TMR_TagReadData trd;
    char epcStr[128];
    char timeStr[128];

    ret = TMR_getNextTag(rp, &trd);
#ifndef BARE_METAL  
  checkerr(rp, ret, 1, "fetching tag");
#endif
    TMR_bytesToHex(trd.tag.epc, trd.tag.epcByteCount, epcStr);

#ifndef BARE_METAL
#ifdef WIN32
	{
		FILETIME ft, utc;
		SYSTEMTIME st;
		char* timeEnd;
		char* end;
		
                
		utc.dwHighDateTime = trd.timestampHigh;
		utc.dwLowDateTime = trd.timestampLow;

		FileTimeToLocalFileTime( &utc, &ft );
		FileTimeToSystemTime( &ft, &st );
		timeEnd = timeStr + sizeof(timeStr)/sizeof(timeStr[0]);
		end = timeStr;
		end += sprintf(end, "%d-%d-%d", st.wYear,st.wMonth,st.wDay);
		end += sprintf(end, "T%d:%d:%d %d", st.wHour,st.wMinute,st.wSecond, st.wMilliseconds);
		end += sprintf(end, ".%06d", trd.dspMicros);
                 
  }
#else
    {
      uint8_t shift;
      uint64_t timestamp;
      time_t seconds;
      int micros;
      char* timeEnd;
      char* end;

      
      shift = 32;
      timestamp = ((uint64_t)trd.timestampHigh<<shift) | trd.timestampLow;
      seconds = timestamp / 1000;
      micros = (timestamp % 1000) * 1000;
       
      /*
       * Timestamp already includes millisecond part of dspMicros,
       * so subtract this out before adding in dspMicros again
       */
      
      micros -= trd.dspMicros / 1000;
      micros += trd.dspMicros;

      timeEnd = timeStr + sizeof(timeStr)/sizeof(timeStr[0]);
      end = timeStr;
      end += strftime(end, timeEnd-end, "%F %H:%M:%S", localtime(&seconds));
      //end += snprintf(end, timeEnd-end, ".%06d", micros);
      //end += strftime(end, timeEnd-end, "%z", localtime(&seconds));
       
    }
#endif
  
    /* start : Check if the tag exist in the database, if not insert it and continue sending notification, if
     * yes, check its age, if more than 2 min continue sending notification if not print message of discarding 
     * notification and update the current time in database continue the while loop. 
     */
    sprintf(query, "select epc, floor(extract(epoch from age(now(), updated_at)) / 60) as age from tag where epc = '%s'", epcStr);
    res = PQexec(conn, query); 
    if (PQresultStatus(res) == PGRES_TUPLES_OK) {
        if (PQntuples(res) == 0) {
        //insert fresh new record
        sprintf(query, "insert into tag(epc) values ('%s')", epcStr);
        PQclear(res);
        res = PQexec(conn, query);
        PQclear(res); 
        }
        else
        {
        //update current time       
        db_age = atoi (PQgetvalue(res, 0, 1));
        
        //check the age of the tag (since it was last read)
        if (db_age < age) {
            printf("Continuing while loop, age of %s is %d minutes which is less than %d minutes\n\n", epcStr, db_age, age);            
            continue;
        }
        else 
        {
            sprintf(query, "update tag set updated_at = now() where epc = '%s'", epcStr);
            res = PQexec(conn, query);
            PQclear(res);
        }
        }
    } 
    else
    {
        continue;
    }
    /* end : check tag exist */
  
    /* start : Send information to the server */
    
    sprintf(parameters, "reader=%s&epc=%s&rssi=%d&antenna=%d&count=%d&frequency=%d&time=%s", reader, epcStr, trd.rssi, trd.antenna, trd.readCount, trd.frequency, timeStr );
    
    message_size=0;
    message_size+=strlen("%s %s HTTP/1.0\r\n");
    message_size+=strlen("GET");
    message_size+=strlen("/api/reader");
    message_size+=strlen("Content-Length: %d\r\n")+10; /* content length */
    message_size+=strlen("\r\n");
    message_size+=  strlen(parameters);
    
    /* fill in the parameters */
    sprintf(message,"%s %s%s%s HTTP/1.0\r\n", "GET", "/api/reader", "?", parameters);
    strcat(message,"Content-Type: application/x-www-form-urlencoded");
    strcat(message,"\r\n");
    strcat(message,"\r\n");    
    
    /* create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) printf("ERROR opening socket");
    /* lookup the ip address */
    server = gethostbyname(host);
    if (server == NULL) printf("ERROR, no such host");
    /* fill in the structure */
    memset(&serv_addr,0,sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portno);
    memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);
    /* connect the socket */
    if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) {
        sprintf(query, "insert into notification(epc,frequency,antenna,read_count,rssi) values ('%s',%d,%d,%d,%d)", epcStr, trd.frequency, trd.antenna, trd.readCount, trd.rssi);
        res = PQexec(conn, query);
        PQclear(res);
        printf("ERROR connecting\n\n");
    }
    else
    {
    /* send the request */
    total = strlen(message);
    sent = 0;
    do {
    bytes = write(sockfd,message+sent,total-sent);
    if (bytes < 0)
        printf("ERROR writing message to socket");
    if (bytes == 0)
        break;
    sent+=bytes;
    } while (sent < total);
    
    /* receive the response */
    memset(response,0,sizeof(response));
    total = sizeof(response)-1;
    received = 0;
    do {
        bytes = read(sockfd,response+received,total-received);
        if (bytes < 0)
            printf("ERROR reading response from socket");
        if (bytes == 0)
            break;
        received+=bytes;
    } while (received < total);

    if (received == total)
        printf("ERROR storing complete response from socket");

    
    /* process response */
    printf("Response:\n%s\n\n",response);
    
    /* end : Send information to the server */
    
    printf("EPC:%s rssi:%d frequency:%d ant:%d count:%d Time:%s\n\n\n", epcStr, trd.rssi, trd.frequency, trd.antenna, trd.readCount, timeStr);
    
    }
    
    /* close the socket */
    close(sockfd);

       
#endif
  }
  
  ret = TMR_read(rp, 500, NULL);
  }

  TMR_destroy(rp);
  return 0;
}
