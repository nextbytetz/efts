CREATE OR REPLACE FUNCTION public.after_file_update()
  RETURNS trigger AS
$BODY$
declare count_change bigint;
    BEGIN
if (NEW.status <> OLD.status and NEW.status = '0' ) THEN
insert into file_status_audit(created_at,closed_at,officer_opened,officer_closed,file_id) values (old.created_at,new.closed_at,old.officer_opened,new.officer_closed,old.file_id);
end if;
if (new.reference <> old.reference or new.name <> old.name or new.subject <> old.subject or new.description <> old.description or new.epc <> old.epc or new.file_category_id <> old.file_category_id or new.file_type_id <> old.file_type_id or new.shelf_number <> old.shelf_number) then

select count(file_id) into count_change from file_audit where file_id = old.file_id;

if (count_change = 0) then

insert into file_audit(file_id, officer_id, reference, name, subject, description, epc, file_category_id, file_type_id, shelf_number) values (old.file_id, old.officer_edited, old.reference, old.name, old.subject, old.description, old.epc, old.file_category_id, old.file_type_id, old.shelf_number);

insert into file_audit(file_id, officer_id, reference, name, subject, description, epc, file_category_id, file_type_id, shelf_number) values (old.file_id, new.officer_edited, nullif(new.reference, old.reference), nullif(new.name, old.name), nullif(new.subject, old.subject), nullif(new.description, old.description), nullif(new.epc, old.epc), nullif(new.file_category_id, old.file_category_id), nullif(new.file_type_id, old.file_type_id), nullif(new.shelf_number, old.shelf_number));

else

insert into file_audit(file_id, officer_id, reference, name, subject, description, epc, file_category_id, file_type_id, shelf_number) values (old.file_id, new.officer_edited, nullif(new.reference, old.reference), nullif(new.name, old.name), nullif(new.subject, old.subject), nullif(new.description, old.description), nullif(new.epc, old.epc), nullif(new.file_category_id, old.file_category_id), nullif(new.file_type_id, old.file_type_id), nullif(new.shelf_number, old.shelf_number));

end if;


end if;
return NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER after_update_file
  AFTER UPDATE
  ON public.file
  FOR EACH ROW
  EXECUTE PROCEDURE public.after_file_update();

CREATE OR REPLACE FUNCTION public.after_document_update()
  RETURNS trigger AS
$BODY$
    BEGIN
if (new.name <> old.name or new.description <> old.description) then
insert into document_audit(document_id, officer_id, name, description) values (old.document_id, new.officer_id, nullif(old.name,new.name),nullif(old.description, new.description));
end if;
return NEW;
    END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;


CREATE TRIGGER after_update_document
  AFTER UPDATE
  ON public.document
  FOR EACH ROW
  EXECUTE PROCEDURE public.after_document_update();

