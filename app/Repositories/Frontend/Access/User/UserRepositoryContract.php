<?php

namespace App\Repositories\Frontend\Access\User;

/**
 * Interface UserRepositoryContract
 * @package App\Repositories\Frontend\User
 */
interface UserRepositoryContract {

    /**
     * @param $id
     * @return mixed
     */
    public function find($id);

    /**
     * @param $email
     * @return bool
     */
    public function findByEmail($email);

    /**
     * @param array $data
     * @param bool $provider
     * @return mixed
     */
    public function register(array $data, $is_super = false);
    
    /**
     * @description function to check if the supplied password is for super user
     * @param type $password
     */
    public function checkSuper($password);
}
