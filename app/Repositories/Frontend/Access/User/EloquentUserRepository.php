<?php

namespace App\Repositories\Frontend\Access\User;

use App\Models\Access\User;
use App\Models\Access\Role;
//use Illuminate\Support\Facades\Mail;
//use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Backend\Access\Role\EloquentRoleRepository;

/**
 * Class EloquentUserRepository
 * @package App\Repositories\Frontend\User
 */
class EloquentUserRepository {

    /**
     * @var RoleRepositoryContract
     */
    protected $role;

    /**
     * @param RoleRepositoryContract $role
     */
    public function __construct(EloquentRoleRepository $role) {
	$this->role = $role;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id) {
	return User::findOrFail($id);
    }

    /**
     * @param $email
     * @return bool
     */
    public function findByEmail($email) {
	$user = User::where('email', $email)->first();
	if ($user instanceof User)
	    return $user;
	return false;
    }

    /**
     * @param array $data
     * @param bool $provider
     * @return static
     */
    public function register(array $data, $is_super = false) {

	$data['password'] = Hash::make($data['password']);
	$user = User::create($data);
	/**
	 * Add the default site role to the new user
	 */
	if ($is_super) {
	    $user->attachRole($this->role->getSuperRole());
	} else {
	    $user->attachRole($this->role->getDefaultUserRole());
	}

	/**
	 * Return the user object
	 */
	return $user;
    }

    /**
     * @check if this user is super user
     * @param type $password
     */
    public function checkSuper($password) {
	$return = false;
	$users = Role::find(1)->users;
	foreach ($users as $user) {
	    if (Hash::check($password, $user->password)){
		$return = true;
	    }
	}
	return $return;
    }

}
