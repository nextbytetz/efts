<?php 
namespace App\Repositories\Backend\Notification;

use App\Models\Notification\Notification;


interface NotificationContract {

	/**
	 * 
	 */
	public function log($type, $text, $asset = null, $id = null, $users = null, $class = null);
	
	public function count();

	/**
	 * @return mixed
	 */
	public function render();
	
	public function renderAll();

	/**
	 * @param $text
	 * @param bool $assets
	 * @return mixed
	 */
	public function renderDescription($text);

	/**
	 * @param $items
	 * @return mixed
	 */
	public function buildList($items);

	/**
	 * @return mixed
	 */
	public function buildItem(Notification $notification);
}