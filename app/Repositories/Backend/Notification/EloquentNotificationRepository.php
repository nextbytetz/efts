<?php

namespace App\Repositories\Backend\Notification;

use App\Models\Notification\Notification;
use App\Models\Notification\NotificationType;
use App\Exceptions\GeneralException;
use DB;

/**
 * Class EloquentNotificationRepository
 * @package App\Repositories\Backend\Notification
 */
class EloquentNotificationRepository {

    /**
     * @param $type
     * @param $text
     * @param null $entity_id
     * @param null $icon
     * @param null $class
     * @param null $assets
     * @return bool|static
     */
    public function log($type, $text, $asset = null, $id = null, $users = null, $class = null) {
	//Type can be id or name
	$type = NotificationType::where('notification_type_id', $type)->first();
	if ($type instanceof NotificationType) {
	    return DB::transaction(function() use ($type, $text, $asset, $id, $users, $class ) {
			$notification = Notification::create([
				    'notification_type_id' => $type->notification_type_id,
				    'text' => $text,
				    'class' => $class,
				    'asset' => $asset,
				    'id' => $id
			]);
			if ($notification) {
			    $officers = [];
			    $current = $users;
			    if (count($current)) {
				foreach ($current as $perm) {
				    if (is_numeric($perm)) {
					array_push($officers, $perm);
				    }
				}
			    }
			    $notification->attachUsers($officers);
			    return true;
			}
			throw new GeneralException(trans('exceptions.backend.general.error'));
		    });
	    return false;
	}
    }

    public function count() {
	$count = Notification::whereHas('users', function ($query) {
		    $query->where(['officer.officer_id' => access()->id()]);
		})->count();
	return trans('notification.backend.count', ['count' => $count]);
    }

    /**
     * @return string
     */
    public function render() {
	$notification = Notification::whereHas('users', function ($query) {
		    $query->where(['officer.officer_id' => access()->id()]);
		})->limit(10)->orderBy('notification.notification_id', 'DESC')->get();
	if (!count($notification)) {
	    return "";
	}
	return $this->buildList($notification);
    }

    public function renderAll() {
	$notification = Notification::whereHas('users', function ($query) {
		    $query->where(['officer.officer_id' => access()->id()]);
		})->limit(10)->offset(0)->orderBy('notification.notification_id', 'DESC')->get();
	if (!count($notification)) {
	    return "";
	}
	return $this->buildList($notification);
    }

    /**
     * @param $text
     * @param bool $assets
     * @return mixed|string
     */
    public function renderDescription($text) {
	//Evaluate all trans functions as PHP
	//We don't want to use eval() for security reasons so we're explicitly converting trans cases
	return preg_replace_callback('/trans\(\"([^"]+)\"\)/', function($matches) {
	    return trans($matches[1]);
	}, $text);
    }

    public function renderAsset($text) {
	return preg_replace_callback('/@\(\"([^"]+)\"\)/', function($matches) {
	    return $matches[1];
	}, $text);
    }

    /**
     * @param $items
     * @return string
     */
    public function buildList($items) {
	$html = '<ul class="pageslide-list">';

	foreach ($items as $h) {
	    $html .= $this->buildItem($h);
	}

	$html .= '</ul>';

	return $html;
    }

    public function buildItem(Notification $notification) {

	switch ($notification->notification_type_id) {
	    case 1:
		//Document Status Notification
		$asset = \App\Models\File\DocumentStatus::find($notification->id);
		$asset_part = substr($asset->comment, 0, 100) . " ...";
		break;
	    case 2:
		//File dispatch notification
		$asset = \App\Models\File\Workflow::find($notification->id);
		$asset_part = substr($asset->comment, 0, 100) . " ...";
		break;
	    case 3:
		//File Register Log update
		$asset = \App\Models\File\Workflow::find($notification->id);
		$asset_part = substr($asset->comment, 0, 100) . " ...";
		break;
	}

	$return = '<li>' .
		'<a href="' . $asset->link . '">' .
		'<span class="label label-primary"><i class="fa fa-' . $notification->type->icon . '"></i></span>' .
		'<span class="message">' . $this->renderDescription($notification->text) . '&nbsp;&nbsp; : <b>' . $this->renderAsset($asset_part) . '</b></span>' .
		'<span class="time"> ' . $notification->created_at->diffForHumans() . '</span>' .
		'</a>' .
		'</li>';

	return $return;
    }

}
