<?php

namespace App\Repositories\Backend\Sysdef\File;

use App\Exceptions\GeneralException;
use App\Repositories\Repository;
use App\Models\File\FileSubject;
use Illuminate\Database\Eloquent\Model;
use DB;

class SubjectRepository extends Repository {

    /**
     * Associated Repository Model
     */
    const MODEL = FileSubject::class;

    public function getForDataTable() {
	return $this->get();
    }
    
    public function getAllSubject() {
	return $this->query()->pluck('name', 'file_subject_id')->all();
    }

    public function create($input) {
	$names = $input["name"];
	foreach ($names as $name) {
	    if (!$this->checkIfExist($name)) {
		$subject = $this->createSubjectStub(['name' => $name]);
		if (!parent::save($subject)) {
		    throw new GeneralException(trans('exceptions.backend.general.error'));
		}
	    }
	}
    }

    public function edit(Model $subject, $input) {
	if ($subject->name != $input['name']) {
	    if ($this->checkIfExist($input['name'])) {
		throw new GeneralException(trans('exceptions.backend.sysdef.file.subject.exist'));
	    }
	}
	$data = ['name' => $input['name']];
	if (!parent::update($subject, $data)) {
	    throw new GeneralException(trans('exceptions.backend.general.error'));
	}
    }

    public function destroy(Model $subject) {
	if ($subject->cases()->count()) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.file.subject.has_cases'));
	}
	parent::delete($subject);
    }

    private function checkIfExist($name) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $name)));
	if ($this->query()->whereRaw("lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) = :name", ['name' => $name])->first()) {
	    return true;
	} else {
	    return false;
	}
    }

    protected function createSubjectStub($input) {
	$subject = self::MODEL;
	$subject = new $subject;
	$subject->name = $input['name'];
	return $subject;
    }

}
