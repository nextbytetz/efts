<?php

namespace App\Repositories\Backend\Sysdef\File;

use App\Exceptions\GeneralException;
use App\Repositories\Repository;
use App\Models\File\FileCase;
use Illuminate\Database\Eloquent\Model;
use DB;

class CaseRepository extends Repository {

    /**
     * Associated Repository Model
     */
    const MODEL = FileCase::class;

    public function getForDataTable() {
	return $this->get();
    }

    public function create($input) {
	$names = $input["name"];
	foreach ($names as $name) {
	    if (!$this->checkIfExist($name, $input['file_subject_id'])) {
		$case = $this->createCaseStub(['name' => $name, 'file_subject_id' => $input['file_subject_id']]);
		if (!parent::save($case)) {
		    throw new GeneralException(trans('exceptions.backend.general.error'));
		}
	    }
	}
    }

    public function edit(Model $case, $input) {
	if ($case->name != $input['name'] Or $case->file_subject_id != $input['file_subject_id']) {
	    if ($this->checkIfExist($input['name'], $input['file_subject_id'])) {
		throw new GeneralException(trans('exceptions.backend.sysdef.file.case.exist'));
	    }
	}
	$data = ['name' => $input['name'], 'file_subject_id' => $input['file_subject_id']];
	if (!parent::update($case, $data)) {
	    throw new GeneralException(trans('exceptions.backend.general.error'));
	}
    }
    
    
    public function destroy(Model $case) {
	parent::delete($case);
    }

    private function checkIfExist($name, $file_subject_id) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $name)));
	if ($this->query()->whereRaw("lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) = :name and file_subject_id = :file_subject_id", ['name' => $name, 'file_subject_id' => $file_subject_id])->first()) {
	    return true;
	} else {
	    return false;
	}
    }

    protected function createCaseStub($input) {
	$case = self::MODEL;
	$case = new $case;
	$case->name = $input['name'];
	$case->file_subject_id = $input['file_subject_id'];
	return $case;
    }

}
