<?php

namespace App\Repositories\Backend\Sysdef\RFID\Zone;

use App\Models\Location\Zone;
use App\Exceptions\GeneralException;
use DB;

class EloquentZoneRepository {

    public function findOrThrowException($id) {

	$zone = Zone::find($id);

	if (!is_null($zone)) {
	    return $zone;
	}
	throw new GeneralException(trans('exceptions.backend.sysdef.rfid.zone.not_found'));
    }

    public function getZonesForDataTable() {
	return Zone::all();
    }

    public function create($input) {
	if ($this->checkNameIfExist($input['name'])) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.rfid.zone.exist'));
	}
	if ($this->checkReaderIfExist($input['reader'], $input['antenna'])) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.rfid.zone.reader_exist'));
	}
	return DB::transaction(function() use ($input) {
		    $zone = Zone::create($input);
		    if ($zone) {
			$zone->attachDepartments($input['departments']);
			return $zone->zone_id;
		    }
		    throw new GeneralException(trans('exceptions.backend.general.error'));
		});
    }

    public function update($id, $input) {
	$zone = $this->findOrThrowException($id);
	$this->checkZoneByName($input, $zone);
	$this->checkReaderByName($input, $zone);
	return DB::transaction(function() use ($input, $id, $zone) {
		    Zone::where(['zone_id' => $id])->update(['name' => $input['name'], 'reader' => $input['reader'], 'antenna' => $input['antenna']]);
		    $zone->departments()->sync([]);
		    $zone->attachDepartments($input['departments']);
		    return $zone->zone_id;
		});
    }

    public function destroy($id) {
	Zone::destroy($id);
	//throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    private function checkZoneByName($input, $zone) {
	//Figure out if name is not the same
	if ($zone->name != $input['name']) {
	    //Check to see if name exists
	    if ($this->checkNameIfExist($input['name']))
		throw new GeneralException(trans('exceptions.backend.sysdef.rfid.zone.exist'));
	}
    }

    private function checkReaderByName($input, $zone) {
	if ($zone->reader != $input['reader']) {
	    if ($this->checkReaderIfExist($input['reader'], $input['antenna']))
		throw new GeneralException(trans('exceptions.backend.sysdef.rfid.zone.reader_exist'));
	}
    }

    private function checkNameIfExist($name) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $name)));
	if (Zone::whereRaw("lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) = :name", ['name' => $name])->first()) {
	    //throw new GeneralException(trans('exceptions.backend.sysdef.location.department.exist'));
	    return true;
	} else {
	    return false;
	}
    }

    private function checkReaderIfExist($reader, $antenna) {
	$reader = strtolower(trim(preg_replace('/\s+/', '', $reader)));
	$antenna = strtolower(trim(preg_replace('/\s+/', '', $antenna)));
	if (Zone::whereRaw("lower(regexp_replace(cast(reader as varchar), '\s+', '', 'g')) = :reader and antenna = :antenna", ['reader' => $reader, 'antenna' => $antenna])->first()) {
	    //throw new GeneralException(trans('exceptions.backend.sysdef.location.department.exist'));
	    return true;
	} else {
	    return false;
	}
    }

}
