<?php

namespace App\Repositories\Backend\Sysdef\RFID\Zone;

interface ZoneRepositoryContract {
    
    public function findOrThrowException($id);

    public function getZonesForDataTable();
    
    public function create($input);
    
    public function update($id, $input);
    
    public function destroy($id);

}