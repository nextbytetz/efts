<?php

namespace App\Repositories\Backend\Sysdef\Location\Region;

use App\Models\Location\Region;
use App\Exceptions\GeneralException;

class EloquentRegionRepository {

    public function __construct() {
	
    }

    public function findOrThrowException($id) {

	$region = Region::find($id);

	if (!is_null($region)) {
	    return $region;
	}
	throw new GeneralException(trans('exceptions.backend.sysdef.location.region.not_found'));
    }

    public function getForDataTable() {
	return Region::get();
    }

    public function getAllRegion() {
	return Region::pluck('name', 'region_id');
    }
    
    public function getCountryRegion($id) {
	return Region::where('country_id', '=', $id)->pluck('name', 'region_id');
    }

    public function getRegion() {
	return Region::with('branches.departments')->orderBy('name', 'asc')->get();
    }

    public function create($input) {
	$names = $input["name"];
	foreach ($names as $name) {
	    if (!$this->checkIfExist($name, $input['country_id'])) {
		$region = Region::create(['name' => $name, 'country_id' => $input['country_id']]);
		if (!$region)
		    throw new GeneralException(trans('exceptions.backend.general.error'));
	    }    
	}
    }

    public function destroy($region_id) {
	$region = $this->findOrThrowException($region_id);
	//Don't delete the region if there are departments associated
	if ($region->branches()->count() > 0) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.location.region.has_branches'));
	}
	if ($region->delete()) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function update($region_id, $input) {
	$region = $this->findOrThrowException($region_id);
	$this->checkRegionByName($input, $region);
	if ($region->update($input)) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    private function checkRegionByName($input, $region) {
	//Figure out if name is not the same
	if ($region->name != $input['name']) {
	    //Check to see if name exists
	    if ($this->checkIfExist($input['name'], $input['country_id']))
		    throw new GeneralException(trans('exceptions.backend.sysdef.location.region.exist'));
	}
    }

    private function checkIfExist($name, $country_id) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $name)));
	if (Region::whereRaw("lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) = :name and country_id = :country_id", ['name' => $name, 'country_id' => $country_id])->first()) {
	    //throw new GeneralException(trans('exceptions.backend.sysdef.location.region.exist'));
	    return true;
	} else {
	    return false;
	}
    }

}
