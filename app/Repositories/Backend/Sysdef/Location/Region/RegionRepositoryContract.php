<?php

namespace App\Repositories\Backend\Sysdef\Location\Region;

interface RegionRepositoryContract {
    
    public function findOrThrowException($id);
    
    public function getForDataTable();
    
    public function getAllRegion();
    
    public function getCountryRegion($id);
    
    public function getRegion();
    
    public function create($input);
    
    public function destroy($region_id);
    
    public function update($region_id, $input);
    
}