<?php

namespace App\Repositories\Backend\Sysdef\Location\Country;

use App\Models\Location\Country;
use App\Models\Location\CountryAll;
use App\Exceptions\GeneralException;

class EloquentCountryRepository {

    public function __construct() {
	
    }

    public function findOrThrowException($id) {

	$country = Country::find($id);

	if (!is_null($country)) {
	    return $country;
	}
	throw new GeneralException(trans('exceptions.backend.sysdef.location.country.not_found'));
    }

    public function getForDataTable() {
	return Country::get();
    }

    public function getAllCountry() {
	return CountryAll::whereNotIn('country_all_id', Country::pluck('country_id'))->pluck('name', 'country_all_id');
    }

    public function getRegisteredCountry() {
	return Country::pluck('name', 'country_id');
    }

    public function getCountry() {
	return Country::with('regions.branches.departments')->orderBy('name', 'asc')->get();
    }

    public function create($input) {
	$country = new Country;
	$countryAll = CountryAll::find($input['country_id']);
	$country->name = $countryAll->name;
	$country->country_id = $countryAll->country_all_id;
	if ($country->save()) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function destroy($country_id) {
	$country = $this->findOrThrowException($country_id);
	//Don't delete the country if there are regions associated
	if ($country->regions()->count() > 0) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.location.country.has_regions'));
	}
	if ($country->delete()) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

}
