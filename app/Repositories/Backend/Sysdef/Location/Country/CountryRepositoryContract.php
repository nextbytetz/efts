<?php

namespace App\Repositories\Backend\Sysdef\Location\Country;

interface CountryRepositoryContract {
    
    public function findOrThrowException($id);
    
    public function getForDataTable();
    
    public function getAllCountry();
    
    public function getRegisteredCountry();
    
    public function getCountry();
    
    public function create($input);
    
    public function destroy($country_id);
    
}