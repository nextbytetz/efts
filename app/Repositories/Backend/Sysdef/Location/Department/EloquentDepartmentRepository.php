<?php

namespace App\Repositories\Backend\Sysdef\Location\Department;

use App\Models\Location\Department;
use App\Exceptions\GeneralException;
use DB;

class EloquentDepartmentRepository {

    public function __construct() {
	
    }

    public function findOrThrowException($id) {

	$department = Department::find($id);

	if (!is_null($department)) {
	    return $department;
	}
	throw new GeneralException(trans('exceptions.backend.sysdef.location.department.not_found'));
    }

    public function getForDataTable() {
	return Department::get();
    }

    public function getAllDepartment() {
	return Department::pluck('name', 'department_id');
    }

    public function getBranchDepartment($id) {
	return Department::where('branch_id', '=', $id)->pluck('name', 'department_id');
    }

    public function create($input) {
	$names = $input["name"];
	foreach ($names as $name) {
	    if (!$this->checkIfExist($name, $input['branch_id'])) {
		$department = Department::create(['name' => $name, 'branch_id' => $input['branch_id']]);
		//add department registration to all public files which has been registered to the branch of this department.
		DB::statement("insert into file_department (file_id, department_id) select file_id, :department_id as department_id from file where file_id in (select file_id from file_department where department_id in (select department_id from department where branch_id = :branch_id))", ['department_id' => $department->department_id, 'branch_id' => $department->branch->branch_id]);
		if (!$department)
		    throw new GeneralException(trans('exceptions.backend.general.error'));
	    }
	}
    }

    public function destroy($department_id) {
	$department = $this->findOrThrowException($department_id);
	//Don't delete the region if there are departments associated
	if ($department->users()->count() > 0) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.location.department.has_users'));
	}
	if ($department->files()->count() > 0) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.location.department.has_files'));
	}
	if ($department->delete()) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function update($department_id, $input) {
	$department = $this->findOrThrowException($department_id);
	$this->checkDepartmentByName($input, $department);
	if ($department->update($input)) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function getBranchDepartments($branch_id, $q) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $q)));
	$data['items'] = DB::select("select department_id, name from department where lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) like :name and branch_id = :branch_id order by department_id asc", ['name' => "%$name%", 'branch_id' => $branch_id]);
	$data['total_count'] = count($data['items']);
	return response()->json($data);
    }

    private function checkDepartmentByName($input, $department) {
	//Figure out if name is not the same
	if ($department->name != $input['name']) {
	    //Check to see if name exists
	    if ($this->checkIfExist($input['name'], $input['branch_id']))
		throw new GeneralException(trans('exceptions.backend.sysdef.location.department.exist'));
	}
    }

    private function checkIfExist($name, $branch_id) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $name)));
	if (Department::whereRaw("lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) = :name and branch_id = :branch_id", ['name' => $name, 'branch_id' => $branch_id])->first()) {
	    //throw new GeneralException(trans('exceptions.backend.sysdef.location.department.exist'));
	    return true;
	} else {
	    return false;
	}
    }

}
