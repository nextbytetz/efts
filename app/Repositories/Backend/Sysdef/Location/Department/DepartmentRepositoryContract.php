<?php

namespace App\Repositories\Backend\Sysdef\Location\Department;

interface DepartmentRepositoryContract {

    public function findOrThrowException($id);

    public function getForDataTable();

    public function getAllDepartment();

    public function create($input);

    public function destroy($department_id);

    public function update($department_id, $input);
    
    public function getBranchDepartments($branch_id, $q);
    
}
