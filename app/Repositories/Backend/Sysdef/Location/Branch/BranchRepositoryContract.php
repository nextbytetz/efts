<?php

namespace App\Repositories\Backend\Sysdef\Location\Branch;

interface BranchRepositoryContract {

    public function findOrThrowException($id);

    public function getForDataTable();

    public function getAllBranch();
    
    public function getRegionBranch($id);

    public function create($input);

    public function destroy($branch_id);

    public function update($branch_id, $input);
}
