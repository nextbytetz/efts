<?php

namespace App\Repositories\Backend\Sysdef\Location\Branch;

use App\Models\Location\Branch;
use App\Exceptions\GeneralException;

class EloquentBranchRepository {

    public function __construct() {
	
    }

    public function findOrThrowException($id) {

	$branch = Branch::find($id);

	if (!is_null($branch)) {
	    return $branch;
	}
	throw new GeneralException(trans('exceptions.backend.sysdef.location.branch.not_found'));
    }

    public function getForDataTable() {
	return Branch::get();
    }

    public function getAllBranch() {
	return Branch::pluck('name', 'branch_id');
    }
    
    public function getRegionBranch($id) {
	return Branch::where('region_id', '=', $id)->pluck('name', 'branch_id');
    }

    public function create($input) {
	$names = $input["name"];
	foreach ($names as $name) {
	    if (!$this->checkIfExist($name, $input['region_id'])) {
		$branch = Branch::create(['name' => $name, 'region_id' => $input['region_id']]);
		if (!$branch)
		    throw new GeneralException(trans('exceptions.backend.general.error'));
	    }
	}
    }

    public function destroy($branch_id) {
	$branch = $this->findOrThrowException($branch_id);
	//Don't delete the region if there are departments associated
	if ($branch->departments()->count() > 0) {
	    throw new GeneralException(trans('exceptions.backend.sysdef.location.branch.has_departments'));
	}
	if ($branch->delete()) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function update($branch_id, $input) {
	$branch = $this->findOrThrowException($branch_id);
	$this->checkBranchByName($input, $branch);
	if ($branch->update($input)) {
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    private function checkBranchByName($input, $branch) {
	//Figure out if name is not the same
	if ($branch->name != $input['name']) {
	    //Check to see if name exists
	    if ($this->checkIfExist($input['name'], $input['region_id'])) {
		throw new GeneralException(trans('exceptions.backend.sysdef.location.branch.exist'));
	    }
	}
    }

    private function checkIfExist($name, $region_id) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $name)));
	if (Branch::whereRaw("lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) = :name and region_id = :region_id", ['name' => $name, 'region_id' => $region_id])->first()) {
	    //throw new GeneralException(trans('exceptions.backend.sysdef.location.branch.exist'));
	    return true;
	} else {
	    return false;
	}
    }

}
