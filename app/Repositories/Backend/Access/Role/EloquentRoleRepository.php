<?php

namespace App\Repositories\Backend\Access\Role;

use App\Models\Access\Role;
use App\Events\Backend\Access\RoleEvent;
use App\Exceptions\GeneralException;
use DB;

/**
 * Class EloquentRoleRepository
 * @package App\Repositories\Role
 */
class EloquentRoleRepository {

    /**
     * @param  $id
     * @param  bool $withPermissions
     * @throws GeneralException
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null|static
     */
    public function findOrThrowException($id, $withPermissions = false) {
	if (!is_null(Role::find($id))) {
	    if ($withPermissions) {
		return Role::with('permissions')
				->find($id);
	    }

	    return Role::find($id);
	}

	throw new GeneralException(trans('exceptions.backend.access.roles.not_found'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getForDataTable() {
	return Role::all();
    }

    /**
     * @param  $per_page
     * @param  string      $order_by
     * @param  string      $sort
     * @return mixed
     */
    public function getRolesPaginated($per_page, $order_by = 'sort', $sort = 'asc') {
	return Role::with('permissions')
			->orderBy($order_by, $sort)
			->paginate($per_page);
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @param  bool    $withPermissions
     * @return mixed
     */
    public function getAllRoles($order_by = 'name', $sort = 'asc', $withPermissions = false) {
	if ($withPermissions) {
	    if (access()->hasRole('super')) {
		$return = Role::with('permissions')
			->orderBy($order_by, $sort)
			->get();
	    } else {
		$return = Role::with('permissions')
			->where('is_super', '<>', '1')
			->orderBy($order_by, $sort)
			->get();
	    }
	} else {
	    if (access()->hasRole('super')) {
		$return = Role::orderBy($order_by, $sort)
			->get();
	    } else {
		$return = Role::where('is_super', '<>', '1')
			->orderBy($order_by, $sort)
			->get();
	    }
	}

	return $return;
    }

    /**
     * @param  $input
     * @throws GeneralException
     * @return bool
     */
    public function create($input) {
	if (Role::where('name', $input['name'])->first()) {
	    throw new GeneralException(trans('exceptions.backend.access.roles.already_exists'));
	}

	$role = new Role;
	$role->name = $input['name'];

	DB::transaction(function() use ($role, $input) {
	    if ($role->save()) {
		$current = explode(',', $input['permissions']);
		$permissions = [];

		if (count($current)) {
		    foreach ($current as $perm) {
			if (is_numeric($perm)) {
			    array_push($permissions, $perm);
			}
		    }
		}
		$role->attachPermissions($permissions);
		event(new RoleEvent($role, 'create_role'));
		return true;
	    }
	    throw new GeneralException(trans('exceptions.backend.access.roles.create_error'));
	});
    }

    /**
     * @param  $id
     * @param  $input
     * @throws GeneralException
     * @return bool
     */
    public function update($id, $input) {
	$role = $this->findOrThrowException($id);

	//See if the role must contain a permission as per config
	if (count($input['permissions']) == 0) {
	    throw new GeneralException(trans('exceptions.backend.access.roles.needs_permission'));
	}

	$role->name = $input['name'];
	DB::transaction(function() use ($role, $input) {
	    if ($role->save()) {

		//Remove all roles first
		$role->permissions()->sync([]);

		//Attach permissions if the role does not have all access
		$current = explode(',', $input['permissions']);
		$permissions = [];

		if (count($current)) {
		    foreach ($current as $perm) {
			if (is_numeric($perm)) {
			    array_push($permissions, $perm);
			}
		    }
		}
		$role->attachPermissions($permissions);
		event(new RoleEvent($role, 'edit_role'));
		return true;
	    }
	    throw new GeneralException(trans('exceptions.backend.access.roles.update_error'));
	});
    }

    /**
     * @param  $id
     * @throws GeneralException
     * @return bool
     */
    public function destroy($id) {

	$role = $this->findOrThrowException($id, true);

	//Don't delete the role is there are users associated
	if ($role->users()->count() > 0) {
	    throw new GeneralException(trans('exceptions.backend.access.roles.has_users'));
	}

	//Detach all associated roles
	$role->permissions()->sync([]);

	if ($role->delete()) {
	    event(new RoleEvent($role, 'delete_role'));
	    return true;
	}

	throw new GeneralException(trans('exceptions.backend.access.roles.delete_error'));
    }

    /**
     * @return mixed
     */
    public function getDefaultUserRole() {
	/* to be changed in furure to find the exactly officer role from the database, 
	 * , currently i assume the database table for default roles will remain unchanged.
	 *  role_id 3 is for officer */
	return Role::where('role_id', 3)->first();
    }

    public function getSuperRole() {
	/* role_id 1 is for super */
	return Role::where('role_id', 1)->first();
    }

}
