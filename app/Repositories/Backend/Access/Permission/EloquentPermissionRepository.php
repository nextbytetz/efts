<?php

namespace App\Repositories\Backend\Access\Permission;

use App\Exceptions\GeneralException;
use App\Models\Access\Permission;

/**
 * Class EloquentPermissionRepository
 * @package App\Repositories\Permission
 */
class EloquentPermissionRepository  {

    /**
     * @param  $id
     * @param  bool                                                                             $withRoles
     * @throws GeneralException
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Support\Collection|null|static
     */
    public function findOrThrowException($id, $withRoles = false) {
	if (!is_null(Permission::find($id))) {
	    if ($withRoles) {
		return Permission::with('roles')->find($id);
	    }

	    return Permission::find($id);
	}

	throw new GeneralException(trans('exceptions.backend.access.permissions.not_found'));
    }

    /**
     * @param  string  $order_by
     * @param  string  $sort
     * @param  bool    $withRoles
     * @return mixed
     */
    public function getAllPermissions($order_by = 'display_name', $sort = 'asc', $withRoles = true) {
	if ($withRoles) {
	    return Permission::with('roles', 'dependencies.permission', 'backDependencies.backPermission')->orderBy($order_by, $sort)->get();
	}

	return Permission::with('dependencies.permission', 'backDependencies.backPermission')->orderBy($order_by, $sort)->get();
    }

}
