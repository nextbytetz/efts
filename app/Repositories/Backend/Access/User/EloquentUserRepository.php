<?php

namespace App\Repositories\Backend\Access\User;

use App\Models\Access\User;
use App\Events\Backend\Access\UserEvent;
use App\Exceptions\GeneralException;
use App\Services\Storage\Traits\AttachmentHandler;
use App\Models\Location\CountryAll;
use DB;
use Hash;

/**
 * Class EloquentUserRepository
 * @package App\Repositories\User
 */
class EloquentUserRepository {

    use AttachmentHandler;

    /**
     * @param  $id
     * @param  bool               $withRoles
     * @throws GeneralException
     * @return mixed
     */
    public function findOrThrowException($id, $withRoles = false) {
	if ($withRoles) {
	    $user = User::with('roles')->find($id);
	} else {
	    $user = User::find($id);
	}

	if (!is_null($user)) {
	    return $user;
	}
	throw new GeneralException(trans('exceptions.backend.access.users.not_found'));
    }

    /**
     * @param int $status
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($column, $value) {
	if (access()->hasRoles(['super'], false)) {
	    $user = User::select(['officer_id', 'department_id', 'firstname', 'othernames', 'username', 'checked', 'active', 'available', 'created_at', 'updated_at'])->where($column, $value)->orderBy('checked', 'desc')->get();
	} else {
	    $user = User::select(['officer_id', 'department_id', 'firstname', 'othernames', 'username', 'checked', 'active', 'available', 'created_at', 'updated_at'])->where([$column => $value, 'department_id' => access()->user()->department_id])->orderBy('checked', 'desc')->get();
	}
	return $user;
    }

    /**
     * @param $id
     * @param $input
     * @param $roles
     * @param $permissions
     * @return bool
     * @throws GeneralException
     */
    public function update($id, $input, $roles, $permissions) {
	$user = $this->findOrThrowException($id);
	$this->checkUserByEmail($input['email'], $user);

	DB::transaction(function() use ($user, $input, $roles, $permissions) {
	    if ($user->update($input)) {
		if (!$user->hasRole('super') Or ( access()->hasRole('super') And access()->id() != $user->officer_id)) {
		    $this->checkUserRolesCount($roles);
		    $this->flushRoles($roles, $user);
		    $this->flushPermissions($permissions, $user);
		}
		event(new UserEvent($user, 'edit_user'));
		return true;
	    }
	    throw new GeneralException(trans('exceptions.backend.access.users.update_error'));
	});
    }

    public function approve($id) {
	$user = $this->findOrThrowException($id);
	$user->checked = 1;
	$user->available = 1;
	$user->checked_date = DB::raw("now()");
	if ($user->save()) {
	    event(new UserEvent($user, 'approve_user'));
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function activate($id, $active) {
	$user = $this->findOrThrowException($id);
	$user->active = $active;
	$user->available = $active;
	if ($user->save()) {
	    ($active == 1) ? event(new UserEvent($user, 'activate_user')) : event(new UserEvent($user, 'deactivate_user'));
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    public function out($id, $available) {
	$user = $this->findOrThrowException($id);
	$user->available = $available;
	if ($user->save()) {
	    ($available == 1) ? event(new UserEvent($user, 'mark_in_office')) : event(new UserEvent($user, 'mark_out_of_office'));
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.general.error'));
    }

    /**
     * @param  $id
     * @param  $input
     * @throws GeneralException
     * @return bool
     */
    public function updatePassword($id, $input) {
	$user = $this->findOrThrowException($id);
	$user->password = bcrypt($input['password']);
	if ($user->save()) {
	    event(new UserEvent($user, 'change_password'));
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.access.users.update_password_error'));
    }

    /**
     * @param  User $user
     * @throws GeneralException
     * @return bool
     */
    public function destroy($id) {
	$user = $this->findOrThrowException($id);

	if ($user->delete()) {
	    event(new UserEvent($user, 'delete_user'));
	    return true;
	}
	throw new GeneralException(trans('exceptions.backend.access.users.delete_error'));
    }

    public function getNewUsers() {
	return User::NewUsers()->first();
    }

    public function approveGroup($group_choice) {
	$arr_list = explode(",", $group_choice);
	foreach ($arr_list as $value) {
	    $this->approve($value);
	}
    }

    public function deleteGroup($group_choice) {
	$arr_list = explode(",", $group_choice);
	foreach ($arr_list as $value) {
	    $this->destroy($value);
	}
    }

    public function updateProfile($input) {
	$user = $this->findOrThrowException(access()->id());
	$this->checkUserByEmail($input['email'], $user);
	$this->checkUserByUsername($input['username'], $user);
	$this->checkUserByPhone($input['phone'], $user);

	$country_all = CountryAll::find($input['country_id']);
	$phone = trim(preg_replace('/\s+/', '', phone($input['phone'], $country_all->code)));
	User::where('officer_id', '=', access()->id())->update(['firstname' => $input['firstname'], 'othernames' => $input['othernames'], 'username' => $input['username'], 'email' => $input['email'], 'phone' => $phone, 'gender' => $input['gender']]);
	$this->saveProfileAttachment($user);
    }

    public function changePassword($input) {
	$user = $this->findOrThrowException(access()->id());
	if (Hash::check($input['old_password'], $user->password)) {
	    $user->password = Hash::make($input['password']);
	    $user->save();
	} else {
	    throw new GeneralException(trans('exceptions.backend.access.users.current_password_error'));
	}
    }

    /**
     * @param  $input
     * @param  $user
     * @throws GeneralException
     */
    private function checkUserByEmail($email, $user) {
	//Figure out if email is not the same
	if ($user->email != $email) {
	    //Check to see if email exists
	    $email = strtolower(trim(preg_replace('/\s+/', '', $email)));
	    if (User::whereRaw("lower(regexp_replace(cast(email as varchar), '\s+', '', 'g')) = :email", ['email' => $email])->first()) {
		throw new GeneralException(trans('exceptions.backend.access.users.email_error'));
	    }
	}
    }

    private function checkUserByUsername($username, $user) {
	//Figure out if email is not the same
	if ($user->username != $username) {
	    //Check to see if email exists
	    $username = strtolower(trim(preg_replace('/\s+/', '', $username)));
	    if (User::whereRaw("lower(regexp_replace(cast(username as varchar), '\s+', '', 'g')) = :username", ['username' => $username])->first()) {
		throw new GeneralException(trans('exceptions.backend.access.users.username_error'));
	    }
	}
    }

    private function checkUserByPhone($phone, $user) {
	//Figure out if email is not the same
	if ($user->phone != $phone) {
	    //Check to see if email exists
	    $phone = strtolower(trim(preg_replace('/\s+/', '', $phone)));
	    if (User::whereRaw("lower(regexp_replace(cast(phone as varchar), '\s+', '', 'g')) = :phone", ['phone' => $phone])->first()) {
		throw new GeneralException(trans('exceptions.backend.access.users.phone_error'));
	    }
	}
    }

    /**
     * @param $roles
     * @param $user
     */
    private function flushRoles($roles, $user) {
	//Flush roles out, then add array of new ones
	$user->detachRoles($user->roles);
	$user->attachRoles($roles['assignees_roles']);
    }

    /**
     * @param $permissions
     * @param $user
     */
    private function flushPermissions($permissions, $user) {
	//Flush permissions out, then add array of new ones if any
	$user->detachPermissions($user->permissions);
	if (count($permissions['permission_user']) > 0) {
	    $user->attachPermissions($permissions['permission_user']);
	}
    }

    /**
     * @param  $roles
     * @throws GeneralException
     */
    private function checkUserRolesCount($roles) {
	//User Updated, Update Roles
	//Validate that there's at least one role chosen
	if (count($roles['assignees_roles']) == 0) {
	    throw new GeneralException(trans('exceptions.backend.access.users.role_needed'));
	}
    }

}
