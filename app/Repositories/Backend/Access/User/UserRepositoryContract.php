<?php

namespace App\Repositories\Backend\Access\User;

use App\Models\Access\User;

/**
 * Interface UserRepositoryContract
 * @package App\Repositories\User
 */
interface UserRepositoryContract {

    /**
     * @param  $id
     * @param  bool               $withRoles
     * @throws GeneralException
     * @return mixed
     */
    public function findOrThrowException($id, $withRoles = false);

    /**
     * @param int $status
     * @param bool $trashed
     * @return mixed
     */
    public function getForDataTable($column, $value);

    /**
     * @param User $user
     * @param $input
     * @param $roles
     * @return mixed
     */
    public function update($id, $input, $roles, $permissions);

    public function approve($id);

    public function activate($id, $active);
    
    public function out($id, $available);

    /**
     * @param  User $user
     * @param  $input
     * @return mixed
     */
    public function updatePassword($id, $input);

    /**
     * @param  User $user
     * @return mixed
     */
    public function destroy($id);
    
    public function getNewUsers();
    
    public function approveGroup($group_choice);
    
    public function deleteGroup($group_choice);
    
    public function updateProfile($input);
    
    public function changePassword($input);
    
}
