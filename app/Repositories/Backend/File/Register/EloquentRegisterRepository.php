<?php

namespace App\Repositories\Backend\File\Register;

use DB;
use Carbon\Carbon;
use App\Exceptions\GeneralException;
use App\Events\Backend\File\RegisterEvent;
use App\Models\File\File;
use App\Models\File\FileCategory;
use App\Models\File\FileType;
use App\Models\File\Folder;
use App\Models\File\Document;
use App\Models\File\Priority;
use App\Models\File\Bulk;
use App\Models\File\FileSubject;
use App\Models\File\FilePickup;
use App\Models\File\Workflow;
use App\Services\Storage\Traits\AttachmentHandler;

class EloquentRegisterRepository {

    use AttachmentHandler;

    public function findOrThrowException($id) {
	$file = File::find($id);
	if (!is_null($file)) {
	    // And $this->checkOwner($file)
	    return $file;
	}
	throw new GeneralException(trans('exceptions.backend.file.not_found'));
    }

    public function findFolder($id) {
	$folder = Folder::find($id);
	return $folder;
    }

    public function getForDataTable($search_text = "") {
	//get all registered files available for this user
	$department_id = access()->user()->department_id;
	if (!empty($department_id)) {
	    $officer_id = access()->id();
	    if ($search_text == "") {
		return File::whereRaw("file_id in (select file_id from file_department where department_id = :department_id union all select file_id from file_officer where officer_id = :officer_id )", ['department_id' => $department_id, 'officer_id' => $officer_id])->get();
	    } else {
		$search_text = strtolower(trim(preg_replace('/\s+/', '', $search_text)));
		return File::whereRaw("file_id in (select file_id from file_department where department_id = :department_id union all select file_id from file_officer where officer_id = :officer_id ) and (lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) like :search_text or lower(regexp_replace(cast(reference as varchar), '\s+', '', 'g')) like :search_text)", ['department_id' => $department_id, 'officer_id' => $officer_id, 'search_text' => "%{$search_text}%"])->get();
	    }
	} else {
	    return File::whereRaw("file_id = 0")->get();
	}
    }

    public function getMyFileForDataTable() {
	$officer_id = access()->id();
	return File::whereRaw("file_id in (select file_id from file_pickup where officer_id = :officer_id and disposed_at is null )", ['officer_id' => $officer_id])->get();
    }

    public function getReceivedFileForDataTable() {
	$officer_id = access()->id();
	return File::whereRaw("file_id in ( select a.file_id from workflow a join workflow_officer b on a.workflow_id = b.workflow_id left join file c on a.file_id = c.file_id left join file_pickup_active e on e.file_id = c.file_id join file_pickup d on a.workflow_id = d.workflow_id where b.officer_id = :officer_id and b.received_at is null and e.file_pickup_id is null and a.done = '0')", ['officer_id' => $officer_id])->get();
    }

    public function getFileRequestForDataTable() {
	$officer_id = access()->id();
	return File::whereRaw("file_id in (select file_id from file_pickup where officer_id = :officer_id and disposed_at is null ) and file_id in (select file_id from file_request where status = '0')", ['officer_id' => $officer_id])->get();
    }

    public function getBulkForDataTable() {
	return Bulk::all();
    }

    public function getFolderForDataTable($folder_id, $file_id) {
	//get all registered documents available for given file);
	//if folder_id is 0, it means the directory is at the root of the file
	if ($folder_id == 0) {
	    $file = File::find($file_id);
	    $folders = $file->folders;
	} else {
	    $folder = Folder::find($folder_id);
	    $folders = $folder->downfolder;
	}
	return $folders;
    }

    public function getAllFileType($file_type_id = 0) {
	switch ($file_type_id) {
	    case 3:
		return FileType::whereIn("file_type_id", [3])->pluck('name', 'file_type_id')->all();
	    case 2:
	    case 1:
		return FileType::whereIn("file_type_id", [1, 2])->pluck('name', 'file_type_id')->all();
	    default:
		return FileType::pluck('name', 'file_type_id')->all();
	}
    }

    public function getAllFileCategory() {
	return FileCategory::pluck('name', 'file_category_id')->all();
    }

    public function getAllPriority() {
	return Priority::pluck('name', 'priority_id')->all();
    }

    public function getAllSubject() {
	return FileSubject::pluck('name', 'file_subject_id')->all();
    }

    public function create($input) {
	$this->checkIfExist($input, 'name');
	if ($input['file_type_id'] == 1 Or $input['file_type_id'] == 2) {
	    $this->checkIfExist($input, 'reference');
	}
	$departments = [];
	$officers = [];
	if ($input['file_type_id'] == 1) {
	    //Public Files
	    if (empty($input['departments'])) {
		//Need to select departments
		return redirect()->back()
				->withFlashDanger(trans_choice('alerts.backend.file_register.no_departments', 2))
				->withInput($input);
	    }
	    $current = explode(',', $input['departments']);
	    if (count($current)) {
		foreach ($current as $perm) {
		    if (is_numeric($perm)) {
			array_push($departments, $perm);
		    }
		}
	    }
	} elseif ($input['file_type_id'] == 2) {
	    //Confidential files
	    if (!isset($input["officers"])) {
		//Need to select officers
		return redirect()->back()
				->withFlashDanger(trans_choice('alerts.backend.file_register.no_officers', 2))
				->withInput($input);
	    }
	    $current = $input["officers"];
	    if (count($current)) {
		foreach ($current as $perm) {
		    if (is_numeric($perm)) {
			array_push($officers, $perm);
		    }
		}
	    }
	} elseif ($input['file_type_id'] == 3) {
	    //Private files
	    $officers = [access()->id()];
	}

	if ($input['file_nature_id'] == 2) {
	    $input['epc'] = str_pad($input['epc'], 24, '0', STR_PAD_LEFT);
	}
	return DB::transaction(function() use ($input, $departments, $officers) {
		    $file = File::create($input);
		    if ($file) {
			if ($input['file_type_id'] == 1) {
			    //Public Files
			    $file->attachDepartments($departments);
			} elseif ($input['file_type_id'] == 2 Or $input['file_type_id'] == 3) {
			    //Confidential files or Private files
			    $file->attachUsers($officers);
			}
			event(new RegisterEvent($file, 'create_file'));
			//return $file->file_id;
			return $file->file_id;
		    }
		    throw new GeneralException(trans('exceptions.backend.general.error'));
		});
    }

    public function update($id, $input, $data) {
	$this->checkIfExist($input, 'name', $id);
	if ($input['file_type_id'] == 1 Or $input['file_type_id'] == 2) {
	    $this->checkIfExist($input, 'reference', $id);
	}
	$departments = [];
	$officers = [];
	if ($input['file_type_id'] == 1) {
	    //Public Files
	    if (empty($input['departments'])) {
		//Need to select departments
		return redirect()->back()
				->withFlashDanger(trans_choice('alerts.backend.file_register.no_departments', 1))
				->withInput($input);
	    }
	    $current = explode(',', $input['departments']);
	    if (count($current)) {
		foreach ($current as $perm) {
		    if (is_numeric($perm)) {
			array_push($departments, $perm);
		    }
		}
	    }
	} elseif ($input['file_type_id'] == 2) {
	    //Confidential files
	    if (!isset($input["officers"])) {
		//Need to select officers
		return redirect()->back()
				->withFlashDanger(trans_choice('alerts.backend.file_register.no_officers', 1))
				->withInput($input);
	    }
	    $current = $input["officers"];
	    if (count($current)) {
		foreach ($current as $perm) {
		    if (is_numeric($perm)) {
			array_push($officers, $perm);
		    }
		}
	    }
	} elseif ($input['file_type_id'] == 3) {
	    //Private files
	    $officers = [access()->id()];
	}

	if ($input['file_nature_id'] == 2) {
	    $input['epc'] = str_pad($input['epc'], 24, '0', STR_PAD_LEFT);
	}
	return DB::transaction(function() use ($input, $departments, $officers, $id, $data) {
		    $file = $this->findOrThrowException($id);
		    File::where(['file_id' => $id])->update($data);
		    //check if file is changed from notice to normal, then releases all the file pickups
		    if ($data['file_category_id'] != $file->file_category_id And $data['file_category_id'] = 1) {
			FilePickup::where(['file_id' => $id, 'disposed_at' => null])->update(['disposed_at' => Carbon::now(), 'released' => '1']);
			Workflow::where(['file_id' => $id])->update(['done' => '1']);
		    }
		    if ($file) {
			$file->users()->sync([]);
			$file->departments()->sync([]);
			if ($input['file_type_id'] == 1) {
			    //Public Files
			    $file->attachDepartments($departments);
			} elseif ($input['file_type_id'] == 2 Or $input['file_type_id'] == 3) {
			    //Confidential files or Private files
			    $file->attachUsers($officers);
			}

			event(new RegisterEvent($file, 'update_file'));
			//return $file->file_id;
			return $file->file_id;
		    }
		    throw new GeneralException(trans('exceptions.backend.general.error'));
		});
    }

    public function createFolder($file_id, $parent_id, $name) {
	$officer_id = access()->id();
	$folder = Folder::create(['file_id' => $file_id, 'parent_id' => $parent_id, 'name' => $name, 'officer_id' => $officer_id]);
	event(new RegisterEvent($folder, 'create_folder'));
	return $folder;
    }

    public function renameFolder($folder_id, $name) {
	$folder = Folder::find($folder_id);
	$old_name = $folder->name;
	$folder->name = $name;
	$folder->save();
	event(new RegisterEvent($folder, 'rename_folder', $old_name));
    }

    public function removeFolder($folder_id) {
	$folder = Folder::find($folder_id);
	$folder->delete();
	event(new RegisterEvent($folder, 'delete_folder'));
    }

    public function moveFile($id, $parent_id, $type) {
	if ($type == 'document') {
	    $document = Document::find($id);
	    $document->folder_id = $parent_id;
	    $document->save();
	} else if ($type == 'folder') {
	    $folder = Folder::find($id);
	    $folder->parent_id = $parent_id;
	    $folder->save();
	}
    }

    public function getRegisterOfficers($department, $q) {
	$departments = [];
	$department = explode(',', $department);
	if (count($department)) {
	    foreach ($department as $value) {
		if (is_numeric($value)) {
		    array_push($departments, $value);
		}
	    }
	}
	$department_list = implode(",", $departments);
	$name = strtolower(trim(preg_replace('/\s+/', '', $q)));
	$data['items'] = DB::select("select officer_id ,  firstname || ' ' || othernames as name from officer where (lower(regexp_replace(cast(firstname as varchar), '\s+', '', 'g')) like :name or lower(regexp_replace(cast(othernames as varchar), '\s+', '', 'g')) like :name) and department_id in ({$department_list}) order by officer_id asc", ['name' => "%$name%"]);
	$data['total_count'] = count($data['items']);
	return response()->json($data);
    }

    public function storeBulk($input) {
	$this->saveBulkAttachment($input);
    }

    public function saveBulk($data, $input) {
	$departments = [];
	$officers = [];
	if ($input['file_type_id'] == 1) {
	    //Public Files
	    if (empty($input['departments'])) {
		//Need to select departments
		return redirect()->back()
				->withFlashDanger(trans_choice('alerts.backend.file_register.no_departments', 2))
				->withInput($input);
	    }
	    $current = explode(',', $input['departments']);
	    if (count($current)) {
		foreach ($current as $perm) {
		    if (is_numeric($perm)) {
			array_push($departments, $perm);
		    }
		}
	    }
	} elseif ($input['file_type_id'] == 2) {
	    //Confidential files
	    if (!isset($input["officers"])) {
		//Need to select officers
		return redirect()->back()
				->withFlashDanger(trans_choice('alerts.backend.file_register.no_officers', 2))
				->withInput($input);
	    }
	    $current = $input["officers"];
	    if (count($current)) {
		foreach ($current as $perm) {
		    if (is_numeric($perm)) {
			array_push($officers, $perm);
		    }
		}
	    }
	}
	return DB::transaction(function() use ($data, $departments, $officers, $input) {
		    $bulk = Bulk::create($data);
		    if ($bulk) {
			if ($input['file_type_id'] == 1) {
			    //Public Files
			    $bulk->attachDepartments($departments);
			} elseif ($input['file_type_id'] == 2) {
			    //Confidential files
			    $bulk->attachUsers($officers);
			}
			event(new RegisterEvent($bulk, 'create_bulk_file'));
			return $bulk;
		    }
		    throw new GeneralException(trans('exceptions.backend.general.error'));
		});
    }

    public function searchAll($term) {
	$files = DB::select("select file_id as id, name as label, name, reference, 1 as qty from file where file_nature_id = 2 and file_type_id <> 3 and (lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) like :search_text or lower(regexp_replace(cast(reference as varchar), '\s+', '', 'g')) like :search_text)", ['search_text' => "%{$term}%"]);
	return response()->json($files);
    }

    public function destroy($id) {
	$file= $this->findOrThrowException($id);
	event(new RegisterEvent($file, 'delete_file'));
	$file->delete();
    }

    private function checkIfExist($input, $index, $id = 0) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $input[$index])));
	if ($id == 0) {
	    if (File::whereRaw("lower(regexp_replace(cast(:index as varchar), '\s+', '', 'g')) = :name", ['index' => $index, 'name' => $name])->first()) {
		throw new GeneralException(trans('exceptions.backend.file.exist', ['name' => $index]));
	    }
	} else {
	    if (File::whereRaw("lower(regexp_replace(cast(:index as varchar), '\s+', '', 'g')) = :name and file_id <> :id", ['name' => $name, 'index' => $index, 'id' => $id])->first()) {
		throw new GeneralException(trans('exceptions.backend.file.exist', ['name' => $index]));
	    }
	}
    }

    private function checkOwner($file) {
	switch ($file->file_type_id) {
	    //Public file
	    case 1:
		$check = $file->departments()->where('file.department_id', '=', access()->department_id)->first();
		break;
	    //Confidential file
	    case 2:
	    case 3:
		$check = $file->users()->where('officer.officer_id', '=', access()->id())->first();
		break;
	}
	if (!count($check))
	    return FALSE;
	return true;
    }

}
