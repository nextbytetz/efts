<?php

namespace App\Repositories\Backend\File\Register;


interface RegisterRepositoryContract {
    
    public function findOrThrowException($id);
    
    public function findFolder($id);
    
    public function getForDataTable($search_text = "");
    
    public function getBulkForDataTable();
    
    public function getFolderForDataTable($folder_id, $file_id);
    
    public function getAllFileType($file_type_id = 0);
    
    public function getAllFileCategory();
    
    public function getAllPriority();
    
    public function getAllSubject();
    
    public function create($input);
    
    public function update($id, $input, $data);
    
    public function createFolder($file_id, $parente_id, $name);
    
    public function renameFolder($folder_id, $name);
    
    public function removeFolder($folder_id);
    
    public function moveFile($id, $parent_id, $type);
    
    public function getRegisterOfficers($department, $q);
    
    public function storeBulk($input);
    
    public function saveBulk($data, $input);
    
    public function searchAll($term);
    
}