<?php

namespace App\Repositories\Backend\File\Process;

interface ProcessRepositoryContract {

    public function findOrThrowException($id);

    public function getLogForDatatable($id);
    
    public function pickUp($id);
    
    public function pickUpForced($id);
    
    public function release($id);
    
    public function logPost($id, $comment);
    
    public function getWorkflow($id);
    
    public function getForwardOfficers($id, $q);
    
    public function cases($id);
    
    public function forwardPost($id, $input);
    
    public function request($id);
    
    public function requestCancel($id);
    
    public function receive($id);
    
    public function editCase($id);
    
    public function pickWarned($id);
    
    public function barcodePrintStyle();
    
    public function readerStore($input);
    
}
