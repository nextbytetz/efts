<?php

namespace App\Repositories\Backend\File\Process;

use DB;
use Carbon\Carbon;
use App\Models\File\File;
use App\Models\File\Track;
use App\Models\Location\Zone;
use App\Models\File\Workflow;
use App\Models\File\WorkflowOfficer;
use App\Models\File\FilePickup;
use App\Models\File\FileRequest;
use App\Models\Notification\Notification;
use App\Events\Backend\Notification\NotificationEvent;
use App\Exceptions\GeneralException;

class EloquentProcessRepository {

    public function findOrThrowException($id) {

	$file = File::find($id);

	if (!is_null($file)) {
	    return $file;
	}
	throw new GeneralException(trans('exceptions.backend.file.not_found'));
    }

    public function getLogForDatatable($id) {
	return Workflow::whereFileId($id)->orderBy('workflow_id', 'desc')->get();
    }

    public function pickUp($id) {
	/* start : Check if this user requested this file before */
	$file = $this->findOrThrowException($id);
	$this->cancelRequest($file);
	/* end : Check if this user requested this file before */
	return $this->pickUpAction($id, request()->input('file_case'));
    }

    public function pickUpForced($id) {
	$file = $this->findOrThrowException($id);
	//$pickup = FilePickup::where(['file_id' => $id, 'officer_id' => access()->id()])->orderBy('file_pickup_id', 'desc')->first();
	$pickup = $file->pickups->first();
	$pickup->disposed_at = Carbon::now();
	$pickup->save();
	$this->cancelRequest($file);
	return $this->pickUpAction($id, request()->input('file_case'), '1');
    }

    public function release($id) {
	$pickup = FilePickup::where(['file_id' => $id, 'officer_id' => access()->id()])->orderBy('file_pickup_id', 'desc')->first();
	$pickup->disposed_at = Carbon::now();
	$pickup->released = '1';
	$pickup->save();
	$this->updatePickUpFile($id, NULL, $pickup->file_pickup_id);
	//returning the file name for the alert display
	return $pickup->file->name;
    }

    public function logPost($id, $input) {
	$comment = $input['comment'];
	$file = $this->findOrThrowException($id);
	switch ($file->file_category_id) {
	    case 1:
		//Normal file
		$pickup = FilePickup::where(['file_id' => $id, 'officer_id' => access()->id()])->orderBy('file_pickup_id', 'desc')->first();
		if (is_null($pickup->workflow_id)) {
		    $workflow = Workflow::create(['file_id' => $id, 'officer_id' => access()->id(), 'comment' => $comment]);
		    $pickup->workflow_id = $workflow->workflow_id;
		    $pickup->save();
		} else {
		    $workflow = Workflow::find($pickup->workflow_id);
		    $workflow->comment = $comment;
		    $workflow->save();
		}
		break;
	    case 2:
		//Notice file
		$workflow = Workflow::create(['file_id' => $id, 'officer_id' => access()->id(), 'comment' => $comment]);
		break;
	}

	if (isset($input['notify'])) {
	    switch ($input['notify']) {
		case '1': //Notify all
		    //$file = $this->findOrThrowException($id);
		    event(new NotificationEvent($workflow, 'file_register', $file->list_users));
		    break;
		case '2': //notify selected
		    event(new NotificationEvent($workflow, 'file_register', $input['officers']));
		    break;
	    }
	}
    }

    public function getWorkflow($id) {
	$pickup = FilePickup::where(['file_id' => $id, 'officer_id' => access()->id()])->orderBy('file_pickup_id', 'desc')->first();
	if (count($pickup)) {
	    $return = Workflow::find($pickup->workflow_id);
	} else {
	    $return = Workflow::find(0);
	}
	return $return;
    }

    public function getForwardOfficers($id, $q) {
	$name = strtolower(trim(preg_replace('/\s+/', '', $q)));
	$my_id = access()->id();
	$data['items'] = DB::select("select officer_id , firstname || ' ' || othernames as name from officer where lower(regexp_replace(cast(firstname as varchar), '\s+', '', 'g')) like :name and (officer_id in (select officer_id from file_officer where file_id = :id) or department_id in (select department_id from file_department where file_id = :id)) and officer_id <> :my_id", ['name' => "%$name%", 'id' => $id, 'my_id' => $my_id]);
	$data['total_count'] = count($data['items']);
	return response()->json($data);
    }

    public function cases($id) {
	$data = DB::select("select file_case_id as id, name as text from file_case where file_subject_id = (select file_subject_id from file where file_id = :file_id) order by name asc", ['file_id' => $id]);
	return response()->json($data);
    }

    public function forwardPost($id, $input) {
	return DB::transaction(function() use ($id, $input) {
		    $file = $this->findOrThrowException($id);
		    $pickup = $file->pickups->first();
		    if (!is_null($pickup->workflow_id)) {
			$workflow = Workflow::find($pickup->workflow_id);
			$workflow->comment = $input['comment'];
			$workflow->save();
		    } else {
			$workflow = Workflow::create(['file_id' => $id, 'officer_id' => access()->id(), 'comment' => $input['comment']]);
			$pickup->workflow_id = $workflow->workflow_id;
			$pickup->save();
		    }
		    //attaching user(s) to the workflow
		    $officers = [];
		    $current = $input["officers"];
		    if (count($current)) {
			foreach ($current as $perm) {
			    if (is_numeric($perm)) {
				array_push($officers, $perm);
			    }
			}
		    }
		    $workflow->attachUsers($officers);
		    $pickup->forwarded = '1';
		    $pickup->disposed_at = Carbon::now();
		    $pickup->save();
		    $file->priority_id = $input["priority_id"];
		    $file->save();
		    //update the file_pickup_id on file table to null, since file has been released
		    $this->updatePickUpFile($id, NULL, $pickup->file_pickup_id);
		    //check if this user who is being forwarded file had pending request
		    $this->cancelRequest($file);
		    //notify file dispatch to officers receiving files
		    event(new NotificationEvent($workflow, 'file_dispatch', $input['officers']));
		    return $file->name;
		});
    }

    public function request($id) {
	$request = FileRequest::firstOrNew(['file_id' => $id, 'officer_id' => access()->id()]);
	$request->status = '0';
	$request->save();
	return $request->file->name;
    }

    public function requestCancel($id) {
	$request = FileRequest::where(['file_id' => $id, 'officer_id' => access()->id()])->first();
	$request->status = '1';
	$request->save();
	return $request->file->name;
    }

    public function receive($id) {
	//$file = findOrThrowException($id);
	//$file->workflow()->first();
	//Get the latest workflow for this file.
	$workflow = Workflow::where(['file_id' => $id])->orderBy('workflow_id', 'desc')->first();
	$workflow_officer = $workflow->workflowOfficers()->where(['workflow_officer.officer_id' => access()->id()])->first();
	$workflow_officer->received_at = Carbon::now();
	$workflow_officer->save();
	$workflow->done = '1';
	$workflow->save();
	$file_case = $workflow->file->pickups()->first()->cases()->pluck("file_case.file_case_id")->all();
	return $this->pickUpAction($id, $file_case);
    }

    public function editCase($id) {
	$file = $this->findOrThrowException($id);
	$pickup = $file->pickups()->first();
	$pickup->cases()->sync([]);
	$file_case = request()->input('file_case');
	foreach ($file_case as $id) {
	    $pickup->cases()->attach($id);
	}
	return response()->json(['success' => true, 'active_cases' => $file->active_case]);
    }

    public function pickWarned($id) {
	$workflow = Workflow::where(['file_id' => $id])->orderBy('workflow_id', 'desc')->first();
	//insert new workflow officer
	WorkflowOfficer::create(['workflow_id' => $workflow->workflow_id, 'officer_id' => access()->id(), 'received_at' => Carbon::now(), 'intent' => '0']);
	$workflow->done = '1';
	$workflow->save();
	//Check if this user requested this file before but he/she is taking the file instead of other officer, then clear this user file request.
	$file = $this->findOrThrowException($id);
	$this->cancelRequest($file);
	return $this->pickUpAction($id, request()->input('file_case'));
    }

    public function barcodePrintStyle() {
	return [
	    40 => trans('labels.backend.file_process.barcodes.40_per_sheet'),
	    30 => trans('labels.backend.file_process.barcodes.30_per_sheet'),
	    24 => trans('labels.backend.file_process.barcodes.24_per_sheet'),
	    20 => trans('labels.backend.file_process.barcodes.20_per_sheet'),
	    18 => trans('labels.backend.file_process.barcodes.18_per_sheet'),
	    14 => trans('labels.backend.file_process.barcodes.14_per_sheet'),
	    12 => trans('labels.backend.file_process.barcodes.12_per_sheet'),
	    10 => trans('labels.backend.file_process.barcodes.10_per_sheet'),
	    50 => trans('labels.backend.file_process.barcodes.continuous_feed')
	];
    }

    /**
     * @description : receive notification from the reader in the following format
     * reader=,epc=,rssi=,antenna,count,frequency,&time
     * @param type $input
     * @return type
     */
    public function readerStore($input) {
	$file = File::where('epc', '=', $input['epc'])->select('epc')->first();
	if (count($file) == 0) {
	    return "Not Present";
	}
	$last_track = Track::with('zone')->where('epc', '=', $input['epc'])->orderBy('track_id', 'desc')->take(1)->first();
	if (count($last_track)) {
	    $present_time = Carbon::parse($input['time']); //new DateTime($input['time']);
	    $last_time = Carbon::parse($last_track->reader_time); //new DateTime($last_track->reader_time);
	    $fDiff = $present_time->diffInSeconds($last_time);

	    if ($last_track->zone->reader == $input['reader'] And $last_track->zone->antenna == $input['antenna']) {
		//from the same reader and antenna				
		if ($fDiff > 0) {
		    //$last_time < $present_time
		    $this->updateTrack($last_track, $input);
		}
	    } else {
		//from different reader or antenna
		if ($fDiff > 0) {
		    //$last_time < $present_time
		    $this->newTrack($input);
		} else {
		    //check the last record from the same reader and antenna
		    $last_same_track = Track::whereHas('zone', function ($query) use ($input) {
				$query->where(['antenna' => $input['antenna'], 'reader' => $input['reader']]);
			    })->where('epc', '=', $input['epc'])->orderBy('track_id', 'desc')->take(1)->first();
		    if (count($last_same_track)) {
			$last_time = Carbon::parse($last_same_track->reader_time); //new DateTime($last_same_track->reader_time);
			$sDiff = $present_time->diffInSeconds($last_time);
			if ($sDiff > 0) {
			    //$last_time < $present_time
			    $this->updateTrack($last_same_track, $input);
			}
		    }
		}
	    }
	} else {
	    $this->newTrack($input);
	}
	return "Received";
    }

    public function getMyFileHistory() {
	$history = DB::select("select created_at::date, count(file_pickup_id) as total from file_pickup where created_at between ((select max(created_at)  - interval '10 day' from file_pickup))::date and (select max(created_at)::date from file_pickup)::date and officer_id = 1 group by created_at::date order by created_at asc");
	return json_encode($history);
    }

    public function getDispatchedFileHistory() {
	$history = DB::select("select disposed_at::date, count(file_pickup_id) as total from file_pickup where created_at between ((select max(disposed_at)  - interval '10 day' from file_pickup))::date and (select max(disposed_at)::date from file_pickup)::date and officer_id = 1 and disposed_at is not null group by disposed_at::date order by disposed_at asc");
	return json_encode($history);
    }

    private function newTrack($input) {
	//insert a new track entry
	$zone = Zone::where(['reader' => $input['reader'], 'antenna' => $input['antenna']])->first();
	if (count($zone)) {
	    Track::create(['zone_id' => $zone->zone_id, 'epc' => $input['epc'], 'frequency' => $input['frequency'], 'read_count' => $input['count'], 'rssi' => $input['rssi'], 'reader_time' => $input['time']]);
	}
    }

    private function updateTrack($track, $input) {
	$track->reader_time = $input['time'];
	$track->frequency = $input['frequency'];
	$track->rssi = $input['rssi'];
	$track->save();
    }

    /**
     * @description : cancel the file request, if the user has already picked the file.
     * @param type $file
     */
    private function cancelRequest($file) {
	$request = $file->requests()->where(['file_request.officer_id' => access()->id(), 'file_request.status' => '0'])->first();
	if (count($request)) {
	    $request->status = "1";
	    $request->save();
	}
    }

    private function updatePickUpFile($id, $file_pickup_id, $old_file_pickup_id = 0) {
	$file = $this->findOrThrowException($id);
	if (is_null($file_pickup_id)) {
	    $file->active_pickups()->detach($old_file_pickup_id);
	} else {
	    $file->active_pickups()->attach($file_pickup_id);
	}
    }

    private function pickUpAction($id, $file_case, $forced = '0') {
	$file = $this->findOrThrowException($id);
	/* retrieve the recent file pickup */
	$recent_pickup = FilePickup::select(['file_pickup_id', 'child_id'])->where('file_id', '=', $id)->orderBy('file_pickup_id', 'desc')->first();
	$pickup = FilePickup::create(['file_id' => $file->file_id, 'officer_id' => access()->id(), 'forced' => $forced]);
	foreach ($file_case as $id) {
	    $pickup->cases()->attach($id);
	}

	/* update the last pickup table in the child_id column/attribute to include the id of the new pickup  */
	if (count($recent_pickup)) {
	    if (access()->id() != $recent_pickup->officer_id) {
		$recent_pickup->child_id = $pickup->file_pickup_id;
		$recent_pickup->save();
	    }
	}

	/* update file_pickup_id in the file table */
	$this->updatePickUpFile($file->file_id, $pickup->file_pickup_id);

	return $file;
    }

}
