<?php

namespace App\Repositories\Backend\Document;

use DB;
use App\Exceptions\GeneralException;
use App\Models\File\Document;
use App\Models\File\DocumentStatus;
use App\Events\Backend\File\RegisterEvent;
use App\Models\File\Folder;
use App\Models\File\File;
use App\Models\File\Priority;
use App\Models\Notification\Notification;
use App\Events\Backend\Notification\NotificationEvent;

class EloquentDocumentRepository {

    public function findOrThrowException($id) {

	$document = Document::find($id);

	if (!is_null($document)) {
	    return $document;
	}
	throw new GeneralException(trans('exceptions.backend.document.not_found'));
    }

    public function getForDataTable($folder_id, $file_id) {
	//get all registered documents available for given file);
	//if folder_id is 0, it means the directory is at the root of the file
	if ($folder_id == 0) {
	    $file = File::find($file_id);
	    $documents = $file->documents;
	} else {
	    /* Get the ersion number */
	    $output = DB::select("show server_version");
	    $version = intval(substr($output[0]->server_version, 0, 1));
	    if ($version > 8) {
		$folders = DB::select("with recursive nodes (folder_id, parent_id) as (select a.folder_id, a.parent_id from folder a where parent_id = :folder_id and deleted_at is null union all select b.folder_id, b.parent_id from folder b, nodes a where b.parent_id = a.folder_id and deleted_at is null) select folder_id, parent_id from folder where folder_id = :folder_id union all select * from nodes", ['folder_id' => $folder_id]);
		$folder_id = array();
		foreach ($folders as $obj) {
		    $folder_id[] = $obj->folder_id;
		}
		$documents = Document::whereIn("folder_id", $folder_id)->get();
	    } else {
		$documents = Document::where("folder_id", "=", $folder_id)->get();
	    }
	    
	}
	return $documents;
    }

    public function create($data) {
	$document = Document::create($data);
	DocumentStatus::create(['comment' => 'Uploaded new document version', 'officer_id' => access()->id(), 'document_id' => $document->document_id, 'action_id' => 1]);
	return $document;
    }

    public function update($data, $id) {
	//$document = $this->findOrThrowException($id);
	Document::where(['document_id' => $id])->update($data);
	DocumentStatus::create(['comment' => 'Uploaded new document version', 'officer_id' => access()->id(), 'document_id' => $id, 'action_id' => 1]);
    }

    public function getAllPriority() {
	return Priority::pluck('name', 'priority_id')->all();
    }

    public function updateStatus($input) {
	$document = Document::find($input['document_id']);
	//$document->description = $input['description'];
	$document->priority_id = $input['priority_id'];
	$document->status = $input['status'];
	$document->save();
	$status = DocumentStatus::create(['comment' => $input['comment'], 'priority_id' => $input['priority_id'], 'status' => $input['status'], 'officer_id' => access()->id(), 'document_id' => $document->document_id, 'action_id' => 2]);
	if (isset($input['notify'])) {
	    switch ($input['notify']) {
		case '1': //Notify all
		    event(new NotificationEvent($status, 'document_status', $document->users));
		    break;
		case '2': //notify selected
		    event(new NotificationEvent($status, 'document_status', $input['officers']));
		    break;
	    }
	}
	return $document;
    }

    public function getStatusForDataTable($id) {
	$status = DocumentStatus::where(['document_id' => $id])->orderBy('document_status_id', 'DESC')->get();
	return $status;
    }

    public function destroyStatus($id) {
	$status = DocumentStatus::find($id);
	$document_id = $status->document_id;
	$status->delete();
	$notification = Notification::where(['id' => $status->document_status_id, 'asset' => 'DocumentStatus'])->first();
	if (!empty($notification)) {
	    //delete users for this notification.
	    $notification->users()->sync([]);
	    $notification->delete();
	}
	return $document_id;
    }

    public function getDocStatus($id) {
	$status = DB::select("select document_status_id as id, b.name as content, a.created_at::date as start from document_status a join action b on a.action_id = b.action_id where document_id = :id", ['id' => $id]);
	return json_encode($status);
    }

    public function destroy($id) {
	$document = $this->findOrThrowException($id);
	$file_id = $document->file->file_id;
	event(new RegisterEvent($document, 'delete_document'));
	$document->delete();
	return $file_id;
    }

    public function searchDocument($text, $id) {
	$text = strtolower(trim(preg_replace('/\s+/', '', $text)));
	$avatar = asset_url() . "/images/document.png";
	$url = url('/');	
	$document = DB::select("select document_id as id, name, '{$avatar}' as avatar, 'Document' as info, '{$url}' || '/storage/document/' || document_id || '/show' as href from document where lower(regexp_replace(cast(name as varchar), '\s+', '', 'g')) like :text and file_id = :id limit 15", ['id' => $id, 'text' => "%{$text}%"]);
	return json_encode($document);
    }

}
