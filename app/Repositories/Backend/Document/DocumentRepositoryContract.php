<?php

namespace App\Repositories\Backend\Document;

interface DocumentRepositoryContract {
    
    public function findOrThrowException($id);
    
    public function getForDataTable($folder_id, $file_id);
    
    public function create($document);
    
    public function update($data, $id);
    
    public function updateStatus($input);
    
    public function getStatusForDataTable($id);
    
    public function destroyStatus($id);
    
    public function getDocStatus($id);
    
    public function destroy($id);
    
    public function searchDocument($text, $id);
    
}