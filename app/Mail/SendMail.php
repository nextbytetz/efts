<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Access\User;

/**
 * Send Sample Mail
 */
class SendMail extends Mailable implements ShouldQueue {

    use Queueable,
	SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    
    /**
     *
     * @var type 
     */
    public $user;
    
    public function __construct(User $user) {
	$this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
	return $this->from(['address' => app_mail(), 'name' => app_name()]  )->view('emails.sample');
    }

}
