<?php

namespace App\DataTables;

use Yajra\Datatables\Services\DataTable;
use App\Models\Report\ClosedRpt;

class ClosedRptDataTable extends DataTable {

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax() {
	return $this->datatables
			->eloquent($this->query())
			->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query() {
	$query = ClosedRpt::query();

	return $this->applyScopes($query)->orderBy('file_id', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html() {
	return $this->builder()
			->columns($this->getColumns())
			->ajax('')
			->parameters([
			    'dom' => 'Bfrtip',
			    'buttons' => ['csv', 'excel', 'print', 'reset', 'reload'],
			    'initComplete' => "function () {
                            this.api().columns([0,1,2,5,7]).every(function () {			    
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });				
                            });
                        }",
	]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
	return [
	    ['data' => 'file', 'name' => 'file', 'title' => trans('labels.backend.report.closed.file'), 'orderable' => false],
	    ['data' => 'reference', 'name' => 'reference', 'title' => trans('labels.backend.report.closed.reference'), 'orderable' => false],
	    ['data' => 'shelf_number', 'name' => 'shelf_number', 'title' => trans('labels.backend.report.closed.shelf'), 'orderable' => false],
	    ['data' => 'subject', 'name' => 'subject', 'title' => trans('labels.backend.report.closed.subject'), 'orderable' => false],
	    ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.report.closed.created'), 'orderable' => false],
	    ['data' => 'officer_opened', 'name' => 'officer_opened', 'title' => trans('labels.backend.report.closed.officer_opened'), 'orderable' => false],
	    ['data' => 'closed_at', 'name' => 'closed_at', 'title' => trans('labels.backend.report.closed.closed'), 'orderable' => false],
	    ['data' => 'officer_closed', 'name' => 'officer_closed', 'title' => trans('labels.backend.report.closed.officer_closed'), 'orderable' => false],
	];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
	return 'closedrptdatatables_' . time();
    }

}
