<?php

namespace App\DataTables;

use App\Models\Report\BasicRpt;
use Yajra\Datatables\Services\DataTable;

class BasicRptDataTable extends DataTable {

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax() {
	return $this->datatables
			->eloquent($this->query())
			->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query() {
	$query = BasicRpt::query()->orderBy('file_id', 'desc');
	return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html() {
	return $this->builder()
			->columns($this->getColumns())
		        ->ajax('')
			->parameters([
			    'dom' => 'Bfrtip',
			    'buttons' => ['csv', 'excel', 'print', 'reset', 'reload'],
			    'initComplete' => "function () {
                            this.api().columns([0,1,2]).every(function () {			    
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });				
                            });
                        }",
	]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
	return [
	    ['data' => 'file', 'name' => 'file', 'title' => trans('labels.backend.report.basic.file'), 'orderable' => false],
	    ['data' => 'reference', 'name' => 'reference', 'title' => trans('labels.backend.report.basic.reference'), 'orderable' => false],
	    ['data' => 'officer', 'name' => 'officer', 'title' => trans('labels.backend.report.basic.desk'), 'orderable' => false],
	    ['data' => 'pick_date', 'name' => 'pick_date', 'title' => trans('labels.backend.report.basic.days'), 'orderable' => false],
	    ['data' => 'state', 'name' => 'state', 'title' => trans('labels.backend.report.basic.state'), 'orderable' => false],
	    ['data' => 'status', 'name' => 'status', 'title' => trans('labels.backend.report.basic.status'), 'orderable' => false],
	    ['data' => 'subject', 'name' => 'subject', 'title' => trans('labels.backend.report.basic.subject'), 'orderable' => false],
	    ['data' => 'created_at', 'name' => 'created_at', 'title' => trans('labels.backend.report.basic.created'), 'orderable' => false],
	];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
	return 'basicrptdatatables_' . time();
    }

}
