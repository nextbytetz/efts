<?php

namespace App\DataTables;

use Yajra\Datatables\Services\DataTable;
use App\Models\Report\TrackingRpt;

class TrackingRptDataTable extends DataTable {

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax() {
	return $this->datatables
			->eloquent($this->query())
			->addColumn('duration', function ($tracking) {
			    return $tracking->duration;
			})
			->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query() {
	$query = TrackingRpt::query()->orderBy('track_id', 'desc');

	return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html() {
	return $this->builder()
			->columns($this->getColumns())
			->ajax('')
			->parameters([
			    'dom' => 'Bfrtip',
			    'buttons' => ['csv', 'excel', 'print', 'reset', 'reload'],
			    'initComplete' => "function () {
                            this.api().columns([0,1,2]).every(function () {			    
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });				
                            });
                        }",
	]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
	return [
	    ['data' => 'file', 'name' => 'file', 'title' => trans('labels.backend.report.tracking.file'), 'orderable' => false],
	    ['data' => 'reference', 'name' => 'reference', 'title' => trans('labels.backend.report.tracking.reference'), 'orderable' => false],
	    ['data' => 'zone', 'name' => 'zone', 'title' => trans('labels.backend.report.tracking.zone'), 'orderable' => false],
	    ['data' => 'updated_at', 'name' => 'updated_at', 'title' => trans('labels.backend.report.tracking.last_read'), 'orderable' => false, 'searchable' => false],
	    ['data' => 'duration', 'name' => 'duration', 'title' => trans('labels.backend.report.tracking.duration'), 'orderable' => false],
	];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
	return 'trackingrptdatatables_' . time();
    }

}
