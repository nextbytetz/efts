<?php

namespace App\DataTables;

use App\Models\Report\UserRpt;
use Yajra\Datatables\Services\DataTable;

class LiveMonitoringDataTable extends DataTable {

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax() {
	return $this->datatables
			->eloquent($this->query())
			->addColumn('period', function ($user_rpt) {
			    return $user_rpt->period;
			})
			->addColumn('case', function ($user_rpt) {
			    return $user_rpt->pickup->case;
			})
			->make(true);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query() {
	$department_id = access()->user()->department_id;
	$officer_id = access()->id();
	if (is_null($department_id)) {
	    $query = UserRpt::whereRaw("file_id = 0");
	} else {
	    $query = UserRpt::whereRaw("file_id in (select file_id from file_department where department_id = {$department_id} union all select file_id from file_officer where officer_id = {$officer_id} )")->orderBy('file_pickup_id', 'desc');
	}
	
	return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html() {
	return $this->builder()
			->columns($this->getColumns())
			->ajax('')
			->parameters([
			    'dom' => 'Bfrtip',
			    'buttons' => ['reset', 'reload'],
			    'initComplete' => "function () {
                            this.api().columns([0,1,2,6,8]).every(function () {			    
                                var column = this;
                                var input = document.createElement(\"input\");
                                $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });				
                            });
                        }",
	]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
	return [
	    ['data' => 'officer', 'name' => 'officer', 'title' => trans('labels.backend.report.user.officer'), 'orderable' => false],
	    ['data' => 'file', 'name' => 'file', 'title' => trans('labels.backend.report.user.file'), 'orderable' => false],
	    ['data' => 'reference', 'name' => 'reference', 'title' => trans('labels.backend.report.user.reference'), 'orderable' => false],
	    ['data' => 'receive_date', 'name' => 'receive_date', 'title' => trans('labels.backend.report.user.received'), 'orderable' => false],
	    ['data' => 'disposed_at', 'name' => 'disposed_at', 'title' => trans('labels.backend.report.user.released'), 'orderable' => false],
	    ['data' => 'period', 'name' => 'period', 'title' => trans('labels.backend.report.user.period'), 'orderable' => false, 'searchable' => false],
	    ['data' => 'receiver', 'name' => 'receiver', 'title' => trans('labels.backend.report.user.receiver'), 'orderable' => false],
	    ['data' => 'case', 'name' => 'case', 'title' => trans('labels.backend.report.user.case'), 'orderable' => false, 'searchable' => false],
	    ['data' => 'desk', 'name' => 'desk', 'title' => trans('labels.backend.report.user.desk'), 'orderable' => false],
	];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
	return 'userrptdatatables_' . time();
    }

}
