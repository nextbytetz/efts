<?php

namespace App\Events\Backend\Notification;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class NotificationEvent extends Event {

    use SerializesModels;

    public $notification;
    public $event;
    public $users;

    public function __construct($notification, $event, $users) {
	$this->notification = $notification;
	$this->event = $event;
	$this->users = $users;
    }

}
