<?php

namespace App\Events\Backend\Access;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class RoleEvent extends Event {

    use SerializesModels;

    /**
     * @var $role
     */
    public $role;
    
    public $event;

    /**
     * @param $role
     */
    public function __construct($role, $event) {
	$this->role = $role;
	$this->event = $event;
    }

}
