<?php

namespace App\Events\Backend\Access;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class UserEvent extends Event {

    use SerializesModels;

    /**
     * @var $user
     */
    public $user;

    /**
     *
     * @var $event 
     */
    public $event;

    /**
     * 
     * @param type $user
     * @param type $message
     * @param type $event
     */
    public function __construct($user, $event) {
	$this->user = $user;
	$this->event = $event;
    }

}
