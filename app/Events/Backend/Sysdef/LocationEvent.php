<?php

namespace App\Events\Backend\Sysdef;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class LocationEvent extends Event {

    use SerializesModels;

    /**
     * @var $user
     */
    public $user;

    /**
     * @param $user
     */
    public function __construct($user) {
	$this->user = $user;
    }

}