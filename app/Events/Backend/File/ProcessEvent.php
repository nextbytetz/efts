<?php

namespace App\Events\Backend\File;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ProcessEvent extends Event {

    use SerializesModels;

    /**
     * @var $file
     */
    public $file;
    
    public $event;

    /**
     * @param $file
     */
    public function __construct($file, $event) {
	$this->file = $file;
	$this->event = $event;
    }

}