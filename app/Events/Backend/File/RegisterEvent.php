<?php

namespace App\Events\Backend\File;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class RegisterEvent extends Event {

    use SerializesModels;

    /**
     * @var $file
     */
    public $file;
    
    public $event;
    
    public $message;

    /**
     * @param $file
     */
    public function __construct($file, $event, $message = "") {
	$this->file = $file;
	$this->event = $event;
	$this->message = $message;
    }

}