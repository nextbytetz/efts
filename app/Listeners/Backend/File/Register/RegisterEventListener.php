<?php

namespace App\Listeners\Backend\File\Register;

class RegisterEventListener {

    /**
     * @var string
     */
    private $history_slug = 'file_register';

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events) {
	$events->listen(
		\App\Events\Backend\File\RegisterEvent::class, 'App\Listeners\Backend\File\Register\RegisterEventListener@processEvent'
	);
    }

    public function processEvent($event) {
	switch ($event->event) {
	    case 'create_file':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.created") <strong>' . $event->file->name . '</strong>', $event->file->file_id, 'plus-square', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'update_file':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.updated") <strong>' . $event->file->name . '</strong>', $event->file->file_id, 'edit', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'create_folder':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.folder.created") <strong>' . $event->file->name . '</strong><br/>' . $event->file->path, $event->file->file_id, 'plus-square', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'rename_folder':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.folder.renamed") <strong>' . $event->message . ", " . $event->file->name . '</strong><br/>' . $event->file->path, $event->file->file_id, 'edit', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'delete_folder':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.folder.deleted") <strong>' . $event->file->name . '</strong><br/>' . $event->file->path, $event->file->file_id, 'trash-o', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'delete_document' :
		history()->log(
			$this->history_slug, 'trans("history.backend.document.deleted") <strong>' . $event->file->path . $event->file->name . '</strong><br/>', $event->file->document_id, 'trash-o', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'create_bulk_file':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.bulk.uploaded") <strong>' . $event->file->name . '</strong>', $event->file->bulk_id, 'upload', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'done_bulk_file':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.bulk.success") <strong>' . $event->file->name . '</strong>', $event->file->bulk_id, 'upload', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'delete_file':
		history()->log(
			$this->history_slug, 'trans("history.backend.file_register.deleted") <strong>' . $event->file->name . '</strong><br/>', $event->file->file_id, 'trash-o', 'fa-stack-1x fa-inverse'
		);
		break;
	}
    }

}
