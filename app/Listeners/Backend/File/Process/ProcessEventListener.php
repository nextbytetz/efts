<?php

namespace App\Listeners\Backend\File\Process;

class ProcessEventListener {

    /**
     * @var string
     */
    private $history_slug = 'file_process';

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events) {
	$events->listen(
		\App\Events\Backend\File\ProcessEvent::class, 'App\Listeners\Backend\File\Process\ProcessEventListener@processEvent'
	);
    }

    public function processEvent($event) {
	switch ($event->event) {
	    
	}
    }

}
