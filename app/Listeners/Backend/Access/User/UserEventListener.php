<?php

namespace App\Listeners\Backend\Access\User;

/**
 * Class UserEventListener
 * @package App\Listeners\Backend\Access\User
 */
class UserEventListener {

    /**
     * @var string
     */
    private $history_slug = 'access_management';

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events) {
	$events->listen(
		\App\Events\Backend\Access\UserEvent::class, 'App\Listeners\Backend\Access\User\UserEventListener@processEvent'
	);
    }

    /**
     * @param $event
     */
    public function processEvent($event) {
	switch ($event->event) {
	    case 'edit_user':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.edited") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'edit', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'approve_user':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.approved") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'user-plus', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'activate_user':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.activated") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'play', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'deactivate_user':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.deactivated") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'pause', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'mark_in_office':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.in") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'sign-in', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'mark_out_of_office':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.out") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'sign-out', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'change_password':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.changed_password") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'refresh', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'delete_user':
		history()->log(
			$this->history_slug, 'trans("history.backend.users.deleted") <strong>' . $event->user->name() . '</strong>', $event->user->officer_id, 'trash', 'fa-stack-1x fa-inverse'
		);
		break;
	}
    }

}
