<?php

namespace App\Listeners\Backend\Access\Role;

/**
 * Class RoleEventListener
 * @package App\Listeners\Backend\Access\Role
 */
class RoleEventListener {

    /**
     * @var string
     */
    private $history_slug = 'role_management';

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events) {
	$events->listen(
		\App\Events\Backend\Access\RoleEvent::class, 'App\Listeners\Backend\Access\Role\RoleEventListener@processEvent'
	);
    }
    
        /**
     * @param $event
     */
    public function processEvent($event) {
	switch ($event->event) {
	    case 'create_role':
		history()->log(
			$this->history_slug, 'trans("history.backend.roles.created") <strong>' . $event->role->name . '</strong>', $event->role->role_id, 'plus-square', 'fa-stack-1x fa-inverse'
		);
		break;	  
	    case 'edit_role':
		history()->log(
			$this->history_slug, 'trans("history.backend.roles.edited") <strong>' . $event->role->name . '</strong>', $event->role->role_id, 'edit', 'fa-stack-1x fa-inverse'
		);
		break;
	    case 'delete_role':
		history()->log(
			$this->history_slug, 'trans("history.backend.roles.deleted") <strong>' . $event->role->name . '</strong>', $event->role->role_id, 'trash', 'fa-stack-1x fa-inverse'
		);
		break;
	}
    }

}
