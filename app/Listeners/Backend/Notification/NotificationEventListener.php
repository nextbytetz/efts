<?php

namespace App\Listeners\Backend\Notification;

class NotificationEventListener {

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events) {
	$events->listen(
		\App\Events\Backend\Notification\NotificationEvent::class, 'App\Listeners\Backend\Notification\NotificationEventListener@processEvent'
	);
    }

    public function processEvent($event) {
	switch ($event->event) {
	    case 'document_status':
		notification()->log(
			1, access()->user()->name() . ' trans("notification.backend.document.status_updated") ' . $event->notification->document->name . ' trans("labels.general.file") , ' . $event->notification->document->file->name , 'DocumentStatus', $event->notification->document_status_id, $event->users
		);
		break;
            case 'file_dispatch':
                notification()->log(
			2, access()->user()->name() . ' trans("notification.backend.file.dispatched") ' . $event->notification->file->name, 'Workflow', $event->notification->workflow_id, $event->users
		);
                break;
	    case 'file_register':
		notification()->log(
			3, access()->user()->name() . ' trans("notification.backend.file.register.updated") ' . $event->notification->file->name, 'Workflow', $event->notification->workflow_id, $event->users
		);
		break;
	}
    }

}
