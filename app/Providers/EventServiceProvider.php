<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
    ];

    /**
     * Class event subscribers
     * @var array
     */
    protected $subscribe = [
	/**
	 * Backend Subscribers
	 */
	/**
	 * Access Subscribers
	 */
	\App\Listeners\Backend\Access\User\UserEventListener::class,
	\App\Listeners\Backend\Access\Role\RoleEventListener::class,
	/**
	 * Data registration subscribers
	 */
	\App\Listeners\Backend\Sysdef\Location\LocationEventListener::class,
	/**
	 * File Register subscribers
	 */
	\App\Listeners\Backend\File\Register\RegisterEventListener::class,
	/**
	 * File Process subscribers
	 */
	\App\Listeners\Backend\File\Process\ProcessEventListener::class,
	/**
	 * Notification Process subscribers
	 */
	\App\Listeners\Backend\Notification\NotificationEventListener::class,
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot() {
	parent::boot();

	//
    }

}
