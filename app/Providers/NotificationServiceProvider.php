<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Backend\Notification\EloquentNotificationRepository;
use App\Repositories\Backend\Notification\Facades\Notification as NotificationFacade;

/**
 * Class NotificationServiceProvider
 * @package App\Providers
 */
class NotificationServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
	$this->app->bind('notification', EloquentNotificationRepository::class);
	$this->registerFacade();
    }

    /**
     *
     */
    public function registerFacade() {
	$this->app->booting(function() {
	    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
	    $loader->alias('Notification', NotificationFacade::class);
	});
    }

}
