<?php

namespace App\Services\Storage\Traits;

use App\Exceptions\GeneralException;

trait DocumentHandler {

    protected function real($path) {
	$temp = realpath($path);
	if (!$temp) {
	    throw new GeneralException(trans("validation.attributes.backend.file.storage.path_exist", ['path' => $path]));
	}
	return $temp;
    }

    protected function directory() {
	if (isset(request()->folder_id) And request()->folder_id > 0) {
	    $folder = $this->files->findFolder(request()->folder_id);
	    $folder_path = $folder->path_id;
	} else {
	    $folder_path = request()->file_id . DIRECTORY_SEPARATOR;
	}
	return $this->base . DIRECTORY_SEPARATOR . $folder_path;
    }

    protected function getFolder() {
	if (isset(request()->folder_id) And request()->folder_id > 0) {
	    $return = request()->folder_id;
	} else {
	    $return = null;
	}
	return $return;
    }

    public function saveDocument() {
	switch (request()->status) {
	    case 'store':
		$this->storeNewDocument();
		break;
	    case 'update':
		$this->storePresentDocument();
		break;
	}
    }

    protected function storeNewDocument() {
	if (request()->hasFile('documents')) {
	    $files = request()->file('documents');
	    foreach ($files as $file) {
		if ($file->isValid()) {
		    $data = $this->documentData($file);
		    $document = $this->documents->create($data);
		    $file->move($this->directory(), $document->document_id . "." . $file->getClientOriginalExtension());
		}
	    }
	}
    }

    protected function storePresentDocument() {
	if (request()->hasFile('documents')) {
	    $files = request()->file('documents');
	    foreach ($files as $file) {
		if ($file->isValid()) {
		    $document_id = request()->document_id;
		    $old_document = $this->documents->findOrThrowException($document_id);
		    $data = $this->documentData($file);
		    $data['version'] = $old_document->version + 1;
		    $this->documents->update($data, $document_id);
		    $file->move($this->directory(), $document_id . "." . $file->getClientOriginalExtension());
		    if ($old_document->ext != $file->getClientOriginalExtension()) {
			unlink($this->directory() . $document_id . "." . $old_document->ext);
		    }
		}
	    }
	}
    }

    protected function documentData($file) {
	$data = [
	    'name' => $file->getClientOriginalName(),
	    'ext' => $file->getClientOriginalExtension(),
	    'size' => $file->getSize(),
	    'mime' => $file->getMimeType(),
	    'folder_id' => $this->getFolder(),
	    'file_id' => request()->file_id,
	    'officer_id' => access()->id(),
	];
	return $data;
    }

}
