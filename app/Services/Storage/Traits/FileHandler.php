<?php

namespace App\Services\Storage\Traits;

use App\Exceptions\GeneralException;

trait FileHandler {

    /**
     * @description : Inspect if the path exists and make sure that is the sub directory of the base path
     * @param type $path
     * @return type
     * @throws GeneralException
     */
    protected function real($path) {
	$temp = realpath($path);
	if (!$temp) {
	    throw new GeneralException('Path does not exist: ' . $path);
	}
	if ($this->base && strlen($this->base)) {
	    if (strpos($temp, $this->base) !== 0) {
		throw new GeneralException('Path is not inside base (' . $this->base . '): ' . $temp);
	    }
	}
	return $temp;
    }

    /**
     * @description : Create a full path of the subdirectory including the base path.
     * @param type $path
     * @return type
     */
    protected function path($path) {
	$path = str_replace('/', DIRECTORY_SEPARATOR, $path);
	$path = trim($path, DIRECTORY_SEPARATOR);
	$path = $this->real($this->base . DIRECTORY_SEPARATOR . $path);
	return $path;
    }

    /**
     * @description : Remove the base directory part from a long path with subdirectories from the base path.
     * @param type $path
     * @return type
     */
    protected function id($path) {
	$path = $this->real($path);
	$path = substr($path, strlen($this->base));
	$path = str_replace(DIRECTORY_SEPARATOR, '/', $path);
	$path = trim($path, '/');
	return strlen($path) ? $path : '/';
    }

    protected function lastId($id) {
	if ($id == "/") {
	    $last_id = null;
	} else {
	    $parts = explode(DIRECTORY_SEPARATOR, $id);
	    //$last_id = $parts[count($parts) - 1];
	    $last_id = array_pop($parts);
	}
	return $last_id;
    }

    /**
     * @description get the document_id from the supplied path
     * @param type $id
     * @return type
     */
    protected function getDocId($id) {
	$new = explode(DIRECTORY_SEPARATOR, $id);
	$doc = array_pop($new);
	$cond = strrpos($doc, ".");
	$document_id = ($cond) ? substr($doc, 0, strrpos($doc, ".")) : $doc;
	return $document_id;
    }

    /**
     * @discription : return the list of all files contained in the specified path
     * @param type $id
     * @param type $with_root
     * @return array
     * @throws Exception
     */
    public function lst($id, $with_root = false) {
	$dir = $this->path($id);
	$lst = @scandir($dir);
	if (!$lst) {
	    return array('success' => false, 'message' => trans('validation.attributes.backend.file.storage.list_error', ['dir' => $dir]));
	}
	$res = array();
	foreach ($lst as $item) {
	    if ($item == '.' || $item == '..' || $item === null) {
		continue;
	    }
	    $tmp = preg_match('([^ a-zа-я-_0-9.]+)ui', $item);
	    if ($tmp === false || $tmp === 1) {
		continue;
	    }
	    if (is_dir($dir . DIRECTORY_SEPARATOR . $item)) {
		if (array_key_exists($item, $this->folders)) {
		    /* this check is important to exclude all the folders which has been soft deleted */
		    $res[] = array('text' => $this->folders[$item], 'children' => true, 'id' => $this->id($dir . DIRECTORY_SEPARATOR . $item), 'icon' => 'folder');
		}
	    } else {
		$document_id = substr($item, 0, strrpos($item, '.'));
		if (array_key_exists($document_id, $this->documents)) {
		    /* this check is important to exclude all the documents which has been soft deleted */
		    $res[] = array('text' => $this->documents[$document_id], 'children' => false, 'id' => $this->id($dir . DIRECTORY_SEPARATOR . $item), 'type' => 'file', 'icon' => 'file file-' . substr($item, strrpos($item, '.') + 1));
		}
	    }
	}
	if ($with_root && $this->id($dir) === '/') {
	    $res = array(array('text' => $this->parentFolder[basename($this->base)], 'children' => $res, 'id' => '/', 'icon' => 'folder', 'state' => array('opened' => true, 'disabled' => true)));
	}
	return $res;
    }

    public function rename($id, $name) {
	$dir = $this->path($id);
	if ($dir === $this->base) {
	    return array('success' => false, 'message' => trans('validation.attributes.backend.file.storage.rename_error'));
	}
	if (preg_match('([^ a-zа-я-_0-9.]+)ui', $name) || !strlen($name)) {
	    return array('success' => false, 'message' => trans('validation.attributes.backend.file.storage.name_error', ['name' => $name]));
	}
	$folder_id = $this->lastId($id);
	$this->files->renameFolder($folder_id, $name);
	return array('id' => $this->id($dir), 'success' => true, 'message' => '');
    }

    public function remove($id) {
	$dir = $this->path($id);
	if ($dir === $this->base) {
	    return array('success' => false, 'message' => trans('validation.attributes.backend.file.storage.remove_error'));
	}
	$folder_id = $this->lastId($id);
	$this->files->removeFolder($folder_id);
	return array('status' => 'OK', 'success' => true, 'message' => '');
    }

    public function removePermanent($folder_id) {
	/*
	  if (is_dir($dir)) {
	  foreach (array_diff(scandir($dir), array(".", "..")) as $f) {
	  $this->remove($this->id($dir . DIRECTORY_SEPARATOR . $f));
	  }
	  rmdir($dir);
	  }
	  if (is_file($dir)) {
	  unlink($dir);
	  }
	 */
    }

    public function moveFile($id, $par) {
	$parent_id = $this->lastId($par);
	$dir = $this->path($id);
	$par = $this->path($par);
	$new = explode(DIRECTORY_SEPARATOR, $dir);
	$new = array_pop($new);
	$new = $par . DIRECTORY_SEPARATOR . $new;	
	if (is_file($dir)) {
	    /* This is the document */
	    $document_id = $this->getDocId($id);
	    $this->files->moveFile($document_id, $parent_id, 'document');
	} else {
	    /* This is the folder */
	    $folder_id = $this->lastId($id);
	    $this->files->moveFile($folder_id, $parent_id, 'folder');
	}
	rename($dir, $new);
	return array('id' => $this->id($new), 'success' => true, 'message' => '');
    }

    /**
     * @status Not used
     * @description copy files and document
     * @param type $id
     * @param type $par
     * @return type
     */
    public function copy($id, $par) {
	$dir = $this->path($id);
	$par = $this->path($par);
	$new = explode(DIRECTORY_SEPARATOR, $dir);
	$new = array_pop($new);
	$new = $par . DIRECTORY_SEPARATOR . $new;
	if (is_file($new) || is_dir($new)) {
	    return array('success' => false, 'message' => trans('validation.attributes.backend.file.storage.path_error', ['new' => $new]));
	}
	if (is_dir($dir)) {
	    mkdir($new);
	    foreach (array_diff(scandir($dir), array(".", "..")) as $f) {
		$this->copy($this->id($dir . DIRECTORY_SEPARATOR . $f), $this->id($new));
	    }
	}
	if (is_file($dir)) {
	    copy($dir, $new);
	}
	return array('id' => $this->id($new), 'success' => true);
    }

    public function createFolder($id, $name, $mkdir = false) {
	$dir = $this->path($id);
	if (preg_match('([^ a-zа-я-_0-9.]+)ui', $name) || !strlen($name)) {
	    return array('success' => false, 'message' => trans('validation.attributes.backend.file.storage.name_error', ['name' => $name]));
	}
	$parent_id = $this->lastId($id);
	$folder = $this->files->createFolder(request()->file_id, $parent_id, $name);
	mkdir($dir . DIRECTORY_SEPARATOR . $folder->folder_id, 0777);
	return array('id' => $this->id($dir . DIRECTORY_SEPARATOR . $folder->folder_id), 'success' => true, 'message' => '');
    }

    public function makeFileDirectory($file_id) {
	$root = file_dir() . DIRECTORY_SEPARATOR . $file_id;
        if (!file_exists(file_dir())) {
            mkdir(file_dir(), 0777, true);
        }
	mkdir($root, 0777);
    }

    public function makeProfileDirectory($path) {
	$root = file_dir() . "/" . $path;
    }

}
