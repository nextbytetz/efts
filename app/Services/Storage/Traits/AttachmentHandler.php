<?php

namespace App\Services\Storage\Traits;

use App\Exceptions\GeneralException;
use App\Jobs\File\RegisterBulk;

trait AttachmentHandler {

    protected function saveBulkAttachment($input) {
	if (request()->hasFile('files')) {
	    $file = request()->file('files');
	    if ($file->isValid()) {
		//Check the excel file
		$data = $this->attachmentBulkData($file);
		$bulk = $this->saveBulk($data, $input);
		$file->move(bulk_dir(), $bulk->bulk_id . "." . $file->getClientOriginalExtension());
		dispatch(new RegisterBulk($bulk));
	    }
	} else {
	    throw new GeneralException(trans('exceptions.backend.general.error'));
	}
    }

    protected function attachmentBulkData($file) {
	$data = [
	    'name' => $file->getClientOriginalName(),
	    'ext' => $file->getClientOriginalExtension(),
	    'size' => $file->getSize(),
	    'mime' => $file->getMimeType(),
	    'officer_id' => access()->id(),
	    'group_name' => request()->input("name"),
	    'priority_id' => request()->input("priority_id"),
	    'file_subject_id' => request()->input("file_subject_id"),
	    'file_type_id' => request()->input("file_type_id"),
	];
	return $data;
    }
    
    protected function saveProfileAttachment($user) {
	if (request()->hasFile('pic')) {
	    $file = request()->file('pic');
	    if ($file->isValid()) {
		//Check the excel file
		$user->pic = $user->officer_id . "." . $file->getClientOriginalExtension();
		$user->save();
		$file->move(profile_dir(), $user->officer_id . "." . $file->getClientOriginalExtension());
	    }
	}
    }
    
    protected function checkExcel() {
	
    }

}
