<?php

namespace App\Services\Mail;

use App\Models\Access\User;
use App\Mail\SendMail;
use Mail;

class MailHandler {

    public function sendSample(User $user) {
	$mail = Mail::to(['email' => $user->email])->send(new SendMail($user));
	return $mail;
    }

}
