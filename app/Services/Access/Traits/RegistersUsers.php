<?php

namespace App\Services\Access\Traits;

//use App\Models\Location\Region;
//use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Requests\Frontend\Auth\RegisterRequest;

/**
 * Class RegistersUsers
 * @package App\Services\Access\Traits
 */
trait RegistersUsers {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm() {
        $countries = $this->countries->getRegisteredCountry();
        return view('frontend.auth.register')
                ->withCountry($countries);
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request) {

        $this->user->register(request()->all());
        //event(new UserRegistered($user));
        return redirect()->route('auth.login')->withFlashSuccess(trans('exceptions.frontend.auth.created'));
    }

}
