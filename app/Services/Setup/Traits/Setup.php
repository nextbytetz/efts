<?php

namespace App\Services\Setup\Traits;

use App\Http\Requests\Frontend\Setup\SaveGeneralSetupRequest;
use App\Http\Requests\Frontend\Setup\SaveDbSetupRequest;
use Artisan;

trait Setup {

    /**
     * @param null $token
     * @return $this
     */
    public function showSetupForm($context = null) {
	$context = strtolower($context);

	switch ($context) {
	    case 'dbnew':
	    case 'dbupdate':
		return view('frontend.setup.database')
				->with('context', $context);

	    case 'general':
		return view('frontend.setup.general', $this->getDbParam());

	    default:
		return redirect()->back()
				->withFlashDanger(trans('exceptions.frontend.invalid_request'));
	}
    }

    public function saveDBSetup(SaveDbSetupRequest $request) {
	//redirect()->back()->withInput($request->all());
	$context = request()->context;
	switch ($context) {
	    case 'dbnew':
		//return $this->saveDBNew();
		return $this->installMigration();
	    case 'dbupdate':
		return $this->saveDBUpdate();
	}
    }

    public function saveGeneralSetup(SaveGeneralSetupRequest $request) {
	//redirect()->back()->withInput($request->all());
	$input = request()->all();
	foreach ($input as $value => $key) {
	    save_db_param($value, $key);
	}
	config(['database.connections.pgsql' => [
		'driver' => 'pgsql',
		'host' => request()->DB_HOST,
		'port' => request()->DB_PORT,
		'database' => request()->DB_DATABASE,
		'username' => request()->DB_USERNAME,
		'password' => request()->DB_PASSWORD,
		'charset' => 'utf8',
		'schema' => request()->DB_SCHEMA
	]]);
	/* testing if configuration are correct, if the following statement gives error, json that will be return will be from exception handler. */
	DB::select("select password from officer a join officer_role b on a.officer_id = b.officer_id where b.role_id = 1 limit 1");
	return redirect("/")->withFlashInfo(trans('exceptions.frontend.setup.setup_success'));
    }

    protected function saveDBUpdate() {
	$this->checkOsEnvironment();
	/*	 * *** begin : setupDbConfig saving update context ***** */
	/* check the current super user password, super user has to be verified before changes in the application can be applied */
	if ($this->user->checkSuper(request()->password_super_user)) {
	    /* Retrieve database connection parameter from the connection file */
	    $host = get_db_param('DB_HOST');
	    $port = get_db_param('DB_PORT');
	    $user = get_db_param('DB_USERNAME');
	    $passwd = get_db_param('DB_PASSWORD');
	    $database = get_db_param('DB_DATABASE');

	    /* Backup the current database structure, data and configuration before cleaning */
	    $output1 = shell_exec("pg_dump -U {$user} -p {$port} -h {$host} --no-owner -x -Fc {$database} > " . app_path() . "/library/db_backup/" . $database . date('Y_m_d_H_i_s') . "custom 2>&1");

	    /* clear all the data in the database */
	    DB::statement("select truncate_tables('public')");

	    /* create and add new office data */
	    $output2 = shell_exec("psql -h {$host} -d {$database} -U {$user} -p {$port} -f '" . app_path() . "/library/db_build/office_data.sql'  2>&1");

	    /* create super admin data */
	    $super = ['password' => request()->password, 'email' => request()->email, 'gender' => request()->gender, 'officeid' => request()->officeid, 'username' => request()->username, 'othernames' => request()->othernames, 'firstname' => request()->firstname, 'checked' => '1'];
	    $this->user->register($super, true);

	    /* DONE SET UP THE APPLICATION */
	    $success = true;
	    $message = trans('exceptions.frontend.setup.setup_success');
	} else {
	    $success = false;
	    $message = trans('exceptions.frontend.setup.invalid_super_pass');
	}
	/*	 * *** end : setupDbConfig saving update context ***** */
	if (!$success) {
	    return redirect()->back()->withFlashInfo($message);
	} else {
	    return redirect("/")->withFlashInfo($message);
	}
    }

    protected function saveDBNew() {
	$this->checkOsEnvironment();
	$function_delimeter = $this->getDelimiter();
	/*	 * *** begin : setupDbConfig saving new context ***** */
	/* Assigning set up form parameters to variables */
	$host = request()->DB_HOST;
	$port = request()->DB_PORT;
	$current_user = request()->CURRENT_DB_USERNAME;
	$current_passwd = request()->CURRENT_DB_PASSWORD;

	$user = strtolower(request()->DB_USERNAME);
	$passwd = request()->DB_PASSWORD;
	$database = strtolower(request()->DB_DATABASE);

	/* create a new user and password if they dont exist */
	$output1 = shell_exec("psql -h {$host} -U {$current_user} -p {$port} -c \"DO {$function_delimeter} begin if not exists ( select 1 from pg_catalog.pg_user where lower(usename) = '{$user}' ) then create role {$user} login password '{$passwd}' SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION; end if; end {$function_delimeter};\" 2>&1");


	/* create a extension db_link if it does not exist in the current database connection connection */
	$output2 = shell_exec("psql -h {$host} -U {$current_user} -p {$port} -c \"DO {$function_delimeter} begin if not exists ( select 1 from pg_catalog.pg_extension where extname = 'dblink' ) then create extension dblink; end if; end {$function_delimeter};\" 2>&1");

	/* create a new database if it does not exist */
	$output3 = shell_exec("psql -h {$host} -U {$current_user} -p {$port} -c \"DO {$function_delimeter} begin if not exists ( select 1 from pg_catalog.pg_database where lower(datname) = '{$database}' ) then PERFORM dblink_exec('host={$host} port={$port} user={$current_user} password={$current_passwd}', 'CREATE DATABASE {$database} WITH OWNER = {$user}'); end if; end {$function_delimeter};\" 2>&1");

	/* drop extension dblink created before */
	$output4 = shell_exec("psql -h {$host} -U {$current_user} -p {$port} -c \"drop extension dblink;\" 2>&1");

	/* restore dump file to create a new database and a new schema public with office structure */
	$output5 = shell_exec("pg_restore -U {$user} -p {$port} -h {$host} -d {$database} --exit-on-error " . app_path() . "/library/db_build/office_structure.custom  2>&1");

	/* changing the database connection to reflect the newly configured database settings for office application */
	config(['database.connections.pgsql' => [
		'driver' => 'pgsql',
		'host' => $host,
		'port' => $port,
		'database' => $database,
		'username' => $user,
		'password' => $passwd,
		'schema' => 'public',
		'charset' => 'utf8'
	]]);

	/* restore the database to create and add new office data */
	$output6 = shell_exec("pg_restore -U {$user} -p {$port} -h {$host} -d {$database} --schema=public --data-only --format=custom --disable-triggers --single-transaction " . app_path() . "/library/db_build/office_data.custom  2>&1");

	/* create super admin data */
	$super_user = ['password' => request()->password, 'email' => request()->email, 'gender' => request()->gender, 'officeid' => request()->officeid, 'username' => request()->username, 'othernames' => request()->othernames, 'firstname' => request()->firstname, 'checked' => '1', 'available' => '1'];
	$this->user->register($super_user, true);

	/* save the new database connection configuration in .env file */
	save_db_param('DB_HOST', $host);
	save_db_param('DB_PORT', $port);
	save_db_param('DB_DATABASE', $database);
	save_db_param('DB_USERNAME', $user);
	save_db_param('DB_PASSWORD', $passwd);

	/* DONE SETTING UP THE APPLICATION */
	$message = trans('exceptions.frontend.setup.setup_success');
	return redirect("/")->withFlashInfo($message);
	/*	 * **** end : setupDbConfig saving new context ***** */
    }

    protected function checkOsEnvironment() {
	/* Detect Operating System for setting pgsql command environment path */
	$os = getOS();
	$success = true;
	if (preg_match('/Linux/', $os)) {
	    $currpath = shell_exec("echo \$PATH 2>&1");
	    putenv("PATH=" . trim($currpath));
	    $output = shell_exec("which psql; which pg_dump; which pg_restore 2>&1");
	    if (preg_match('/which/', $output)) {
		$success = false;
		$message = trans('exceptions.frontend.set_up.missing_command', ['currpath' => $currpath]);
	    } else {
		/* check if .pgpass file exists and if not create */
		$pgpass_file = trim(shell_exec("echo ~")) . "/.pgpass";
		$this->savePgPassFile($pgpass_file);
		/* setting function delimeter for running psql commands, in window $$ has not problem, but in linux $$ conflicts, so dollar signs are escaped */
	    }
	} elseif (preg_match('/Windows/', $os)) {
	    //$currpath = shell_exec("echo %PATH% 2>&1");
	    $currpath = getenv("PATH");
	    putenv("PATH=" . trim($currpath));
	    $output = shell_exec("where psql & where pg_dump & where pg_restore 2>&1");
	    if (preg_match('/INFO/', $output)) {
		$success = false;
		$message = trans('exceptions.frontend.setup.missing_command', ['currpath' => $currpath]);
	    } else {
		/* check if .pgpass file exists and if not create */
		$pgpass_file = trim(shell_exec("echo %APPDATA% 2>&1")) . "\postgresql\pgpass.conf";
		$this->savePgPassFile($pgpass_file);
		/* setting function delimeter for running psql commands, in window $$ has not problem, but in linux $$ conflicts, so dollar signs are escaped  */
	    }
	} elseif (strpos($os, 'Mac')) {
	    $success = false;
	    $message = 'Setup not yet programmed for this OS';
	} else {
	    $success = false;
	    $message = trans('exceptions.frontend.setup.incompatible_os', ['os' => $os]);
	}
	if (!$success) {
	    return redirect()->back()->withFlashInfo($message);
	}
    }

    public function savePgPassFile($pgpass_file) {
	/* two lines in pgpass, one for current priviledged database owner & new reistered database account */
	if (isset(request()->CURRENT_DB_USERNAME)) {
	    $content = request()->DB_HOST . ":" . request()->DB_PORT . ":*:" . request()->CURRENT_DB_USERNAME . ":" . request()->CURRENT_DB_PASSWORD . "\n";
	    createFile($pgpass_file, $content);
	}
	$content = request()->DB_HOST . ":" . request()->DB_PORT . ":*:" . request()->DB_USERNAME . ":" . request()->DB_PASSWORD . "\n";
	createFile($pgpass_file, $content);
    }

    protected function getDelimiter() {
	$os = getOS();
	if (preg_match('/Linux/', $os)) {
	    $function_delimeter = "\\$\$";
	} elseif (preg_match('/Windows/', $os)) {
	    $function_delimeter = "$$";
	}
	return $function_delimeter;
    }

    public function getDbParam() {
	$param = ['DB_HOST' => get_db_param('DB_HOST'), 'DB_PORT' => get_db_param('DB_PORT'), 'DB_DATABASE' => get_db_param('DB_DATABASE'), 'DB_USERNAME' => get_db_param('DB_USERNAME'), 'DB_SCHEMA' => get_db_param('DB_SCHEMA')];
	return $param;
    }

    public function installMigration() {
	

	$host = request()->DB_HOST;
	$port = request()->DB_PORT;
	$database = strtolower(request()->DB_DATABASE);
	$schema = request()->DB_SCHEMA;
	$user = strtolower(request()->DB_USERNAME);
	$passwd = request()->DB_PASSWORD;
	
	save_db_param('DB_HOST', $host);
	save_db_param('DB_PORT', $port);
	save_db_param('DB_DATABASE', $database);
	save_db_param('DB_SCHEMA', $schema);
	save_db_param('DB_USERNAME', $user);
	save_db_param('DB_PASSWORD', $passwd);
	
	config(['database.connections.pgsql' => [
		'driver' => 'pgsql',
		'host' => request()->DB_HOST,
		'port' => request()->DB_PORT,
		'database' => request()->DB_DATABASE,
		'username' => request()->DB_USERNAME,
		'password' => request()->DB_PASSWORD,
		'charset' => 'utf8',
		'schema' => request()->DB_SCHEMA
	]]);
	
	Artisan::call('migrate', array('--path' => 'database/migrations/version1.0.0', '--force' => true));

	Artisan::call('db:seed');

	
	/* create super admin data */
	$super_user = ['password' => request()->password, 'email' => request()->email, 'gender' => request()->gender, 'officeid' => request()->officeid, 'username' => request()->username, 'othernames' => request()->othernames, 'firstname' => request()->firstname, 'checked' => '1', 'available' => '1'];
	$this->user->register($super_user, true);
	
	/* DONE SETTING UP THE APPLICATION */
	$message = trans('exceptions.frontend.setup.setup_success');
	return redirect("/")->withFlashInfo($message);
	/*	 * **** end : setupDbConfig saving new context ***** */
	
    }

}
