<?php

namespace App\Console\Commands\File;

use Illuminate\Console\Command;



class BulkImportManager extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:excelfile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This imports an excel file';
    
    protected $chunkSize = 100;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
	parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
	
    }

}
