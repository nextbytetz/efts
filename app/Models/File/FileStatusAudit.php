<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileStatusAudit
 */
class FileStatusAudit extends Model
{
    protected $table = 'file_status_audit';

    protected $primaryKey = 'file_status_audit_id';

	public $timestamps = true;

    protected $fillable = [
        'closed_at',
        'officer_opened',
        'officer_closed',
        'file_id',
        'date_created'
    ];

    protected $guarded = [];

        
}