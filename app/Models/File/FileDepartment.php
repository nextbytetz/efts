<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileDepartment
 */
class FileDepartment extends Model {

    protected $table = 'file_department';
    protected $primaryKey = 'file_department_id';
    public $timestamps = true;
    protected $fillable = [
	'file_id',
	'department_id',
	'status'
    ];
    protected $guarded = [];

}
