<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileOfficer
 */
class FileOfficer extends Model
{
    protected $table = 'file_officer';

    protected $primaryKey = 'file_officer_id';

	public $timestamps = true;

    protected $fillable = [
        'file_id',
        'officer_id',
        'status'
    ];

    protected $guarded = [];

        
}