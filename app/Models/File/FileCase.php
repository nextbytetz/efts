<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Attribute\FileCaseAttribute;
use App\Models\File\Traits\Relationship\FileCaseRelationship;

/**
 * Class FileCase
 */
class FileCase extends Model {

    use FileCaseAttribute,
	FileCaseRelationship;

    protected $table = 'file_case';
    protected $primaryKey = 'file_case_id';
    public $timestamps = true;
    protected $fillable = [
	'file_subject_id',
	'name',
    ];
    protected $guarded = [];

}
