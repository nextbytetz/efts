<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Attribute\BulkAttribute;
use App\Models\File\Traits\Relationship\BulkRelationship;
use App\Models\File\Traits\BulkAccess;

class Bulk extends Model {

    use BulkAttribute,
	BulkRelationship,
	BulkAccess;

    protected $table = 'bulk';
    protected $primaryKey = 'bulk_id';
    public $timestamps = true;
    protected $dates = ['created_at'];
    protected $guarded = [
	'notice'
    ];

}
