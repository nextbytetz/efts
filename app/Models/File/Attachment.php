<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Attachment
 */
class Attachment extends Model
{
    protected $table = 'attachment';

    protected $primaryKey = 'attachment_id';

	public $timestamps = true;

    protected $fillable = [
        'path',
        'mime'
    ];

    protected $guarded = [];

        
}