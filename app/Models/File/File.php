<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\File\Traits\Relationship\FileRelationship;
use App\Models\File\Traits\Attribute\FileAttribute;
use App\Models\File\Traits\FileAccess;

/**
 * Class File
 */
class File extends Model {
    
    use FileRelationship, FileAttribute, FileAccess, SoftDeletes;

    protected $table = 'file';
    protected $primaryKey = 'file_id';
    public $timestamps = true;
    protected $fillable = [
	'reference',
	'name',
	'subject',
	'status',
	'description',
	'file_category_id',
	'file_type_id',
	'file_subject_id',
	'shelf_number',
	'file_nature_id',
	'officer_opened',
	'epc',
	'officer_edited',
    ];
    protected $guarded = [];
    protected $dates = ['deleted_at'];

}
