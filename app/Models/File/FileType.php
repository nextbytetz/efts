<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileType
 */
class FileType extends Model
{
    protected $table = 'file_type';

    protected $primaryKey = 'file_type_id';

	public $timestamps = true;

    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

        
}