<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Relationship\FilePickupRelationship;
use App\Models\File\Traits\Attribute\FilePickupAttribute;
/**
 * Class FilePickup
 */
class FilePickup extends Model {
    
    use FilePickupRelationship, FilePickupAttribute;

    protected $table = 'file_pickup';
    protected $primaryKey = 'file_pickup_id';
    public $timestamps = true;
    protected $fillable = [
	'file_id',
	'officer_id',
        'forced',
	'file_case_id',
    ];
    protected $guarded = [];

}
