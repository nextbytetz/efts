<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Attribute\PriorityAttribute;

/**
 * Class File
 */
class Priority extends Model {
    
    use PriorityAttribute;

    protected $table = 'priority';
    protected $primaryKey = 'priority_id';
    public $timestamps = true;
    protected $fillable = [
'name'
    ];
    protected $guarded = [];

}