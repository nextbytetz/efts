<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Attribute\TrackAttribute;
use App\Models\File\Traits\Relationship\TrackRelationship;

class Track extends Model {

    use TrackAttribute,
	TrackRelationship;

    protected $table = 'track';
    protected $primaryKey = 'track_id';
    public $timestamps = true;
    protected $guarded = [
	'created_at',
	'updated_at',
    ];

}
