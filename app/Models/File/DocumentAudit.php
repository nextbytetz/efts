<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentAudit
 */
class DocumentAudit extends Model
{
    protected $table = 'document_audit';

    protected $primaryKey = 'document_audit_id';

	public $timestamps = true;

    protected $fillable = [
        'document_id',
        'officer_id',
        'path',
        'name',
        'description',
        'real_name'
    ];

    protected $guarded = [];

        
}