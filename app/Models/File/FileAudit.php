<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileAudit
 */
class FileAudit extends Model
{
    protected $table = 'file_audit';

    protected $primaryKey = 'file_audit_id';

	public $timestamps = true;

    protected $fillable = [
        'file_id',
        'officer_id',
        'reference',
        'name',
        'subject',
        'description',
        'epc',
        'file_category_id',
        'file_type_id',
        'shelf_number'
    ];

    protected $guarded = [];

        
}