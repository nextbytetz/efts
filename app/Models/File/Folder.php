<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\File\Traits\Relationship\FolderRelationship;
use App\Models\File\Traits\Attribute\FolderAttribute;

/**
 * Class File
 */
class Folder extends Model {
    
    use FolderRelationship, FolderAttribute, SoftDeletes;

    protected $table = 'folder';
    protected $primaryKey = 'folder_id';
    public $timestamps = true;
    protected $fillable = [
	'file_id',
	'parent_id',
	'name',
	'officer_id'
    ];
    protected $guarded = [];
    protected $dates = ['deleted_at'];

}
