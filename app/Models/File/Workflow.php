<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Relationship\WorkflowRelationship;
use App\Models\File\Traits\Attribute\WorkflowAttribute;
use App\Models\File\Traits\WorkflowAccess;

/**
 * Class Workflow
 */
class Workflow extends Model {

    use WorkflowRelationship,
	WorkflowAttribute,
	WorkflowAccess;

    protected $table = 'workflow';
    protected $primaryKey = 'workflow_id';
    public $timestamps = true;
    protected $fillable = [
	'file_id',
	'officer_id',
	'comment',
    ];
    protected $guarded = [];

}
