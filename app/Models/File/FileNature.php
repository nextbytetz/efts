<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileNature
 */
class FileNature extends Model {

    protected $table = 'file_nature';
    protected $primaryKey = 'file_nature_id';
    public $timestamps = true;
    protected $fillable = [
	'name'
    ];
    protected $guarded = [];

}
