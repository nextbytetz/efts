<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Attribute\FileSubjectAttribute;
use App\Models\File\Traits\Relationship\FileSubjectRelationship;

/**
 * Class FileSubject
 */
class FileSubject extends Model {

    use FileSubjectAttribute,
	FileSubjectRelationship;

    protected $table = 'file_subject';
    protected $primaryKey = 'file_subject_id';
    public $timestamps = true;
    protected $fillable = [
	'name',
    ];
    protected $guarded = [];

}
