<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\File\Traits\Attribute\DocumentAttribute;
use App\Models\File\Traits\Relationship\DocumentRelationship;

/**
 * Class Document
 */
class Document extends Model {

    use SoftDeletes,
	DocumentAttribute,
	DocumentRelationship;

    protected $table = 'document';
    protected $primaryKey = 'document_id';
    public $timestamps = true;
    protected $fillable = [
	'name',
	'ext',
	'size',
	'mime',
	'folder_id',
	'file_id',
	'officer_id',
    ];
    protected $guarded = [];
    protected $dates = ['deleted_at'];

}
