<?php

namespace App\Models\File\Traits\Relationship;

trait DocumentRelationship {

    public function folder() {
	return $this->belongsTo(\App\Models\File\Folder::class, 'folder_id', 'folder_id');
    }

    public function file() {
	return $this->belongsTo(\App\Models\File\File::class, 'file_id', 'file_id');
    }

    public function user() {
	return $this->hasOne(\App\Models\Access\User::class, 'officer_id', 'officer_id');
    }

    public function priority() {
	return $this->hasOne(\App\Models\File\Priority::class, 'priority_id', 'priority_id');
    }

}
