<?php

namespace App\Models\File\Traits\Relationship;

trait WorkflowOfficerRelationship {
  
    public function user() {
	return $this->belongsTo(\App\Models\Access\User::class, 'officer_id', 'officer_id');
    }

    public function workflow() {
	return $this->belongsTo(\App\Models\File\Workflow::class, 'workflow_id', 'workflow_id');
    }

}
