<?php

namespace App\Models\File\Traits\Relationship;

trait TrackRelationship {

    public function file() {
	return $this->belongsTo(\App\Models\File\File::class, 'epc', 'epc');
    }

    public function zone() {
	return $this->belongsTo(\App\Models\Location\Zone::class, 'zone_id', 'zone_id');
    }

}
