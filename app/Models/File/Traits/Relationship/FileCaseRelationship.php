<?php

namespace App\Models\File\Traits\Relationship;

trait FileCaseRelationship {

    public function subject() {
	return $this->belongsTo(\App\Models\File\FileSubject::class, 'file_subject_id', 'file_subject_id');
    }

}
