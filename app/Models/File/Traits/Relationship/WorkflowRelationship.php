<?php

namespace App\Models\File\Traits\Relationship;

trait WorkflowRelationship {
  
    public function users() {
	return $this->belongsToMany(\App\Models\Access\User::class, 'workflow_officer', 'workflow_id', 'officer_id')->withPivot('received_at', 'intent');
    }

    public function file() {
	return $this->belongsTo(\App\Models\File\File::class, 'file_id', 'file_id');
    }
    
    public function priority() {
	return $this->hasOne(\App\Models\File\Priority::class, 'priority_id', 'priority_id');
    }
    
    public function user() {
	return $this->belongsTo(\App\Models\Access\User::class, 'officer_id', 'officer_id');
    }
    
    public function workflowOfficers() {
	return $this->hasMany(\App\Models\File\WorkflowOfficer::class, 'workflow_id', 'workflow_id')->orderBy('workflow_officer_id', 'desc');
    }

}
