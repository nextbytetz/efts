<?php

namespace App\Models\File\Traits\Relationship;

trait FileSubjectRelationship {

    public function files() {
	return $this->hasMany(\App\Models\File\File::class, 'file_subject_id', 'file_subject_id');
    }

    public function cases() {
	return $this->hasMany(\App\Models\File\FileCase::class, 'file_subject_id', 'file_subject_id');
    }

}
