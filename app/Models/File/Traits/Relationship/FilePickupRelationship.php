<?php

namespace App\Models\File\Traits\Relationship;

trait FilePickupRelationship {

    public function user() {
	return $this->belongsTo(\App\Models\Access\User::class, 'officer_id', 'officer_id');
    }

    public function file() {
	return $this->belongsTo(\App\Models\File\File::class, 'file_id', 'file_id');
    }

    public function cases() {
	return $this->belongsToMany(\App\Models\File\FileCase::class, 'pickup_case', 'file_pickup_id', 'file_case_id');
    }

}
