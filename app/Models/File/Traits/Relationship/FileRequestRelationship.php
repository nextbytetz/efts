<?php

namespace App\Models\File\Traits\Relationship;

trait FileRequestRelationship {
  
    public function user() {
	return $this->belongsTo(\App\Models\Access\User::class, 'officer_id', 'officer_id');
    }

    public function file() {
	return $this->belongsTo(\App\Models\File\File::class, 'file_id', 'file_id');
    }

}