<?php

namespace App\Models\File\Traits\Relationship;

trait FileRelationship {

    public function departments() {
	return $this->belongsToMany(\App\Models\Location\Department::class, 'file_department', 'file_id', 'department_id');
    }

    public function users() {
	return $this->belongsToMany(\App\Models\Access\User::class, 'file_officer', 'file_id', 'officer_id');
    }

    public function requests() {
	return $this->hasMany(\App\Models\File\FileRequest::class, 'file_id', 'file_id');
    }

    public function statusAudit() {
	return $this->hasMany(\App\Models\File\FileStatusAudit::class, 'file_id', 'file_id');
    }

    public function documents() {
	return $this->hasMany(\App\Models\File\Document::class, 'file_id', 'file_id')->orderBy('document_id', 'desc');
    }

    public function audit() {
	return $this->hasMany(\App\Models\File\FileAudit::class, 'file_id', 'file_id');
    }

    public function workflow() {
	return $this->hasMany(\App\Models\File\Workflow::class, 'file_id', 'file_id')->orderBy('workflow_id', 'desc');
    }

    public function category() {
	return $this->belongsTo(\App\Models\File\FileCategory::class, 'file_category_id', 'file_category_id');
    }

    public function nature() {
	return $this->belongsTo(\App\Models\File\FileNature::class, 'file_nature_id', 'file_nature_id');
    }

    public function type() {
	return $this->belongsTo(\App\Models\File\FileType::class, 'file_type_id', 'file_type_id');
    }

    public function folders() {
	return $this->hasMany(\App\Models\File\Folder::class, 'file_id', 'file_id');
    }

    public function pickups() {
	return $this->hasMany(\App\Models\File\FilePickup::class, 'file_id', 'file_id')->orderBy('file_pickup_id', 'desc');
    }

    public function active_pickups() {
	return $this->belongsToMany(\App\Models\File\FilePickup::class, 'file_pickup_active', 'file_id', 'file_pickup_id');
    }

    public function userOpened() {
	return $this->hasOne(\App\Models\Access\User::class, 'officer_id', 'officer_opened');
    }

    public function userEdited() {
	return $this->hasOne(\App\Models\Access\User::class, 'officer_id', 'officer_edited');
    }

    public function priority() {
	return $this->hasOne(\App\Models\File\Priority::class, 'priority_id', 'priority_id');
    }

    public function subject() {
	return $this->hasOne(\App\Models\File\FileSubject::class, 'file_subject_id', 'file_subject_id');
    }

    public function track() {
	return $this->hasMany(\App\Models\File\Track::class, 'epc', 'epc')->orderBy('track_id', 'desc');
    }

}
