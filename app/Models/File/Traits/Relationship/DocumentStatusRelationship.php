<?php

namespace App\Models\File\Traits\Relationship;

trait DocumentStatusRelationship {

    public function user() {
	return $this->hasOne(\App\Models\Access\User::class, 'officer_id', 'officer_id');
    }

    public function document() {
	return $this->hasOne(\App\Models\File\Document::class, 'document_id', 'document_id');
    }

    public function priority() {
	return $this->hasOne(\App\Models\File\Priority::class, 'priority_id', 'priority_id');
    }

}
