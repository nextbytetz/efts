<?php

namespace App\Models\File\Traits\Relationship;

trait FolderRelationship {

    public function documents() {
	return $this->hasMany(\App\Models\File\Document::class, 'folder_id', 'folder_id');
    }

    public function upFolder() {
	return $this->belongsTo(\App\Models\File\Folder::class, 'parent_id', 'folder_id');
    }

    public function file() {
	return $this->belongsTo(\App\Models\File\File::class, 'file_id', 'file_id');
    }

    public function downFolder() {
	return $this->hasMany(\App\Models\File\Folder::class, 'parent_id', 'folder_id');
    }

}
