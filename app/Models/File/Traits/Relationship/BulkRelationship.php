<?php

namespace App\Models\File\Traits\Relationship;

trait BulkRelationship {

    public function users() {
	return $this->belongsToMany(\App\Models\Access\User::class, 'bulk_officer', 'bulk_id', 'officer_id');
    }

    public function departments() {
	return $this->belongsToMany(\App\Models\Location\Department::class, 'bulk_department', 'bulk_id', 'department_id');
    }

    public function user() {
	return $this->belongsTo(\App\Models\Access\User::class, 'officer_id', 'officer_id');
    }

}
