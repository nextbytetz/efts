<?php

namespace App\Models\File\Traits;

trait WorkflowAccess {

    public function attachUser($user) {
	if (is_object($user)) {
	    $user = $user->getKey();
	}

	if (is_array($user)) {
	    $user = $user['officer_id'];
	}

	$this->users()->attach($user);
    }

    public function detachUser($user) {
	if (is_object($user)) {
	    $user = $user->getKey();
	}

	if (is_array($user)) {
	    $user = $user['officer_id'];
	}

	$this->users()->detach($user);
    }

    public function attachUsers($users) {
	foreach ($users as $user) {
	    $this->attachUser($user);
	}
    }

    public function detachUsers($users) {
	foreach ($users as $user) {
	    $this->detachUser($user);
	}
    }

}