<?php

namespace App\Models\File\Traits\Attribute;

trait FolderAttribute {

    public function getPathAttribute() {
	$loop = true;
	$stack = array();
	$folder = $this;
	while ($loop) {
	    array_push($stack, $folder->name);
	    if (count($folder->upfolder)) {
		$folder = \App\Models\File\Folder::where(['folder_id' => $folder->parent_id])->first();
	    } else {
		$loop = false;
	    }
	}
	array_push($stack, $this->file->name);
	$reversed = array_reverse($stack);
	$path = "";
	foreach ($reversed as $value) {
	    $path = $path . $value . " " . DIRECTORY_SEPARATOR . " ";
	}
	return $path;
    }

    public function getPathIdAttribute() {
	$loop = true;
	$stack = array();
	$folder = $this;
	while ($loop) {
	    array_push($stack, $folder->folder_id);
	    if (count($folder->upfolder)) {
		$folder = \App\Models\File\Folder::where(['folder_id' => $folder->parent_id])->first();
	    } else {
		$loop = false;
	    }
	}
	array_push($stack, $this->file->file_id);
	$reversed = array_reverse($stack);
	$path = "";
	foreach ($reversed as $value) {
	    $path = $path . $value . DIRECTORY_SEPARATOR;
	}
	return $path;
    }

}
