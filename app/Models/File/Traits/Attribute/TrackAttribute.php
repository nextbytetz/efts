<?php

namespace App\Models\File\Traits\Attribute;

use Carbon\Carbon;

trait TrackAttribute {

    public function getCreatedAtAttribute($value) {
	return Carbon::parse($value)->diffForHumans();
    }
    
    public function getUpdatedAtAttribute($value) {
	return Carbon::parse($value)->diffForHumans();
    }

}
