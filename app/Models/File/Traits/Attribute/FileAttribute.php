<?php

namespace App\Models\File\Traits\Attribute;

use Carbon\Carbon;

trait FileAttribute {

    public function getIconAttribute() {
	return "<img src='" . public_url() . "/assets/images/folder.png" . "'  alt='' height='24' width='24'>";
    }

    public function getGeneralInfoAttribute() {
	return
		$this->openFile() . "</br>" .
		"<font color='DarkSlateGray'>" . trans("labels.backend.file.created_by") . "&nbsp;:&nbsp;" . $this->useropened->name() . ", " . trans("labels.backend.file.created") . "&nbsp;:&nbsp;" . $this->created_at->diffForHumans() . "</font></br>" .
		"<font color='MidnightBlue'>" . trans("labels.backend.file.subject") . "&nbsp;:&nbsp;" . (count($this->subject) ? $this->subject->name : "") . "</font></br>" .
		"<font color='DarkGreen'>" . trans("labels.backend.file.nature") . "&nbsp;:&nbsp;" . $this->nature->name . "</font><br/>" .
		"<font color='DarkBlue'>" . trans("labels.backend.file.state.title") . "&nbsp;:&nbsp;" . $this->fileState() . "</font><br/>" .
		"<font color='RebeccaPurple'>" . trans("labels.general.action") . "&nbsp;:&nbsp;" . $this->fileAction() . "</font><br/>" .
		"<font color='Teal'>" . trans("labels.backend.file.request") . "&nbsp;:&nbsp;" . $this->requestUsersSummary() . "</font><br/>" .
		"<i class='fa fa-location-arrow'></i>&nbsp;&nbsp;" . trans("labels.backend.file.location") . "&nbsp;:&nbsp;" . $this->fileLocation();
    }

    public function openFile() {
	if (in_array($this->getState(), [4, 7]))
	    return "<i class='fa fa-check-square-o'></i>&nbsp;&nbsp;<b><a href='" . route('file.register.show', $this) . "'>" . $this->name . "</a></b>";
	return "<b>" . $this->name . "</b>";
    }

    public function fileLocation() {
	if (count($this->track()->first())) {
	    $departments = [];
	    foreach ($this->track()->first()->zone->departments as $department) {
		$departments[] = $department->name;
	    }
	    $department_list = implode(", ", $departments);
	    $location = "<b>" . $this->track()->first()->zone->name . "</b>" . "; " . trans('labels.general.departments') . " - " . $department_list;
	} else {
	    $location = trans('labels.general.untracked');
	}
	return "<u style='border-bottom: 1px dashed #c00;text-decoration: none;'>" . $location . "</u>";
    }

    public function fileState() {
	switch ($this->getState()) {
	    case 1:
		return trans("labels.backend.file.state.disposed");
	    case 2:
		return trans("labels.backend.file.state.picked") . " " . $this->pickedUser();
	    case 3:
		return trans("labels.backend.file.state.pending") . " " . $this->pendingUsers();
	    case 4:
	    case 7:
		return trans("labels.backend.file.state.picked") . " " . $this->pickedUsersSummary();
	    case 5:
		return trans("labels.backend.file.state.picked") . " " . $this->pickedUsersSummary();
	    case 6:
		return trans("labels.backend.file.state.pending") . " " . $this->pendingUsers();
	}
	return '';
    }

    /**
     * 
     * @return string
     * 1,3 : File should be Picked up.
     * 2 : File should be disposed. 
     */
    public function fileAction() {
	switch ($this->getState()) {
	    //Pick file
	    case 1:
	    case 5:
		return "<i class='fa fa-level-down'></i>&nbsp;&nbsp;" . $this->getPickFileLink();
	    //Request file | Forced Pick file (this action will logout user)
	    case 2:
		return "<i class='fa fa-caret-square-o-left'></i>&nbsp;&nbsp;" . $this->getRequestFileLink() . "&nbsp;&nbsp;|&nbsp;&nbsp;" . "<i class='fa fa-caret-square-o-down'></i>&nbsp;&nbsp;" . $this->getForcedPickFileLink();
	    //Forward/Dipatch file
	    case 4:
	    case 7:
		if ($this->file_category_id == 1) {
		    return "<i class='fa fa-mail-forward'></i>&nbsp;&nbsp;" . link_to_route('file.process.dispatch', trans("labels.backend.file_process.dispose_file"), [$this->file_id], ['class' => 'btn btn-xs btn-success']) . "&nbsp;&nbsp;|&nbsp;&nbsp;<i class='fa fa-mail-reply'></i>&nbsp;&nbsp;" . link_to_route('file.process.release', trans('labels.backend.file_process.release'), [$this->file_id], ['class' => 'btn btn-xs btn-warning']);
		} else {
		    return trans('labels.general.none');
		}
	    //Receive file
	    case 6:
		return "<i class='fa fa-level-down'></i>&nbsp;&nbsp;" . link_to_route('file.process.receive', trans("labels.backend.file_process.receive"), [$this->file_id], ['class' => 'btn btn-xs btn-primary']);
	    //Pick file with warning
	    case 3:
		return "<i class='fa fa-level-down'></i>&nbsp;&nbsp;" . $this->getWarnedPickFileLink();
	}
	return '';
    }

    public function getRequestFileLink() {
	$request = $this->requests()->where(['file_request.officer_id' => access()->id(), 'file_request.status' => '0'])->first();
	if (count($request)) {
	    //cancel created request
	    return link_to_route('file.process.request.cancel', trans("labels.backend.file_process.request.cancel"), [$this->file_id], ['class' => 'btn btn-xs btn-primary']);
	} else {
	    //create new request	    
	    return link_to_route('file.process.request', trans("labels.backend.file_process.request.make"), [$this->file_id], ['class' => 'btn btn-xs btn-primary']);
	}
    }

    public function getPickFileLink() {
	return '<a href="' . route('file.process.pick', $this->file_id) . '"
                 data-method="case_modal"
		 data-choice="2"
		 data-file-name="' . $this->name . '"
		 data-file-id="' . $this->file_id . '"    
		 data-create-case-url="' . route('sysdef.file.case.create') . '?file_subject_id=' . (count($this->subject) ? $this->subject->file_subject_id : 0) . '" 
		 data-href="' . route('file.process.pick', $this->file_id) . '" 
                 class="btn btn-xs btn-info">' . trans("labels.backend.file_process.pick_file") . '</a> ';
    }

    public function getForcedPickFileLink() {
	return '<a href="' . route('file.process.pick.forced', $this->file_id) . '"
                 data-method="confirm"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.file_process.forced_pick') . '"
		 data-choice="2"
		 data-file-name="' . $this->name . '"
		 data-file-id="' . $this->file_id . '"    
		 data-create-case-url="' . route('sysdef.file.case.create') . '?file_subject_id=' . (count($this->subject) ? $this->subject->file_subject_id : 0) . '" 
		 data-href="' . route('file.process.pick.forced', $this->file_id) . '" 
                 class="btn btn-xs btn-danger">' . trans("labels.backend.file_process.pick_forced") . '</a> ';
    }

    public function getWarnedPickFileLink() {
	return '<a href="' . route('file.process.pick.warned', $this->file_id) . '"
                 data-method="confirm"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.file_process.pick_warned') . '"
		 data-choice="2"
		 data-file-name="' . $this->name . '"
		 data-file-id="' . $this->file_id . '"    
		 data-create-case-url="' . route('sysdef.file.case.create') . '?file_subject_id=' . (count($this->subject) ? $this->subject->file_subject_id : 0) . '" 
		 data-href="' . route('file.process.pick.warned', $this->file_id) . '" 
                 class="btn btn-xs btn-warning">' . trans("labels.backend.file_process.pick_file") . '</a> ';
    }

    /**
     * 4 : {{My Files}}
     * 6 : {{Received Files}}
     * 7 : {{File Requests}}
     * @return int
     * 1 : Disposed/Released [Action : Pick file, location : Disposed/Released]
     * 2 : Pickup By ... (Another Officer) [Action : Request file, location : Picked up by .......(mention officer)]
     * 3 : Pending Receipt to .... (Officers)  [Action : Pick file with warning, location : Pending receipt to .....(mention officer)]
     * 4 : Pickup By .... (Me) , [Action : Forward/Dispatch file, location : Pickedup by .....(mention officer)] {{My File}}
     * 5 : Pickup By at least one officer but [Action : Pick file, location : Picked up by ....... (mention few officer)] 
     * 6 : Pending Receipt to .... (Officers including me)  [Action : Receive File, location : Pending receipt to .....(mention officer)] {{Received Files}}
     * 7 : Pickup By .... (Me, Has file request) See Officers who have requested file, [Action : Forward/Dispatch file, location : Pickedup by .....(mention officer)] {{File Request}}
     */
    public function getState() {
	//check if this file category is normal (can only be sent to one user)
	if ($this->file_category_id == 1) {
	    return $this->getStateNormal();
	} else {
	    return $this->getStateNotice();
	}
    }

    private function getStateNotice() {
	//this file category is notice (It has no need to be dispatched ...)
	//check if this file has pickups even one.
	if (count($this->pickups()->first())) {
	    $checkPickup = $this->pickups()->where('disposed_at', '=', null)->first();
	    if (count($checkPickup)) {
		$myPickUp = $this->pickups()->where('officer_id', '=', access()->id())->first();
		if (count($myPickUp) And is_null($myPickUp->disposed_at)) {
		    return 4;
		}
		return 5;
	    } else {
		return 1;
	    }
	} else {
	    return 1;
	}
    }

    private function getStateNormal() {
	//get all active pickups for this file
	$pickUps = $this->pickups()->first();
	//this file category is normal
	//Check if file has even one pickup records
	if (count($pickUps)) {
	    //Check if file has been picked up but not yet disposed/released
	    if (is_null($pickUps->disposed_at)) {
		//check if the file has been picked up by me
		if ($pickUps->user->officer_id === access()->id()) {
		    //check if this file has active requests
		    if (count($this->requests()->where('status', '=', '0')->first())) {
			return 7;
		    }
		    return 4;
		} else {
		    return 2;
		}
	    } else {
		$workflow = $this->workflow->first();
		//check if this file has even one workflow and workflow done is 0 (Has pending users to receive file)
		if (count($workflow) And $workflow->done == 0) {
		    //check when the last time workflow received is not later than recent released date, if release date is later, then file released.
		    $workflow_officer = $workflow->workflowOfficers()->first();
		    if (count($workflow_officer) And ! is_null($workflow_officer->received_at)) {
			$startTime = Carbon::parse($workflow_officer->received_at);
			$finishTime = Carbon::parse($pickUps->disposed_at);
			$totalDuration = $finishTime->diffInSeconds($startTime);
			if ($totalDuration > 0) {
			    return 1;
			}
		    }
		    //check if i am one of the officers who has to receive the file
		    $users = $workflow->users()->where('officer.officer_id', '=', access()->id())->first();
		    if (count($users)) {
			return 6;
		    }
		    return 3;
		} else {
		    return 1;
		}
	    }
	} else {
	    return 1;
	}
    }

    public function getSearchLink() {
	return link_to_route('file.register.direct.search', $this->name, [$this->file_id], ['class' => 'btn btn-xs btn-info']);
    }

    public function pickedUser() {
	return $this->pickups->first()->user->name();
    }

    public function pickedUsers() {
	$officer = [];
	$pickups = $this->pickups()->where(['disposed_at' => null])->get();
	foreach ($pickups as $pickup) {
	    $officer[] = $pickup->user->name();
	}
	return implode(", ", $officer);
    }

    public function pickedUsersSummary() {
	$officer = [];
	$pickups = $this->pickups()->where(['disposed_at' => null])->get();
	foreach ($pickups as $pickup) {
	    $officer[] = $pickup->user->name();
	}
	if (count($officer) > 5) {
	    $officer = array_slice($officer, 0, 5);
	    return implode(", ", $officer) . " " . link_to_route('file.process.pick.users', trans("labels.general.others"), [$this->file_id]);
	} else {
	    return implode(", ", $officer);
	}
    }

    public function requestUsers() {
	$requests = $this->requests()->where(['status' => '0'])->get();
	if (count($requests)) {
	    $officer = [];
	    foreach ($requests as $request) {
		$officer[] = $request->user->name();
	    }
	    return implode(", ", $officer);
	}
	return trans("labels.general.none");
    }

    public function requestUsersSummary() {
	$requests = $this->requests()->where(['status' => '0'])->get();
	if (count($requests)) {
	    $officer = [];
	    foreach ($requests as $request) {
		$officer[] = $request->user->name();
	    }
	    if (count($officer) > 5) {
		$officer = array_slice($officer, 0, 5);
		return implode(", ", $officer) . " " . link_to_route('file.process.request.users', trans("labels.general.others"), [$this->file_id]);
	    } else {
		return implode(", ", $officer);
	    }
	}
	return trans("labels.general.none");
    }

    public function pendingUsers() {
	$officer = [];
	foreach ($this->workflow->first()->users as $user) {
	    $officer[] = $user->name();
	}
	if (count($officer) > 5) {
	    $officer = array_slice($officer, 0, 5);
	    return implode(", ", $officer) . " " . link_to_route('file.process.workflow.users', trans("labels.general.others"), [$this->file_id]);
	} else {
	    return implode(", ", $officer);
	}
    }

    public function getListUsersAttribute() {
	$users = [];
	switch ($this->file_type_id) {
	    case '1' :
		foreach ($this->departments as $department) {
		    $officers = $department->users()->where('officer.officer_id', '<>', access()->id())->pluck('officer.officer_id')->all();
		    $users = array_merge($users, $officers);
		}
		break;
	    case '2':
		$officers = $this->users()->where('officer.officer_id', '<>', access()->id())->pluck('officer.officer_id')->all();
		$users = array_merge($users, $officers);
		break;
	}
	return $users;
    }
    
    public function recentLog() {
	$workflow = $this->workflow->first();
	if (count($workflow))
	    return $workflow->showComment();
	return trans('labels.general.none');
    }

    public function getStatusInfoAttribute() {
	return
		$this->folders->count() . " " . trans("labels.backend.file.folder") . "</br>" .
		$this->documents->count() . " " . trans("labels.backend.file.document") . "</br>" .
		$this->userCount();
    }

    public function getActiveCaseAttribute() {
	$cases = $this->pickups()->first()->cases;
	$case_arr = [];
	foreach ($cases as $case) {
	    $case_arr[] = $case->name;
	}
	return implode(", ", $case_arr);
    }

    public function getEditFileCaseLink() {
	return '<a href="' . route('file.process.cases.edit', $this->file_id) . '"
                 data-method="case_modal"
		 data-choice="2"
		 data-file-name="' . $this->name . '"
		 data-file-id="' . $this->file_id . '"    
		 data-create-case-url="' . route('sysdef.file.case.create') . '?file_subject_id=' . (count($this->subject) ? $this->subject->file_subject_id : 0) . '" 
		 data-href="' . route('file.process.cases.edit', $this->file_id) . '" 
                 class="btn btn-xs btn-info">' . trans("labels.backend.file_process.case.edit") . '</a> ';
    }

    public function userCount() {
	if ($this->file_type_id == 1) {
//Public file
	    $return = $this->departments->count() . " " . trans("labels.general.departments");
	} else {
//Confidential or Private file
	    $return = $this->users->count() . " " . trans("labels.general.officers");
	}
	return $return;
    }

    public function getDeleteButtonAttribute() {
	if (access()->id() == $this->officer_opened) {
	    return '<a href="' . route('file.register.destroy', $this->file_id) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.general.delete_message') . '"
                 ><i class="fa fa-trash"></i>&nbsp;&nbsp;' . trans('buttons.general.crud.delete') . '</a> ';
	}
	return '';
    }

    public function getEditButtonAttribute() {
	if (access()->allow('create_edit_file')) {
	    return '<a href="' . route('file.register.edit', $this) . '"><i class="fa fa-pencil"></i>&nbsp;&nbsp;' . trans('buttons.general.crud.edit') . '</a> ';
	}
	return '';
    }

    public function getCopyButtonAttribute() {
	return '<a href="#"><i class="fa fa-copy"></i>&nbsp;&nbsp;' . trans('buttons.general.copy') . '</a> ';
    }

    public function getActionButtonsAttribute() {
	return
		'<div class="btn-group">' .
		'<button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' .
		trans('labels.general.actions') .
		'&nbsp;<span class="caret"></span>' .
		'</button>' .
		'<ul class="dropdown-menu dropdown-menu-right" role="menu">' .
		'<li>' . $this->getDeleteButtonAttribute() . '</li>' .
		'<li>' . $this->getEditButtonAttribute() . '</li>' .
		'<li>' . $this->getCopyButtonAttribute() . '</li>' .
		'</ul>' .
		'</div>';
    }

}
