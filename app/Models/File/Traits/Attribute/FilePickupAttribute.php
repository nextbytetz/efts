<?php

namespace App\Models\File\Traits\Attribute;

trait FilePickupAttribute {

    public function getCaseAttribute() {
	$cases = $this->cases;
	$case_arr = [];
	foreach ($cases as $case) {
	    $case_arr[] = $case->name;
	}
	return implode(", ", $case_arr);
    }

}
