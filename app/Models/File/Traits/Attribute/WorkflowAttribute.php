<?php

namespace App\Models\File\Traits\Attribute;

trait WorkflowAttribute {

    public function getLogLabelAttribute() {
	return
		"<i class='fa fa-user'></i>&nbsp;&nbsp;" . trans("labels.general.officer") . "&nbsp;&nbsp;:&nbsp;&nbsp;" . $this->user->name() . "<br/>" .
		trans("labels.backend.file.dispatch.to") . "&nbsp;&nbsp;:&nbsp;&nbsp;" . $this->showdispatchUsers() . "<br/>" .
		trans("labels.backend.file.table.priority") . "&nbsp;&nbsp;:&nbsp;&nbsp;" . $this->showPriority() . "<br/>" .
		"<b>" . trans("labels.general.message") . "</b>" . "&nbsp;&nbsp;:&nbsp;&nbsp;" . $this->showComment();
    }

    public function getTimeLabelAttribute() {
	return "<i class='fa fa-clock-o'></i> &nbsp;&nbsp;" . $this->updated_at->diffForHumans();
    }

    public function showDispatchUsers() {
	if (count($this->users)) {
	    $intent = [];
	    $other = [];
	    foreach ($this->users as $user) {
		if ($user->pivot->intent == '1') {
		    $intent[] = $user->name();
		} else {
		    $other[] = $user->name();
		}
	    }
	    if (count($other)) {
		return implode(", ", $intent) . "; " . trans("labels.backend.file.state.picked") . " " . implode(", ", $other);
	    } else {
		return implode(", ", $intent);
	    }
	}
	return trans("labels.general.none");
    }

    public function showPriority() {
	if (is_null($this->priority_id))
	    return trans("labels.general.none");
	return $this->priority->name;
    }

    public function showComment() {
	/*
	  $comment = preg_replace_callback('/@\(\"([^"]+)\"\)/', function($matches) {
	  $document = $this->file->documents()->where("name", "=", $matches[1])->first();
	  if (count($document)) {
	  return link_to_route('storage.document.show', $matches[1], [$document->document_id]);
	  } else {
	  return $matches[1];
	  }
	  }, $this->comment);
	  return $comment;
	 */
	return $this->comment;
    }

    public function getLinkAttribute() {
	return route('file.process.log', $this->file->file_id);
    }

}
