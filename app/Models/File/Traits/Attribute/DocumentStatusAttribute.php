<?php

namespace App\Models\File\Traits\Attribute;

trait DocumentStatusAttribute {
    
    public function getLinkAttribute() {
	return route('storage.document.show', $this->document->document_id);
    }

    public function getDeleteButtonAttribute() {
	if (access()->id() == $this->officer_id) {
	    return '<a href="' . route('storage.document.destroy_status', $this) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.general.delete_message') . '"
                 class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
	    ;
	}
	return '';
    }

    public function getEditButtonAttribute() {
	if (access()->id() == $this->officer_id) {
	    return '<a href="' . route('storage.document.edit_status', $this) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
	}
	return '';
    }

    public function getActionButtonsAttribute() {
	return
		$this->getDeleteButtonAttribute();
		//$this->getEditButtonAttribute();
    }

}
