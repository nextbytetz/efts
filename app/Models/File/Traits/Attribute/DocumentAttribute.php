<?php

namespace App\Models\File\Traits\Attribute;

trait DocumentAttribute {

    public function getIconAttribute() {
	$return = "<a style='font-size: 1.80em;' target='_blank' class='link-icon' href='" . route("storage.document.file", $this->document_id) . "' data-ext='" . "." . $this->ext . "' title='" . $this->mime . "'></a><br/>" . $this->ext;
	return $return;
    }

    public function getGeneralInfoAttribute() {
	return
		"<a href='" . route('storage.document.show', $this) . "'>" . $this->name . "</a></br>" .
		trans("labels.backend.file.created_by") . "&nbsp;:&nbsp;<b>" . $this->user->name() . "</b>, " . trans("labels.backend.file.created") . "&nbsp;:&nbsp;<b>" . $this->created_at->diffForHumans() . "</b>, " . trans("labels.backend.file.version") . "&nbsp;:&nbsp;<b>" . $this->version . "</b><br/>" . trans("labels.backend.file.directory") . "&nbsp;:&nbsp;<b>" . $this->getPathAttribute() . "</b>";
    }

    public function getDownloadAttribute() {
	return "<a href='" . route('storage.document.download', $this) . "'>&nbsp;&nbsp;<i class='fa fa-cloud-download'></i> " . trans("labels.general.download") . "</a>";
    }

    public function getStatusInfoAttribute() {
	if ($this->isActive())
	    return "<label class='label label-success'>" . trans('labels.general.open') . "</label>";
	return "<label class='label label-danger'>" . trans('labels.general.closed') . "</label>";
    }

    public function getUsersAttribute() {
	$users = [];
	switch ($this->file->file_type_id) {
	    case '1' :
		foreach ($this->file->departments as $department) {
		    $officers = $department->users()->where('officer_id', '<>', access()->id())->pluck('officer_id')->all();
		    $users = array_merge($users, $officers);
		}
		break;
	    case '2':
		$officers = $this->file->users()->where('officer_id', '<>', access()->id())->pluck('officer_id')->all();
		$users = array_merge($users, $officers);
		break;
	}
	return $users;
    }

    public function isActive() {
	return $this->status == 1;
    }

    public function getPathAttribute() {
	if (count($this->folder)) {
	    return $this->folder->path;
	} else {
	    return $this->file->name . " /";
	}
    }

    public function getPathIdAttribute() {
	if (count($this->folder)) {
	    return $this->folder->path_id;
	} else {
	    return $this->file->file_id . " /";
	}
    }

    public function getLinkAttribute() {
	return trim(preg_replace('/\s+/', '', file_dir() . DIRECTORY_SEPARATOR . $this->getPathIdAttribute() . $this->document_id . ((!empty($this->ext)) ? "." . $this->ext : ".")));
    }

    public function getDeleteButtonAttribute() {
	if (access()->id() == $this->officer_id) {
	    return '<a href="' . route('storage.document.destroy', $this) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.general.delete_message') . '"
                 ><i class="fa fa-trash"></i>&nbsp;&nbsp;' . trans('buttons.general.crud.delete') . '</a> ';
	    ;
	}
	return '';
    }

    public function getEditButtonAttribute() {
	if (access()->allow('create_edit_file')) {
	    return '<a href="' . route('storage.document.edit', $this) . '"><i class="fa fa-pencil"></i>&nbsp;&nbsp;' . trans('buttons.general.crud.edit') . '</a> ';
	}
	return '';
    }

    public function getCopyButtonAttribute() {
	return '<a href="#"><i class="fa fa-copy"></i>&nbsp;&nbsp;' . trans('buttons.general.copy') . '</a> ';
    }

    public function getActionButtonsAttribute() {
	return
		'<div class="btn-group">' .
		'<button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' .
		trans('labels.general.actions') .
		'&nbsp;<span class="caret"></span>' .
		'</button>' .
		'<ul class="dropdown-menu dropdown-menu-right" role="menu">' .
		'<li>' . $this->getDeleteButtonAttribute() . '</li>' .
		'<li>' . $this->getEditButtonAttribute() . '</li>' .
		'<li>' . $this->getCopyButtonAttribute() . '</li>' .
		'</ul>' .
		'</div>';
    }

}
