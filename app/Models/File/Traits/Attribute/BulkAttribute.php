<?php

namespace App\Models\File\Traits\Attribute;

use Carbon\Carbon;

trait BulkAttribute {

    public function getLinkAttribute() {
	return bulk_dir() . "/" . $this->bulk_id . ((!empty($this->ext)) ? "." . $this->ext : ".");
    }

    public function size() {
	return formatBytes((float) $this->size);
    }

    public function getCreatedAtAttribute($value) {
	return Carbon::parse($value)->diffForHumans();
    }

    public function getStatusAttribute($value) {
	switch ($value) {
	    case '1':
		return "<label class='label label-warning'>" . trans('labels.general.pending') . "</label>";
	    case '2':
		return "<label class='label label-info'>" . trans('labels.general.progress') . "</label>";
	    case '3':
		return "<label class='label label-success'>" . trans('labels.general.success') . "</label>";
	    case '4':
		return "<label class='label label-danger'>" . trans('labels.general.fail') . "</label>";
	}
    }

    public function getOfficerAttribute() {
	return $this->user->name();
    }

    public function getNotice() {
	if ($this->notice == "") {
	    return trans('labels.general.none');
	} else {
	    return $this->notice;
	}
    }

    public function getDeleteButtonAttribute() {
	if ($this->status == '3') {
	    return '<a href="' . route('file.register.delete.bulk', $this->bulk_id) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.general.delete_message') . '"
                 class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
	}
	return '';
    }

    public function getActionButtonsAttribute() {
	return
		$this->getDeleteButtonAttribute();
    }

    public function getGeneralInfoAttribute() {
	return
		"<b>" . trans('labels.backend.file.table.name') . "</b>&nbsp;:&nbsp;" . $this->group_name . "</br>" .
		"<b>" . trans('labels.backend.file.table.file') . "</b>&nbsp;:&nbsp;" . $this->name . "</br>" .
		"<b>" . trans('labels.backend.file.table.size') . "</b>&nbsp;:&nbsp;" . $this->size() . "<br/>" .
		"<b>" . trans('labels.backend.file.table.user') . "</b>&nbsp;:&nbsp;" . $this->getOfficerAttribute() . "<br/>" .
		"<b>" . trans("labels.backend.file.notice") . "</b>&nbsp;:&nbsp;" . $this->getNotice();
    }

}
