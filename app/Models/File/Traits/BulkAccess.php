<?php

namespace App\Models\File\Traits;

trait BulkAccess {

    public function saveDepartments($inputDepartments) {
	if (!empty($inputDepartments)) {
	    $this->departments()->sync($inputDepartments);
	} else {
	    $this->departments()->detach();
	}
    }

    public function attachDepartment($department) {
	if (is_object($department)) {
	    $department = $department->getKey();
	}

	if (is_array($department)) {
	    $department = $department['department_id'];
	}

	$this->departments()->attach($department);
    }

    public function detachDepartment($department) {
	if (is_object($department)) {
	    $department = $department->getKey();
	}

	if (is_array($department)) {
	    $department = $department['department_id'];
	}

	$this->departments()->detach($department);
    }

    public function attachDepartments($departments) {
	foreach ($departments as $department) {
	    $this->attachDepartment($department);
	}
    }

    public function detachDepartments($departments) {
	foreach ($departments as $department) {
	    $this->detachDepartment($department);
	}
    }

    public function attachUser($user) {
	if (is_object($user)) {
	    $user = $user->getKey();
	}

	if (is_array($user)) {
	    $user = $user['officer_id'];
	}

	$this->users()->attach($user);
    }

    public function detachUser($user) {
	if (is_object($user)) {
	    $user = $user->getKey();
	}

	if (is_array($user)) {
	    $user = $user['officer_id'];
	}

	$this->users()->detach($user);
    }

    public function attachUsers($users) {
	foreach ($users as $user) {
	    $this->attachUser($user);
	}
    }

    public function detachUsers($users) {
	foreach ($users as $user) {
	    $this->detachUser($user);
	}
    }

}
