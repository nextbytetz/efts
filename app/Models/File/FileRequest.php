<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Relationship\FileRequestRelationship;
use App\Models\File\Traits\Attribute\FileRequestAttribute;

/**
 * Class FileRequest
 */
class FileRequest extends Model {
    
    use FileRequestRelationship, FileRequestAttribute;

    protected $table = 'file_request';
    protected $primaryKey = 'file_request_id';
    public $timestamps = true;
    protected $fillable = [
	'file_id',
	'officer_id',
	'status'
    ];
    protected $guarded = [];

}
