<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FileCategory
 */
class FileCategory extends Model
{
    protected $table = 'file_category';

    protected $primaryKey = 'file_category_id';

	public $timestamps = true;

    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

        
}