<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Attribute\DocumentStatusAttribute;
use App\Models\File\Traits\Relationship\DocumentStatusRelationship;

/**
 * Class Document
 */
class DocumentStatus extends Model {

    use DocumentStatusAttribute,
	DocumentStatusRelationship;

    protected $table = 'document_status';
    protected $primaryKey = 'document_status_id';
    public $timestamps = true;
    protected $fillable = [
	'comment',
	'priority_id',
	'status',
	'officer_id',
	'document_id',
	'action_id',
    ];
    protected $guarded = [];

}
