<?php

namespace App\Models\File;

use Illuminate\Database\Eloquent\Model;
use App\Models\File\Traits\Relationship\WorkflowOfficerRelationship;
use App\Models\File\Traits\Attribute\WorkflowOfficerAttribute;

/**
 * Class Workflow
 */
class WorkflowOfficer extends Model {
    
    use WorkflowOfficerRelationship, WorkflowOfficerAttribute;

    protected $table = 'workflow_officer';
    protected $primaryKey = 'workflow_officer_id';
    public $timestamps = true;
    protected $fillable = [
	'workflow_id',
	'officer_id',
	'received_at',
	'intent',
    ];
    protected $guarded = [];

}
