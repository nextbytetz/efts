<?php

namespace App\Models\Access;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\Traits\Attribute\UserAttribute;
use App\Models\Access\Traits\UserAccess;
use App\Models\Access\Traits\Relationship\UserRelationship;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {

    use UserAttribute,
	UserAccess,
	UserRelationship,
	Notifiable;

    protected $table = 'officer';
    protected $primaryKey = 'officer_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
	'firstname', 'othernames', 'username', 'password', 'email', 'department_id', 'officeid', 'gender', 'checked', 'available'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
	'password', 'remember_token',
    ];

    /**
     * Inactive officers scope
     */
    public function scopeNewUsers($query) {
	return $query->where('checked', '0');
    }

}
