<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Traits\Relationship\PermissionGroupRelationship;

/**
 * Class Permission
 */
class PermissionGroup extends Model {
    
    use PermissionGroupRelationship;

    protected $table = 'permission_group';
    protected $primaryKey = 'permission_group_id';
    public $timestamps = true;
    protected $fillable = [
    ];
    protected $guarded = [];
    
}
