<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OfficerRole
 */
class OfficerRole extends Model
{
    protected $table = 'officer_role';

    protected $primaryKey = 'officer_role_id';

	public $timestamps = true;

    protected $fillable = [
        'officer_id',
        'role_id',
        'status'
    ];

    protected $guarded = [];

        
}