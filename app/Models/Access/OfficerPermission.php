<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OfficerPermission
 */
class OfficerPermission extends Model {

    protected $table = 'officer_permission';
    protected $primaryKey = 'officer_permission_id';
    public $timestamps = true;
    protected $fillable = [
	'officer_id',
	'permission_id',
	'status'
    ];
    protected $guarded = [];

}
