<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Traits\Relationship\PermissionDependencyRelationship;

/**
 * Class PermissionDependency
 */
class PermissionDependency extends Model {
    
    use PermissionDependencyRelationship;

    protected $table = 'permission_dependency';
    protected $primaryKey = 'permission_dependency_id';
    public $timestamps = true;
    protected $fillable = [
	'permission_id',
	'dependency_id'
    ];
    protected $guarded = [];
    
}
