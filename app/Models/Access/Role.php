<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Traits\Relationship\RoleRelationship;
use App\Models\Access\Traits\Attribute\RoleAttribute;
use App\Models\Access\Traits\RoleAccess;

/**
 * Class Role
 */
class Role extends Model
{
    
    use RoleRelationship, RoleAttribute, RoleAccess;
    
    protected $table = 'role';

    protected $primaryKey = 'role_id';

	public $timestamps = true;

    protected $fillable = [
        'name',
        'sys_role'
    ];

    protected $guarded = [];

        
}