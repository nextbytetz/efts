<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Officer
 */
class Officer extends Model
{
    protected $table = 'officer';

    protected $primaryKey = 'officer_id';

	public $timestamps = true;

    protected $fillable = [
        'firstname',
        'othernames',
        'username',
        'password',
        'email',
        'department_id',
        'officeid',
        'gender',
        'checked',
        'level_id',
        'lastlogin',
        'pic',
        'phone',
        'home',
        'zipcode',
        'birth_date',
        'active',
        'remember_token',
        'officer_checked',
        'checked_date'
    ];

    protected $guarded = [];

        
}