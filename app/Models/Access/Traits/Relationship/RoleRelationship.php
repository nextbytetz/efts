<?php

namespace App\Models\Access\Traits\Relationship;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait RoleRelationship {

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
	return $this->belongsToMany(\App\Models\Access\User::class, 'officer_role', 'role_id', 'officer_id');
    }

    /**
     * @return mixed
     */
    public function permissions() {
	return $this->belongsToMany(\App\Models\Access\Permission::class, 'role_permission', 'role_id', 'permission_id');
    }

}
