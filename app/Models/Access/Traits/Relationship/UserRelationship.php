<?php

namespace App\Models\Access\Traits\Relationship;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait UserRelationship {

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles() {
	return $this->belongsToMany(\App\Models\Access\Role::class, 'officer_role', 'officer_id', 'role_id');
    }

    /**
     * Many-to-Many relations with Permission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions() {
	return $this->belongsToMany(\App\Models\Access\Permission::class, 'officer_permission', 'officer_id', 'permission_id');
    }
    
    /**
     * One to one relationship with department
     */
    public function department() {
	return $this->hasOne(\App\Models\Location\Department::class, 'department_id', 'department_id');
    }

}
