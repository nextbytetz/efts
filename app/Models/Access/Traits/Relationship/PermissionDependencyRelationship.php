<?php

namespace App\Models\Access\Traits\Relationship;

/**
 * Class PermissionDependencyRelationship
 * @package App\Models\Access\Permission\Traits\Relationship
 */
trait PermissionDependencyRelationship {

    /**
     * @return mixed
     */
    public function permission() {
	return $this->hasOne(\App\Models\Access\Permission::class, 'permission_id', 'dependency_id');
    }
    
    /**
     * @return mixed
     */
    public function backPermission(){
	return $this->hasOne(\App\Models\Access\Permission::class, 'permission_id', 'permission_id');
    }

}
