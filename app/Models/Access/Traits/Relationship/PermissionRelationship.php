<?php

namespace App\Models\Access\Traits\Relationship;

/**
 * Class PermissionRelationship
 * @package App\Models\Access\Permission\Traits\Relationship
 */
trait PermissionRelationship {

    /**
     * @return mixed
     */
    public function roles() {
	return $this->belongsToMany(\App\Models\Access\Role::class, 'role_permission', 'permission_id', 'role_id');
    }

    /**
     * @return mixed
     */
    public function group() {
	return $this->belongsTo(\App\Models\Access\PermissionGroup::class, 'permission_group_id', 'permission_group_id');
    }

    /**
     * @return mixed
     */
    public function users() {
	return $this->belongsToMany(\App\Models\Access\User::class, 'officer_permission', 'permission_id', 'officer_id');
    }

    /**
     * @return mixed
     */
    public function dependencies() {
	return $this->hasMany(\App\Models\Access\PermissionDependency::class, 'permission_id', 'permission_id');
    }
    
    /**
     * @return mixed
     */
    public function backDependencies(){
	return $this->hasMany(\App\Models\Access\PermissionDependency::class, 'dependency_id', 'permission_id');
    }
    
}
