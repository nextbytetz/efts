<?php

namespace App\Models\Access\Traits\Relationship;

/**
 * Class PermissionRelationship
 * @package App\Models\Access\Permission\Traits\Relationship
 */
trait PermissionGroupRelationship {

    /**
     * return @mixed
     */
    public function permissions(){
	return $this->hasMany(\App\Models\Access\Permission::class, 'permission_group_id', 'permission_group_id')->orderBy('permission_group_id', 'asc');
    }
    
}
