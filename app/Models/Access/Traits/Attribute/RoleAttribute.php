<?php

namespace App\Models\Access\Traits\Attribute;

/**
 * Class RoleAttribute
 * @package App\Models\Access\Role\Traits\Attribute
 */
trait RoleAttribute {

    /**
     * @return string
     */
    public function getEditButtonAttribute() {
	if ($this->sys_role != 1) {
	    if (access()->allow('edit_role')) {
		return '<a href="' . route('access.role.edit', $this->role_id) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
	    }
	}

	return '';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute() {
	//Can't delete master admin role
	if ($this->sys_role != 1) {
	    if (access()->allow('delete_role')) {
		return '<a href="' . route('access.role.destroy', $this->role_id) . '" class="btn btn-xs btn-danger" data-trans-button-cancel="'. trans('buttons.general.cancel') .'" data-trans-button-confirm="'. trans('buttons.general.confirm') .'" data-trans-title="'. trans('labels.general.warning') .'" data-trans-text="'. trans('strings.backend.general.delete_message') .'" data-method="delete"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
	    }
	}

	return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
	return $this->getEditButtonAttribute() .
		$this->getDeleteButtonAttribute();
    }

}
