<?php

namespace App\Models\Access\Traits\Attribute;

use Carbon\Carbon;

/**
 * Class UserAttribute
 * @package App\Models\Access\Traits\Attribute
 */
trait UserAttribute {

    /**
     * @return string
     */
    public function getConfirmedLabelAttribute() {
	if ($this->isConfirmed())
	    return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
	return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
    }

    /**
     * @return string
     */
    public function getAvailableLabelAttribute() {
	if ($this->isAvailable())
	    return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
	return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
    }

    public function getActiveLabelAttribute() {
	if ($this->isActive())
	    return "<label class='label label-success'>" . trans('labels.general.yes') . "</label>";
	return "<label class='label label-danger'>" . trans('labels.general.no') . "</label>";
    }

    /**
     * @return mixed
     */
    public function getPicAttribute($value) {
	if (is_null($value)) {
	    return asset_url() . '/images/anonymous.jpg';
	} else {
	    return public_url() . "/storage/profile/" . $value;
	}
    }

    public function getGenderLabelAttribute() {
	if ($this->gender == 'M') {
	    return "<label class='label label-success'>" . trans('labels.general.male') . "</label>";
	} else {
	    return "<label class='label label-success'>" . trans('labels.general.female') . "</label>";
	}
    }

    public function getLastloginAttribute($value) {
	if (!is_null($value)) {
	    return Carbon::parse($value)->diffForHumans();
	} else {
	    return "";
	}
    }

    public function getAdminAttribute() {
	$admins = $this->department->users()->whereHas('roles', function($query) {
		    $query->where('role.role_id', '=', 2);
		})->get();
	$officers = [];
	if (count($admins)) {
	    foreach ($admins as $admin) {
		$officers[] = $admin->name();
	    }
	    return implode("<br/>", $officers);
	}
	return "";
    }

    public function getRoleLabelAttribute() {
	$roles = [];
	if ($this->roles()->count() > 0) {
	    foreach ($this->roles as $role) {
		array_push($roles, $role->name);
	    }
	    return implode("<br/>", $roles);
	} else {
	    return trans('labels.general.none');
	}
    }

    /**
     * @return bool
     */
    public function isActive() {
	return $this->active == 1;
    }

    public function name() {
	return $this->firstname . ' ' . $this->othernames;
    }

    public function getFullNameAttribute() {
	return $this->firstname . ' ' . $this->othernames;
    }

    /**
     * @return bool
     */
    public function isConfirmed() {
	return $this->checked == 1;
    }

    /**
     * @return bool
     */
    public function isAvailable() {
	return $this->available == 1;
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute() {
	if (access()->allow('edit_user')) {
	    //if ($this->officer_id != access()->id() And ! $this->hasRole('super')) {
	    return '<a href="' . route('access.user.edit', $this) . '"><i class="fa fa-pencil"></i>&nbsp;&nbsp;' . trans('buttons.general.crud.edit') . '</a> ';
	    //}
	}
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute() {
	if (access()->allow('delete_user')) {
	    if ($this->officer_id != access()->id() And ( !$this->isAvailable() And ! $this->isConfirmed())) {
		return '<a href="' . route('access.user.destroy', $this) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.general.delete_message') . '"
                 ><i class="fa fa-trash"></i>&nbsp;&nbsp;' . trans('buttons.general.crud.delete') . '</a> ';
	    }
	}
	return '';
    }

    /**
     * @return string
     */
    public function getOutButtonAttribute() {
	if ($this->isActive() And $this->isConfirmed()) {
	    if (access()->allow('in_out_office')) {
		switch ($this->available) {
		    case 1:
			return '<a href="' . route('access.user.out', [
				    $this,
				    0
				]) . '"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;' . trans('buttons.backend.access.users.out_office') . '</a> ';
		    // No break

		    case 0:
			return '<a href="' . route('access.user.out', [
				    $this,
				    1
				]) . '"><i class="fa fa-sign-in" data-toggle="tooltip"></i>&nbsp;&nbsp;' . trans('buttons.backend.access.users.in_office') . '</a> ';
		    // No break

		    default:
			return '';
		    // No break
		}
	    }
	}
	return '';
    }

    /**
     * @return string
     */
    public function getChangePasswordButtonAttribute() {
	if (access()->allow('change_user_password')) {
	    if (!$this->hasRole('super') And ! $this->hasRole('admin')) {
		return '<a href="' . route('access.user.change_password', $this) . '"><i class="fa fa-refresh"></i>&nbsp;&nbsp;' . trans('buttons.backend.access.users.change_password') . '</a> ';
	    }
	}
	return '';
    }

    /**
     * @return string
     */
    public function getConfirmButtonAttribute() {
	if (access()->allow('approve_user')) {

	    if (!$this->isAvailable() And ! $this->isConfirmed()) {
		return '<a href="' . route('access.user.approve', $this) . '"><i class="fa fa-check"></i>&nbsp;&nbsp;' . trans('buttons.general.approve') . '</a> ';
	    }
	}
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute() {
	if (($this->officer_id != access()->id()) Or ( $this->hasRole('super') And access()->hasRole('super') And $this->officer_id != access()->id())) {
	    if (access()->allow('deactivate_reactivate_user')) {
		switch ($this->active) {
		    case 0:
			return '<a href="' . route('access.user.activate', [
				    $this,
				    1
				]) . '"><i class="fa fa-play"></i>&nbsp;&nbsp;' . trans('buttons.backend.access.users.activate') . '</a> ';
		    // No break

		    case 1:
			return '<a href="' . route('access.user.activate', [
				    $this,
				    0
				]) . '"><i class="fa fa-pause"></i>&nbsp;&nbsp;' . trans('buttons.backend.access.users.deactivate') . '</a> ';
		    // No break

		    default:
			return '';
		    // No break
		}
	    }
	}
	return '';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
	return
		'<div class="btn-group">' .
		'<button type="button" class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' .
		trans('labels.general.actions') .
		'&nbsp;<span class="caret"></span>' .
		'</button>' .
		'<ul class="dropdown-menu dropdown-menu-right" role="menu">' .
		'<li>' . $this->getEditButtonAttribute() . '</li>' .
		'<li>' . $this->getOutButtonAttribute() . '</li>' .
		'<li>' . $this->getChangePasswordButtonAttribute() . '</li>' .
		'<li>' . $this->getStatusButtonAttribute() . '</li>' .
		'<li>' . $this->getConfirmButtonAttribute() . '</li>' .
		'<li>' . $this->getDeleteButtonAttribute() . '</li>' .
		'</ul>' .
		'</div>';
    }

}
