<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RolePermission
 */
class RolePermission extends Model {

    protected $table = 'role_permission';
    protected $primaryKey = 'role_permission_id';
    public $timestamps = true;
    protected $fillable = [
	'role_id',
	'permission_id',
	'status'
    ];
    protected $guarded = [];

}
