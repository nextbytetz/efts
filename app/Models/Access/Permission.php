<?php

namespace App\Models\Access;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\Traits\Relationship\PermissionRelationship;

/**
 * Class Permission
 */
class Permission extends Model {

    use PermissionRelationship;
    
    protected $table = 'permission';
    protected $primaryKey = 'permission_id';
    public $timestamps = true;
    protected $fillable = [
	'name',
	'is_super',
	'parent_id',
	'description'
    ];
    protected $guarded = [];

}
