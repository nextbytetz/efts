<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Models\Report\Traits\Relationship\ClosedRptRelationship;
use App\Models\Report\Traits\Attribute\ClosedRptAttribute;

/**
 * Class File
 */
class ClosedRpt extends Model {
    
    use ClosedRptRelationship, ClosedRptAttribute;

    protected $table = 'closed_rpt';
    protected $primaryKey = 'file_id';
    public $timestamps = true;

}
