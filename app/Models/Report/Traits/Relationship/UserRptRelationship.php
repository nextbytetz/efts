<?php

namespace App\Models\Report\Traits\Relationship;

trait UserRptRelationship {

    public function receiver() {
	return $this->hasOne(\App\Models\File\FilePickup::class, 'child_id', 'file_pickup_id');
    }

    public function pickup() {
	return $this->hasOne(\App\Models\File\FilePickup::class, 'file_pickup_id', 'file_pickup_id');
    }

    public function file() {
	return $this->hasOne(\App\Models\File\File::class, 'file_id', 'file_id');
    }

}
