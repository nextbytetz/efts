<?php

namespace App\Models\Report\Traits\Attribute;

use Carbon\Carbon;

trait TrackingRptAttribute {

    public function getDurationAttribute() {
	if (!is_null($this->created_at)) {
	    $present_time = Carbon::parse($this->updated_at);
	} else {
	    $present_time = Carbon::parse($this->updated_at);
	}	
	return $present_time->diffForHumans(Carbon::parse($this->created_at), true);
    }

    public function getUpdatedAtAttribute($value) {
	return Carbon::parse($value)->toDayDateTimeString();
    }

}
