<?php

namespace App\Models\Report\Traits\Attribute;

use Carbon\Carbon;

trait BasicRptAttribute {

    public function getCreatedAtAttribute($value) {
	return Carbon::parse($value)->diffForHumans();
    }
    
    public function getPickDateAttribute($value) {
	if (!is_null($value)) {
	    $present_time = Carbon::now();
	    return $present_time->diffForHumans(Carbon::parse($value), true);
	} else {
	    return "-";
	}	
    }
    
    public function getOfficerAttribute($value) {
	if (!is_null($value)) {
	    return $value;
	} else {
	    return "-";
	}
    }

}
