<?php

namespace App\Models\Report\Traits\Attribute;

use Carbon\Carbon;

trait UserRptAttribute {

    public function getReceiveDateAttribute($value) {
	return Carbon::parse($value)->toDayDateTimeString();
    }

    public function getDisposedAtAttribute($value) {
	if (!is_null($value)) {
	    return Carbon::parse($value)->toDayDateTimeString();
	} else {
	    return '';
	}	
    }

    public function getReceiverAttribute($value) {
	if (!is_null($value)) {
	    return $value;
	} else {
	    return '-';//trans('labels.general.none');
	}	
    }
    
    public function getDeskAttribute($value) {
	if (!is_null($value)) {
	    return $value;
	} else {
	    return '-';
	}
    }

    public function getPeriodAttribute($value) {
	if (!is_null($this->disposed_at)) {
	    $present_time = Carbon::parse($this->disposed_at);
	    return $present_time->diffForHumans(Carbon::parse($this->receive_date), true);
	} else {
	    $present_time = Carbon::now();
	    return $present_time->diffForHumans(Carbon::parse($this->receive_date), true);
	}
    }

}
