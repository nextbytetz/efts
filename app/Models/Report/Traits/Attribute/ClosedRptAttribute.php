<?php

namespace App\Models\Report\Traits\Attribute;

use Carbon\Carbon;

trait ClosedRptAttribute {

    public function getCreatedAtAttribute($value) {
	return Carbon::parse($value)->toDayDateTimeString();
    }
    
    public function getUpdatedAtAttribute($value) {
	if (!is_null($value)) {
	    return Carbon::parse($value)->toDayDateTimeString();
	} else {
	    return '';
	}
    }

}
