<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Models\Report\Traits\Relationship\BasicNoticeRptRelationship;
use App\Models\Report\Traits\Attribute\BasicNoticeRptAttribute;

/**
 * Class File
 */
class BasicNoticeRpt extends Model {
    
    use BasicNoticeRptRelationship, BasicNoticeRptAttribute;

    protected $table = 'basic_notice_rpt';
    protected $primaryKey = 'file_id';
    public $timestamps = true;

}
