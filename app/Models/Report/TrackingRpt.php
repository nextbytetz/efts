<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Models\Report\Traits\Relationship\TrackingRptRelationship;
use App\Models\Report\Traits\Attribute\TrackingRptAttribute;

/**
 * Class File
 */
class TrackingRpt extends Model {
    
    use TrackingRptRelationship, TrackingRptAttribute;

    protected $table = 'tracking_rpt';
    protected $primaryKey = 'track_id';
    public $timestamps = true;

}
