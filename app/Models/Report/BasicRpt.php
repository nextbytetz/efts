<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Models\Report\Traits\Relationship\BasicRptRelationship;
use App\Models\Report\Traits\Attribute\BasicRptAttribute;

/**
 * Class File
 */
class BasicRpt extends Model {
    
    use BasicRptRelationship, BasicRptAttribute;

    protected $table = 'basic_rpt';
    protected $primaryKey = 'file_id';
    public $timestamps = true;

}
