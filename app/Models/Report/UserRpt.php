<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use App\Models\Report\Traits\Relationship\UserRptRelationship;
use App\Models\Report\Traits\Attribute\UserRptAttribute;

/**
 * Class File
 */
class UserRpt extends Model {
    
    use UserRptRelationship, UserRptAttribute;

    protected $table = 'user_rpt';
    protected $primaryKey = 'file_pickup_id';
    public $timestamps = true;

}
