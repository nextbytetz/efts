<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HistoryType
 * package App
 */
class NotificationType extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notification_type';
    
    protected $primaryKey = 'notification_type_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

}
