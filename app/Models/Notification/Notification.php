<?php

namespace App\Models\Notification;

use App\Models\Access\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Notification\Traits\Attribute\NotificationAttribute;
use App\Models\Notification\Traits\NotificationAccess;

/**
 * Class History
 * package App
 */
class Notification extends Model {

    use NotificationAttribute, NotificationAccess;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notification';
    
    protected $primaryKey = 'notification_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['notification_type_id', 'class', 'text', 'asset', 'id'];

    
    public function users() {
	return $this->belongsToMany(User::class, 'notification_officer', 'notification_id', 'officer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type() {
	return $this->hasOne(NotificationType::class, 'notification_type_id', 'notification_type_id');
    }

}
