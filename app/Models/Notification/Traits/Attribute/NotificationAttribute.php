<?php

namespace App\Models\Notification\Traits\Attribute;

trait NotificationAttribute {

    /**
     * @return string
     */
    public function getNotificationLabelAttribute() {
	$text = preg_replace_callback('/trans\(\"([^"]+)\"\)/', function($matches) {
	    return trans($matches[1]);
	}, $this->text);
	return "<span class='fa-stack fa-lg'><i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-" . $this->icon . " " . $this->class . "'></i></span>&nbsp;&nbsp;" . $text;
    }

    public function getTimeLabelAttribute() {
	return "<i class='fa fa-clock-o'></i> &nbsp;&nbsp;" . $this->created_at->diffForHumans();
    }

}
