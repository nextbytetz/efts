<?php

namespace App\Models\History\Traits\Attribute;

trait HistoryAttribute {

    /**
     * @return string
     */
    public function getHistoryLabelAttribute() {
	$text = preg_replace_callback('/trans\(\"([^"]+)\"\)/', function($matches) {
	    return trans($matches[1]);
	}, $this->text);
	return "<span class='fa-stack fa-lg'><i class='fa fa-circle fa-stack-2x'></i><i class='fa fa-" . $this->icon . " " . $this->class . "'></i></span>&nbsp;&nbsp;" . trans("labels.general.officer") . "&nbsp;&nbsp;:&nbsp;&nbsp;" . $this->user->name() . "<br/>" .$text;
    }

    public function getTimeLabelAttribute() {
	return "<i class='fa fa-clock-o'></i> &nbsp;&nbsp;" . $this->created_at->diffForHumans();
    }

}
