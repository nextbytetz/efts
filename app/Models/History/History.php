<?php

namespace App\Models\History;

use App\Models\Access\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\History\Traits\Attribute\HistoryAttribute;

/**
 * Class History
 * package App
 */
class History extends Model {

    use HistoryAttribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'history';
    
    protected $primaryKey = 'history_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['history_type_id', 'officer_id', 'entity_id', 'icon', 'class', 'text', 'assets'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user() {
	return $this->hasOne(User::class, 'officer_id', 'officer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function type() {
	return $this->hasOne(HistoryType::class, 'history_type_id', 'history_type_id');
    }

}
