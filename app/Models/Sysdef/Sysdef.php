<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Sysdef
 */
class Sysdef extends Model
{
    protected $table = 'sysdef';

    protected $primaryKey = 'sysdef_id';

	public $timestamps = false;

    protected $fillable = [];

    protected $guarded = [];

        
}