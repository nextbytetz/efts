<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;
use App\Models\Location\Traits\CustomLocation;
use App\Models\Location\Traits\Relationship\DepartmentRelationship;
use App\Models\Location\Traits\Attribute\DepartmentAttribute;

/**
 * Class Department
 */
class Department extends Model {

    use CustomLocation,
	DepartmentRelationship, DepartmentAttribute;

    protected $table = 'department';
    protected $primaryKey = 'department_id';
    public $timestamps = true;
    protected $fillable = [
	'name',
	'branch_id'
    ];
    protected $guarded = [];

}
