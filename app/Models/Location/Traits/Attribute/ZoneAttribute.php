<?php

namespace App\Models\Location\Traits\Attribute;

use Carbon\Carbon;

trait ZoneAttribute {

    public function getCreatedAtAttribute($value) {
	return Carbon::parse($value)->diffForHumans();
    }

    public function getUpdatedAtAttribute($value) {
	return Carbon::parse($value)->diffForHumans();
    }

    public function getDeleteButtonAttribute() {
	return '<a href="' . route('sysdef.rfid.zone.destroy', $this->zone_id) . '"
                 data-method="delete"
                 data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                 data-trans-button-confirm="' . trans('buttons.general.confirm') . '"
                 data-trans-title="' . trans('labels.general.warning') . '"
		 data-trans-text="' . trans('strings.backend.general.delete_message') . '"
                 class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
    }

    public function getEditButtonAttribute() {

	return '<a href="' . route('sysdef.rfid.zone.edit', $this->zone_id) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    public function getActionButtonsAttribute() {
	return
		$this->getEditButtonAttribute() .
		$this->getDeleteButtonAttribute();
    }

}
