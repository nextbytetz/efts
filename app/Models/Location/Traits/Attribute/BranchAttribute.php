<?php

namespace App\Models\Location\Traits\Attribute;

trait BranchAttribute {

    /**
     * @return string
     */
    public function getEditButtonAttribute() {

	return '<a href="' . route('sysdef.branch.edit', $this->branch_id) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute() {

	return '<a href="' . url('sysdef/branch', $this->branch_id) . '" class="btn btn-xs btn-danger" data-trans-button-cancel="' . trans('buttons.general.cancel') . '" data-trans-button-confirm="' . trans('buttons.general.confirm') . '" data-trans-title="' . trans('labels.general.warning') . '" data-trans-text="' . trans('strings.backend.general.delete_message') . '" data-method="delete"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a>';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
	return $this->getEditButtonAttribute() .
		$this->getDeleteButtonAttribute();
    }

}
