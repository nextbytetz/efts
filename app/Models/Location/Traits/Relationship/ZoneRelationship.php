<?php

namespace App\Models\Location\Traits\Relationship;

trait ZoneRelationship {

    public function departments() {
        return $this->belongsToMany(\App\Models\Location\Department::class, 'zone_department', 'zone_id', 'department_id');
    }

}
