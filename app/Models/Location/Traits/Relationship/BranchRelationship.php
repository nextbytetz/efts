<?php

namespace App\Models\Location\Traits\Relationship;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait BranchRelationship {
    
        
    /**
     * One to one relationship with branch
     */
    
    public function region() {
	return $this->belongsTo(\App\Models\Location\Region::class, 'region_id', 'region_id');
    }
    
    public function departments(){
	return $this->hasMany(\App\Models\Location\Department::class, 'branch_id', 'branch_id');
    }
    
}