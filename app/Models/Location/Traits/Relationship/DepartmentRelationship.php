<?php

namespace App\Models\Location\Traits\Relationship;

/**
 * Class UserRelationship
 * @package App\Models\Access\User\Traits\Relationship
 */
trait DepartmentRelationship {

    /**
     * One to one relationship with branch
     */
    public function branch() {
	return $this->belongsTo(\App\Models\Location\Branch::class, 'branch_id', 'branch_id');
    }

    public function users() {
	return $this->hasMany(\App\Models\Access\User::class, 'department_id', 'department_id');
    }

    public function files() {
	return $this->belongsToMany(\App\Models\File\File::class, 'file_department', 'department_id', 'file_id');
    }

}
