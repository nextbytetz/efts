<?php

namespace App\Models\Location\Traits\Relationship;

trait RegionRelationship {

    /**
     * function has many through relationship to department class
     */
    public function departments() {
	return $this->hasManyThrough(\App\Models\Location\Department::class, \App\Models\Location\Branch::class, 'region_id', 'branch_id');
    }

    /**
     * @return mixed
     */
    public function branches() {
	return $this->hasMany(\App\Models\Location\Branch::class, 'region_id', 'region_id');
    }
    
    public function country(){
	return $this->belongsTo(\App\Models\Location\Country::class, 'country_id', 'country_id');
    }

}
