<?php

namespace App\Models\Location\Traits\Relationship;

trait CountryRelationship {

    /**
     * @return mixed
     */
    public function regions() {
	return $this->hasMany(\App\Models\Location\Region::class, 'country_id', 'country_id');
    }

}
