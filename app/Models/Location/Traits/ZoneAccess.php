<?php

namespace App\Models\Location\Traits;

trait ZoneAccess {

    public function saveDepartments($inputDepartments) {
	if (!empty($inputDepartments)) {
	    $this->departments()->sync($inputDepartments);
	} else {
	    $this->departments()->detach();
	}
    }

    public function attachDepartment($department) {
	if (is_object($department)) {
	    $department = $department->getKey();
	}

	if (is_array($department)) {
	    $department = $department['department_id'];
	}

	$this->departments()->attach($department);
    }

    public function detachDepartment($department) {
	if (is_object($department)) {
	    $department = $department->getKey();
	}

	if (is_array($department)) {
	    $department = $department['department_id'];
	}

	$this->departments()->detach($department);
    }

    public function attachDepartments($departments) {
	foreach ($departments as $department) {
	    $this->attachDepartment($department);
	}
    }

    public function detachDepartments($departments) {
	foreach ($departments as $department) {
	    $this->detachDepartment($department);
	}
    }

}


