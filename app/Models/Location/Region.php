<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;
use App\Models\Location\Traits\CustomLocation;
use App\Models\Location\Traits\Relationship\RegionRelationship;
use App\Models\Location\Traits\Attribute\RegionAttribute;

/**
 * Class Region
 */
class Region extends Model {
    
    use CustomLocation, RegionRelationship, RegionAttribute;

    protected $table = 'region';
    protected $primaryKey = 'region_id';
    public $timestamps = true;
    protected $guarded = [];

    
}
