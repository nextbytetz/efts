<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;
use App\Models\Location\Traits\Attribute\CountryAttribute;
use App\Models\Location\Traits\Relationship\CountryRelationship;
/**
 * Class Country
 */
class Country extends Model {
    
    use CountryAttribute, CountryRelationship;

    protected $table = 'country';
    protected $primaryKey = 'country_id';
    public $timestamps = true;
    protected $fillable = [
	'name'
    ];
    protected $guarded = [];

}
