<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CountryAll
 */
class CountryAll extends Model
{
    protected $table = 'country_all';

    protected $primaryKey = 'country_all_id';

	public $timestamps = false;

    protected $fillable = [
        'name',
        'code'
    ];

    protected $guarded = [];

        
}