<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;
use App\Models\Location\Traits\ZoneAccess;
use App\Models\Location\Traits\Attribute\ZoneAttribute;
use App\Models\Location\Traits\Relationship\ZoneRelationship;

class Zone extends Model {

    use ZoneAccess,
	ZoneAttribute,
	ZoneRelationship;

    protected $table = 'zone';
    protected $primaryKey = 'zone_id';
    public $timestamps = true;
    protected $fillable = [
	'reader',
	'name',
	'antenna',
    ];

}
