<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;
use App\Models\Location\Traits\Relationship\BranchRelationship;
use App\Models\Location\Traits\Attribute\BranchAttribute;

/**
 * Class Branch
 */
class Branch extends Model {
    
    use BranchRelationship, BranchAttribute;

    protected $table = 'branch';
    protected $primaryKey = 'branch_id';
    public $timestamps = true;
    protected $fillable = [
	'name',
	'region_id'
    ];
    protected $guarded = [];

}
