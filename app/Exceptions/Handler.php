<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use \Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
	AuthorizationException::class,
	HttpException::class,
	ModelNotFoundException::class,
	ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e) {
	Log::error('[' . $e->getCode() . '] ' . $e->getMessage() . ' on line ' . @$e->getTrace()[0]['line'] . ' of file ' . @$e->getTrace()[0]['file']);
	/* parent::report($e); */
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception) {
	if ($request->expectsJson()) {
	    return response()->json(['error' => 'Unauthenticated.'], 401);
	}
	return redirect()->guest('login');
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e) {

	/**
	 * Redirect if token mismatch error
	 * Usually because user stayed on the same screen too long and their session expired
	 */
	/*
	  if ($e instanceof TokenMismatchException) {
	  return redirect('/')->withInput()->withFlashInfo("Session expired");
	  }
	 */
	/*
	  if ($e instanceof \PDOException) {
	  include_once app_path() . '/pgsql_error.php';
	  // Extract error code from example string : e.g. SQLSTATE[08006] [7] FATAL: database "office" does not exist
	  $code = substr($e->getMessage(), 9, 5);
	  $errMessage = "POSTGRES DATABASE ERROR [{$code}] : " . $PGError[$code];
	  //$errMessage = $e->getMessage();
	  if ($request->ajax()) {
	  return response()->json(['success' => false, 'message' => $errMessage]);
	  } else {
	  if (in_array($request->route()->getName(), array('frontend.setup'))) {
	  return redirect()->back()->withFlashDanger($errMessage . ", \n" . trans('exceptions.frontend.config_app_message'));
	  } else {
	  return redirect('setup/general')->withFlashDanger($errMessage . ", \n" . trans('exceptions.frontend.config_app_message'));
	  }
	  }
	  }
	 */
	/**
	 * All instances of GeneralException redirect back with a flash message to show a bootstrap alert-error
	 */
	if ($e instanceof GeneralException) {
	    return redirect()->back()->withInput()->withFlashDanger($e->getMessage());
	}

	if ($e instanceof FileException) {
	    return redirect()->route('file.register.all')->withFlashDanger($e->getMessage());
	}

	return parent::render($request, $e);
    }

}
