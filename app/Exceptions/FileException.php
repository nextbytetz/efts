<?php

namespace App\Exceptions;

use Exception;

/**
 * Class GeneralException
 * @package App\Exceptions
 */
class FileException extends Exception {}