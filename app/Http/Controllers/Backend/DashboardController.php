<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\History\EloquentHistoryRepository;
use App\DataTables\LiveMonitoringDataTable;
use App\Repositories\Backend\File\Process\EloquentProcessRepository;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller {

    protected $histories;

    public function __construct(EloquentHistoryRepository $histories) {
	$this->histories = $histories;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index(LiveMonitoringDataTable $dataTable, EloquentProcessRepository $file) {
	$myFileHistory = $file->getMyFileHistory();
        $dispatchedFileHistory = $file->getDispatchedFileHistory();
	return $dataTable->render('backend.dashboard.dashboard', compact('myFileHistory', 'dispatchedFileHistory'));
    }

    public function getHistory($history_type) {
	return Datatables::of($this->histories->getTypeForDatatable($history_type))
			->addColumn('history', function($history) {
			    return $history->history_label;
			})
			->editColumn('created_at', function ($history) {
			    return $history->time_label;
			})
			->make(true);
    }

    public function getNotification() {
	return view('backend.dashboard.notification');
    }

}
