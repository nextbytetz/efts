<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Models\Access\User;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\Access\User\UpdateUserPasswordRequest;
use App\Http\Requests\Backend\Access\User\UpdateProfilePasswordRequest;
use App\Http\Requests\Backend\Access\User\StoreProfileRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use App\Repositories\Backend\Access\User\EloquentUserRepository;
use App\Repositories\Backend\Access\Role\EloquentRoleRepository;
use App\Repositories\Backend\Access\Permission\EloquentPermissionRepository;
use App\Repositories\Backend\Sysdef\Location\Country\EloquentCountryRepository;
use App\Repositories\Backend\Sysdef\Location\Region\EloquentRegionRepository;
use App\Repositories\Backend\Sysdef\Location\Branch\EloquentBranchRepository;
use App\Repositories\Backend\Sysdef\Location\Department\EloquentDepartmentRepository;

class UserController extends Controller {

    /**
     * @var UserRepositoryContract
     */
    protected $users;

    /**
     * @var RoleRepositoryContract
     */
    protected $roles;

    /**
     * @var PermissionRepositoryContract
     */
    protected $permissions;

    /**
     * @param UserRepositoryContract $users
     * @param RoleRepositoryContract $roles
     */
    public function __construct(EloquentUserRepository $users, EloquentRoleRepository $roles, EloquentPermissionRepository $permissions) {
	$this->users = $users;
	$this->roles = $roles;
	$this->permissions = $permissions;
    }

    /**
     * @param ManageUserRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
	return view('backend.access.index')
			->withNewUsers($this->users->getNewUsers());
    }

    /**
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function get() {
	return Datatables::of($this->users->getForDataTable(request()->column, request()->value))
			->addColumn('check', function ($user) {
			    if ($user->checked == '0') {
				return '<input type="checkbox" name="group_choice[]" value="' . $user->officer_id . '">';
			    } else {
				return '';
			    }
			})
			->editColumn('checked', function($user) {
			    return $user->confirmed_label;
			})
			->editColumn('created_at', function ($user) {
			    return $user->created_at->diffForHumans();
			})
			->editColumn('updated_at', function($user) {
			    return $user->updated_at->diffForHumans();
			})
			->editColumn('available', function($user) {
			    return $user->available_label;
			})
			->addColumn('roles', function($user) {
			    return $user->role_label;
			})
			->addColumn('department', function ($user) {
			    return (count($user->department)) ? $user->department->name : '';
			})
			->addColumn('branch_name', function ($user) {
			    return (count($user->department)) ? $user->department->branch->name : '';
			})
			->addColumn('region', function ($user) {
			    return (count($user->department)) ? $user->department->branch->region->name : '';
			})
			->addColumn('actions', function($user) {
			    return $user->action_buttons;
			})
			->make(true);
    }

    /**
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function deactivated() {
	return view('backend.access.deactivated');
    }

    /**
     * @return mixed
     */
    public function edit($officer_id, EloquentCountryRepository $countries, EloquentRegionRepository $regions, EloquentBranchRepository $branches, EloquentDepartmentRepository $departments) {
	$user = $this->users->findOrThrowException($officer_id, false);
	return view('backend.access.edit')
			->withUser($user)
			->withUserRoles($user->roles->pluck('role_id')->all())
			->withRoles($this->roles->getAllRoles('name', 'asc', true))
			->withUserPermissions($user->permissions->pluck('permission_id')->all())
			->withPermissions($this->permissions->getAllPermissions())
			->withCountry($countries->getRegisteredCountry())
			->withRegions($regions->getCountryRegion((count($user->department) ? $user->department->branch->region->country->country_id : 0)))
			->withBranches($branches->getRegionBranch((count($user->department) ? $user->department->branch->region->region_id : 0)))
			->withDepartments($departments->getBranchDepartment((count($user->department) ? $user->department->branch->branch_id : 0)));
    }

    /**
     * @param User $user
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update($id, UpdateUserRequest $request) {
	$this->users->update($id, $request->except('assignees_roles', 'permission_user', 'country_id', 'region_id', 'branch_id'), $request->only('assignees_roles'), $request->only('permission_user')
	);
	return redirect()->route('access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param User $user
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function changePassword($id) {
	return view('backend.access.change_password')
			->withUser($this->users->findOrThrowException($id));
    }

    /**
     * @param User $user
     * @param UpdateUserPasswordRequest $request
     * @return mixed
     */
    public function updatePassword($id, UpdateUserPasswordRequest $request) {
	$this->users->updatePassword($id, $request->all());
	return redirect()->route('access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated_password'));
    }

    public function approve($id) {
	$this->users->approve($id);
	return redirect()->route('access.user.index')->withFlashSuccess(trans('alerts.backend.users.approved'));
    }

    public function activate($id, $activate) {
	$this->users->activate($id, $activate);
	return redirect()->route('access.user.index')->withFlashSuccess(trans('alerts.backend.users.changed_status'));
    }

    public function out($id, $available) {
	$this->users->out($id, $available);
	return redirect()->route('access.user.index')->withFlashSuccess(trans('alerts.backend.users.changed_status'));
    }

    /**
     * @param User $user
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function destroy($id) {
	$this->users->destroy($id);
	return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    public function approveGroup() {
	$this->users->approveGroup(request()->input('group_choice'));
	return redirect()->route('access.user.index')->withFlashSuccess(trans('alerts.backend.users.group.approved'));
    }

    public function deleteGroup() {
	$this->users->deleteGroup(request()->input('group_choice'));
	return redirect()->route('access.user.index')->withFlashSuccess(trans('alerts.backend.users.group.deleted'));
    }

    public function profile(EloquentCountryRepository $countries, EloquentRegionRepository $regions, EloquentBranchRepository $branches, EloquentDepartmentRepository $departments) {
	$user = $this->users->findOrThrowException(access()->id());
	return view('backend.access.profile')
			->withUser($user)
			->withCountry($countries->getRegisteredCountry())
			->withRegions($regions->getCountryRegion((count($user->department) ? $user->department->branch->region->country->country_id : 0)))
			->withBranches($branches->getRegionBranch((count($user->department) ? $user->department->branch->region->region_id : 0)))
			->withDepartments($departments->getBranchDepartment((count($user->department) ? $user->department->branch->branch_id : 0)));
    }

    public function profileStore(StoreProfileRequest $request) {
	$this->users->updateProfile($request->all());
	return redirect()->route('access.user.profile')->withFlashSuccess(trans('alerts.backend.users.profile.updated'));
    }

    public function password() {
	return view('backend.access.password');
    }

    public function passwordStore(UpdateProfilePasswordRequest $request) {
	$this->users->changePassword($request->all());
	return redirect()->route('access.user.password')->withFlashSuccess(trans('alerts.backend.users.profile.password'));
    }

}
