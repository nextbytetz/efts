<?php

namespace App\Http\Controllers\Backend\Access\Role;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Access\Role\EloquentRoleRepository;
use App\Http\Requests\Backend\Access\Role\UpdateRoleRequest;
use App\Repositories\Backend\Access\Permission\EloquentPermissionRepository;
use App\Repositories\Backend\Access\Permission\Group\EloquentPermissionGroupRepository;

/**
 * Class RoleController
 * @package App\Http\Controllers\Access
 */
class RoleController extends Controller {

    /**
     * @var RoleRepositoryContract
     */
    protected $roles;

    /**
     * @var PermissionRepositoryContract
     */
    protected $permissions;

    /**
     * @param RoleRepositoryContract       $roles
     * @param PermissionRepositoryContract $permissions
     */
    public function __construct(
    EloquentRoleRepository $roles, EloquentPermissionRepository $permissions
    ) {
	$this->roles = $roles;
	$this->permissions = $permissions;
    }

    /**
     * @return mixed
     */
    public function index() {
	return view('backend.access.roles.index');
    }

    /**
     * @param ManageRoleRequest $request
     * @return mixed
     */
    public function get() {
	return Datatables::of($this->roles->getForDataTable())
			->addColumn('permissions', function($role) {
			    $permissions = [];
			    if (count($role->permissions) > 0) {
				foreach ($role->permissions as $permission) {
				    array_push($permissions, $permission->display_name);
				}

				return implode(", ", $permissions);
			    } else {
				return '<span class="label label-danger">' . trans('labels.general.none') . '</span>';
			    }
			})
			->addColumn('users', function($role) {
			    return $role->users()->count();
			})
			->addColumn('actions', function($role) {
			    return $role->action_buttons;
			})
			->make(true);
    }

    /**
     * @param  PermissionGroupRepositoryContract $group
     * @return mixed
     */
    public function create(EloquentPermissionGroupRepository $group) {
	return view('backend.access.roles.create')
			->withGroups($group->getAllGroups());
    }

    /**
     * @return mixed
     */
    public function store(UpdateRoleRequest $request) {
	$this->roles->create($request->all());
	return redirect()->route('access.role.index')->withFlashSuccess(trans('alerts.backend.roles.created'));
    }

    /**
     * @param  $id
     * @param  PermissionGroupRepositoryContract $group
     * @return mixed
     */
    public function edit($id, EloquentPermissionGroupRepository $group) {
	$role = $this->roles->findOrThrowException($id, true);
	return view('backend.access.roles.edit')
			->withRole($role)
			->withRolePermissions($role->permissions->pluck('permission_id'))
			->withGroups($group->getAllGroups());
    }

    /**
     * @param  $id
     * @param  UpdateRoleRequest $request
     * @return mixed
     */
    public function update($id, UpdateRoleRequest $request) {
	$this->roles->update($id, $request->all());
	return redirect()->route('access.role.index')->withFlashSuccess(trans('alerts.backend.roles.updated'));
    }

    /**
     * @param  $id
     * @return mixed
     */
    public function destroy($id) {
	$this->roles->destroy($id);
	return redirect()->route('access.role.index')->withFlashSuccess(trans('alerts.backend.roles.deleted'));
    }

}
