<?php

namespace App\Http\Controllers\Backend\Sysdef;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Sysdef\RFID\Zone\EloquentZoneRepository;
use App\Repositories\Backend\Sysdef\Location\Country\EloquentCountryRepository;
use App\Repositories\Backend\Sysdef\Location\Region\EloquentRegionRepository;
use App\Repositories\Backend\Sysdef\Location\Branch\EloquentBranchRepository;
use App\Repositories\Backend\Sysdef\Location\Department\EloquentDepartmentRepository;
use App\Http\Requests\Backend\Sysdef\Zone\StoreZoneRequest;

class RFIDController extends Controller {

    protected $zones;

    public function __construct(EloquentZoneRepository $zones) {
	$this->zones = $zones;
    }

    public function index() {
	return view('backend.sysdef.rfid.zone.index');
    }

    public function get() {
	return Datatables::of($this->zones->getZonesForDataTable())
			->addColumn('departments', function($zone) {
			    $departments = [];
			    if ($zone->departments()->count() > 0) {
				foreach ($zone->departments as $department) {
				    array_push($departments, $department->name);
				}
				return implode("<br/>", $departments);
			    } else {
				return trans('labels.general.none');
			    }
			})
			->addColumn('actions', function($zone) {
			    return $zone->action_buttons;
			})
			->make(true);
    }

    public function create(EloquentCountryRepository $countries) {
	return view('backend.sysdef.rfid.zone.create')
			->withCountry($countries->getRegisteredCountry());
    }

    public function store(StoreZoneRequest $request) {
	$this->zones->create($request->all());
	return redirect()->route('sysdef.rfid.zone.create')->withFlashSuccess(trans('alerts.backend.sysdef.rfid.zone.created'));
    }

    public function edit($id, EloquentCountryRepository $countries, EloquentRegionRepository $regions, EloquentBranchRepository $branches) {
	$zone = $this->zones->findOrThrowException($id);
	return view('backend.sysdef.rfid.zone.edit')
			->withZone($zone)
			->withCountry($countries->getRegisteredCountry())
			->withRegions($regions->getCountryRegion($zone->departments()->first()->branch->region->country->country_id))
			->withBranches($branches->getRegionBranch($zone->departments()->first()->branch->region->region_id));
    }

    public function update($id, StoreZoneRequest $request) {
	$this->zones->update($id, $request->all());
	return redirect()->route('sysdef.rfid.zone.index')->withFlashSuccess(trans('alerts.backend.sysdef.rfid.zone.updated'));
    }
    
    public function destroy($id) {
	$this->zones->destroy($id);
	return redirect()->route('sysdef.rfid.zone.index')->withFlashSuccess(trans('alerts.backend.sysdef.rfid.zone.deleted'));
    }

    public function getDepartments(EloquentDepartmentRepository $departments) {
	return $departments->getBranchDepartments(request()->input('branch_id'), request()->input('q'));
    }

}
