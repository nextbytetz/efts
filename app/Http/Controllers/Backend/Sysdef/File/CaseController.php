<?php

namespace App\Http\Controllers\Backend\Sysdef\File;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Models\File\FileCase;
use App\Repositories\Backend\Sysdef\File\CaseRepository;
use App\Repositories\Backend\Sysdef\File\SubjectRepository;
use App\Http\Requests\Backend\Sysdef\File\StoreCaseRequest;

class CaseController extends Controller {

    protected $case;
    protected $subjects;

    public function __construct(CaseRepository $case, SubjectRepository $subjects) {
	$this->case = $case;
	$this->subjects = $subjects;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
	return view('backend.sysdef.file.case.index');
    }

    public function get() {
	return Datatables::of($this->case->getForDataTable())
			->addColumn('subject', function($case) {
			    return $case->subject->name;
			})
			->addColumn('actions', function($subject) {
			    return $subject->action_buttons;
			})
			->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
	$file_subject_id = isset(request()->file_subject_id) ? request()->file_subject_id : 0;
	return view('backend.sysdef.file.case.create')
			->withSubjects($this->subjects->getAllSubject())
			->withFileSubjectId($file_subject_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCaseRequest $request) {
	$this->case->create($request->all());
	return redirect()->route('sysdef.file.case.create')->withFlashSuccess(trans('alerts.backend.sysdef.file.case.created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FileCase $case) {
	return view('backend.sysdef.file.case.edit')
			->withSubjects($this->subjects->getAllSubject())
			->withCase($case);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FileCase $case, StoreCaseRequest $request) {
	$this->case->edit($case, $request->all());
	return redirect()->route('sysdef.file.case.index')->withFlashSuccess(trans('alerts.backend.sysdef.file.case.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileCase $case) {
	$this->case->destroy($case);
	return redirect()->route('sysdef.file.case.index')->withFlashSuccess(trans('alerts.backend.sysdef.file.case.deleted'));
    }

}
