<?php

namespace App\Http\Controllers\Backend\Sysdef\File;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Models\File\FileSubject;
use App\Repositories\Backend\Sysdef\File\SubjectRepository;
use App\Http\Requests\Backend\Sysdef\File\StoreSubjectRequest;

class SubjectController extends Controller {

    protected $subjects;

    public function __construct(SubjectRepository $subjects) {
	$this->subjects = $subjects;
    }

    public function index() {
	return view('backend.sysdef.file.subject.index');
    }

    public function get() {
	return Datatables::of($this->subjects->getForDataTable())
			->addColumn('cases', function($subject) {
			    return $subject->cases()->count();
			})
			->addColumn('files', function($subject) {
			    return $subject->files()->count();
			})
			->addColumn('actions', function($subject) {
			    return $subject->action_buttons;
			})
			->make(true);
    }

    public function create() {
	return view('backend.sysdef.file.subject.create');
    }

    public function store(StoreSubjectRequest $request) {
	$this->subjects->create($request->all());
	return redirect()->route('sysdef.file.subject.create')->withFlashSuccess(trans('alerts.backend.sysdef.file.subject.created'));
    }

    public function edit(FileSubject $subject) {
	return view('backend.sysdef.file.subject.edit')
			->withSubject($subject);
    }

    public function update(FileSubject $subject, StoreSubjectRequest $request) {
	$this->subjects->edit($subject, $request->all());
	return redirect()->route('sysdef.file.subject.index')->withFlashSuccess(trans('alerts.backend.sysdef.file.subject.updated'));
    }

    public function destroy(FileSubject $subject) {
	$this->subjects->destroy($subject);
	return redirect()->route('sysdef.file.subject.index')->withFlashSuccess(trans('alerts.backend.sysdef.file.subject.deleted'));
    }

}
