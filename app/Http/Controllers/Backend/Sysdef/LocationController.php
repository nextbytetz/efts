<?php

namespace App\Http\Controllers\Backend\Sysdef;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\Sysdef\Location\Country\EloquentCountryRepository;
use App\Repositories\Backend\Sysdef\Location\Region\EloquentRegionRepository;
use App\Repositories\Backend\Sysdef\Location\Branch\EloquentBranchRepository;
use App\Repositories\Backend\Sysdef\Location\Department\EloquentDepartmentRepository;
use App\Http\Requests\Backend\Sysdef\Location\StoreBranchRequest;
use App\Http\Requests\Backend\Sysdef\Location\StoreCountryRequest;
use App\Http\Requests\Backend\Sysdef\Location\StoreDepartmentRequest;
use App\Http\Requests\Backend\Sysdef\Location\StoreRegionRequest;
use App\Http\Requests\Backend\Sysdef\Location\UpdateRequest;

class LocationController extends Controller {

    protected $countries;
    protected $regions;
    protected $branches;
    protected $departments;

    public function __construct(EloquentCountryRepository $countries, EloquentRegionRepository $regions, EloquentBranchRepository $branches, EloquentDepartmentRepository $departments) {
	$this->countries = $countries;
	$this->regions = $regions;
	$this->branches = $branches;
	$this->departments = $departments;
    }

    public function showCountry() {
	return view('backend.sysdef.location.country.index');
    }

    public function getCountry() {
	return Datatables::of($this->countries->getForDataTable())
			->editColumn('created_at', function ($country) {
			    return $country->created_at->diffForHumans();
			})
			->editColumn('updated_at', function($country) {
			    return (!is_null($country->updated_at)) ? $country->updated_at->diffForHumans() : '';
			})
			->addColumn('regions', function($country) {
			    return $country->regions()->count();
			})
			->addColumn('actions', function($country) {
			    return $country->action_buttons;
			})
			->make(true);
    }

    public function createCountry() {
	$countries = $this->countries->getAllCountry();
	return view('backend.sysdef.location.country.create')
			->withCountry($countries);
    }

    public function storeCountry(StoreCountryRequest $request) {
	$this->countries->create($request->all());
	return redirect()->route('sysdef.country.create')->withFlashSuccess(trans('alerts.backend.sysdef.location.country.created'));
    }

    public function editCountry($country_id) {
	$country = $this->countries->findOrThrowException($country_id);
	return view('backend.sysdef.location.country.edit')->withCountry($country);
    }

    public function destroyCountry($country_id) {
	$this->countries->destroy($country_id);
	return redirect()->route('sysdef.country.index')->withFlashSuccess(trans('alerts.backend.sysdef.location.country.deleted'));
    }

    public function showRegion() {
	return view('backend.sysdef.location.region.index');
    }

    public function getRegion() {
	return Datatables::of($this->regions->getForDataTable())
			->editColumn('created_at', function ($region) {
			    return $region->created_at->diffForHumans();
			})
			->editColumn('updated_at', function($region) {
			    return (!is_null($region->updated_at)) ? $region->updated_at->diffForHumans() : '';
			})
			->addColumn('country', function($region) {
			    return $region->country->name;
			})
			->addColumn('branches', function($region) {
			    return $region->branches()->count();
			})
			->addColumn('actions', function($region) {
			    return $region->action_buttons;
			})
			->make(true);
    }

    public function createRegion() {
	$countries = $this->countries->getRegisteredCountry();
	return view('backend.sysdef.location.region.create')
			->withCountry($countries);
    }

    public function storeRegion(StoreRegionRequest $request) {
	$this->regions->create($request->all());
	return redirect()->route('sysdef.region.create')->withFlashSuccess(trans('alerts.backend.sysdef.location.region.created'));
    }

    public function editRegion($region_id) {
	$region = $this->regions->findOrThrowException($region_id);
	return view('backend.sysdef.location.region.edit')
			->withRegion($region);
    }

    public function updateRegion($region_id, UpdateRequest $request) {
	$this->regions->update($region_id, $request->all());
	return redirect()->route('sysdef.region.index')->withFlashSuccess(trans('alerts.backend.sysdef.location.region.updated'));
    }

    public function destroyRegion($region_id) {
	$this->regions->destroy($region_id);
	return redirect()->route('sysdef.region.index')->withFlashSuccess(trans('alerts.backend.sysdef.location.region.deleted'));
    }

    public function showBranch() {
	return view('backend.sysdef.location.branch.index');
    }

    public function getBranch() {
	return Datatables::of($this->branches->getForDataTable())
			->editColumn('created_at', function ($branch) {
			    return $branch->created_at->diffForHumans();
			})
			->editColumn('updated_at', function($branch) {
			    return (!is_null($branch->updated_at)) ? $branch->updated_at->diffForHumans() : '';
			})
			->addColumn('region', function($branch) {
			    return $branch->region->name;
			})
			->addColumn('country', function($branch) {
			    return $branch->region->country->name;
			})
			->addColumn('departments', function($branch) {
			    return $branch->departments()->count();
			})
			->addColumn('actions', function($branch) {
			    return $branch->action_buttons;
			})
			->make(true);
    }

    public function createBranch() {
	$countries = $this->countries->getRegisteredCountry();
	return view('backend.sysdef.location.branch.create')
			->withCountry($countries);
    }

    public function storeBranch(StoreBranchRequest $request) {
	$this->branches->create($request->all());
	return redirect()->route('sysdef.branch.create')->withFlashSuccess(trans('alerts.backend.sysdef.location.branch.created'));
    }

    public function editBranch($branch_id) {
	$branch = $this->branches->findOrThrowException($branch_id);
	return view('backend.sysdef.location.branch.edit')
			->withBranch($branch);
    }

    public function updateBranch($branch_id, UpdateRequest $request) {
	$this->branches->update($branch_id, $request->all());
	return redirect()->route('sysdef.branch.index')->withFlashSuccess(trans('alerts.backend.sysdef.location.branch.updated'));
    }

    public function destroyBranch($branch_id) {
	$this->branches->destroy($branch_id);
	return redirect()->route('sysdef.branch.index')->withFlashSuccess(trans('alerts.backend.sysdef.location.branch.deleted'));
    }

    public function showDepartment() {
	return view('backend.sysdef.location.department.index');
    }

    public function getDepartment() {
	return Datatables::of($this->departments->getForDataTable())
			->editColumn('created_at', function ($department) {
			    return $department->created_at->diffForHumans();
			})
			->editColumn('updated_at', function($department) {
			    return (!is_null($department->updated_at)) ? $department->updated_at->diffForHumans() : '';
			})
			->addColumn('branch', function($department) {
			    return $department->branch->name;
			})
			->addColumn('region', function($department) {
			    return $department->branch->region->name;
			})
			->addColumn('country', function($department) {
			    return $department->branch->region->country->name;
			})
			->addColumn('users', function($department) {
			    return $department->users()->count();
			})
			->addColumn('actions', function($department) {
			    return $department->action_buttons;
			})
			->make(true);
    }

    public function createDepartment() {
	$countries = $this->countries->getRegisteredCountry();
	return view('backend.sysdef.location.department.create')
			->withCountry($countries);
    }

    public function storeDepartment(StoreDepartmentRequest $request) {
	$this->departments->create($request->all());
	return redirect()->route('sysdef.department.create')->withFlashSuccess(trans('alerts.backend.sysdef.location.department.created'));
    }

    public function editDepartment($department_id) {
	$department = $this->departments->findOrThrowException($department_id);
	return view('backend.sysdef.location.department.edit')
			->withDepartment($department);
    }

    public function updateDepartment($department_id, UpdateRequest $request) {
	$this->departments->update($department_id, $request->all());
	return redirect()->route('sysdef.department.index')->withFlashSuccess(trans('alerts.backend.sysdef.location.department.updated'));
    }

    public function destroyDepartment($department_id) {
	$this->departments->destroy($department_id);
	return redirect()->route('sysdef.department.index')->withFlashSuccess(trans('alerts.backend.sysdef.location.department.deleted'));
    }

}
