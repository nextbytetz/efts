<?php

namespace App\Http\Controllers\Backend\File\Register;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\File\Register\CreateFileRequest;
use App\Http\Requests\Backend\File\Register\StoreFileRequest;
use App\Http\Requests\Backend\File\Register\StoreBulkFileRequest;
use App\Http\Requests\Backend\File\Register\UpdateFileRequest;
use App\Repositories\Backend\File\Register\EloquentRegisterRepository;
use App\Repositories\Backend\Sysdef\Location\Country\EloquentCountryRepository;
use App\Repositories\Backend\History\EloquentHistoryRepository;
use App\Services\Storage\Traits\FileHandler;

class RegisterController extends Controller {

    protected $files;
    protected $countries;

    use FileHandler;

    public function __construct(EloquentRegisterRepository $files, EloquentCountryRepository $countries) {
	$this->files = $files;
	$this->countries = $countries;
    }

    public function index() {
	return view('backend.file.register.index');
    }

    public function create(CreateFileRequest $request) {
	if (!in_array($request['file_nature'], [1, 2])) {
	    return redirect()->route('file.register.index')->withFlashDanger(trans('alerts.backend.file_register.invalid_create'));
	}
	return view('backend.file.register.create')
			->withFileNature($request['file_nature'])
			->withFileTypes($this->files->getAllFileType())
			->withFileCategories($this->files->getAllFileCategory())
			->withCountries($this->countries->getCountry())
			->withPriorities($this->files->getAllPriority())
			->withFileSubjects($this->files->getAllSubject());
    }

    public function store(StoreFileRequest $request) {
	$file_id = $this->files->create($request->all());
	$this->makeFileDirectory($file_id);
	return redirect()->route('file.register.index')->withFlashSuccess(trans('alerts.backend.file_register.created'));
    }

    public function edit($file_id) {
	$file = $this->files->findOrThrowException($file_id);
	if (count($file->users)) {
	    $departments = [];
	    foreach ($file->users as $user) {
		$departments[] = $user->department->department_id;
	    }
	} else {
	    $departments = $file->departments->pluck('department_id')->all();
	}
	return view('backend.file.register.edit')
			->withFile($file)
			->withFileDepartments($departments)
			->withFileOfficers($file->users)
			->withFileNature($file->file_nature_id)
			->withFileTypes($this->files->getAllFileType($file->file_type_id))
			->withFileCategories($this->files->getAllFileCategory())
			->withCountries($this->countries->getCountry())
			->withPriorities($this->files->getAllPriority())
			->withFileSubjects($this->files->getAllSubject());
    }

    public function show($file_id) {
	$file = access()->checkFileOwner($file_id);
	$folder_id = isset(request()->folder_id) ? request()->folder_id : 0;
	return view('backend.file.register.show')
			->withFile($file)
			->withFolderId($folder_id);
    }

    public function update($id, UpdateFileRequest $request) {
	$file_id = $this->files->update($id, $request->all(), $request->except(['_token', '_method', 'departments', 'officers']));
	return redirect()->route('file.register.direct.search', $file_id)->withFlashSuccess(trans('alerts.backend.file_register.updated'));
    }

    public function destroy($id) {
	$this->files->destroy($id);
	return redirect()->route('file.register.all')->withFlashSuccess(trans('alerts.backend.file_register.deleted'));
    }

    public function all() {
	return view('backend.file.register.all');
    }

    public function get() {
	//get all registered files available for this user
	return Datatables::of($this->files->getForDataTable())
			->addColumn('icon', function($file) {
			    return $file->icon;
			})
			->addColumn('name', function($file) {
			    return $file->general_info;
			})
			->addColumn('priority', function($file) {
			    return $file->priority->name;
			})
			->addColumn('status', function($file) {
			    return $file->status_info;
			})
			->addColumn('actions', function($file) {
			    return $file->action_buttons;
			})
			->make(true);
    }

    public function bulk() {
	return view('backend.file.register.bulk')
			->withFileNature(2)
			->withFileTypes($this->files->getAllFileType(1))
			->withCountries($this->countries->getCountry())
			->withPriorities($this->files->getAllPriority())
			->withFileSubjects($this->files->getAllSubject());
    }

    public function storeBulk(StoreBulkFileRequest $request) {
	$this->files->storeBulk($request->all());
	return redirect()->route('file.register.bulk', ['#recent_import'])->withFlashSuccess(trans('alerts.backend.file_register.bulk_uploaded'));
    }

    public function getBulk() {
	return Datatables::of($this->files->getBulkForDataTable())
			->addColumn('general', function($bulk) {
			    return $bulk->general_info;
			})
			->editColumn('actions', function ($bulk) {
			    return $bulk->action_buttons;
			})
			->make(true);
    }

    public function history($id) {
	access()->checkFileOwner($id);
	return view("backend.file.register.history")
			->withFileId($id);
    }

    public function getHistory(EloquentHistoryRepository $histories) {
	//get the datatable for the history of the file
	return Datatables::of($histories->getSpecificTypeForDatatable(request()->history_type, request()->file_id))
			->addColumn('history', function($history) {
			    return $history->history_label;
			})
			->editColumn('created_at', function ($history) {
			    return $history->time_label;
			})
			->make(true);
    }

    public function search() {
	return view('backend.file.register.search')
			->withSearchText(request()->search_item);
    }

    public function searchDirect($id) {
	$file = $this->files->findOrThrowException($id);
	return view('backend.file.register.search')
			->withSearchText($file->name);
    }

    public function getSearch() {
	return Datatables::of($this->files->getForDataTable(request()->search_text))
			->addColumn('icon', function($file) {
			    return $file->icon;
			})
			->addColumn('name', function($file) {
			    return $file->general_info;
			})
			->addColumn('priority', function($file) {
			    return $file->priority->name;
			})
			->addColumn('status', function($file) {
			    return $file->status_info;
			})
			->addColumn('actions', function($file) {
			    return $file->action_buttons;
			})
			->make(true);
    }

    public function getRegisterOfficers() {
	return $this->files->getRegisterOfficers(request()->departments, request()->q);
    }
            
    public function searchAll() {
	return $this->files->searchAll(request()->input("term"));
    }

}
