<?php

namespace App\Http\Controllers\Backend\File\Register\Storage;

use App\Services\Storage\Traits\DocumentHandler;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Models\File\Document;
use App\Repositories\Backend\Document\EloquentDocumentRepository;
use App\Repositories\Backend\File\Register\EloquentRegisterRepository;
use App\Http\Requests\Backend\Document\StoreStatusRequest;

class DocumentController extends Controller {

    protected $documents;
    protected $files;
    protected $base = null;

    use DocumentHandler;

    /**
     * @description : 
     * @throws GeneralException
     */
    public function __construct(EloquentDocumentRepository $documents, EloquentRegisterRepository $files) {
        $this->documents = $documents;
        $this->files = $files;
        $this->base = $this->real(file_dir() . DIRECTORY_SEPARATOR);
        if (!$this->base) {
            throw new GeneralException(trans("validation.attributes.backend.file.storage.base_error"));
        }
    }

    public function index() {
        
    }

    public function get() {
        //get all registered files available for this user
        return Datatables::of($this->documents->getForDataTable(request()->folder_id, request()->file_id))
                        ->addColumn('icon', function($document) {
                            return $document->icon;
                        })
                        ->addColumn('name', function($document) {
                            return $document->general_info;
                        })
                        ->addColumn('priority', function($document) {
                            return $document->priority->name;
                        })
                        ->addColumn('status', function($document) {
                            return $document->status_info;
                        })
                        ->addColumn('actions', function($document) {
                            return $document->action_buttons;
                        })
                        ->make(true);
    }

    public function getStatus() {
        //get all registered files available for this user
        return Datatables::of($this->documents->getStatusForDataTable(request()->document_id))
                        ->addColumn('date', function($status) {
                            return $status->created_at;
                        })
                        ->addColumn('priority', function($status) {
                            return $status->priority->name;
                        })
                        ->addColumn('user', function($status) {
                            return $status->user->name();
                        })
                        ->addColumn('comment', function($status) {
                            return $status->comment;
                        })
                        ->addColumn('actions', function($status) {
                            return $status->action_buttons;
                        })
                        ->make(true);
    }

    public function store() {
        $this->saveDocument();
	$status = request()->input('status');
	if ($status == 'update') {
	    Document::where(['document_id' => request()->input('document_id')])->update(['description' => request()->input('description')]);
	    return redirect()->to(app('url')->previous(). '#edit_document')->withFlashSuccess(trans('alerts.backend.document.updated'));
	}
        return response()->json(true);
    }

    public function show($id) {
        $document = $this->documents->findOrThrowException($id);
        access()->checkFileOwner($document->file->file_id);
        $folder_id = (count($document->folder)) ? $document->folder->folder_id : 0;
        return view('backend.file.register.document.show')
                        ->withDocument($document)
                        ->withAction('show')
                        ->withFolderId($folder_id)
                        ->withPriority($this->documents->getAllPriority())
                        ->withDocStatus($this->documents->getDocStatus($id));
    }

    public function destroy($id) {
        $file_id = $this->documents->destroy($id);
        return redirect()->route('file.register.show', $file_id)->withFlashSuccess(trans('alerts.backend.document.deleted'));
    }

    public function edit($id) {
        $document = $this->documents->findOrThrowException($id);
        access()->checkFileOwner($document->file->file_id);
        $folder_id = (count($document->folder)) ? $document->folder->folder_id : 0;
        return view('backend.file.register.document.show')
                        ->withDocument($document)
                        ->withAction('edit')
                        ->withFolderId($folder_id)
                        ->withPriority($this->documents->getAllPriority())
                        ->withDocStatus($this->documents->getDocStatus($id));
    }

    public function status(StoreStatusRequest $request) {
        $document = $this->documents->updateStatus($request->all());
        return redirect()->route('storage.document.show', $document->document_id)->withFlashSuccess(trans('alerts.backend.document.status.updated'));
    }

    public function editStatus($id) {
        
    }

    public function destroyStatus($id) {
        $document_id = $this->documents->destroyStatus($id);
        return redirect()->route('storage.document.show', $document_id)->withFlashSuccess(trans('alerts.backend.document.status.deleted'));
    }

    public function download($id) {
        $document = $this->documents->findOrThrowException($id);
        access()->checkFileOwner($document->file->file_id);
        return response()->download($document->link, $document->name);
    }

    public function file($id) {
        $document = $this->documents->findOrThrowException($id);
        access()->checkFileOwner($document->file->file_id);
        return response()->file($document->link);
    }
    
        
    public function searchDocument() {
        return $this->documents->searchDocument(request()->q, request()->file_id);
    }
    
    public function transfer() {
	return view('backend.file.register.document.transfer');
    }

}
