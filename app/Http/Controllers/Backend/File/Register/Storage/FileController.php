<?php

namespace App\Http\Controllers\Backend\File\Register\Storage;

use App\Services\Storage\Traits\FileHandler;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Repositories\Backend\File\Register\EloquentRegisterRepository;

class FileController extends Controller {

    protected $files;
    protected $base = null;
    protected $folders = null;
    protected $documents = null;
    protected $parentFolder = null;

    use FileHandler;

    /**
     * @description : Initialize the base file directory to be used
     * @throws GeneralException
     */
    public function __construct(EloquentRegisterRepository $files) {
	$this->files = $files;
	$this->base = $this->real(file_dir() . DIRECTORY_SEPARATOR);
	if (!$this->base) {
	    throw new GeneralException('Base directory does not exist');
	}
    }

    /**
     * list the directory listing of the file
     */
    public function index() {
	$this->base = $this->base . DIRECTORY_SEPARATOR . request()->file_id;
	$file = $this->files->findOrThrowException(request()->file_id);
	$this->parentFolder = $file->pluck('name', 'file_id')->all();
	if (count($file->folders)) {
	    $this->folders = $file->folders->pluck('name', 'folder_id')->all();
	}
	if (count($file->documents)) {
	    $this->documents = $file->documents->pluck('name', 'document_id')->all();
	}
	$node = isset(request()->id) && request()->id !== '#' ? request()->id : '/';
	$rslt = $this->lst($node, (isset(request()->id) && request()->id === '#'));
	return response()->json($rslt);
    }

    public function show($id) {
	$this->base = $this->base . DIRECTORY_SEPARATOR . $id;
	$file = $this->files->findOrThrowException($id);
	$this->parentFolder = $file->pluck('name', 'file_id')->all();
	if (count($file->folders)) {
	    $this->folders = $file->folders->pluck('name', 'folder_id')->all();
	}
	if (count($file->documents)) {
	    $this->documents = $file->documents->pluck('name', 'document_id')->all();
	}
	$node = isset(request()->id) && request()->id !== '#' ? request()->id : '/';
	$rslt = $this->lst($node, (isset(request()->id) && request()->id === '#'));
	return response()->json($rslt);
    }

    /**
     * Used to create a document in the folder.
     */
    public function store() {
	
    }

    /**
     * Used to create a folder in the file.
     */
    public function create() {
	$this->base = $this->base . DIRECTORY_SEPARATOR . request()->file_id;
	$node = isset(request()->id) && request()->id !== '#' ? request()->id : '/';
	$rslt = $this->createFolder($node, isset(request()->text) ? request()->text : '', 'file');
	return response()->json($rslt);
    }

    /**
     * Used to update a document in the folder
     */
    public function update() {
	
    }

    /**
     * Delete a document or folder in the file
     */
    public function destroy($file_id) {
	$this->base = $this->base . DIRECTORY_SEPARATOR . $file_id;
	$node = isset(request()->id) && request()->id !== '#' ? request()->id : '/';
	$rslt = $this->remove($node);
	return response()->json($rslt);
    }

    /**
     * Used to edit folder in the file
     */
    public function edit() {
	$this->base = $this->base . DIRECTORY_SEPARATOR . request()->file_id;
	$node = isset(request()->id) && request()->id !== '#' ? request()->id : '/';
	$rslt = $this->rename($node, isset(request()->text) ? request()->text : '');
	return response()->json($rslt);
    }

    /**
     * move document of folder from one location to another
     */
    public function move() {
	$this->base = $this->base . DIRECTORY_SEPARATOR . request()->file_id;
	$node = isset(request()->id) && request()->id !== '#' ? request()->id : '/';
	$parn = isset(request()->parent) && request()->parent !== '#' ? request()->parent : '/';
	$rslt = $this->moveFile($node, $parn);
	return response()->json($rslt);
    }

    public function get() {
	//get all registered files available for this user
	return Datatables::of($this->files->getFolderForDataTable(request()->folder_id, request()->file_id))
			->addColumn('icon', function($document) {
			    return $document->icon;
			})
			->addColumn('name', function($document) {
			    return $document->general_info;
			})
			->addColumn('status', function($document) {
			    return $document->status_info;
			})
			->addColumn('actions', function($document) {
			    return $document->action_buttons;
			})
			->make(true);
    }

}
