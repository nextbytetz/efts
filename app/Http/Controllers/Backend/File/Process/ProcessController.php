<?php

namespace App\Http\Controllers\Backend\File\Process;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests\Backend\File\Process\DispatchFileRequest;
use App\Repositories\Backend\File\Process\EloquentProcessRepository;
use App\Repositories\Backend\File\Register\EloquentRegisterRepository;
use Illuminate\Http\Request;
use DNS1D;

class ProcessController extends Controller {

    protected $files;

    public function __construct(EloquentProcessRepository $files) {
	$this->files = $files;
    }

    public function index() {
	
    }

    public function log($id) {
	$file = access()->checkFileOwner($id);
	return view("backend.file.register.log")
			->withFileId($id)
			->withWorkflow($this->files->getWorkflow($id))
			->withFileCategoryId($file->file_category_id);
    }

    public function logPost($id) {
	$this->files->logPost($id, request()->all());
	return response()->json(true);
    }

    public function getLog() {
	//get the datatable for the history of the file
	return Datatables::of($this->files->getLogForDatatable(request()->file_id))
			->addColumn('log', function($log) {
			    return $log->log_label;
			})
			->addColumn('updated_at', function ($log) {
			    return $log->time_label;
			})
			->make(true);
    }

    public function myFileList() {
	return view('backend.file.process.list');
    }

    /**
     * state 4 : {{My Files}}
     * state 6 : {{Received Files}}
     * state 7 : {{File Requests}}
     * @param RegisterRepositoryContract $files
     * @return type
     */
    public function getMyFileList(EloquentRegisterRepository $files) {
	$state = request()->state;
	switch ($state) {
	    case 4:
		$datatable = $files->getMyFileForDataTable();
		break;
	    case 6:
		$datatable = $files->getReceivedFileForDataTable();
		break;
	    case 7:
		$datatable = $files->getFileRequestForDataTable();
		break;
	}
	return Datatables::of($datatable)
			->addColumn('icon', function($file) {
			    return $file->icon;
			})
			->addColumn('name', function($file) {
			    return $file->general_info;
			})
			->addColumn('priority', function($file) {
			    return $file->priority->name;
			})
			->addColumn('status', function($file) {
			    return $file->status_info;
			})
			->addColumn('actions', function($file) {
			    return $file->action_buttons;
			})
			->make(true);
    }

    public function search() {
	return view('backend.file.process.search');
    }

    public function forward($id, EloquentRegisterRepository $files) {
	$file = access()->checkFileOwner($id);
	return view("backend.file.register.forward")
			->withPriorities($files->getAllPriority())
			->withWorkflow($this->files->getWorkflow($id))
			->withFile($file);
    }

    public function forwardPost($id, DispatchFileRequest $request) {
	$file = $this->files->forwardPost($id, $request->all());
	return redirect()->route("file.register.all")->withFlashSuccess(trans('alerts.backend.file_process.dispatched', ['name' => $file]));
    }

    public function pick($id) {
	$file = $this->files->pickUp($id);
	return redirect()->route("file.register.show", $file->file_id)->withFlashSuccess(trans('alerts.backend.file_process.picked'));
    }

    public function pickForced($id) {
	$file = $this->files->pickUpForced($id);
	return redirect()->route("file.register.show", $file->file_id)->withFlashSuccess(trans('alerts.backend.file_process.picked'));
    }

    public function pickUsers($id) {
	
    }

    public function requestUsers($id) {
	
    }

    public function workflowUsers($id) {
	
    }

    public function release($id) {
	access()->checkFileOwner($id);
	$file_name = $this->files->release($id);
	return redirect()->route("file.register.all")->withFlashSuccess(trans('alerts.backend.file_process.released', ['name' => $file_name]));
    }

    public function getForwardOfficers($id) {
	return $this->files->getForwardOfficers($id, request()->q);
    }

    public function request($id) {
	$file_name = $this->files->request($id);
	return redirect()->back()->withFlashSuccess(trans('alerts.backend.file_process.requested', ['name' => $file_name]));
    }

    public function pickWarned($id) {
	//pick the file while it has not been recepted to officer
	$file = $this->files->pickWarned($id);
	return redirect()->route("file.register.show", $file->file_id)->withFlashSuccess(trans('alerts.backend.file_process.received', ['name' => $file->name]));
    }

    public function receive($id) {
	$file = $this->files->receive($id);
	return redirect()->route("file.register.show", $file->file_id)->withFlashSuccess(trans('alerts.backend.file_process.received', ['name' => $file->name]));
    }

    public function requestCancel($id) {
	$file_name = $this->files->requestCancel($id);
	return redirect()->back()->withFlashSuccess(trans('alerts.backend.file_process.requested_cancelled', ['name' => $file_name]));
    }

    public function cases($id) {
	return $this->files->cases($id);
    }

    public function editCase($id) {
	return $this->files->editCase($id);
    }

    public function barcodes(Request $request) {
	if ($request->isMethod('post')) {
	    $basic = [
		'print_option' => 'required',
		'file' => 'required',
	    ];
	    $stye_50 = [];
	    if ($request->input('style') == 50) {
		$opt = [
		    'cf_width' => 'required',
		    'cf_height' => 'required',
		];
		$stye_50 = [
		    'cf_width' => $request->input('cf_width'),
		    'cf_height' => $request->input('cf_height'),
		    'cf_orientation' => $request->input('cf_orientation'),
		];
		$basic = array_merge($basic, $opt);
	    }
	    $this->validate($request, $basic);

	    $style = $request->input('style');
	    $bci_size = ($style == 10 || $style == 12 ? 50 : ($style == 14 || $style == 18 ? 30 : 20));
	    $print_option = $request->only('print_option');
	    $file = $request->input('file');
	    $quantity = $request->input('quantity');
	    $name = $request->input('name');
	    $reference = $request->input('reference');

	    $s = sizeof($file);
	    for ($m = 0; $m < $s; $m++) {
		$barcodes[] = [
		    'file' => $file[$m],
		    'quantity' => $quantity[$m],
		    'name' => $name[$m],
		    'reference' => $reference[$m],
		    'barcode' => '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($file[$m], "C128", 1, $bci_size) . '" alt="barcode"   />',
		];
	    }
	    return view('backend.file.process.barcodes')
			    ->withStyles($this->files->barcodePrintStyle())
			    ->withInput(array_shift($print_option))
			    ->withBarcodes($barcodes)
			    ->withStyle($style)
			    ->withStyle50($stye_50);
	} elseif ($request->isMethod('get')) {
	    return view('backend.file.process.barcodes')
			    ->withStyles($this->files->barcodePrintStyle());
	}
    }

}
