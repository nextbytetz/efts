<?php

namespace App\Http\Controllers\Backend\File\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\BasicRptDataTable;
use App\DataTables\ClosedRptDataTable;
use App\DataTables\TrackingRptDataTable;
use App\DataTables\UserRptDataTable;
use App\DataTables\BasicNoticeRptDataTable;

class ReportController extends Controller {

    public function __construct() {

    }

    public function basic(BasicRptDataTable $dataTable) {
	return $dataTable->render('backend.file.report.basic');
    }

    public function closed(ClosedRptDataTable $dataTable) {
	return $dataTable->render('backend.file.report.closed');
    }

    public function tracking(TrackingRptDataTable $dataTable) {
	return $dataTable->render('backend.file.report.tracking');
    }

    public function user(UserRptDataTable $dataTable) {
	return $dataTable->render('backend.file.report.user');
    }
    
    public function basicNotice(BasicNoticeRptDataTable $dataTable) {
	return $dataTable->render('backend.file.report.basic_notice');
    }

}
