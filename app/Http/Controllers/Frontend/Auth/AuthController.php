<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Services\Access\Traits\AuthenticatesAndRegistersUsers;
use App\Services\Access\Traits\ResetsPasswords;
use App\Repositories\Frontend\Access\User\EloquentUserRepository;
use App\Repositories\Backend\Sysdef\Location\Country\EloquentCountryRepository;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login & Reset Password Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users and reseting the password of existing users
      | . By default, this controller uses
      | a simple trait to add these behaviors.
      |
     */

use AuthenticatesAndRegistersUsers,
    ResetsPasswords;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Where to redirect users after they logout
     *
     * @var string
     */
    protected $redirectAfterLogout = '/';
    
    
    protected $countries;

    /**
     * @param UserRepositoryContract $user
     */
    public function __construct(EloquentUserRepository $user, EloquentCountryRepository $countries) {
	$this->user = $user;
        $this->countries = $countries;
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath() {
	return property_exists($this, 'redirectTo') ? $this->redirectTo : '/dashboard';
    }

}
