<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Services\Setup\Traits\Setup;
use App\Repositories\Frontend\Access\User\EloquentUserRepository;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller {
    
    use Setup;

    
    public function __construct(EloquentUserRepository $user) {
	$this->user = $user;
    }
    /**
     * @return \Illuminate\View\View
     */
    public function terms() {
	return view('frontend.terms');
    }
    
    /**
     * @return \Illuminate\View\View
     */
    public function help() {
	return view('frontend.help');
    }

}
