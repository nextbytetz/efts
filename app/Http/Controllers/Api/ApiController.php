<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Mail\MailHandler;
use App\Repositories\Backend\File\Process\EloquentProcessRepository;
use App\Models\Access\User;

class ApiController extends Controller {
        
    protected $files;

    public function __construct(EloquentProcessRepository $files) {
	$this->files = $files;
    }

    public function sendEmail(User $user) {
	$mail = new MailHandler;
	return $mail->sendSample($user);
    }

    public function reader() {
	return $this->files->readerStore(request()->all());
    }

    
    
}
