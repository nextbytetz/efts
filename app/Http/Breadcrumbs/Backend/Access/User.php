<?php

Breadcrumbs::register('access.user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.access.users.management'), route('access.user.index'));
});

Breadcrumbs::register('access.user.deactivated', function ($breadcrumbs) {
    $breadcrumbs->parent('access.user.index');
    $breadcrumbs->push(trans('menus.backend.access.users.deactivated'), route('access.user.deactivated'));
});

Breadcrumbs::register('access.user.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('access.user.index');
    $breadcrumbs->push(trans('menus.backend.access.users.edit'), route('access.user.edit', $id));
});

Breadcrumbs::register('access.user.change_password', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('access.user.index');
    $breadcrumbs->push(trans('menus.backend.access.users.change_password'), route('access.user.change_password', $id));
});

Breadcrumbs::register('access.user.profile', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('menus.backend.access.users.profile'), route('access.user.profile'));
});

Breadcrumbs::register('access.user.password', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('menus.backend.access.users.password'), route('access.user.password'));
});

