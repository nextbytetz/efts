<?php

Breadcrumbs::register('access.role.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('menus.backend.access.roles.management'), route('access.role.index'));
});

Breadcrumbs::register('access.role.create', function ($breadcrumbs) {
    $breadcrumbs->parent('access.role.index');
    $breadcrumbs->push(trans('menus.backend.access.roles.create'), route('access.role.create'));
});

Breadcrumbs::register('access.role.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('access.role.index');
    $breadcrumbs->push(trans('menus.backend.access.roles.edit'), route('access.role.edit', $id));
});