<?php

Breadcrumbs::register('backend.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(trans('labels.general.dashboard'), route('backend.dashboard'));
});

Breadcrumbs::register('backend.notification', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.general.notification.title'), route('backend.notification'));
});

require __DIR__ . '/Access.php';
require __DIR__ . '/Sysdef.php';
require __DIR__ . '/File.php';