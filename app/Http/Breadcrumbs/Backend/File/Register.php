<?php

use App\Models\File\File;
use App\Models\File\Document;

Breadcrumbs::register('file.register.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.file_register.title'), route('file.register.index'));
});

Breadcrumbs::register('file.register.create', function ($breadcrumbs) {
    $breadcrumbs->parent('file.register.index');
    $breadcrumbs->push(trans('labels.backend.file_register.create_file'), route('file.register.create'));
});

Breadcrumbs::register('file.register.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('file.register.index');
    $breadcrumbs->push(trans('labels.backend.file_register.edit'), route('file.register.edit', $id));
});

Breadcrumbs::register('file.register.all', function ($breadcrumbs) {
    $breadcrumbs->parent('file.register.index');
    $breadcrumbs->push(trans('labels.backend.file_register.all'), route('file.register.all'));
});

Breadcrumbs::register('file.register.search', function ($breadcrumbs) {
    $breadcrumbs->parent('file.register.index');
    $breadcrumbs->push(trans('labels.backend.file_register.search'), route('file.register.search'));
});

Breadcrumbs::register('file.register.direct.search', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('file.register.index');
    $breadcrumbs->push(trans('labels.backend.file_register.search'), route('file.register.direct.search', $id));
});

Breadcrumbs::register('file.register.bulk', function ($breadcrumbs) {
    $breadcrumbs->parent('file.register.index');
    $breadcrumbs->push(trans('labels.backend.file_register.bulk_register'), route('file.register.bulk'));
});

Breadcrumbs::register('file.register.show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('file.register.all');
    $file = File::find($id);
    $breadcrumbs->push(trans('labels.backend.file_register.show') . " (" . substr($file->name, 0, 30) . " ...)", route('file.register.show', $id));
});

Breadcrumbs::register('file.register.history', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('file.register.show', $id);
    $breadcrumbs->push(trans('labels.backend.file_register.history'), route('file.register.history', $id));
});

Breadcrumbs::register('storage.document.show', function ($breadcrumbs, $id) {
    $document = Document::find($id);
    $breadcrumbs->parent('file.register.show', $document->file->file_id);
    $breadcrumbs->push(trans('labels.backend.file_register.document.show'), route('storage.document.show', $id));
});

Breadcrumbs::register('storage.document.edit', function ($breadcrumbs, $id) {
    $document = Document::find($id);
    $breadcrumbs->parent('file.register.show', $document->file->file_id);
    $breadcrumbs->push(trans('labels.backend.file_register.document.edit'), route('storage.document.edit', $id));
});
