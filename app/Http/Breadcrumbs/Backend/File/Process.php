<?php

Breadcrumbs::register('file.process.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.file_process.title'), route('file.process.index'));
});

Breadcrumbs::register('file.process.barcodes', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.file_process.barcodes.title'), route('file.process.barcodes'));
});

Breadcrumbs::register('file.process.log', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('file.register.show', $id);
    $breadcrumbs->push(trans('labels.backend.file.register_log'), route('file.process.log', $id));
});

Breadcrumbs::register('file.process.dispatch', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('file.register.show', $id);
    $breadcrumbs->push(trans('labels.backend.file.dispatch.file'), route('file.process.dispatch', $id));
});

Breadcrumbs::register('file.process.list', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.file_process.file_list'), route('file.process.list'));
});

Breadcrumbs::register('file.process.search', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.file_process.search_file'), route('file.process.search'));
});

Breadcrumbs::register('storage.document.transfer', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.file_process.transfer_document'), route('storage.document.transfer'));
});