<?php

Breadcrumbs::register('report.backend.basic', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.report.basic.title'), route('report.backend.basic'));
});

Breadcrumbs::register('report.backend.basic_notice', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.report.basic.notice'), route('report.backend.basic_notice'));
});

Breadcrumbs::register('report.backend.user', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.report.user.title'), route('report.backend.user'));
});

Breadcrumbs::register('report.backend.closed', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.report.closed.title'), route('report.backend.closed'));
});

Breadcrumbs::register('report.backend.tracking', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.report.tracking.title'), route('report.backend.tracking'));
});