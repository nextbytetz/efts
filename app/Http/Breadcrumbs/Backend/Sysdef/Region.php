<?php


Breadcrumbs::register('sysdef.region.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.region.management'), route('sysdef.region.index'));
});

Breadcrumbs::register('sysdef.region.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sysdef.region.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.region.create'), route('sysdef.region.create'));
});

Breadcrumbs::register('sysdef.region.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sysdef.region.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.region.edit'), route('sysdef.region.edit', $id));
});