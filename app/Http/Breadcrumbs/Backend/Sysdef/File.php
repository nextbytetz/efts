<?php


Breadcrumbs::register('sysdef.file.subject.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.sysdef.file.subject.management'), route('sysdef.file.subject.index'));
});

Breadcrumbs::register('sysdef.file.subject.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sysdef.file.subject.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.file.subject.create'), route('sysdef.file.subject.create'));
});

Breadcrumbs::register('sysdef.file.subject.edit', function ($breadcrumbs, $subject) {
    $breadcrumbs->parent('sysdef.file.subject.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.file.subject.edit'), route('sysdef.file.subject.edit', $subject));
});

Breadcrumbs::register('sysdef.file.case.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.sysdef.file.case.management'), route('sysdef.file.case.index'));
});

Breadcrumbs::register('sysdef.file.case.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sysdef.file.case.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.file.case.create'), route('sysdef.file.case.create'));
});

Breadcrumbs::register('sysdef.file.case.edit', function ($breadcrumbs, $case) {
    $breadcrumbs->parent('sysdef.file.case.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.file.case.edit'), route('sysdef.file.case.edit', $case));
});