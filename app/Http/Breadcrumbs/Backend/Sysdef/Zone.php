<?php


Breadcrumbs::register('sysdef.rfid.zone.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.sysdef.rfid.zone.management'), route('sysdef.rfid.zone.index'));
});

Breadcrumbs::register('sysdef.rfid.zone.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sysdef.rfid.zone.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.rfid.zone.create'), route('sysdef.rfid.zone.create'));
});

Breadcrumbs::register('sysdef.rfid.zone.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sysdef.rfid.zone.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.rfid.zone.edit'), route('sysdef.rfid.zone.edit', $id));
});