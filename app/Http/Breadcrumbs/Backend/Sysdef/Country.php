<?php

Breadcrumbs::register('sysdef.country.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.country.management'), route('sysdef.country.index'));
});

Breadcrumbs::register('sysdef.country.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sysdef.country.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.country.create'), route('sysdef.country.create'));
});

Breadcrumbs::register('sysdef.country.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sysdef.country.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.country.edit'), route('sysdef.country.edit', $id));
});