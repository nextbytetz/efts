<?php


Breadcrumbs::register('sysdef.branch.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.branch.management'), route('sysdef.branch.index'));
});

Breadcrumbs::register('sysdef.branch.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sysdef.branch.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.branch.create'), route('sysdef.branch.create'));
});

Breadcrumbs::register('sysdef.branch.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sysdef.branch.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.branch.edit'), route('sysdef.branch.edit', $id));
});