<?php


Breadcrumbs::register('sysdef.department.index', function ($breadcrumbs) {
    $breadcrumbs->parent('backend.dashboard');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.department.management'), route('sysdef.department.index'));
});

Breadcrumbs::register('sysdef.department.create', function ($breadcrumbs) {
    $breadcrumbs->parent('sysdef.department.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.department.create'), route('sysdef.department.create'));
});

Breadcrumbs::register('sysdef.department.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('sysdef.department.index');
    $breadcrumbs->push(trans('labels.backend.sysdef.location.department.edit'), route('sysdef.department.edit', $id));
});