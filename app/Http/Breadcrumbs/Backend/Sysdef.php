<?php

require __DIR__ . '/Sysdef/Country.php';
require __DIR__ . '/Sysdef/Region.php';
require __DIR__ . '/Sysdef/Branch.php';
require __DIR__ . '/Sysdef/Department.php';
require __DIR__ . '/Sysdef/Zone.php';
require __DIR__ . '/Sysdef/File.php';