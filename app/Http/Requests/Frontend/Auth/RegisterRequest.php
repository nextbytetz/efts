<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'Required|Min:3|Max:15|Alpha',
            'othernames' => 'Required|Min:3|Max:15|Alpha',
            'username' => 'Required|Min:3|Max:15|AlphaNum|unique:officer',
            'country_id' => 'Required',
	    'region' => 'Required',
            'branch_id' => 'Required',
            'department_id' => 'Required',
            'gender' => 'Required',
	    'password' => 'Required|Min:6|Confirmed',
            'password_confirmation' => 'Required|Min:6',
            'agree' => 'Required',
	    'email' => 'Email',
	    'officeid' => 'unique:officer',
        ];
    }
}
