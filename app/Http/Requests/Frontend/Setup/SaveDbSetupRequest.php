<?php

namespace App\Http\Requests\Frontend\Setup;

use App\Http\Requests\Request;

/**
 * Class ResetPasswordRequest
 * @package App\Http\Requests\Frontend\Access
 */
class SaveDbSetupRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	if (request()->context == 'dbnew') {
	    return [
		'DB_HOST' => 'required',
		'DB_PORT' => 'required',
		'DB_DATABASE' => 'required',
		'DB_USERNAME' => 'required',
		'DB_PASSWORD' => 'required',
		//'CURRENT_DB_USERNAME' => 'required',
		//'CURRENT_DB_PASSWORD' => 'required',
		'DB_SCHEMA' =>  'required',
		'firstname' => 'Required|Min:3|Max:15|Alpha',
		'othernames' => 'Required|Min:3|Max:15|Alpha',
		'username' => 'Required|Min:3|Max:15|AlphaNum',
		'gender' => 'Required',
		'password' => 'Required|Min:6|Confirmed',
		'password_confirmation' => 'Required|Min:6',
		'email' => 'required',
		'officeid' => 'required',
	    ];
	} else {
	    return [
		'firstname' => 'Required|Min:3|Max:15|Alpha',
		'othernames' => 'Required|Min:3|Max:15|Alpha',
		'username' => 'Required|Min:3|Max:15|AlphaNum',
		'gender' => 'Required',
		'password' => 'Required|Min:6|Confirmed',
		'password_confirmation' => 'Required|Min:6',
		'password_super_user' => 'required',
		'email' => 'required',
		'officeid' => 'required',
	    ];
	}
    }

}
