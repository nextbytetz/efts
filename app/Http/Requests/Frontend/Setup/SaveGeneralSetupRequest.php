<?php

namespace App\Http\Requests\Frontend\Setup;

use App\Http\Requests\Request;

/**
 * Class ResetPasswordRequest
 * @package App\Http\Requests\Frontend\Access
 */
class SaveGeneralSetupRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	return [
	    'DB_HOST' => 'required',
	    'DB_PORT' => 'required',
	    'DB_DATABASE' => 'required',
	    'DB_USERNAME' => 'required',
	    'DB_PASSWORD' => 'required',
	    'DB_SCHEMA' =>  'required',
	];
    }

}
