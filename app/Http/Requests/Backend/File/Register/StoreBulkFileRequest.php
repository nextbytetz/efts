<?php

namespace App\Http\Requests\Backend\File\Register;

use App\Http\Requests\Request;

/**
 * Class UpdateRoleRequest
 * @package App\Http\Requests\Backend\Access\Role
 */
class StoreBulkFileRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return access()->allow('register_bulk');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	return [
	    'name' => 'required',
	    'priority_id' => 'required',
	    'files' => 'required',
	    'file_type_id' => 'required',
	    'file_subject_id' => 'required',
	];
    }

}
