<?php

namespace App\Http\Requests\Backend\File\Register;

use App\Http\Requests\Request;

/**
 * Class UpdateRoleRequest
 * @package App\Http\Requests\Backend\Access\Role
 */
class UpdateFileRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return access()->allow('create_edit_file');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	$basic = [
	    'name' => 'required',
	    //'subject' => 'Alpha',
	    //'description' => 'AlphaNum',
	    //'epc' => 'Integer',
	    //'shelf_number' => 'AlphaNum',
	    'status' => 'Required',
	    'file_type_id' => 'Required',
	    'file_subject_id' => 'required'
	];
	if (request()->file_nature_id == 1) {
	    $opt = [
		'file_category_id' => 'Required',
	    ];
	    $basic = array_merge($basic, $opt);
	}
	if (request()->file_type_id == 1 Or request()->file_type_id == 2) {
	    $opt = [
		'reference' => 'Required',
	    ];
	    $basic = array_merge($basic, $opt);
	}
	return $basic;
    }

}
