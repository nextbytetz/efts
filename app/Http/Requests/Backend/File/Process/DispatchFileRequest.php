<?php

namespace App\Http\Requests\Backend\File\Process;

use App\Http\Requests\Request;

/**
 * Class DispatchFileRequest
 * @package App\Http\Requests\Backend\Access\Role
 */
class DispatchFileRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'priority_id' => 'Required',
            'officers' => 'Required',
            'comment' => 'Required',
        ];
    }

}
