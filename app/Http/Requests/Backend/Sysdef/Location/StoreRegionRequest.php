<?php


namespace App\Http\Requests\Backend\Sysdef\Location;

use App\Http\Requests\Request;


class StoreRegionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	return [
	    'country_id' => 'required',
	    'name' => 'required',
	];
    }

}