<?php


namespace App\Http\Requests\Backend\Sysdef\Zone;

use App\Http\Requests\Request;


class StoreZoneRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	return [
	    'country_id' => 'required',
	    'region_id' => 'required',
	    'branch_id' => 'required',
	    'departments' => 'required',
	    'name' => 'required',
	    'reader' => 'required',
	    'antenna' => 'required|Numeric|Between:1,2'
	];
    }

}