<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class UpdateUserPasswordRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class UpdateProfilePasswordRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	return [
	    'old_password' => 'required|min:6',
	    'password' => 'required|min:6|confirmed',
	    'password_confirmation' => 'required|min:6',
	];
    }

}
