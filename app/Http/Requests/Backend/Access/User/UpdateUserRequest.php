<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;

/**
 * Class UpdateUserPasswordRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class UpdateUserRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	return [
	    'firstname' => 'required|min:2',
	    'othernames' => 'required|min:2',
	    'email' => 'Email',
	    'officeid' => 'required',
	    'country_id' => 'required',
	    'region_id' => 'required',
	    'branch_id' => 'required',
	    'department_id' => 'required',
	];
    }

}
