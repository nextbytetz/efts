<?php

namespace App\Http\Requests\Backend\Access\User;

use App\Http\Requests\Request;
use App\Models\Location\CountryAll;

/**
 * Class UpdateUserPasswordRequest
 * @package App\Http\Requests\Backend\Access\User
 */
class StoreProfileRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

	$basic = [
	    'firstname' => 'required|min:2',
	    'othernames' => 'required|min:2',
	    'email' => 'required|Email',
	    'username' => 'required|min:2',
	    'country_id' => 'required',
	    'gender' => 'required',
	    'phone' => 'required',
	];
	if (!is_null(request()->input('country_id'))) {
	    $country_all = CountryAll::find(request()->input('country_id'));
	    $opt = [
		'phone' => 'required|phone:' . $country_all->code,
	    ];
	    $basic = array_merge($basic, $opt);
	}
	return $basic;
    }

}
