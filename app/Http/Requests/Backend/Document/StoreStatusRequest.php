<?php

namespace App\Http\Requests\Backend\Document;

use App\Http\Requests\Request;

/**
 * Class UpdateRoleRequest
 * @package App\Http\Requests\Backend\Access\Role
 */
class StoreStatusRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
	return access()->allow('create_edit_file');
    }

    protected function getRedirectUrl() {
	$url = $this->redirector->getUrlGenerator();
	return $url->previous() . request()->anchor;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
	$basic = [
	    'comment' => 'required',
	    'priority_id' => 'Required',
	    'status' => 'Required',
	    'notify' => 'Required',
	];
	if (request()->notify == 2) {
	    $opt = [
		'officers' => 'Required',
	    ];
	    $basic = array_merge($basic, $opt);
	}
	return $basic;
    }

}
