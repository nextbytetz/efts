<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class RouteNeedsRole
 * @package App\Http\Middleware
 */
class RouteNeedsRole
{
    /**
     * @param  $request
     * @param  callable   $next
     * @param  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (! access()->hasRole($role)) {
            return redirect()->route('backend.dashboard')->withFlashInfo(trans('auth.general_error'));
        }

        return $next($request);
    }
}