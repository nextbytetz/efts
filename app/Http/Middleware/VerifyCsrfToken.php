<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
	    //
    ];

    public function handle($request, Closure $next) {
	if (
		$this->isReading($request) ||
		$this->runningUnitTests() ||
		$this->shouldPassThrough($request) ||
		$this->tokensMatch($request)
	) {
	    return $this->addCookieToResponse($request, $next($request));
	}

	// redirect the user back to the login page and show error
	return redirect()->back()->withInput()->withFlashInfo(trans('exceptions.frontend.auth.token_mismatch'));
    }

}
