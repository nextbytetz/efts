<?php

namespace App\Jobs\File;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\File\Bulk;
use App\Models\File\File;
use App\Events\Backend\File\RegisterEvent;

class RegisterBulk implements ShouldQueue {

    use InteractsWithQueue,
	Queueable,
	SerializesModels;

    protected $bulk;
    protected $chunkSize = 100;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Bulk $bulk) {
	$this->bulk = $bulk;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

	$bulk = $this->bulk;
	
	$bulk->status = '2';
	$bulk->save();

	Excel::load($this->bulk->link, function($reader) use ($bulk) {
	    $objWorksheet = $reader->getActiveSheet();
	    //exclude the heading
	    $bulk->total_rows = $objWorksheet->getHighestRow() - 1;
	    $bulk->save();
	});
	Excel::filter('chunk')
		->selectSheetsByIndex(0)
		->load($this->bulk->link)
		->chunk($this->chunkSize, function($result) use ($bulk) {
		    $rows = $result->toArray();
		    //let's do more processing (change values in cells) here as needed
		    $counter = 0;
		    foreach ($rows as $row) {
			$file = File::create(['name' => $row['name'], 'reference' => $row['reference'], 'file_subject_id' => $bulk->file_subject_id, 'file_nature_id' => 2, 'officer_opened' => $bulk->officer_id, 'officer_edited' => $bulk->officer_id]);
			//DB::table('data')->insert($rows[$k]);
			//inserting into database
			switch ($bulk->file_type_id) {
			    case 1:
				//Public file,
				$file->attachDepartments($bulk->departments->pluck('department_id')->all());
				break;
			    case 2:
				//Confidential file,
				$file->attachUsers($bulk->users->pluck('officer_id')->all());
				break;
			}
			$counter++;
		    }
		    //$bulk = $bulk->fresh(); //reload from the database
		    $bulk->rows_imported = $bulk->rows_imported + $counter;
		    $bulk->save();
		}
	);
	$bulk->status = '3';
	$bulk->save();
	event(new RegisterEvent($bulk, 'done_bulk_file'));
    }

    public function failed(Exception $e) {
	// Send user notification of failure, etc...
	$this->bulk->status = '4';
	$this->bulk->notice = $e->getMessage();
	$this->bulk->save();
    }

}
