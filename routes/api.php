<?php

/* all routes which need a api access */
Route::group(['namespace' => 'Api'], function () {
    require (__DIR__ . '/Routes/Api/Api.php');
});
