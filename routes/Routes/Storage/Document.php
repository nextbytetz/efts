<?php


/**
 * Route resource
 */
Route::resource('document', 'DocumentController', ['except' => ['show'], 'as' => 'storage']);
/**
 * Specific Routes
 */
Route::group(['prefix' => 'document'], function() {
    Route::get('get', 'DocumentController@get')->name('storage.document.get');
    Route::post('status', 'DocumentController@status');
    Route::get('status', 'DocumentController@getStatus')->name('storage.document.status');
    Route::delete('{id}/destroy_status', 'DocumentController@destroyStatus')->name('storage.document.destroy_status');
    Route::get('{id}/edit_status', 'DocumentController@editStatus')->name('storage.document.edit_status');
    Route::get('transfer', 'DocumentController@transfer')->name('storage.document.transfer');
});
/**
 * Search for a document in the add file register log
 */
Route::get('document/search', 'DocumentController@searchDocument')->name('storage.document.search');

Route::group(['prefix' => 'document/{document}'], function() {
    Route::get('show', 'DocumentController@show')->name('storage.document.show');
    Route::get('download', 'DocumentController@download')->name('storage.document.download');
    Route::get('file', 'DocumentController@file')->name('storage.document.file');
});
