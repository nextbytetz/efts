<?php

/**
 * Route resource
 */
Route::resource('file', 'FileController', ['except' => ['show', 'destroy'], 'as' => 'storage']);
/**
 * Specific Routes
 */
Route::group(['prefix' => 'file'], function() {
    Route::get('move', 'FileController@move')->name('storage.file.move');
    Route::get('get', 'FileController@get')->name('storage.file.get');
    Route::get('{file}/show', 'FileController@show')->name('storage.file.show');
    Route::post('{file}/destroy', 'FileController@destroy')->name('storage.file.destroy');
});
