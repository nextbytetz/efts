<?php

Route::group([
    'middleware' => 'auth',
    'prefix' => 'backend',
	], function() {
    Route::get('basic', 'ReportController@basic')->name('report.backend.basic');
    Route::get('closed', 'ReportController@closed')->name('report.backend.closed');
    Route::get('tracking', 'ReportController@tracking')->name('report.backend.tracking');
    Route::get('user', 'ReportController@user')->name('report.backend.user');
    Route::get('basic_notice', 'ReportController@basicNotice')->name('report.backend.basic_notice');
});
