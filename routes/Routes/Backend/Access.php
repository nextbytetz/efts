<?php

Route::group([
    'prefix' => 'access',
    'namespace' => 'Access',
	], function() {

    /**
     * User profile
     */
    Route::get('user/profile', 'User\UserController@profile')->name('access.user.profile');
    Route::post('user/profile', 'User\UserController@profileStore');
    /**
     * User Management
     */
    Route::group([
	'middleware' => 'access.routeNeedsPermission:view_access',
	    ], function() {
	Route::group(['namespace' => 'User'], function() {


	    Route::resource('user', 'UserController', ['except' => ['show'], 'as' => 'access']);

	    /**
	     * For DataTables
	     */
	    Route::get('user/get', 'UserController@get')->name('access.user.get');

	    /**
	     * User Status'
	     */
	    Route::get('user/deactivated', 'UserController@deactivated')->name('access.user.deactivated');
	    Route::get('user/super_user_history', 'UserController@super_user_history')->name('access.user.super_user_history');

	    /**
	     * User Password
	     */
	    Route::get('user/password', 'UserController@password')->name('access.user.password');
	    Route::post('user/password', 'UserController@passwordStore');

	    /**
	     * Misc
	     */
	    Route::get('account/confirm/resend/{user}', 'UserController@resendConfirmationEmail')->name('account.confirm.resend');

	    /**
	     * Specific User
	     */
	    Route::group(['prefix' => 'user/{user}'], function() {
		Route::get('out/{available}', 'UserController@out')->name('access.user.out')->where(['available' => '[0,1]']);
		Route::get('activate/{active}', 'UserController@activate')->name('access.user.activate')->where(['active' => '[0,1]']);
		Route::get('approve', 'UserController@approve')->name('access.user.approve');
		Route::get('password/change', 'UserController@changePassword')->name('access.user.change_password');
		Route::post('password/change', 'UserController@updatePassword')->name('access.user.change_password');
	    });

	    /**
	     * Group option for users
	     */
	    Route::group(['prefix' => 'user/group'], function() {
		Route::post('approve', 'UserController@approveGroup')->name('access.user.group.approve');
		Route::delete('delete', 'UserController@deleteGroup')->name('access.user.group.delete');
	    });
	});
    });

    /**
     * Role Management
     */
    Route::group([
	'middleware' => 'access.routeNeedsPermission:view_access',
	    ], function() {
	Route::group(['namespace' => 'Role'], function () {
	    Route::resource('role', 'RoleController', ['except' => ['show'], 'as' => 'access']);

	    //For DataTables
	    Route::get('role/get', 'RoleController@get')->name('access.role.get');
	});
    });
});
