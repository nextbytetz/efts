<?php

/**
 * File module
 */
Route::group([
    'prefix' => 'file',
    'namespace' => 'File',
	], function() {
    /**
     * File registration modules
     */
    Route::group(['namespace' => 'Register'], function() {
	/**
	 * Route resource
	 */
	Route::resource('register', 'RegisterController', ['except' => ['show'], 'as' => 'file']);

	/**
	 * Specific File for specific routes
	 */
	Route::group(['prefix' => 'register/{file}'], function() {
	    Route::get('show', 'RegisterController@show')->name('file.register.show');
	    Route::get('history', 'RegisterController@history')->name('file.register.history');
	    Route::get('search', 'RegisterController@searchDirect')->name('file.register.direct.search');
	});
	/**
	 * Specific Routes
	 */
	Route::group(['prefix' => 'register'], function() {
	    Route::get('bulk', 'RegisterController@bulk')->name('file.register.bulk');
	    Route::delete('{bulk}/bulk', 'RegisterController@destroyBulk')->name('file.register.delete.bulk');
	    Route::post('bulk/store', 'RegisterController@storeBulk')->name('file.register.store.bulk');
	    Route::get('all', 'RegisterController@all')->name('file.register.all');
	    Route::get('search', 'RegisterController@search')->name('file.register.search');
	    Route::get('officers', 'RegisterController@getRegisterOfficers')->name('file.register.officers');
	    /**
	     * For searching files
	     */
	    Route::get('searchall', 'RegisterController@searchAll')->name('file.register.searchall');
	    /**
	     * For DataTables
	     */
	    Route::get('get', 'RegisterController@get')->name('file.register.get');
	    Route::get('get_history', 'RegisterController@getHistory')->name('file.register.get_history');
	    Route::get('get_search', 'RegisterController@getSearch')->name('file.register.get_search');
	    Route::get('get_bulk', 'RegisterController@getBulk')->name('file.register.get_bulk');
	});
    });

    /**
     * File process module
     */
    Route::group(['namespace' => 'Process', 'prefix' => 'process'], function() {
	Route::get('index', 'ProcessController@index')->name('file.process.index');
	/**
	 * Specific File
	 */
	Route::group(['prefix' => '{file}'], function() {
	    Route::get('log', 'ProcessController@log')->name('file.process.log');
	    Route::post('log', 'ProcessController@logPost');
	    Route::get('dispatch', 'ProcessController@forward')->name('file.process.dispatch');
	    Route::get('dispatch/officers', 'ProcessController@getForwardOfficers')->name('file.process.dispatch.officers');
	    Route::post('dispatch', 'ProcessController@forwardPost');
	    Route::post('pick', 'ProcessController@pick')->name('file.process.pick');
	    Route::get('receive', 'ProcessController@receive')->name('file.process.receive');
	    Route::post('pick/forced', 'ProcessController@pickForced')->name('file.process.pick.forced');
	    Route::post('pick/warned', 'ProcessController@pickWarned')->name('file.process.pick.warned');
	    Route::get('release', 'ProcessController@release')->name('file.process.release');
	    Route::get('pick/users', 'ProcessController@pickUsers')->name('file.process.pick.users');
	    Route::get('request', 'ProcessController@request')->name('file.process.request');
	    Route::get('request/cancel', 'ProcessController@requestCancel')->name('file.process.request.cancel');
	    Route::get('request/users', 'ProcessController@requestUsers')->name('file.process.request.users');
	    Route::get('workflow/users', 'ProcessController@workflowUsers')->name('file.process.workflow.users');
	    Route::get('cases', 'ProcessController@cases')->name('file.process.cases');
	    Route::post('cases/edit', 'ProcessController@editCase')->name('file.process.cases.edit');
	});
	/**
	 * For Datatables
	 */
	Route::get('get_log', 'ProcessController@getLog')->name('file.process.get_log');
	Route::get('get_list', 'ProcessController@getMyFileList')->name('file.process.get_list');
	/**
	 * For Searches
	 */
	Route::get('search', 'ProcessController@search')->name('file.process.search');
	/**
	 * For listing user files (File)
	 */
	Route::get('list', 'ProcessController@myFileList')->name('file.process.list');
	Route::get('search', 'ProcessController@search')->name('file.process.search');
	/**
	 * For printing barcodes
	 */
	Route::any('barcodes', 'ProcessController@barcodes')->name('file.process.barcodes');
    });
});
