<?php

Route::get('dashboard', 'DashboardController@index')->name('backend.dashboard');
Route::get('history/{history_type}', 'DashboardController@getHistory')->name('backend.history');
Route::get('notification', 'DashboardController@getNotification')->name('backend.notification');