<?php

/**
 * Data definition registration
 */
Route::group([
    'prefix' => 'sysdef',
    'namespace' => 'Sysdef',
	], function() {

    /**
     * RFID Zones Registration
     */
    Route::group(['prefix' => 'rfid'], function() {
	/* Resource methods have mainly used for managing zones */
	Route::resource('zone', 'RFIDController', ['except' => ['show'], 'as' => 'sysdef.rfid']);
	/**
	 * RFID Specific routes
	 */
	/* Resource methods have mainly used for managing zones, this is used to get all the registered zones for the datatable */
	Route::group(['prefix' => 'zone'], function() {
	    Route::get('get', 'RFIDController@get')->name('sysdef.rfid.zone.get');
	    Route::get('departments', 'RFIDController@getDepartments')->name('sysdef.rfid.zone.departments');
	});
    });
    /**
     * File Sysdef registration
     */
    /* Resource File */
    Route::group(['prefix' => 'file', 'namespace' => 'File'], function() {
	Route::resource('subject', 'SubjectController', ['except' => ['show'], 'as' => 'sysdef.file']);
	Route::resource('case', 'CaseController', ['except' => ['show'], 'as' => 'sysdef.file']);
	/* individual Subjects Routes */
	Route::group(['prefix' => 'subject'], function() {
	    Route::get('get', 'SubjectController@get')->name('sysdef.file.subject.get');
	});
	/* individual Case Routes */
	Route::group(['prefix' => 'case'], function() {
	    Route::get('get', 'CaseController@get')->name('sysdef.file.case.get');
	});
    });


    /**
     * Location registration
     */
    Route::group([
	'middleware' => 'access.routeNeedsPermission:data_registration',
	    ], function() {
	Route::group(['prefix' => 'country'], function() {
	    Route::get('index', 'LocationController@showCountry')
		    ->name('sysdef.country.index');
	    Route::get('get', 'LocationController@getCountry')->name('sysdef.country.get');
	    Route::get('create', 'LocationController@createCountry')
		    ->name('sysdef.country.create');
	    Route::post('store', 'LocationController@storeCountry');
	    Route::delete('{country_id}', 'LocationController@destroyCountry');
	});
	Route::group(['prefix' => 'region'], function() {
	    Route::get('index', 'LocationController@showRegion')
		    ->name('sysdef.region.index');
	    Route::get('get', 'LocationController@getRegion')->name('sysdef.region.get');
	    Route::get('create', 'LocationController@createRegion')
		    ->name('sysdef.region.create');
	    Route::post('store', 'LocationController@storeRegion');
	    Route::get('{region_id}/edit', 'LocationController@editRegion')
		    ->name('sysdef.region.edit');
	    Route::post('update/{region_id}', 'LocationController@updateRegion');
	    Route::delete('{region_id}', 'LocationController@destroyRegion');
	});
	Route::group(['prefix' => 'branch'], function() {
	    Route::get('index', 'LocationController@showBranch')
		    ->name('sysdef.branch.index');
	    Route::get('get', 'LocationController@getBranch')->name('sysdef.branch.get');
	    Route::get('create', 'LocationController@createBranch')
		    ->name('sysdef.branch.create');
	    Route::post('store', 'LocationController@storeBranch');
	    Route::get('{branch_id}/edit', 'LocationController@editBranch')
		    ->name('sysdef.branch.edit');
	    Route::post('update/{branch_id}', 'LocationController@updateBranch');
	    Route::delete('{branch_id}', 'LocationController@destroyBranch');
	});
	Route::group(['prefix' => 'department'], function() {
	    Route::get('index', 'LocationController@showDepartment')
		    ->name('sysdef.department.index');
	    Route::get('get', 'LocationController@getDepartment')->name('sysdef.department.get');
	    Route::get('create', 'LocationController@createDepartment')
		    ->name('sysdef.department.create');
	    Route::post('store', 'LocationController@storeDepartment');
	    Route::get('{department_id}/edit', 'LocationController@editDepartment')
		    ->name('sysdef.department.edit');
	    Route::post('update/{department_id}', 'LocationController@updateDepartment');
	    Route::delete('{department_id}', 'LocationController@destroyDepartment');
	});
    });
});
