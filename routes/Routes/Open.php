<?php

use App\Models\Location\Department;
use App\Models\Location\Region;
use App\Models\Location\Branch;

Route::get('getregion', function() {
    return view('includes.partials.regions')
		    ->withRegion(Region::where(request()->all())->get());
});

Route::get('getbranch', function() {
    return view('includes.partials.branches')
		    ->withBranch(Branch::where(request()->all())->get());
});

Route::get('getdepartment', function() {
    return view('includes.partials.departments')
		    ->withDepartment(Department::where(request()->all())->get());
});

Route::get('getfolders', function() {
    return view('includes.partials.folders_request')
		    ->withName(request()->name)
		    ->withFolderId(request()->folder_id);
});
