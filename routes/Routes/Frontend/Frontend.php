<?php

/**
 * Frontend Controllers
 */
Route::get('terms', 'FrontendController@terms')->name('frontend.terms');
Route::get('help', 'FrontendController@help')->name('frontend.help');
Route::get('setup/{context?}', 'FrontendController@showSetupForm')
	->name('frontend.setup');
Route::post('setup/database', 'FrontendController@saveDBSetup');
Route::post('setup/general', 'FrontendController@saveGeneralSetup');
Route::get('/debug', function () {
    return view('debug');
})->name("debug");

