<?php

/**
 * Frontend Access Controllers
 */
Route::group(['namespace' => 'Auth'], function () {

    /**
     * These routes require the user to be logged in
     */
    Route::group(['middleware' => 'auth'], function () {
	Route::get('logout', 'AuthController@logout')->name('auth.logout');
    });

    /**
     * These routes require the user NOT be logged in
     */
    Route::group(['middleware' => 'guest'], function () {
	// Authentication Routes
	Route::get('/', 'AuthController@showLoginForm')
		->name('auth.login');
	Route::post('login', 'AuthController@login');

	// Registration Routes
	Route::get('register', 'AuthController@showRegistrationForm')
		->name('auth.register');
	Route::post('register', 'AuthController@register');

	// Password Reset Routes
	Route::get('password/reset/{token?}', 'AuthController@showResetForm')
		->name('auth.password.reset');
	Route::post('password/email', 'AuthController@sendResetLinkEmail');
	Route::post('password/reset', 'AuthController@reset');
    });
});
