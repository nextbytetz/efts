<?php

/*
 * ----------------------------------------------------------------------
 * Starting web middleware to handle web requests
 * -----------------------------------------------------------------------
 */

/**
 * Frontend Routes
 * Namespaces indicate folder structure
 */
/**
 * Switch between the included languages
 */
Route::group(['namespace' => 'Language'], function () {
    require (__DIR__ . '/Routes/Language/Language.php');
});

/**
 * Frontend Routes
 * Namespaces indicate folder structure
 */
Route::group(['namespace' => 'Frontend'], function () {
    /* all the frontend routes without using Auth Controller */
    require (__DIR__ . '/Routes/Frontend/Frontend.php');
    /* all the frontend routes which use Auth Controller like login, register etc */
    require (__DIR__ . '/Routes/Frontend/Access.php');
});

/**
 * Backend Routes
 * Namespaces indicate folder structure
 * Admin middleware groups web, auth, and routeNeedsPermission
 */
Route::group(['namespace' => 'Backend', 'prefix' => '', 'middleware' => 'auth'], function () {
    /**
     * These routes need view-backend permission
     * (good if you want to allow more than one group in the backend,
     * then limit the backend features by different roles or permissions)
     *
     * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.
     */
    /* dashboard routes */
    require (__DIR__ . '/Routes/Backend/Dashboard.php');
    /* for list all the routes for access management */
    require (__DIR__ . '/Routes/Backend/Access.php');
    /* list all the routes for system definition/data registration */
    require (__DIR__ . '/Routes/Backend/Sysdef.php');
    /* list all the routes for the file module */
    require (__DIR__ . '/Routes/Backend/File.php');
    /* for list all the routes for the actions for viewing log history */
    require (__DIR__ . '/Routes/Backend/LogViewer.php');
});
Route::group(['namespace' => 'Backend\File\Register\Storage', 'prefix' => 'storage'], function () {
    /* all routes which create,delete,edit,update the files on the disk */
    require (__DIR__ . '/Routes/Storage/File.php');
    require (__DIR__ . '/Routes/Storage/Document.php');
});
Route::group(['namespace' => 'Backend\File\Report', 'prefix' => 'report'], function () {
    /* list all the routes for the report module */
    require (__DIR__ . '/Routes/Backend/Report.php');
});
/* all routes which need a free access and do not need to go through any middleware */
require (__DIR__ . '/Routes/Open.php');
