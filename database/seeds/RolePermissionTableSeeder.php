<?php

use Illuminate\Database\Seeder;

class RolePermissionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_permission')->delete();
        
        \DB::table('role_permission')->insert(array (
            0 => 
            array (
                'role_permission_id' => 1,
                'role_id' => 1,
                'permission_id' => 2,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'role_permission_id' => 2,
                'role_id' => 1,
                'permission_id' => 3,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'role_permission_id' => 3,
                'role_id' => 1,
                'permission_id' => 4,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'role_permission_id' => 4,
                'role_id' => 1,
                'permission_id' => 5,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'role_permission_id' => 5,
                'role_id' => 1,
                'permission_id' => 6,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'role_permission_id' => 6,
                'role_id' => 1,
                'permission_id' => 7,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'role_permission_id' => 7,
                'role_id' => 1,
                'permission_id' => 8,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'role_permission_id' => 8,
                'role_id' => 1,
                'permission_id' => 11,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'role_permission_id' => 9,
                'role_id' => 1,
                'permission_id' => 10,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'role_permission_id' => 10,
                'role_id' => 1,
                'permission_id' => 12,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'role_permission_id' => 11,
                'role_id' => 1,
                'permission_id' => 13,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'role_permission_id' => 12,
                'role_id' => 1,
                'permission_id' => 14,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'role_permission_id' => 13,
                'role_id' => 1,
                'permission_id' => 15,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'role_permission_id' => 14,
                'role_id' => 1,
                'permission_id' => 16,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'role_permission_id' => 15,
                'role_id' => 1,
                'permission_id' => 17,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'role_permission_id' => 16,
                'role_id' => 1,
                'permission_id' => 18,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'role_permission_id' => 17,
                'role_id' => 1,
                'permission_id' => 19,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'role_permission_id' => 18,
                'role_id' => 1,
                'permission_id' => 20,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'role_permission_id' => 19,
                'role_id' => 1,
                'permission_id' => 21,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'role_permission_id' => 20,
                'role_id' => 1,
                'permission_id' => 22,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'role_permission_id' => 21,
                'role_id' => 1,
                'permission_id' => 23,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'role_permission_id' => 22,
                'role_id' => 1,
                'permission_id' => 24,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'role_permission_id' => 23,
                'role_id' => 1,
                'permission_id' => 25,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'role_permission_id' => 24,
                'role_id' => 1,
                'permission_id' => 26,
                'status' => '1',
                'created_at' => '2016-07-03 15:05:42',
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'role_permission_id' => 25,
                'role_id' => 2,
                'permission_id' => 2,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'role_permission_id' => 26,
                'role_id' => 2,
                'permission_id' => 3,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'role_permission_id' => 27,
                'role_id' => 2,
                'permission_id' => 4,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'role_permission_id' => 28,
                'role_id' => 2,
                'permission_id' => 5,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'role_permission_id' => 29,
                'role_id' => 2,
                'permission_id' => 6,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'role_permission_id' => 30,
                'role_id' => 2,
                'permission_id' => 7,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'role_permission_id' => 31,
                'role_id' => 2,
                'permission_id' => 8,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'role_permission_id' => 32,
                'role_id' => 2,
                'permission_id' => 9,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'role_permission_id' => 33,
                'role_id' => 2,
                'permission_id' => 13,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'role_permission_id' => 34,
                'role_id' => 2,
                'permission_id' => 14,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'role_permission_id' => 35,
                'role_id' => 2,
                'permission_id' => 15,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'role_permission_id' => 36,
                'role_id' => 2,
                'permission_id' => 16,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'role_permission_id' => 37,
                'role_id' => 2,
                'permission_id' => 17,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'role_permission_id' => 38,
                'role_id' => 2,
                'permission_id' => 18,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'role_permission_id' => 39,
                'role_id' => 2,
                'permission_id' => 19,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'role_permission_id' => 40,
                'role_id' => 2,
                'permission_id' => 20,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'role_permission_id' => 41,
                'role_id' => 2,
                'permission_id' => 21,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'role_permission_id' => 42,
                'role_id' => 2,
                'permission_id' => 22,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'role_permission_id' => 43,
                'role_id' => 2,
                'permission_id' => 23,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'role_permission_id' => 44,
                'role_id' => 2,
                'permission_id' => 24,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'role_permission_id' => 45,
                'role_id' => 2,
                'permission_id' => 25,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'role_permission_id' => 46,
                'role_id' => 2,
                'permission_id' => 26,
                'status' => '1',
                'created_at' => '2016-07-03 15:18:15',
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'role_permission_id' => 47,
                'role_id' => 3,
                'permission_id' => 22,
                'status' => '1',
                'created_at' => '2016-07-03 15:19:58',
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'role_permission_id' => 48,
                'role_id' => 3,
                'permission_id' => 23,
                'status' => '1',
                'created_at' => '2016-07-03 15:19:58',
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'role_permission_id' => 49,
                'role_id' => 3,
                'permission_id' => 25,
                'status' => '1',
                'created_at' => '2016-07-03 15:19:58',
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'role_permission_id' => 50,
                'role_id' => 3,
                'permission_id' => 26,
                'status' => '1',
                'created_at' => '2016-07-03 15:19:58',
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'role_permission_id' => 51,
                'role_id' => 4,
                'permission_id' => 16,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'role_permission_id' => 52,
                'role_id' => 4,
                'permission_id' => 17,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'role_permission_id' => 53,
                'role_id' => 4,
                'permission_id' => 18,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'role_permission_id' => 54,
                'role_id' => 4,
                'permission_id' => 19,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'role_permission_id' => 55,
                'role_id' => 4,
                'permission_id' => 20,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'role_permission_id' => 56,
                'role_id' => 4,
                'permission_id' => 21,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'role_permission_id' => 57,
                'role_id' => 4,
                'permission_id' => 22,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'role_permission_id' => 58,
                'role_id' => 4,
                'permission_id' => 23,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'role_permission_id' => 59,
                'role_id' => 4,
                'permission_id' => 24,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'role_permission_id' => 60,
                'role_id' => 4,
                'permission_id' => 25,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'role_permission_id' => 61,
                'role_id' => 4,
                'permission_id' => 26,
                'status' => '1',
                'created_at' => '2016-07-03 15:23:15',
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'role_permission_id' => 62,
                'role_id' => 1,
                'permission_id' => 27,
                'status' => '1',
                'created_at' => '2016-07-03 15:33:26',
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'role_permission_id' => 65,
                'role_id' => 2,
                'permission_id' => 27,
                'status' => '1',
                'created_at' => '2016-07-03 15:34:00',
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'role_permission_id' => 68,
                'role_id' => 4,
                'permission_id' => 27,
                'status' => '1',
                'created_at' => '2016-07-03 15:34:34',
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'role_permission_id' => 69,
                'role_id' => 1,
                'permission_id' => 1,
                'status' => '1',
                'created_at' => '2016-07-13 15:09:34',
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'role_permission_id' => 70,
                'role_id' => 2,
                'permission_id' => 1,
                'status' => '1',
                'created_at' => '2016-07-13 15:09:40',
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'role_permission_id' => 71,
                'role_id' => 1,
                'permission_id' => 28,
                'status' => '1',
                'created_at' => '2016-07-18 18:35:33',
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'role_permission_id' => 72,
                'role_id' => 2,
                'permission_id' => 28,
                'status' => '1',
                'created_at' => '2016-07-18 18:35:45',
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'role_permission_id' => 129,
                'role_id' => 8,
                'permission_id' => 7,
                'status' => '1',
                'created_at' => '2016-08-05 18:15:01',
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'role_permission_id' => 130,
                'role_id' => 8,
                'permission_id' => 1,
                'status' => '1',
                'created_at' => '2016-08-05 18:15:01',
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'role_permission_id' => 131,
                'role_id' => 8,
                'permission_id' => 2,
                'status' => '1',
                'created_at' => '2016-08-05 18:15:01',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
