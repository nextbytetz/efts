<?php

use Illuminate\Database\Seeder;

class FileTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('file_type')->delete();
        
        \DB::table('file_type')->insert(array (
            0 => 
            array (
                'file_type_id' => 1,
                'name' => 'Public',
                'created_at' => '2015-10-23 11:49:47',
            ),
            1 => 
            array (
                'file_type_id' => 2,
                'name' => 'Confidential',
                'created_at' => '2015-10-23 11:50:04',
            ),
            2 => 
            array (
                'file_type_id' => 3,
                'name' => 'Private',
                'created_at' => '2015-10-23 11:50:11',
            ),
        ));
        
        
    }
}
