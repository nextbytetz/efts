<?php

use Illuminate\Database\Seeder;

class FileCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('file_category')->delete();
        
        \DB::table('file_category')->insert(array (
            0 => 
            array (
                'file_category_id' => 1,
                'name' => 'Normal',
                'created_at' => '2015-10-23 11:46:22',
            ),
            1 => 
            array (
                'file_category_id' => 2,
                'name' => 'Notice',
                'created_at' => '2015-10-23 11:46:29',
            ),
        ));
        
        
    }
}
