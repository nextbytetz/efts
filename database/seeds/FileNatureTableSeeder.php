<?php

use Illuminate\Database\Seeder;

class FileNatureTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('file_nature')->delete();
        
        \DB::table('file_nature')->insert(array (
            0 => 
            array (
                'file_nature_id' => 1,
                'name' => 'Electronic',
                'created_at' => '2016-06-15 20:21:52',
            ),
            1 => 
            array (
                'file_nature_id' => 2,
                'name' => 'Physical',
                'created_at' => '2016-06-15 20:22:01',
            ),
        ));
        
        
    }
}
