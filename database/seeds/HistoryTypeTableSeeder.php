<?php

use Illuminate\Database\Seeder;

class HistoryTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('history_type')->delete();
        
        \DB::table('history_type')->insert(array (
            0 => 
            array (
                'history_type_id' => 1,
                'name' => 'access_management',
                'created_at' => '2016-08-01 16:08:42',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'history_type_id' => 2,
                'name' => 'data_register',
                'created_at' => '2016-08-01 16:08:51',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'history_type_id' => 3,
                'name' => 'Data Register',
                'created_at' => '2016-08-01 16:09:21',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'history_type_id' => 4,
                'name' => 'file_register',
                'created_at' => '2016-08-01 17:00:45',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'history_type_id' => 5,
                'name' => 'file_process',
                'created_at' => '2016-08-01 17:00:56',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'history_type_id' => 6,
                'name' => 'role_management',
                'created_at' => '2016-08-04 14:38:45',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
