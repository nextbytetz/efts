<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
	// $this->call(UsersTableSeeder::class);
	$this->call('FileNatureTableSeeder');
	$this->call('FileCategoryTableSeeder');
	$this->call('FileTypeTableSeeder');
	$this->call('RoleTableSeeder');
	$this->call('CountryAllTableSeeder');
	$this->call('PermissionGroupTableSeeder');
	$this->call('PermissionTableSeeder');
	$this->call('PermissionDependencyTableSeeder');
	$this->call('RolePermissionTableSeeder');
	$this->call('HistoryTypeTableSeeder');
	$this->call('NotificationTypeTableSeeder');
	$this->call('ActionTableSeeder');
	$this->call('PriorityTableSeeder');
    }

}
