<?php

use Illuminate\Database\Seeder;

class PermissionDependencyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_dependency')->delete();
        
        \DB::table('permission_dependency')->insert(array (
            0 => 
            array (
                'permission_dependency_id' => 1,
                'permission_id' => 2,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:40:03',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'permission_dependency_id' => 2,
                'permission_id' => 3,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:40:19',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'permission_dependency_id' => 3,
                'permission_id' => 4,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:41:35',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'permission_dependency_id' => 4,
                'permission_id' => 5,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:41:53',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'permission_dependency_id' => 5,
                'permission_id' => 5,
                'dependency_id' => 2,
                'created_at' => '2016-07-12 16:43:57',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'permission_dependency_id' => 6,
                'permission_id' => 6,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:44:58',
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'permission_dependency_id' => 7,
                'permission_id' => 7,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:45:19',
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'permission_dependency_id' => 8,
                'permission_id' => 7,
                'dependency_id' => 2,
                'created_at' => '2016-07-12 16:45:40',
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'permission_dependency_id' => 9,
                'permission_id' => 8,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:46:28',
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'permission_dependency_id' => 10,
                'permission_id' => 9,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:46:49',
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'permission_dependency_id' => 11,
                'permission_id' => 10,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:47:06',
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'permission_dependency_id' => 12,
                'permission_id' => 11,
                'dependency_id' => 1,
                'created_at' => '2016-07-12 16:47:21',
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'permission_dependency_id' => 13,
                'permission_id' => 17,
                'dependency_id' => 22,
                'created_at' => '2016-07-12 16:48:39',
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'permission_dependency_id' => 14,
                'permission_id' => 18,
                'dependency_id' => 22,
                'created_at' => '2016-07-12 16:49:12',
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'permission_dependency_id' => 15,
                'permission_id' => 20,
                'dependency_id' => 16,
                'created_at' => '2016-07-12 16:50:00',
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'permission_dependency_id' => 16,
                'permission_id' => 24,
                'dependency_id' => 22,
                'created_at' => '2016-07-12 16:51:11',
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'permission_dependency_id' => 17,
                'permission_id' => 25,
                'dependency_id' => 22,
                'created_at' => '2016-07-12 16:51:33',
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'permission_dependency_id' => 18,
                'permission_id' => 26,
                'dependency_id' => 22,
                'created_at' => '2016-07-12 16:52:13',
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'permission_dependency_id' => 19,
                'permission_id' => 21,
                'dependency_id' => 23,
                'created_at' => '2016-07-17 11:46:59',
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'permission_dependency_id' => 20,
                'permission_id' => 13,
                'dependency_id' => 1,
                'created_at' => '2016-07-17 11:49:58',
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'permission_dependency_id' => 21,
                'permission_id' => 14,
                'dependency_id' => 1,
                'created_at' => '2016-07-17 11:50:44',
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'permission_dependency_id' => 22,
                'permission_id' => 15,
                'dependency_id' => 1,
                'created_at' => '2016-07-17 11:51:10',
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'permission_dependency_id' => 23,
                'permission_id' => 19,
                'dependency_id' => 23,
                'created_at' => '2016-07-17 11:54:44',
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'permission_dependency_id' => 24,
                'permission_id' => 28,
                'dependency_id' => 1,
                'created_at' => '2016-07-18 18:34:45',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
