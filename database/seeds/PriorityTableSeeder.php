<?php

use Illuminate\Database\Seeder;

class PriorityTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('priority')->delete();
        
        \DB::table('priority')->insert(array (
            0 => 
            array (
                'priority_id' => 1,
                'name' => 'High',
                'created_at' => '2016-08-23 21:38:03',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'priority_id' => 2,
                'name' => 'Medium',
                'created_at' => '2016-08-23 21:38:14',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'priority_id' => 3,
                'name' => 'Low',
                'created_at' => '2016-08-23 21:38:22',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'priority_id' => 4,
                'name' => 'Obsolete',
                'created_at' => '2016-08-23 21:38:34',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'priority_id' => 5,
                'name' => 'Urgent',
                'created_at' => '2016-09-23 12:21:57',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
