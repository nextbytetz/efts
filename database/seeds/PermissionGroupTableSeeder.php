<?php

use Illuminate\Database\Seeder;

class PermissionGroupTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_group')->delete();
        
        \DB::table('permission_group')->insert(array (
            0 => 
            array (
                'permission_group_id' => 1,
                'name' => 'Access Management',
                'created_at' => '2016-07-12 15:12:41',
            ),
            1 => 
            array (
                'permission_group_id' => 2,
                'name' => 'System',
                'created_at' => '2016-07-12 15:12:56',
            ),
            2 => 
            array (
                'permission_group_id' => 3,
                'name' => 'Roles',
                'created_at' => '2016-07-12 15:13:02',
            ),
            3 => 
            array (
                'permission_group_id' => 4,
                'name' => 'File Register',
                'created_at' => '2016-07-12 15:13:27',
            ),
            4 => 
            array (
                'permission_group_id' => 5,
                'name' => 'File Process',
                'created_at' => '2016-07-12 15:13:37',
            ),
            5 => 
            array (
                'permission_group_id' => 6,
                'name' => 'Report',
                'created_at' => '2016-07-12 15:13:43',
            ),
        ));
        
        
    }
}
