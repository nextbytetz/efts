<?php

use Illuminate\Database\Seeder;

class CountryAllTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('country_all')->delete();
        
        \DB::table('country_all')->insert(array (
            0 => 
            array (
                'country_all_id' => 1,
                'name' => 'Andorra',
                'code' => 'AD',
            ),
            1 => 
            array (
                'country_all_id' => 2,
                'name' => 'United Arab Emirates',
                'code' => 'AE',
            ),
            2 => 
            array (
                'country_all_id' => 3,
                'name' => 'Afghanistan',
                'code' => 'AF',
            ),
            3 => 
            array (
                'country_all_id' => 4,
                'name' => 'Antigua and Barbuda',
                'code' => 'AG',
            ),
            4 => 
            array (
                'country_all_id' => 5,
                'name' => 'Anguilla',
                'code' => 'AI',
            ),
            5 => 
            array (
                'country_all_id' => 6,
                'name' => 'Albania',
                'code' => 'AL',
            ),
            6 => 
            array (
                'country_all_id' => 7,
                'name' => 'Armenia',
                'code' => 'AM',
            ),
            7 => 
            array (
                'country_all_id' => 8,
                'name' => 'Angola',
                'code' => 'AO',
            ),
            8 => 
            array (
                'country_all_id' => 9,
                'name' => 'Antarctica',
                'code' => 'AQ',
            ),
            9 => 
            array (
                'country_all_id' => 10,
                'name' => 'Argentina',
                'code' => 'AR',
            ),
            10 => 
            array (
                'country_all_id' => 11,
                'name' => 'American Samoa',
                'code' => 'AS',
            ),
            11 => 
            array (
                'country_all_id' => 12,
                'name' => 'Austria',
                'code' => 'AT',
            ),
            12 => 
            array (
                'country_all_id' => 13,
                'name' => 'Australia',
                'code' => 'AU',
            ),
            13 => 
            array (
                'country_all_id' => 14,
                'name' => 'Aruba',
                'code' => 'AW',
            ),
            14 => 
            array (
                'country_all_id' => 15,
                'name' => 'Åland Islands',
                'code' => 'AX',
            ),
            15 => 
            array (
                'country_all_id' => 16,
                'name' => 'Azerbaijan',
                'code' => 'AZ',
            ),
            16 => 
            array (
                'country_all_id' => 17,
                'name' => 'Bosnia and Herzegovina',
                'code' => 'BA',
            ),
            17 => 
            array (
                'country_all_id' => 18,
                'name' => 'Barbados',
                'code' => 'BB',
            ),
            18 => 
            array (
                'country_all_id' => 19,
                'name' => 'Bangladesh',
                'code' => 'BD',
            ),
            19 => 
            array (
                'country_all_id' => 20,
                'name' => 'Belgium',
                'code' => 'BE',
            ),
            20 => 
            array (
                'country_all_id' => 21,
                'name' => 'Burkina Faso',
                'code' => 'BF',
            ),
            21 => 
            array (
                'country_all_id' => 22,
                'name' => 'Bulgaria',
                'code' => 'BG',
            ),
            22 => 
            array (
                'country_all_id' => 23,
                'name' => 'Bahrain',
                'code' => 'BH',
            ),
            23 => 
            array (
                'country_all_id' => 24,
                'name' => 'Burundi',
                'code' => 'BI',
            ),
            24 => 
            array (
                'country_all_id' => 25,
                'name' => 'Benin',
                'code' => 'BJ',
            ),
            25 => 
            array (
                'country_all_id' => 26,
                'name' => 'Saint Barthélemy',
                'code' => 'BL',
            ),
            26 => 
            array (
                'country_all_id' => 27,
                'name' => 'Bermuda',
                'code' => 'BM',
            ),
            27 => 
            array (
                'country_all_id' => 28,
                'name' => 'Brunei Darussalam',
                'code' => 'BN',
            ),
            28 => 
            array (
                'country_all_id' => 29,
                'name' => 'Bolivia',
                'code' => 'BO',
            ),
            29 => 
            array (
                'country_all_id' => 30,
                'name' => 'Caribbean Netherlands ',
                'code' => 'BQ',
            ),
            30 => 
            array (
                'country_all_id' => 31,
                'name' => 'Brazil',
                'code' => 'BR',
            ),
            31 => 
            array (
                'country_all_id' => 32,
                'name' => 'Bahamas',
                'code' => 'BS',
            ),
            32 => 
            array (
                'country_all_id' => 33,
                'name' => 'Bhutan',
                'code' => 'BT',
            ),
            33 => 
            array (
                'country_all_id' => 34,
                'name' => 'Bouvet Island',
                'code' => 'BV',
            ),
            34 => 
            array (
                'country_all_id' => 35,
                'name' => 'Botswana',
                'code' => 'BW',
            ),
            35 => 
            array (
                'country_all_id' => 36,
                'name' => 'Belarus',
                'code' => 'BY',
            ),
            36 => 
            array (
                'country_all_id' => 37,
                'name' => 'Belize',
                'code' => 'BZ',
            ),
            37 => 
            array (
                'country_all_id' => 38,
                'name' => 'Canada',
                'code' => 'CA',
            ),
            38 => 
            array (
                'country_all_id' => 39,
            'name' => 'Cocos (Keeling) Islands',
                'code' => 'CC',
            ),
            39 => 
            array (
                'country_all_id' => 40,
                'name' => 'Congo, Democratic Republic of',
                'code' => 'CD',
            ),
            40 => 
            array (
                'country_all_id' => 41,
                'name' => 'Central African Republic',
                'code' => 'CF',
            ),
            41 => 
            array (
                'country_all_id' => 42,
                'name' => 'Congo',
                'code' => 'CG',
            ),
            42 => 
            array (
                'country_all_id' => 43,
                'name' => 'Switzerland',
                'code' => 'CH',
            ),
            43 => 
            array (
                'country_all_id' => 44,
                'name' => 'Cote d\'Ivoire',
                'code' => 'CI',
            ),
            44 => 
            array (
                'country_all_id' => 45,
                'name' => 'Cook Islands',
                'code' => 'CK',
            ),
            45 => 
            array (
                'country_all_id' => 46,
                'name' => 'Chile',
                'code' => 'CL',
            ),
            46 => 
            array (
                'country_all_id' => 47,
                'name' => 'Cameroon',
                'code' => 'CM',
            ),
            47 => 
            array (
                'country_all_id' => 48,
                'name' => 'China',
                'code' => 'CN',
            ),
            48 => 
            array (
                'country_all_id' => 49,
                'name' => 'Colombia',
                'code' => 'CO',
            ),
            49 => 
            array (
                'country_all_id' => 50,
                'name' => 'Costa Rica',
                'code' => 'CR',
            ),
            50 => 
            array (
                'country_all_id' => 51,
                'name' => 'Cuba',
                'code' => 'CU',
            ),
            51 => 
            array (
                'country_all_id' => 52,
                'name' => 'Cape Verde',
                'code' => 'CV',
            ),
            52 => 
            array (
                'country_all_id' => 53,
                'name' => 'Curaçao',
                'code' => 'CW',
            ),
            53 => 
            array (
                'country_all_id' => 54,
                'name' => 'Christmas Island',
                'code' => 'CX',
            ),
            54 => 
            array (
                'country_all_id' => 55,
                'name' => 'Cyprus',
                'code' => 'CY',
            ),
            55 => 
            array (
                'country_all_id' => 56,
                'name' => 'Czech Republic',
                'code' => 'CZ',
            ),
            56 => 
            array (
                'country_all_id' => 57,
                'name' => 'Germany',
                'code' => 'DE',
            ),
            57 => 
            array (
                'country_all_id' => 58,
                'name' => 'Djibouti',
                'code' => 'DJ',
            ),
            58 => 
            array (
                'country_all_id' => 59,
                'name' => 'Denmark',
                'code' => 'DK',
            ),
            59 => 
            array (
                'country_all_id' => 60,
                'name' => 'Dominica',
                'code' => 'DM',
            ),
            60 => 
            array (
                'country_all_id' => 61,
                'name' => 'Dominican Republic',
                'code' => 'DO',
            ),
            61 => 
            array (
                'country_all_id' => 62,
                'name' => 'Algeria',
                'code' => 'DZ',
            ),
            62 => 
            array (
                'country_all_id' => 63,
                'name' => 'Ecuador',
                'code' => 'EC',
            ),
            63 => 
            array (
                'country_all_id' => 64,
                'name' => 'Estonia',
                'code' => 'EE',
            ),
            64 => 
            array (
                'country_all_id' => 65,
                'name' => 'Egypt',
                'code' => 'EG',
            ),
            65 => 
            array (
                'country_all_id' => 66,
                'name' => 'Western Sahara',
                'code' => 'EH',
            ),
            66 => 
            array (
                'country_all_id' => 67,
                'name' => 'Eritrea',
                'code' => 'ER',
            ),
            67 => 
            array (
                'country_all_id' => 68,
                'name' => 'Spain',
                'code' => 'ES',
            ),
            68 => 
            array (
                'country_all_id' => 69,
                'name' => 'Ethiopia',
                'code' => 'ET',
            ),
            69 => 
            array (
                'country_all_id' => 70,
                'name' => 'Finland',
                'code' => 'FI',
            ),
            70 => 
            array (
                'country_all_id' => 71,
                'name' => 'Fiji',
                'code' => 'FJ',
            ),
            71 => 
            array (
                'country_all_id' => 72,
                'name' => 'Falkland Islands',
                'code' => 'FK',
            ),
            72 => 
            array (
                'country_all_id' => 73,
                'name' => 'Micronesia, Federated States of',
                'code' => 'FM',
            ),
            73 => 
            array (
                'country_all_id' => 74,
                'name' => 'Faroe Islands',
                'code' => 'FO',
            ),
            74 => 
            array (
                'country_all_id' => 75,
                'name' => 'France',
                'code' => 'FR',
            ),
            75 => 
            array (
                'country_all_id' => 76,
                'name' => 'Gabon',
                'code' => 'GA',
            ),
            76 => 
            array (
                'country_all_id' => 77,
                'name' => 'United Kingdom',
                'code' => 'GB',
            ),
            77 => 
            array (
                'country_all_id' => 78,
                'name' => 'Grenada',
                'code' => 'GD',
            ),
            78 => 
            array (
                'country_all_id' => 79,
                'name' => 'Georgia',
                'code' => 'GE',
            ),
            79 => 
            array (
                'country_all_id' => 80,
                'name' => 'French Guiana',
                'code' => 'GF',
            ),
            80 => 
            array (
                'country_all_id' => 81,
                'name' => 'Guernsey',
                'code' => 'GG',
            ),
            81 => 
            array (
                'country_all_id' => 82,
                'name' => 'Ghana',
                'code' => 'GH',
            ),
            82 => 
            array (
                'country_all_id' => 83,
                'name' => 'Gibraltar',
                'code' => 'GI',
            ),
            83 => 
            array (
                'country_all_id' => 84,
                'name' => 'Greenland',
                'code' => 'GL',
            ),
            84 => 
            array (
                'country_all_id' => 85,
                'name' => 'Gambia',
                'code' => 'GM',
            ),
            85 => 
            array (
                'country_all_id' => 86,
                'name' => 'Guinea',
                'code' => 'GN',
            ),
            86 => 
            array (
                'country_all_id' => 87,
                'name' => 'Guadeloupe',
                'code' => 'GP',
            ),
            87 => 
            array (
                'country_all_id' => 88,
                'name' => 'Equatorial Guinea',
                'code' => 'GQ',
            ),
            88 => 
            array (
                'country_all_id' => 89,
                'name' => 'Greece',
                'code' => 'GR',
            ),
            89 => 
            array (
                'country_all_id' => 90,
                'name' => 'South Georgia and the South Sandwich Islands',
                'code' => 'GS',
            ),
            90 => 
            array (
                'country_all_id' => 91,
                'name' => 'Guatemala',
                'code' => 'GT',
            ),
            91 => 
            array (
                'country_all_id' => 92,
                'name' => 'Guam',
                'code' => 'GU',
            ),
            92 => 
            array (
                'country_all_id' => 93,
                'name' => 'Guinea-Bissau',
                'code' => 'GW',
            ),
            93 => 
            array (
                'country_all_id' => 94,
                'name' => 'Guyana',
                'code' => 'GY',
            ),
            94 => 
            array (
                'country_all_id' => 95,
                'name' => 'Hong Kong',
                'code' => 'HK',
            ),
            95 => 
            array (
                'country_all_id' => 96,
                'name' => 'Heard and McDonald Islands',
                'code' => 'HM',
            ),
            96 => 
            array (
                'country_all_id' => 97,
                'name' => 'Honduras',
                'code' => 'HN',
            ),
            97 => 
            array (
                'country_all_id' => 98,
                'name' => 'Croatia',
                'code' => 'HR',
            ),
            98 => 
            array (
                'country_all_id' => 99,
                'name' => 'Haiti',
                'code' => 'HT',
            ),
            99 => 
            array (
                'country_all_id' => 100,
                'name' => 'Hungary',
                'code' => 'HU',
            ),
            100 => 
            array (
                'country_all_id' => 101,
                'name' => 'Indonesia',
                'code' => 'ID',
            ),
            101 => 
            array (
                'country_all_id' => 102,
                'name' => 'Ireland',
                'code' => 'IE',
            ),
            102 => 
            array (
                'country_all_id' => 103,
                'name' => 'Israel',
                'code' => 'IL',
            ),
            103 => 
            array (
                'country_all_id' => 104,
                'name' => 'Isle of Man',
                'code' => 'IM',
            ),
            104 => 
            array (
                'country_all_id' => 105,
                'name' => 'India',
                'code' => 'IN',
            ),
            105 => 
            array (
                'country_all_id' => 106,
                'name' => 'British Indian Ocean Territory',
                'code' => 'IO',
            ),
            106 => 
            array (
                'country_all_id' => 107,
                'name' => 'Iraq',
                'code' => 'IQ',
            ),
            107 => 
            array (
                'country_all_id' => 108,
                'name' => 'Iran',
                'code' => 'IR',
            ),
            108 => 
            array (
                'country_all_id' => 109,
                'name' => 'Iceland',
                'code' => 'IS',
            ),
            109 => 
            array (
                'country_all_id' => 110,
                'name' => 'Italy',
                'code' => 'IT',
            ),
            110 => 
            array (
                'country_all_id' => 111,
                'name' => 'Jersey',
                'code' => 'JE',
            ),
            111 => 
            array (
                'country_all_id' => 112,
                'name' => 'Jamaica',
                'code' => 'JM',
            ),
            112 => 
            array (
                'country_all_id' => 113,
                'name' => 'Jordan',
                'code' => 'JO',
            ),
            113 => 
            array (
                'country_all_id' => 114,
                'name' => 'Japan',
                'code' => 'JP',
            ),
            114 => 
            array (
                'country_all_id' => 115,
                'name' => 'Kenya',
                'code' => 'KE',
            ),
            115 => 
            array (
                'country_all_id' => 116,
                'name' => 'Kyrgyzstan',
                'code' => 'KG',
            ),
            116 => 
            array (
                'country_all_id' => 117,
                'name' => 'Cambodia',
                'code' => 'KH',
            ),
            117 => 
            array (
                'country_all_id' => 118,
                'name' => 'Kiribati',
                'code' => 'KI',
            ),
            118 => 
            array (
                'country_all_id' => 119,
                'name' => 'Comoros',
                'code' => 'KM',
            ),
            119 => 
            array (
                'country_all_id' => 120,
                'name' => 'Saint Kitts and Nevis',
                'code' => 'KN',
            ),
            120 => 
            array (
                'country_all_id' => 121,
                'name' => 'North Korea',
                'code' => 'KP',
            ),
            121 => 
            array (
                'country_all_id' => 122,
                'name' => 'South Korea',
                'code' => 'KR',
            ),
            122 => 
            array (
                'country_all_id' => 123,
                'name' => 'Kuwait',
                'code' => 'KW',
            ),
            123 => 
            array (
                'country_all_id' => 124,
                'name' => 'Cayman Islands',
                'code' => 'KY',
            ),
            124 => 
            array (
                'country_all_id' => 125,
                'name' => 'Kazakhstan',
                'code' => 'KZ',
            ),
            125 => 
            array (
                'country_all_id' => 126,
                'name' => 'Lao People\'s Democratic Republic',
                'code' => 'LA',
            ),
            126 => 
            array (
                'country_all_id' => 127,
                'name' => 'Lebanon',
                'code' => 'LB',
            ),
            127 => 
            array (
                'country_all_id' => 128,
                'name' => 'Saint Lucia',
                'code' => 'LC',
            ),
            128 => 
            array (
                'country_all_id' => 129,
                'name' => 'Liechtenstein',
                'code' => 'LI',
            ),
            129 => 
            array (
                'country_all_id' => 130,
                'name' => 'Sri Lanka',
                'code' => 'LK',
            ),
            130 => 
            array (
                'country_all_id' => 131,
                'name' => 'Liberia',
                'code' => 'LR',
            ),
            131 => 
            array (
                'country_all_id' => 132,
                'name' => 'Lesotho',
                'code' => 'LS',
            ),
            132 => 
            array (
                'country_all_id' => 133,
                'name' => 'Lithuania',
                'code' => 'LT',
            ),
            133 => 
            array (
                'country_all_id' => 134,
                'name' => 'Luxembourg',
                'code' => 'LU',
            ),
            134 => 
            array (
                'country_all_id' => 135,
                'name' => 'Latvia',
                'code' => 'LV',
            ),
            135 => 
            array (
                'country_all_id' => 136,
                'name' => 'Libya',
                'code' => 'LY',
            ),
            136 => 
            array (
                'country_all_id' => 137,
                'name' => 'Morocco',
                'code' => 'MA',
            ),
            137 => 
            array (
                'country_all_id' => 138,
                'name' => 'Monaco',
                'code' => 'MC',
            ),
            138 => 
            array (
                'country_all_id' => 139,
                'name' => 'Moldova',
                'code' => 'MD',
            ),
            139 => 
            array (
                'country_all_id' => 140,
                'name' => 'Montenegro',
                'code' => 'ME',
            ),
            140 => 
            array (
                'country_all_id' => 141,
            'name' => 'Saint-Martin (France)',
                'code' => 'MF',
            ),
            141 => 
            array (
                'country_all_id' => 142,
                'name' => 'Madagascar',
                'code' => 'MG',
            ),
            142 => 
            array (
                'country_all_id' => 143,
                'name' => 'Marshall Islands',
                'code' => 'MH',
            ),
            143 => 
            array (
                'country_all_id' => 144,
                'name' => 'Macedonia',
                'code' => 'MK',
            ),
            144 => 
            array (
                'country_all_id' => 145,
                'name' => 'Mali',
                'code' => 'ML',
            ),
            145 => 
            array (
                'country_all_id' => 146,
                'name' => 'Myanmar',
                'code' => 'MM',
            ),
            146 => 
            array (
                'country_all_id' => 147,
                'name' => 'Mongolia',
                'code' => 'MN',
            ),
            147 => 
            array (
                'country_all_id' => 148,
                'name' => 'Macau',
                'code' => 'MO',
            ),
            148 => 
            array (
                'country_all_id' => 149,
                'name' => 'Northern Mariana Islands',
                'code' => 'MP',
            ),
            149 => 
            array (
                'country_all_id' => 150,
                'name' => 'Martinique',
                'code' => 'MQ',
            ),
            150 => 
            array (
                'country_all_id' => 151,
                'name' => 'Mauritania',
                'code' => 'MR',
            ),
            151 => 
            array (
                'country_all_id' => 152,
                'name' => 'Montserrat',
                'code' => 'MS',
            ),
            152 => 
            array (
                'country_all_id' => 153,
                'name' => 'Malta',
                'code' => 'MT',
            ),
            153 => 
            array (
                'country_all_id' => 154,
                'name' => 'Mauritius',
                'code' => 'MU',
            ),
            154 => 
            array (
                'country_all_id' => 155,
                'name' => 'Maldives',
                'code' => 'MV',
            ),
            155 => 
            array (
                'country_all_id' => 156,
                'name' => 'Malawi',
                'code' => 'MW',
            ),
            156 => 
            array (
                'country_all_id' => 157,
                'name' => 'Mexico',
                'code' => 'MX',
            ),
            157 => 
            array (
                'country_all_id' => 158,
                'name' => 'Malaysia',
                'code' => 'MY',
            ),
            158 => 
            array (
                'country_all_id' => 159,
                'name' => 'Mozambique',
                'code' => 'MZ',
            ),
            159 => 
            array (
                'country_all_id' => 160,
                'name' => 'Namibia',
                'code' => 'NA',
            ),
            160 => 
            array (
                'country_all_id' => 161,
                'name' => 'New Caledonia',
                'code' => 'NC',
            ),
            161 => 
            array (
                'country_all_id' => 162,
                'name' => 'Niger',
                'code' => 'NE',
            ),
            162 => 
            array (
                'country_all_id' => 163,
                'name' => 'Norfolk Island',
                'code' => 'NF',
            ),
            163 => 
            array (
                'country_all_id' => 164,
                'name' => 'Nigeria',
                'code' => 'NG',
            ),
            164 => 
            array (
                'country_all_id' => 165,
                'name' => 'Nicaragua',
                'code' => 'NI',
            ),
            165 => 
            array (
                'country_all_id' => 166,
                'name' => 'The Netherlands',
                'code' => 'NL',
            ),
            166 => 
            array (
                'country_all_id' => 167,
                'name' => 'Norway',
                'code' => 'NO',
            ),
            167 => 
            array (
                'country_all_id' => 168,
                'name' => 'Nepal',
                'code' => 'NP',
            ),
            168 => 
            array (
                'country_all_id' => 169,
                'name' => 'Nauru',
                'code' => 'NR',
            ),
            169 => 
            array (
                'country_all_id' => 170,
                'name' => 'Niue',
                'code' => 'NU',
            ),
            170 => 
            array (
                'country_all_id' => 171,
                'name' => 'New Zealand',
                'code' => 'NZ',
            ),
            171 => 
            array (
                'country_all_id' => 172,
                'name' => 'Oman',
                'code' => 'OM',
            ),
            172 => 
            array (
                'country_all_id' => 173,
                'name' => 'Panama',
                'code' => 'PA',
            ),
            173 => 
            array (
                'country_all_id' => 174,
                'name' => 'Peru',
                'code' => 'PE',
            ),
            174 => 
            array (
                'country_all_id' => 175,
                'name' => 'French Polynesia',
                'code' => 'PF',
            ),
            175 => 
            array (
                'country_all_id' => 176,
                'name' => 'Papua New Guinea',
                'code' => 'PG',
            ),
            176 => 
            array (
                'country_all_id' => 177,
                'name' => 'Philippines',
                'code' => 'PH',
            ),
            177 => 
            array (
                'country_all_id' => 178,
                'name' => 'Pakistan',
                'code' => 'PK',
            ),
            178 => 
            array (
                'country_all_id' => 179,
                'name' => 'Poland',
                'code' => 'PL',
            ),
            179 => 
            array (
                'country_all_id' => 180,
                'name' => 'St. Pierre and Miquelon',
                'code' => 'PM',
            ),
            180 => 
            array (
                'country_all_id' => 181,
                'name' => 'Pitcairn',
                'code' => 'PN',
            ),
            181 => 
            array (
                'country_all_id' => 182,
                'name' => 'Puerto Rico',
                'code' => 'PR',
            ),
            182 => 
            array (
                'country_all_id' => 183,
                'name' => 'Palestine, State of',
                'code' => 'PS',
            ),
            183 => 
            array (
                'country_all_id' => 184,
                'name' => 'Portugal',
                'code' => 'PT',
            ),
            184 => 
            array (
                'country_all_id' => 185,
                'name' => 'Palau',
                'code' => 'PW',
            ),
            185 => 
            array (
                'country_all_id' => 186,
                'name' => 'Paraguay',
                'code' => 'PY',
            ),
            186 => 
            array (
                'country_all_id' => 187,
                'name' => 'Qatar',
                'code' => 'QA',
            ),
            187 => 
            array (
                'country_all_id' => 188,
                'name' => 'Réunion',
                'code' => 'RE',
            ),
            188 => 
            array (
                'country_all_id' => 189,
                'name' => 'Romania',
                'code' => 'RO',
            ),
            189 => 
            array (
                'country_all_id' => 190,
                'name' => 'Serbia',
                'code' => 'RS',
            ),
            190 => 
            array (
                'country_all_id' => 191,
                'name' => 'Russian Federation',
                'code' => 'RU',
            ),
            191 => 
            array (
                'country_all_id' => 192,
                'name' => 'Rwanda',
                'code' => 'RW',
            ),
            192 => 
            array (
                'country_all_id' => 193,
                'name' => 'Saudi Arabia',
                'code' => 'SA',
            ),
            193 => 
            array (
                'country_all_id' => 194,
                'name' => 'Solomon Islands',
                'code' => 'SB',
            ),
            194 => 
            array (
                'country_all_id' => 195,
                'name' => 'Seychelles',
                'code' => 'SC',
            ),
            195 => 
            array (
                'country_all_id' => 196,
                'name' => 'Sudan',
                'code' => 'SD',
            ),
            196 => 
            array (
                'country_all_id' => 197,
                'name' => 'Sweden',
                'code' => 'SE',
            ),
            197 => 
            array (
                'country_all_id' => 198,
                'name' => 'Singapore',
                'code' => 'SG',
            ),
            198 => 
            array (
                'country_all_id' => 199,
                'name' => 'Saint Helena',
                'code' => 'SH',
            ),
            199 => 
            array (
                'country_all_id' => 200,
                'name' => 'Slovenia',
                'code' => 'SI',
            ),
            200 => 
            array (
                'country_all_id' => 201,
                'name' => 'Svalbard and Jan Mayen Islands',
                'code' => 'SJ',
            ),
            201 => 
            array (
                'country_all_id' => 202,
                'name' => 'Slovakia',
                'code' => 'SK',
            ),
            202 => 
            array (
                'country_all_id' => 203,
                'name' => 'Sierra Leone',
                'code' => 'SL',
            ),
            203 => 
            array (
                'country_all_id' => 204,
                'name' => 'San Marino',
                'code' => 'SM',
            ),
            204 => 
            array (
                'country_all_id' => 205,
                'name' => 'Senegal',
                'code' => 'SN',
            ),
            205 => 
            array (
                'country_all_id' => 206,
                'name' => 'Somalia',
                'code' => 'SO',
            ),
            206 => 
            array (
                'country_all_id' => 207,
                'name' => 'Suriname',
                'code' => 'SR',
            ),
            207 => 
            array (
                'country_all_id' => 208,
                'name' => 'South Sudan',
                'code' => 'SS',
            ),
            208 => 
            array (
                'country_all_id' => 209,
                'name' => 'Sao Tome and Principe',
                'code' => 'ST',
            ),
            209 => 
            array (
                'country_all_id' => 210,
                'name' => 'El Salvador',
                'code' => 'SV',
            ),
            210 => 
            array (
                'country_all_id' => 211,
            'name' => 'Sint Maarten (Dutch part)',
                'code' => 'SX',
            ),
            211 => 
            array (
                'country_all_id' => 212,
                'name' => 'Syria',
                'code' => 'SY',
            ),
            212 => 
            array (
                'country_all_id' => 213,
                'name' => 'Swaziland',
                'code' => 'SZ',
            ),
            213 => 
            array (
                'country_all_id' => 214,
                'name' => 'Turks and Caicos Islands',
                'code' => 'TC',
            ),
            214 => 
            array (
                'country_all_id' => 215,
                'name' => 'Chad',
                'code' => 'TD',
            ),
            215 => 
            array (
                'country_all_id' => 216,
                'name' => 'French Southern Territories',
                'code' => 'TF',
            ),
            216 => 
            array (
                'country_all_id' => 217,
                'name' => 'Togo',
                'code' => 'TG',
            ),
            217 => 
            array (
                'country_all_id' => 218,
                'name' => 'Thailand',
                'code' => 'TH',
            ),
            218 => 
            array (
                'country_all_id' => 219,
                'name' => 'Tajikistan',
                'code' => 'TJ',
            ),
            219 => 
            array (
                'country_all_id' => 220,
                'name' => 'Tokelau',
                'code' => 'TK',
            ),
            220 => 
            array (
                'country_all_id' => 221,
                'name' => 'Timor-Leste',
                'code' => 'TL',
            ),
            221 => 
            array (
                'country_all_id' => 222,
                'name' => 'Turkmenistan',
                'code' => 'TM',
            ),
            222 => 
            array (
                'country_all_id' => 223,
                'name' => 'Tunisia',
                'code' => 'TN',
            ),
            223 => 
            array (
                'country_all_id' => 224,
                'name' => 'Tonga',
                'code' => 'TO',
            ),
            224 => 
            array (
                'country_all_id' => 225,
                'name' => 'Turkey',
                'code' => 'TR',
            ),
            225 => 
            array (
                'country_all_id' => 226,
                'name' => 'Trinidad and Tobago',
                'code' => 'TT',
            ),
            226 => 
            array (
                'country_all_id' => 227,
                'name' => 'Tuvalu',
                'code' => 'TV',
            ),
            227 => 
            array (
                'country_all_id' => 228,
                'name' => 'Taiwan',
                'code' => 'TW',
            ),
            228 => 
            array (
                'country_all_id' => 229,
                'name' => 'Tanzania',
                'code' => 'TZ',
            ),
            229 => 
            array (
                'country_all_id' => 230,
                'name' => 'Ukraine',
                'code' => 'UA',
            ),
            230 => 
            array (
                'country_all_id' => 231,
                'name' => 'Uganda',
                'code' => 'UG',
            ),
            231 => 
            array (
                'country_all_id' => 232,
                'name' => 'United States Minor Outlying Islands',
                'code' => 'UM',
            ),
            232 => 
            array (
                'country_all_id' => 233,
                'name' => 'United States',
                'code' => 'US',
            ),
            233 => 
            array (
                'country_all_id' => 234,
                'name' => 'Uruguay',
                'code' => 'UY',
            ),
            234 => 
            array (
                'country_all_id' => 235,
                'name' => 'Uzbekistan',
                'code' => 'UZ',
            ),
            235 => 
            array (
                'country_all_id' => 236,
                'name' => 'Vatican',
                'code' => 'VA',
            ),
            236 => 
            array (
                'country_all_id' => 237,
                'name' => 'Saint Vincent and the Grenadines',
                'code' => 'VC',
            ),
            237 => 
            array (
                'country_all_id' => 238,
                'name' => 'Venezuela',
                'code' => 'VE',
            ),
            238 => 
            array (
                'country_all_id' => 239,
            'name' => 'Virgin Islands (British)',
                'code' => 'VG',
            ),
            239 => 
            array (
                'country_all_id' => 240,
            'name' => 'Virgin Islands (U.S.)',
                'code' => 'VI',
            ),
            240 => 
            array (
                'country_all_id' => 241,
                'name' => 'Vietnam',
                'code' => 'VN',
            ),
            241 => 
            array (
                'country_all_id' => 242,
                'name' => 'Vanuatu',
                'code' => 'VU',
            ),
            242 => 
            array (
                'country_all_id' => 243,
                'name' => 'Wallis and Futuna Islands',
                'code' => 'WF',
            ),
            243 => 
            array (
                'country_all_id' => 244,
                'name' => 'Samoa',
                'code' => 'WS',
            ),
            244 => 
            array (
                'country_all_id' => 245,
                'name' => 'Yemen',
                'code' => 'YE',
            ),
            245 => 
            array (
                'country_all_id' => 246,
                'name' => 'Mayotte',
                'code' => 'YT',
            ),
            246 => 
            array (
                'country_all_id' => 247,
                'name' => 'South Africa',
                'code' => 'ZA',
            ),
            247 => 
            array (
                'country_all_id' => 248,
                'name' => 'Zambia',
                'code' => 'ZM',
            ),
            248 => 
            array (
                'country_all_id' => 249,
                'name' => 'Zimbabwe',
                'code' => 'ZW',
            ),
        ));
        
        
    }
}
