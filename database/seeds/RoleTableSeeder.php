<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role')->delete();
        
        \DB::table('role')->insert(array (
            0 => 
            array (
                'role_id' => 1,
                'name' => 'Super',
                'created_at' => '2016-07-02 17:40:10',
                'updated_at' => NULL,
                'sys_role' => '1',
                'is_super' => '1',
            ),
            1 => 
            array (
                'role_id' => 2,
                'name' => 'Admin',
                'created_at' => '2016-07-02 17:40:10',
                'updated_at' => NULL,
                'sys_role' => '1',
                'is_super' => '1',
            ),
            2 => 
            array (
                'role_id' => 3,
                'name' => 'Officer',
                'created_at' => '2016-07-02 17:40:10',
                'updated_at' => NULL,
                'sys_role' => '1',
                'is_super' => '0',
            ),
            3 => 
            array (
                'role_id' => 4,
                'name' => 'Registry',
                'created_at' => '2016-07-02 17:44:06',
                'updated_at' => NULL,
                'sys_role' => '1',
                'is_super' => '0',
            ),
            4 => 
            array (
                'role_id' => 8,
                'name' => 'Searcher',
                'created_at' => '2016-07-22 23:25:29',
                'updated_at' => '2016-07-30 09:01:03',
                'sys_role' => '0',
                'is_super' => '0',
            ),
        ));
        
        
    }
}
