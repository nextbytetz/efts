<?php

use Illuminate\Database\Seeder;

class ActionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('action')->delete();
        
        \DB::table('action')->insert(array (
            0 => 
            array (
                'action_id' => 1,
                'name' => 'New Document Version',
                'created_at' => '2016-09-09 09:49:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'action_id' => 2,
                'name' => 'Document Status Updated',
                'created_at' => '2016-09-09 09:49:15',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
