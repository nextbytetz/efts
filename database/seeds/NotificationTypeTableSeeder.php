<?php

use Illuminate\Database\Seeder;

class NotificationTypeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('notification_type')->delete();
        
        \DB::table('notification_type')->insert(array (
            0 => 
            array (
                'notification_type_id' => 1,
                'name' => 'document_update_status',
                'created_at' => '2016-08-26 11:11:18',
                'updated_at' => NULL,
                'icon' => 'file-text',
            ),
            1 => 
            array (
                'notification_type_id' => 2,
                'name' => 'file_dispatch',
                'created_at' => '2016-09-24 18:22:58',
                'updated_at' => NULL,
                'icon' => 'inbox',
            ),
            2 => 
            array (
                'notification_type_id' => 3,
                'name' => 'file_register',
                'created_at' => '2016-10-02 22:32:05',
                'updated_at' => NULL,
                'icon' => 'comment',
            ),
        ));
        
        
    }
}
