<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission')->delete();
        
        \DB::table('permission')->insert(array (
            0 => 
            array (
                'permission_id' => 1,
                'display_name' => 'View Access Management',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:44:57',
                'description' => NULL,
                'permission_group_id' => 1,
                'name' => 'view_access',
            ),
            1 => 
            array (
                'permission_id' => 2,
                'display_name' => 'Edit User',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:50:10',
                'description' => NULL,
                'permission_group_id' => 1,
                'name' => 'edit_user',
            ),
            2 => 
            array (
                'permission_id' => 3,
                'display_name' => 'Change User Password',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:50:29',
                'description' => NULL,
                'permission_group_id' => 1,
                'name' => 'change_user_password',
            ),
            3 => 
            array (
                'permission_id' => 4,
                'display_name' => 'Approve New Users',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:50:46',
                'description' => NULL,
                'permission_group_id' => 1,
                'name' => 'approve_user',
            ),
            4 => 
            array (
                'permission_id' => 5,
                'display_name' => 'Deactivate & Reactivate User',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:51:38',
                'description' => NULL,
                'permission_group_id' => 1,
                'name' => 'deactivate_reactivate_user',
            ),
            5 => 
            array (
                'permission_id' => 6,
                'display_name' => 'Restore & Delete User',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:52:55',
                'description' => NULL,
                'permission_group_id' => 1,
                'name' => 'restore_delete_user',
            ),
            6 => 
            array (
                'permission_id' => 7,
                'display_name' => 'Mark user in/out of office',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:53:17',
                'description' => NULL,
                'permission_group_id' => 1,
                'name' => 'in_out_office',
            ),
            7 => 
            array (
                'permission_id' => 8,
                'display_name' => 'Approve user\'s departmental shift',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:53:43',
                'description' => 'Approve user\'s departmental shift when user shift from one department to another',
                'permission_group_id' => 1,
                'name' => 'user_departmental_shift',
            ),
            8 => 
            array (
                'permission_id' => 9,
                'display_name' => 'Replace super role',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:54:48',
                'description' => 'Replace the current existing super user',
                'permission_group_id' => 1,
                'name' => 'replace_super',
            ),
            9 => 
            array (
                'permission_id' => 10,
                'display_name' => 'Promote & Demote admin role',
                'is_super' => '1',
                'created_at' => '2016-07-03 13:55:06',
                'description' => 'Promote and demote to and from admin role respectively',
                'permission_group_id' => 1,
                'name' => 'promote_demote_admin',
            ),
            10 => 
            array (
                'permission_id' => 11,
                'display_name' => 'Promote & Demote super role',
                'is_super' => '1',
                'created_at' => '2016-07-03 13:55:32',
                'description' => 'Promote and demote to and from super role respectively',
                'permission_group_id' => 1,
                'name' => 'promote_demote_super',
            ),
            11 => 
            array (
                'permission_id' => 12,
                'display_name' => 'Data registration',
                'is_super' => '0',
                'created_at' => '2016-07-03 13:58:32',
                'description' => 'Add the system data like countries, regions, branches and departments.',
                'permission_group_id' => 2,
                'name' => 'data_registration',
            ),
            12 => 
            array (
                'permission_id' => 13,
                'display_name' => 'Create Role',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:02:53',
                'description' => NULL,
                'permission_group_id' => 3,
                'name' => 'create_role',
            ),
            13 => 
            array (
                'permission_id' => 14,
                'display_name' => 'Edit Role',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:03:06',
                'description' => NULL,
                'permission_group_id' => 3,
                'name' => 'edit_role',
            ),
            14 => 
            array (
                'permission_id' => 15,
                'display_name' => 'Delete Role',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:03:55',
                'description' => NULL,
                'permission_group_id' => 3,
                'name' => 'delete_role',
            ),
            15 => 
            array (
                'permission_id' => 16,
                'display_name' => 'Create & Edit file',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:04:52',
                'description' => NULL,
                'permission_group_id' => 4,
                'name' => 'create_edit_file',
            ),
            16 => 
            array (
                'permission_id' => 17,
                'display_name' => 'Create document in a file',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:05:10',
                'description' => NULL,
                'permission_group_id' => 4,
                'name' => 'create_document',
            ),
            17 => 
            array (
                'permission_id' => 18,
                'display_name' => 'View file register history',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:06:25',
                'description' => 'View file workflows, file status change logs, file info change logs and document info change logs',
                'permission_group_id' => 4,
                'name' => 'file_register',
            ),
            18 => 
            array (
                'permission_id' => 19,
                'display_name' => 'Print Barcodes',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:09:27',
                'description' => NULL,
                'permission_group_id' => 4,
                'name' => 'print_barcode',
            ),
            19 => 
            array (
                'permission_id' => 20,
                'display_name' => 'Register Bulk file records',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:09:48',
                'description' => NULL,
                'permission_group_id' => 4,
                'name' => 'register_bulk',
            ),
            20 => 
            array (
                'permission_id' => 21,
            'display_name' => 'Register electronic product code (EPC)',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:10:25',
                'description' => NULL,
                'permission_group_id' => 4,
                'name' => 'register_epc',
            ),
            21 => 
            array (
                'permission_id' => 22,
                'display_name' => 'Can receive file',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:11:24',
                'description' => NULL,
                'permission_group_id' => 5,
                'name' => 'receive_file',
            ),
            22 => 
            array (
                'permission_id' => 23,
                'display_name' => 'Search file',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:11:37',
                'description' => NULL,
                'permission_group_id' => 5,
                'name' => 'search_file',
            ),
            23 => 
            array (
                'permission_id' => 24,
                'display_name' => 'Transfer documents between files',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:12:06',
                'description' => 'Transfer document from one file to another',
                'permission_group_id' => 5,
                'name' => 'transfer_document',
            ),
            24 => 
            array (
                'permission_id' => 25,
                'display_name' => 'Process file',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:19:26',
                'description' => 'Add file details, file priority, file status & comments',
                'permission_group_id' => 5,
                'name' => 'process_file',
            ),
            25 => 
            array (
                'permission_id' => 26,
                'display_name' => 'Can pick file',
                'is_super' => '0',
                'created_at' => '2016-07-03 14:19:54',
                'description' => 'Can pick the file available or recently disposed by another officer',
                'permission_group_id' => 5,
                'name' => 'pick_file',
            ),
            26 => 
            array (
                'permission_id' => 27,
                'display_name' => 'View perfomance report',
                'is_super' => '0',
                'created_at' => '2016-07-03 15:25:50',
                'description' => NULL,
                'permission_group_id' => 6,
                'name' => 'perfomance_report',
            ),
            27 => 
            array (
                'permission_id' => 28,
                'display_name' => 'Delete New Registered User',
                'is_super' => '0',
                'created_at' => '2016-07-18 18:32:29',
                'description' => 'Delete a new registered user who has never been confirmed',
                'permission_group_id' => 1,
                'name' => 'delete_user',
            ),
        ));
        
        
    }
}
