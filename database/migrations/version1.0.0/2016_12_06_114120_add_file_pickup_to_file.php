<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFilePickupToFile extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();
	Schema::table('file', function (Blueprint $table) {
	    $table->bigInteger('file_pickup_id')->nullable();
	    $table->index(['file_pickup_id']); //index for the foreign keys
	    $table->foreign('file_pickup_id')->references('file_pickup_id')->on('file_pickup')->onDelete('restrict')->onUpdate('cascade');
	});
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	Schema::table('file', function ($table) {
	    $table->dropColumn('file_pickup_id');
	});
	DB::commit();
    }

}
