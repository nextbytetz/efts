<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//
// NOTE Migration Created: 2016-10-05 13:44:53
// --------------------------------------------------

class CreateEformatsDatabase extends Migration {

//
// NOTE - Make changes to the database.
// --------------------------------------------------

    public function up() {
	
	DB::beginTransaction();

//
// NOTE -- action
// --------------------------------------------------

	Schema::create('action', function(Blueprint $table) {
	    $table->increments('action_id');
	    $table->string('name', 150);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE action IS 'Store different actions perfomed in the system. '");


//
// NOTE -- attachment
// --------------------------------------------------

	Schema::create('attachment', function(Blueprint $table) {
	    $table->bigIncrements('attachment_id');
	    $table->string('path', 500);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->string('mime', 5);
	});
	DB::statement("COMMENT ON TABLE attachment IS 'Table to store all the uploaded files , documents and any attachement.'");


//
// NOTE -- branch
// --------------------------------------------------

	Schema::create('branch', function(Blueprint $table) {
	    $table->increments('branch_id');
	    $table->string('name', 200);
	    $table->smallInteger('region_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['region_id']);
	});
	DB::statement("COMMENT ON TABLE branch IS 'Table to store all the branche locations registered for this application.'");


//
// NOTE -- country
// --------------------------------------------------

	Schema::create('country', function(Blueprint $table) {
	    $table->increments('country_id');
	    $table->string('name', 130);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE country IS 'Table to store all the contries registered for this application'");


//
// NOTE -- country_all
// --------------------------------------------------

	Schema::create('country_all', function(Blueprint $table) {
	    $table->increments('country_all_id');
	    $table->string('name', 130);
	    $table->string('code', 2);
	});
	DB::statement("COMMENT ON TABLE country_all IS 'Table to store all countries in the world'");


//
// NOTE -- department
// --------------------------------------------------

	Schema::create('department', function(Blueprint $table) {
	    $table->increments('department_id');
	    $table->string('name', 200);
	    $table->smallInteger('branch_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['branch_id']);
	});
	DB::statement("COMMENT ON TABLE department IS 'Table to store all the departments registered in the system.'");


//
// NOTE -- document
// --------------------------------------------------

	Schema::create('document', function(Blueprint $table) {
	    $table->bigIncrements('document_id');
	    $table->string('name', 500);
	    $table->text('description')->nullable();
	    $table->bigInteger('file_id');
	    $table->bigInteger('officer_id');
	    $table->char('status', 1)->default("1")->comment("Set whether this file is active or inactive. 1 - ACTIVE/OPEN 0 - INACTIVE/CLOSED");
	    $table->char('type', 1)->nullable()->default("0")->comment("1 - live document, can be edited online and saved on line 0 - not live document");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->bigInteger('folder_id')->nullable();
	    $table->timestamp('deleted_at')->nullable();
	    $table->string('ext', 20)->nullable()->comment("Extension of the document i.e  .pdf, .doc, .docx");
	    $table->float('size')->nullable()->comment("Size of the document");
	    $table->string('mime', 150)->nullable()->comment("Getting MIME Type of uploaded file");
	    $table->smallInteger('priority_id')->default("2");
	    $table->integer('version')->default("1");
	    $table->index(['file_id']);
	    $table->index(['officer_id']);
	    $table->index(['folder_id']);
	});
	DB::statement("COMMENT ON TABLE document IS 'Table to store all the documents registered in the files.'");
	DB::statement("COMMENT ON column document.status IS 'Set whether this file is active or inactive. 1 - ACTIVE/OPEN 0 - INACTIVE/CLOSED'");
	DB::statement("COMMENT ON column document.type IS '1 - live document, can be edited online and saved on line 0 - not live document'");
	DB::statement("COMMENT ON column document.ext IS 'Extension of the document i.e  .pdf, .doc, .docx'");
	DB::statement("COMMENT ON column document.size IS 'Size of the document'");
	DB::statement("COMMENT ON column document.mime IS 'Getting MIME Type of uploaded file'");


//
// NOTE -- document_audit
// --------------------------------------------------

	Schema::create('document_audit', function(Blueprint $table) {
	    $table->bigIncrements('document_audit_id');
	    $table->bigInteger('document_id');
	    $table->integer('officer_id');
	    $table->string('path', 500)->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->string('name', 500)->nullable();
	    $table->text('description')->nullable();
	    $table->index(['document_id']);
	    $table->index(['officer_id']);
	});
	DB::statement("COMMENT ON TABLE document_audit IS 'Table to store all the change in the document information. Keep the document audit trail.'");


//
// NOTE -- document_status
// --------------------------------------------------

	Schema::create('document_status', function(Blueprint $table) {
	    $table->bigIncrements('document_status_id');
	    $table->text('comment')->nullable();
	    $table->smallInteger('priority_id')->default("2");
	    $table->char('status', 1)->default("1");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->integer('officer_id');
	    $table->bigInteger('document_id');
	    $table->smallInteger('action_id')->nullable();
	    $table->index(['officer_id']);
	    $table->index(['document_id']);
	});
	DB::statement("COMMENT ON TABLE document_status IS 'table to track the status update of the document from the time it is created to when it is closed.'");

//
// NOTE -- file
// --------------------------------------------------

	Schema::create('file', function(Blueprint $table) {
	    $table->bigIncrements('file_id');
	    $table->string('reference', 200)->nullable()->unique();
	    $table->string('name', 500);
	    $table->string('subject', 200);
	    $table->char('status', 1)->default("1")->comment("Table to store the status of the file 1 - OPEN, 2 - CLOSED");
	    $table->text('description')->nullable();
	    $table->smallInteger('file_category_id')->default("1");
	    $table->smallInteger('file_type_id');
	    $table->string('shelf_number', 100)->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('closed_at')->nullable();
	    $table->integer('officer_opened')->nullable();
	    $table->integer('officer_closed')->nullable();
	    $table->smallInteger('file_nature_id');
	    $table->string('epc', 30)->nullable()->comment("Store the corresponding electronic product code (epc) for the RFID tag attached on the physical file.");
	    $table->timestamp('updated_at')->nullable();
	    $table->integer('officer_edited');
	    $table->timestamp('deleted_at')->nullable();
	    $table->smallInteger('priority_id')->default("2");
	    $table->index(['officer_opened']);
	});
	DB::statement("COMMENT ON TABLE file IS 'Table to store all the files registered in the application.'");
	DB::statement("COMMENT ON column file.status IS 'Table to store the status of the file 1 - OPEN, 2 - CLOSED'");
	DB::statement("COMMENT ON column file.epc IS 'Store the corresponding electronic product code (epc) for the RFID tag attached on the physical file.'");

//
// NOTE -- file_audit
// --------------------------------------------------

	Schema::create('file_audit', function(Blueprint $table) {
	    $table->bigIncrements('file_audit_id');
	    $table->bigInteger('file_id');
	    $table->integer('officer_id')->nullable();
	    $table->string('reference', 200)->nullable();
	    $table->string('name', 500)->nullable();
	    $table->string('subject', 200)->nullable();
	    $table->text('description')->nullable();
	    $table->string('epc', 30)->nullable();
	    $table->smallInteger('file_category_id')->nullable();
	    $table->smallInteger('file_type_id')->nullable();
	    $table->string('shelf_number', 100)->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->index(['file_id']);
	    $table->index(['officer_id']);
	});
	DB::statement("COMMENT ON TABLE file_audit IS 'Table to track all the file changes registered by the application.'");


//
// NOTE -- file_category
// --------------------------------------------------

	Schema::create('file_category', function(Blueprint $table) {
	    $table->increments('file_category_id');
	    $table->string('name', 200)->unique();
	    $table->timestamp('created_at')->useCurrent();
	});
	DB::statement("COMMENT ON TABLE file_category IS 'Table to store the file category; 1. Normal file means the file can only be forwarded to only one person. 2. Notice file means that the file can be forwarded to many people. This apply only to electronic files.'");


//
// NOTE -- file_department
// --------------------------------------------------

	Schema::create('file_department', function(Blueprint $table) {
	    $table->bigIncrements('file_department_id');
	    $table->bigInteger('file_id');
	    $table->smallInteger('department_id');
	    $table->char('status', 1)->default("1")->comment("state whether the assignment is active or inactive. 1 - active 0 - inactive");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['file_id']);
	    $table->index(['department_id']);
	});
	DB::statement("COMMENT ON TABLE file_department IS 'Table to store the file department accessibility. Departments which will be able to access this file. This case will apply to Public file types.'");
	DB::statement("COMMENT ON column file_department.status IS 'state whether the assignment is active or inactive. 1 - active 0 - inactive'");


//
// NOTE -- file_nature
// --------------------------------------------------

	Schema::create('file_nature', function(Blueprint $table) {
	    $table->increments('file_nature_id');
	    $table->string('name', 200);
	    $table->timestamp('created_at')->useCurrent();
	});
	DB::statement("COMMENT ON TABLE file_nature IS 'Table to store the nature of files to be stored in the application. Electronic document and Physical document.'");


//
// NOTE -- file_officer
// --------------------------------------------------

	Schema::create('file_officer', function(Blueprint $table) {
	    $table->bigIncrements('file_officer_id');
	    $table->bigInteger('file_id');
	    $table->integer('officer_id');
	    $table->char('status', 1)->default("1")->comment("State whether this assignment is active or inactive. 1 - active 0 - inactive");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['file_id']);
	    $table->index(['officer_id']);
	});
	DB::statement("COMMENT ON TABLE file_officer IS 'Table to store the file officer accesibility. Officer who will be able to access this file. This will apply to confidential and private file type.'");
	DB::statement("COMMENT ON column file_officer.status IS 'State whether this assignment is active or inactive. 1 - active 0 - inactive'");
	


//
// NOTE -- file_pickup
// --------------------------------------------------

	Schema::create('file_pickup', function(Blueprint $table) {
	    $table->bigIncrements('file_pickup_id');
	    $table->bigInteger('file_id');
	    $table->integer('officer_id');
	    $table->timestamp('disposed_at')->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->char('forwarded', 1)->default("0")->comment("Determines whether the file was dispatched after pick up or not. 0 No  1 Yes");
	    $table->bigInteger('workflow_id')->nullable()->comment("Workflow associated with this pickup");
	    $table->string('forced', 1)->default("0");
	    $table->index(['officer_id']);
	    $table->index(['file_id']);
	});
	DB::statement("COMMENT ON TABLE file_pickup IS 'record the picking and disposing of files by officers. This will be used to control how long the file stayed at what location.'");
	DB::statement("COMMENT ON column file_pickup.forwarded IS 'Determines whether the file was dispatched after pick up or not. 0 No  1 Yes'");
	DB::statement("COMMENT ON column file_pickup.workflow_id IS 'Workflow associated with this pickup'");
	DB::statement("COMMENT ON column file_pickup.forced IS 'Determines whether the file pickup is forced or not forced. This file is currently held by someone. 1 for forced pickup, 0 for not forced pickup.'");


//
// NOTE -- file_request
// --------------------------------------------------

	Schema::create('file_request', function(Blueprint $table) {
	    $table->bigIncrements('file_request_id');
	    $table->bigInteger('file_id');
	    $table->integer('officer_id');
	    $table->char('status', 1)->default("0")->comment("Set the status of the file request. 1 - file has been sent for the request 0 - file has not been sent for the request");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['file_id', 'officer_id']);
	});
	DB::statement("COMMENT ON TABLE file_request IS 'Table to store all the file request from officers.'");
	DB::statement("COMMENT ON column file_request.status IS 'Set the status of the file request. 1 - file has been sent for the request 0 - file has not been sent for the request'");


//
// NOTE -- file_status_audit
// --------------------------------------------------

	Schema::create('file_status_audit', function(Blueprint $table) {
	    $table->bigIncrements('file_status_audit_id');
	    $table->timestamp('created_at')->nullable();
	    $table->timestamp('closed_at')->nullable();
	    $table->integer('officer_opened');
	    $table->integer('officer_closed')->nullable();
	    $table->bigInteger('file_id');
	    $table->timestamp('date_created')->useCurrent();
	    $table->index(['officer_opened']);
	});
	DB::statement("COMMENT ON TABLE file_status_audit IS 'Table to store the status change of the file between open date and close date and the officers who made the change.'");


//
// NOTE -- file_subject
// --------------------------------------------------

	Schema::create('file_subject', function(Blueprint $table) {
	    $table->bigIncrements('file_subject_id');
	    $table->string('name', 500);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE file_subject IS 'Table to store the file subject entered by user during file registration for future auto suggesting of the last saved file subjects to user to avoid hassle of refilling the same file subject from time to time.'");


//
// NOTE -- file_type
// --------------------------------------------------

	Schema::create('file_type', function(Blueprint $table) {
	    $table->increments('file_type_id');
	    $table->string('name', 200)->unique();
	    $table->timestamp('created_at')->useCurrent();
	});
	DB::statement("COMMENT ON TABLE file_type IS 'Table to store the file type to be registered in the application. The file type will define the accessibility level of the file amoung officers. 1. Public : file will be accessible in the selected department branches.2. Confidential : file will be accessible to selected officers from the selected departments. 3. Private : file will be accessible to one officer only.'");


//
// NOTE -- folder
// --------------------------------------------------

	Schema::create('folder', function(Blueprint $table) {
	    $table->bigIncrements('folder_id');
	    $table->bigInteger('file_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->bigInteger('parent_id')->nullable()->comment("refering to the folder_id");
	    $table->string('name', 200)->comment("name of the sub folder of the file");
	    $table->integer('officer_id')->nullable();
	    $table->timestamp('deleted_at')->nullable();
	    $table->index(['file_id']);
	    $table->index(['parent_id']);
	});
	DB::statement("COMMENT ON TABLE folder IS 'Document directory division within a file'");
	DB::statement("COMMENT ON column folder.parent_id IS 'refering to the folder_id'");
	DB::statement("COMMENT ON column folder.name IS 'name of the sub folder of the file'");

//
// NOTE -- history
// --------------------------------------------------

	Schema::create('history', function(Blueprint $table) {
	    $table->bigIncrements('history_id');
	    $table->smallInteger('history_type_id');
	    $table->integer('officer_id');
	    $table->integer('entity_id')->nullable();
	    $table->string('icon', 255)->nullable();
	    $table->string('class', 255)->nullable();
	    $table->string('text', 255);
	    $table->string('assets', 255)->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE history IS 'Table to store different actions performed by the user of the application. e.g creating a new user, deleting a role, transfer documents etc'");


//
// NOTE -- history_type
// --------------------------------------------------

	Schema::create('history_type', function(Blueprint $table) {
	    $table->increments('history_type_id');
	    $table->string('name', 255);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE history_type IS 'Stores different types of history performed by application user. '");


//
// NOTE -- notification
// --------------------------------------------------

	Schema::create('notification', function(Blueprint $table) {
	    $table->bigIncrements('notification_id');
	    $table->string('class', 255)->nullable()->comment("specify any additional class for the icon of the notification");
	    $table->string('text', 255)->comment("message of the notification");
	    $table->string('asset', 255)->nullable()->comment("Store the table to extract the specific information about this notification, e.g notification for document_status_update, its asset will be document_status table whereby it will be stored matching its eloquent class name (DocumentStatus) in PHP source codes for easy execution.");
	    $table->bigInteger('id')->nullable()->comment("Primary key id of the specified asset table.");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->smallInteger('notification_type_id');
	});
	DB::statement("COMMENT ON TABLE notification IS 'Store all saved notifications. Contain message alerts which displays to user instructing to follow a certain lead'");
	DB::statement("COMMENT ON column notification.class IS 'specify any additional class for the icon of the notification'");
	DB::statement("COMMENT ON column notification.text IS 'message of the notification'");
	DB::statement("COMMENT ON column notification.asset IS 'Store the table to extract the specific information about this notification, e.g notification for document_status_update, its asset will be document_status table whereby it will be stored matching its eloquent class name (DocumentStatus) in PHP source codes for easy execution.'");
	DB::statement("COMMENT ON column notification.id IS 'Primary key id of the specified asset table.'");



//
// NOTE -- notification_officer
// --------------------------------------------------

	Schema::create('notification_officer', function(Blueprint $table) {
	    $table->bigIncrements('notification_officer_id');
	    $table->bigInteger('notification_id');
	    $table->integer('officer_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['officer_id']);
	    $table->index(['notification_id']);
	});
	DB::statement("COMMENT ON TABLE notification_officer IS 'Store for each notification which user will be able to view'");


//
// NOTE -- notification_type
// --------------------------------------------------

	Schema::create('notification_type', function(Blueprint $table) {
	    $table->increments('notification_type_id');
	    $table->string('name', 100);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->string('icon', 50)->nullable();
	});
	DB::statement("COMMENT ON TABLE notification_type IS 'Store different types of notification, used to direct the user into the right plate'");


//
// NOTE -- officer
// --------------------------------------------------

	Schema::create('officer', function(Blueprint $table) {
	    $table->increments('officer_id');
	    $table->string('firstname', 70);
	    $table->string('othernames', 140);
	    $table->string('username', 80)->unique();
	    $table->string('password', 300);
	    $table->string('email', 100)->unique();
	    $table->smallInteger('department_id')->nullable();
	    $table->string('officeid', 100)->nullable();
	    $table->char('gender', 1);
	    $table->char('checked', 1)->default("0")->comment("0 - New user & has not been checked to login 1 - User has been checked and can log in");
	    $table->timestamp('lastlogin')->nullable();
	    $table->string('pic', 100)->nullable();
	    $table->string('phone', 50)->nullable();
	    $table->string('home', 100)->nullable();
	    $table->string('zipcode', 10)->nullable();
	    $table->date('birth_date')->nullable();
	    $table->char('active', 1)->default("1")->comment("1  -  Active & can log in 0  -  Inactive & can not log in");
	    $table->timestamp('created_at')->nullable()->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->string('remember_token', 100)->nullable()->comment("Column for supporting Laravel");
	    $table->integer('officer_checked')->nullable()->comment("officer who checked/activated this user to activate in the system for the first time");
	    $table->timestamp('checked_date')->nullable()->comment("Date when this officer was activated/approved for the first time");
	    $table->char('available', 1)->default("0")->comment("Check if the user is available/in office or out of office. 1 if user is in office and 0 if user is out of office.");
	    $table->index(['department_id']);
	});
	DB::statement("COMMENT ON TABLE officer IS 'Table to store all officers using the system.'");
	DB::statement("COMMENT ON column officer.checked IS '0 - New user & has not been checked to login 1 - User has been checked and can log in'");
	DB::statement("COMMENT ON column officer.active IS '1  -  Active & can log in 0  -  Inactive & can not log in'");
	DB::statement("COMMENT ON column officer.remember_token IS 'Column for supporting Laravel'");
	DB::statement("COMMENT ON column officer.officer_checked IS 'officer who checked/activated this user to activate in the system for the first time'");
	DB::statement("COMMENT ON column officer.checked_date IS 'Date when this officer was activated/approved for the first time'");
	DB::statement("COMMENT ON column officer.available IS 'Check if the user is available/in office or out of office. 1 if user is in office and 0 if user is out of office.'");


//
// NOTE -- officer_permission
// --------------------------------------------------

	Schema::create('officer_permission', function(Blueprint $table) {
	    $table->bigIncrements('officer_permission_id');
	    $table->integer('officer_id');
	    $table->smallInteger('permission_id');
	    $table->char('status', 1)->default("1")->comment("Specify whether the assignment of permission to an officer is active or inactive.");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['officer_id']);
	    $table->index(['permission_id']);
	});
	DB::statement("COMMENT ON TABLE officer_permission IS 'List the additional permission which an officer might have been given apart from those permissions specified in the role(s) specified for this officer.'");
	DB::statement("COMMENT ON column officer_permission.status IS 'Specify whether the assignment of permission to an officer is active or inactive.'");


//
// NOTE -- officer_role
// --------------------------------------------------

	Schema::create('officer_role', function(Blueprint $table) {
	    $table->bigIncrements('officer_role_id');
	    $table->integer('officer_id');
	    $table->smallInteger('role_id');
	    $table->char('status', 1)->default("1")->comment("Specify whether the assignment of officer to this role is active or inactive.");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['officer_id']);
	    $table->index(['role_id']);
	});
	DB::statement("COMMENT ON TABLE officer_role IS 'Specify the assignment of officers to different application roles.'");
	DB::statement("COMMENT ON column officer_role.status IS 'Specify whether the assignment of officer to this role is active or inactive.'");

//
// NOTE -- permission
// --------------------------------------------------

	Schema::create('permission', function(Blueprint $table) {
	    $table->increments('permission_id');
	    $table->string('display_name', 100)->nullable();
	    $table->char('is_super', 1)->default("0")->comment("Show whether this permission is only for super role or not. 1 - permission is for super user 0 - permission is not for super user.");
	    $table->timestamp('created_at')->useCurrent();
	    $table->string('description', 200)->nullable();
	    $table->smallInteger('permission_group_id')->nullable();
	    $table->string('name', 100)->nullable();
	});
	DB::statement("COMMENT ON TABLE permission IS 'List all permissions registered for the application which will be given to user for allowing to access different functionalities'");
	DB::statement("COMMENT ON column permission.is_super IS 'Show whether this permission is only for super role or not. 1 - permission is for super user 0 - permission is not for super user.'");


//
// NOTE -- permission_dependency
// --------------------------------------------------

	Schema::create('permission_dependency', function(Blueprint $table) {
	    $table->increments('permission_dependency_id');
	    $table->smallInteger('permission_id');
	    $table->smallInteger('dependency_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['permission_id']);
	});
	DB::statement("COMMENT ON TABLE permission_dependency IS 'List permission that depend on the other permission, in order for the a certain permission to exist, the other permission must be enabled too.'");


//
// NOTE -- permission_group
// --------------------------------------------------

	Schema::create('permission_group', function(Blueprint $table) {
	    $table->increments('permission_group_id');
	    $table->string('name', 100);
	    $table->timestamp('created_at')->useCurrent();
	});
	DB::statement("COMMENT ON TABLE permission_group IS 'List the group of registered permissions'");


//
// NOTE -- priority
// --------------------------------------------------

	Schema::create('priority', function(Blueprint $table) {
	    $table->increments('priority_id');
	    $table->string('name', 50);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE priority IS 'List priorities available for files and documents'");


//
// NOTE -- region
// --------------------------------------------------

	Schema::create('region', function(Blueprint $table) {
	    $table->increments('region_id');
	    $table->string('name', 200);
	    $table->smallInteger('country_id');
	    $table->timestamp('created_at')->nullable()->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['country_id']);
	});
	DB::statement("COMMENT ON TABLE region IS 'Table to store all the regions registered in the system.'");


//
// NOTE -- role
// --------------------------------------------------

	Schema::create('role', function(Blueprint $table) {
	    $table->increments('role_id');
	    $table->string('name', 100)->unique();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->char('sys_role', 1)->default("0")->comment("Set whether the role is default in the application or has been created. Default aplication roles will never be deleted. 1 - role is default by the application 0 - role has been created.");
	    $table->char('is_super', 1)->default("0")->comment("Show whether this role is only for super role or not. 1 - role is for super user 0 - role is not for super user.");
	});
	DB::statement("COMMENT ON TABLE role IS 'Store all the registered roles in the system.'");
	DB::statement("COMMENT ON column role.sys_role IS 'Set whether the role is default in the application or has been created. Default aplication roles will never be deleted. 1 - role is default by the application 0 - role has been created.'");
	DB::statement("COMMENT ON column role.is_super IS 'Show whether this role is only for super role or not. 1 - role is for super user 0 - role is not for super user.'");


//
// NOTE -- role_permission
// --------------------------------------------------

	Schema::create('role_permission', function(Blueprint $table) {
	    $table->bigIncrements('role_permission_id');
	    $table->smallInteger('role_id');
	    $table->smallInteger('permission_id');
	    $table->char('status', 1)->default("1")->comment("Specify whether the assignment of permission in a role is active or inactive.");
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['role_id']);
	    $table->index(['permission_id']);
	});
	DB::statement("COMMENT ON TABLE role_permission IS 'List the permission available for each role registered.'");
	DB::statement("COMMENT ON column role_permission.status IS 'Specify whether the assignment of permission in a role is active or inactive.'");


//
// NOTE -- sysdef
// --------------------------------------------------

	Schema::create('sysdef', function(Blueprint $table) {
	    $table->increments('sysdef_id');
	});
	DB::statement("COMMENT ON TABLE sysdef IS 'Store the system settings and parameter defaults.'");


//
// NOTE -- workflow
// --------------------------------------------------

	Schema::create('workflow', function(Blueprint $table) {
	    $table->bigIncrements('workflow_id');
	    $table->bigInteger('file_id');
	    $table->integer('officer_id');
	    $table->text('comment');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->useCurrent();
	    $table->smallInteger('priority_id')->default("2");
	    $table->char('done', 1)->nullable()->comment("Specify whether all officers if specified for a workflow has receipted/recived the file. 0 for No, 1 for Yes.");
	    $table->index(['file_id']);
	    $table->index(['officer_id']);
	});
	DB::statement("COMMENT ON TABLE workflow IS 'Store all the comments for the file for officers and monitor the file movement of the file amoung different levels.'");
	DB::statement("COMMENT ON column workflow.done IS 'Specify whether all officers if specified for a workflow has receipted/recived the file. 0 for No, 1 for Yes. If this value is 0, the there must be one officer who didnot received the file and most possibly someone else must have forced picked the file with warning'");


//
// NOTE -- workflow_officer
// --------------------------------------------------

	Schema::create('workflow_officer', function(Blueprint $table) {
	    $table->bigIncrements('workflow_officer_id');
	    $table->bigInteger('workflow_id');
	    $table->integer('officer_id');
	    $table->timestamp('received_at')->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['officer_id']);
	});
	DB::statement("COMMENT ON TABLE workflow_officer IS 'Workflow message directed to officers'");


//
// NOTE -- branch_foreign
// --------------------------------------------------

	Schema::table('branch', function(Blueprint $table) {
	    $table->foreign('region_id')->references('region_id')->on('region')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- country_foreign
// --------------------------------------------------

	Schema::table('country', function(Blueprint $table) {
	    $table->foreign('country_id')->references('country_all_id')->on('country_all')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- department_foreign
// --------------------------------------------------

	Schema::table('department', function(Blueprint $table) {
	    $table->foreign('branch_id')->references('branch_id')->on('branch')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- document_foreign
// --------------------------------------------------

	Schema::table('document', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('folder_id')->references('folder_id')->on('folder')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('priority_id')->references('priority_id')->on('priority')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- document_audit_foreign
// --------------------------------------------------

	Schema::table('document_audit', function(Blueprint $table) {
	    $table->foreign('document_id')->references('document_id')->on('document')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- document_status_foreign
// --------------------------------------------------

	Schema::table('document_status', function(Blueprint $table) {
	    $table->foreign('action_id')->references('action_id')->on('action')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('document_id')->references('document_id')->on('document')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('priority_id')->references('priority_id')->on('priority')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- file_foreign
// --------------------------------------------------

	Schema::table('file', function(Blueprint $table) {
	    $table->foreign('file_category_id')->references('file_category_id')->on('file_category')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('file_type_id')->references('file_type_id')->on('file_type')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('officer_closed')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('officer_edited')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('officer_opened')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('priority_id')->references('priority_id')->on('priority')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- file_audit_foreign
// --------------------------------------------------

	Schema::table('file_audit', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- file_department_foreign
// --------------------------------------------------

	Schema::table('file_department', function(Blueprint $table) {
	    $table->foreign('department_id')->references('department_id')->on('department')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	});


//
// NOTE -- file_officer_foreign
// --------------------------------------------------

	Schema::table('file_officer', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- file_pickup_foreign
// --------------------------------------------------

	Schema::table('file_pickup', function(Blueprint $table) {
	    $table->foreign('workflow_id')->references('workflow_id')->on('workflow')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- file_request_foreign
// --------------------------------------------------

	Schema::table('file_request', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- file_status_audit_foreign
// --------------------------------------------------

	Schema::table('file_status_audit', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	});


//
// NOTE -- folder_foreign
// --------------------------------------------------

	Schema::table('folder', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- history_foreign
// --------------------------------------------------

	Schema::table('history', function(Blueprint $table) {
	    $table->foreign('history_type_id')->references('history_type_id')->on('history_type')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- notification_foreign
// --------------------------------------------------

	Schema::table('notification', function(Blueprint $table) {
	    $table->foreign('notification_type_id')->references('notification_type_id')->on('notification_type')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- notification_officer_foreign
// --------------------------------------------------

	Schema::table('notification_officer', function(Blueprint $table) {
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('notification_id')->references('notification_id')->on('notification')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- officer_foreign
// --------------------------------------------------

	Schema::table('officer', function(Blueprint $table) {
	    $table->foreign('department_id')->references('department_id')->on('department')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- officer_permission_foreign
// --------------------------------------------------

	Schema::table('officer_permission', function(Blueprint $table) {
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('permission_id')->references('permission_id')->on('permission')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- officer_role_foreign
// --------------------------------------------------

	Schema::table('officer_role', function(Blueprint $table) {
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('role_id')->references('role_id')->on('role')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- permission_foreign
// --------------------------------------------------

	Schema::table('permission', function(Blueprint $table) {
	    $table->foreign('permission_group_id')->references('permission_group_id')->on('permission_group')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- permission_dependency_foreign
// --------------------------------------------------

	Schema::table('permission_dependency', function(Blueprint $table) {
	    $table->foreign('permission_id')->references('permission_id')->on('permission')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('dependency_id')->references('permission_id')->on('permission')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- region_foreign
// --------------------------------------------------

	Schema::table('region', function(Blueprint $table) {
	    $table->foreign('country_id')->references('country_all_id')->on('country_all')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- role_permission_foreign
// --------------------------------------------------

	Schema::table('role_permission', function(Blueprint $table) {
	    $table->foreign('permission_id')->references('permission_id')->on('permission')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('role_id')->references('role_id')->on('role')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- workflow_foreign
// --------------------------------------------------

	Schema::table('workflow', function(Blueprint $table) {
	    $table->foreign('file_id')->references('file_id')->on('file')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('priority_id')->references('priority_id')->on('priority')->onDelete('restrict')->onUpdate('cascade');
	});


//
// NOTE -- workflow_officer_foreign
// --------------------------------------------------

	Schema::table('workflow_officer', function(Blueprint $table) {
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('workflow_id')->references('workflow_id')->on('workflow')->onDelete('cascade')->onUpdate('cascade');
	});
	
	DB::commit();
	
    }

//
// NOTE - Revert the changes to the database.
// --------------------------------------------------

    public function down() {
	
	DB::beginTransaction();

	Schema::drop('action');
	Schema::drop('attachment');
	Schema::drop('branch');
	Schema::drop('country');
	Schema::drop('country_all');
	Schema::drop('department');
	Schema::drop('deps_saved_ddl');
	Schema::drop('document');
	Schema::drop('document_audit');
	Schema::drop('document_status');
	Schema::drop('failed_jobs');
	Schema::drop('file');
	Schema::drop('file_audit');
	Schema::drop('file_category');
	Schema::drop('file_department');
	Schema::drop('file_nature');
	Schema::drop('file_officer');
	Schema::drop('file_pickup');
	Schema::drop('file_request');
	Schema::drop('file_status_audit');
	Schema::drop('file_subject');
	Schema::drop('file_type');
	Schema::drop('folder');
	Schema::drop('history');
	Schema::drop('history_type');
	Schema::drop('jobs');
	Schema::drop('notification');
	Schema::drop('notification_officer');
	Schema::drop('notification_type');
	Schema::drop('officer');
	Schema::drop('officer_permission');
	Schema::drop('officer_role');
	Schema::drop('password_resets');
	Schema::drop('permission');
	Schema::drop('permission_dependency');
	Schema::drop('permission_group');
	Schema::drop('priority');
	Schema::drop('region');
	Schema::drop('role');
	Schema::drop('role_permission');
	Schema::drop('sysdef');
	Schema::drop('workflow');
	Schema::drop('workflow_officer');
	
	DB::commit();
    }

}
