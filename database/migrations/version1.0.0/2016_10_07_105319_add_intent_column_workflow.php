<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIntentColumnWorkflow extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::table('workflow_officer', function ($table) {
	    $table->char('intent', 1)->default("1");
	});
	\DB::statement("comment on column workflow_officer.intent is 'shows whether the recepient was intended in the previous dispatch. 1 user was intended, 0 user was not intended in the file dispatched but rather picked the file while the file was not sent to him/her'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	Schema::table('workflow_officer', function ($table) {
	    $table->dropColumn('intent');
	});
    }

}
