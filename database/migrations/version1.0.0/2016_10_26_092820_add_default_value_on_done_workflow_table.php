<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueOnDoneWorkflowTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();
	Schema::table('workflow', function ($table) {
	    $table->string('done', 1)->default("0")->change();
	});
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	Schema::table('workflow', function ($table) {
	    $table->string('done', 1)->default(null)->nullable()->change();
	});
	DB::commit();
    }

}
