<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAndModifySubjectCaseTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();
	DB::statement("COMMENT ON TABLE file_subject IS 'table to store all the file subjects, e.g Employer file, customer file'");
	Schema::table('file_subject', function ($table) {
	    $table->dropColumn('file_subject_id');
	});
	Schema::table('file_subject', function ($table) {
	    $table->increments('file_subject_id');
	});
	Schema::table('file', function ($table) {
	    $table->dropColumn('subject');
	    $table->integer('file_subject_id')->nullable();
	    $table->index(['file_subject_id']);
	    $table->foreign('file_subject_id')->references('file_subject_id')->on('file_subject')->onDelete('restrict')->onUpdate('cascade');
	});
	Schema::create('file_case', function(Blueprint $table) {
	    $table->increments('file_case_id');
	    $table->integer('file_subject_id');
	    $table->string('name', 500);
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['file_subject_id']); //index for the foreign keys
	    $table->foreign('file_subject_id')->references('file_subject_id')->on('file_subject')->onDelete('restrict')->onUpdate('cascade');
	});
	DB::statement("COMMENT ON TABLE file_case IS 'table to store all the cases concerning different file subjects, e.g for file subject employer file, case may be holiday'");
	Schema::table('file_pickup', function (Blueprint $table) {
	    $table->string('released', 1)->default("0");
	    $table->integer('file_case_id')->nullable();
	    $table->index(['file_case_id']); //index for the foreign keys
	    $table->foreign('file_case_id')->references('file_case_id')->on('file_case')->onDelete('restrict')->onUpdate('cascade');
	});
	DB::statement("COMMENT ON column file_pickup.released IS 'determine if this file pickup during disposing, was it released or transfered to another person 0 - not released/ transfered to another person, 1 - was released'");
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	DB::statement("COMMENT ON TABLE file_subject IS 'Table to store the file subject entered by user during file registration for future auto suggesting of the last saved file subjects to user to avoid hassle of refilling the same file subject from time to time.'");
	Schema::table('file', function ($table) {
	    $table->string('subject', 500)->nullable();
	    $table->dropColumn('file_subject_id');
	});
	Schema::table('file_pickup', function ($table) {
	    $table->dropColumn('released');
	    $table->dropColumn('file_case_id');
	});
	Schema::drop('file_case');
	Schema::table('file_subject', function ($table) {
	    $table->dropColumn('file_subject_id');
	});
	Schema::table('file_subject', function ($table) {
	    $table->increments('file_subject_id');
	});
	DB::commit();
    }

}
