<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReaderTimeOnTrackTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();

	Schema::table('track', function ($table) {
	    $table->timestamp('reader_time');
	});

	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();

	Schema::table('track', function ($table) {
	    $table->dropColumn('reader_time');
	});

	DB::commit();
    }

}
