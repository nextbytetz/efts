<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileTypeIdBulk extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();

	Schema::table('bulk', function ($table) {
	    $table->integer('file_type_id')->default(1);
	    $table->foreign('file_type_id')->references('file_type_id')->on('file_type')->onDelete('restrict')->onUpdate('cascade');
	});

	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();

	Schema::table('bulk', function ($table) {
	    $table->dropColumn('file_type_id');
	});

	DB::commit();
    }

}
