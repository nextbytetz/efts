<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnEpcOnFile extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();
	Schema::table('file', function ($table) {
	    $table->index(['epc']);
	});
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	Schema::table('file', function ($table) {
	    $table->dropIndex(['epc']);
	});
	DB::commit();
    }

}
