<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZoneAndTrackingTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();

	Schema::create('zone', function(Blueprint $table) {
	    $table->increments('zone_id');
	    $table->string('reader', 20);
	    $table->string('name', 100);
	    $table->integer('antenna');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE zone IS 'Table to store all of the rfid zones existing in the office'");
	DB::statement("COMMENT ON column zone.reader IS 'name of the rfid fixed reader fixed in the specified zone'");
	DB::statement("COMMENT ON column zone.name IS 'name of the rfid zone defined'");
	DB::statement("COMMENT ON column zone.antenna IS 'Is the antenna number according to the port on which the antenna has connected to the fixed reader. Can either de 1 or 2. Because sargas reader has two antenna ports'");

	Schema::create('zone_department', function(Blueprint $table) {
	    $table->increments('zone_department_id');
	    $table->integer('zone_id');
	    $table->integer('department_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['zone_id']); //index for the foreign keys
	    $table->index(['department_id']); //index for the foreign keys
	    $table->foreign('zone_id')->references('zone_id')->on('zone')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('department_id')->references('department_id')->on('department')->onDelete('restrict')->onUpdate('cascade');
	});
	DB::statement("COMMENT ON TABLE zone_department IS 'Table to store the department which belong to the ertain zone'");

	Schema::create('track', function(Blueprint $table) {
	    $table->bigIncrements('track_id');
	    $table->integer('zone_id');
	    $table->string('epc', 30);
	    $table->float('frequency');
	    $table->integer('antenna');
	    $table->integer('read_count');
	    $table->integer('rssi');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['epc']); //index for the foreign keys
	    $table->index(['zone_id']); //index for the foreign keys
	    $table->foreign('zone_id')->references('zone_id')->on('zone')->onDelete('cascade')->onUpdate('cascade');
	});
	DB::statement("COMMENT ON TABLE track IS 'Table to store the tracking details received from the rfid fixed readers through the web apis'");

	Schema::table('file', function ($table) {
	    $table->index(['bulk_id']);
	});

	Schema::table('bulk', function ($table) {
	    $table->index(['officer_id']);
	});

	Schema::table('bulk_department', function ($table) {
	    $table->index(['bulk_id']);
	    $table->index(['department_id']);
	});

	Schema::table('bulk_officer', function ($table) {
	    $table->index(['bulk_id']);
	    $table->index(['officer_id']);
	});
	
	Schema::table('file', function(Blueprint $table) {
	    $table->dropForeign(['bulk_id']);
	});

	Schema::table('file', function(Blueprint $table) {
	    $table->foreign('bulk_id')->references('bulk_id')->on('bulk')->onDelete('set null')->onUpdate('cascade');
	});

	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	Schema::table('bulk_department', function ($table) {
	    $table->dropIndex(['bulk_id']);
	    $table->dropIndex(['department_id']);
	});
	Schema::table('bulk_officer', function ($table) {
	    $table->dropIndex(['bulk_id']);
	    $table->dropIndex(['officer_id']);
	});
	Schema::drop('track');
	Schema::drop('zone_department');
	Schema::drop('zone');
	DB::commit();
    }

}
