<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePicColumnOnOfficer extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();
	Schema::table('officer', function ($table) {
	    $table->string('pic', 6)->nullable()->change();
	});
	\DB::statement("comment on column officer.pic is 'store the name of the saved image of the officer'");
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	Schema::table('officer', function ($table) {
	    $table->string('pic', 200)->nullable()->change();
	});
	\DB::statement("comment on column officer.pic is ''");
	DB::commit();
    }

}
