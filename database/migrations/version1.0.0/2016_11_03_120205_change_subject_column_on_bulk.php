<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSubjectColumnOnBulk extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	Schema::table('bulk', function ($table) {
	    $table->dropColumn('subject');
	    $table->integer('file_subject_id');
	    $table->index(['file_subject_id']);
	    $table->foreign('file_subject_id')->references('file_subject_id')->on('file_subject')->onDelete('restrict')->onUpdate('cascade');
	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	Schema::table('bulk', function ($table) {
	    $table->string('subject', 200)->nullable();
	    $table->dropColumn('file_subject_id');
	});
    }

}
