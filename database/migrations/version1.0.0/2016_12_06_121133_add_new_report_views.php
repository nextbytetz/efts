<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewReportViews extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();
	Schema::table('file_pickup', function (Blueprint $table) {
	    $table->bigInteger('child_id')->nullable();
	    $table->index(['child_id']);
	    $table->foreign('child_id')->references('file_pickup_id')->on('file_pickup')->onUpdate('cascade');
	});
	DB::statement("create or replace view basic_rpt as select a.name as file, a.reference, a.file_category_id, a.file_id, a.file_type_id, a.file_nature_id, a.priority_id, c.officer_id, case when c.officer_id is not null then c.firstname || ' ' || c.othernames else null end as officer, case when b.file_pickup_id is not null then b.created_at else null end as pick_date, case when b.file_pickup_id is null then 'Released' else 'Picked Up' end as state, case when a.status = '1' then 'OPEN' else 'CLOSED' end as status, d.name as subject, a.created_at from file a left join file_pickup b on a.file_pickup_id = b.file_pickup_id left join officer c on c.officer_id = b.officer_id left join file_subject d on a.file_subject_id = d.file_subject_id where a.file_category_id = 1 and a.file_type_id <> 3");
	DB::statement("create or replace view user_rpt as  select b.name as file, b.reference, b.file_id, b.file_category_id, b.file_type_id, b.file_nature_id, a.disposed_at, b.priority_id, a.officer_id, c.firstname || ' ' || c.othernames as officer, a.created_at as receive_date, a.workflow_id, a.file_pickup_id, a.child_id, case when f.officer_id is not null then f.firstname || ' ' || f.othernames else null end as desk, case when g.officer_id is not null then g.firstname || ' ' || g.othernames else null end as receiver from file_pickup a join file b on a.file_id = b.file_id join officer c on c.officer_id = a.officer_id left join file_pickup d on a.child_id = d.file_pickup_id left join file_pickup e on b.file_pickup_id = e.file_pickup_id left join officer f on e.officer_id = f.officer_id left join officer g on g.officer_id = d.officer_id where b.file_category_id = 1 and b.file_type_id <> 3");
	DB::statement("create or replace view closed_rpt as select a.file_id, a.name as file, a.reference, a.shelf_number, d.name as subject, a.created_at, a.closed_at, b.firstname || ' ' || b.othernames as officer_opened, c.firstname || ' ' || c.othernames as officer_closed from file a join officer b on a.officer_opened = b.officer_id left join officer c on a.officer_closed = c.officer_id join file_subject d on a.file_subject_id = d.file_subject_id where a.status = '2' and a.file_category_id = 1 and a.file_type_id <> 3");
	DB::statement("create or replace view tracking_rpt as select b.name as file, b.reference, c.name as zone, a.updated_at , a.created_at, a.track_id from track a join file b on a.epc =  b.epc join zone c on a.zone_id = c.zone_id");
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	DB::statement("drop view if exists basic_rpt");
	DB::statement("drop view if exists user_rpt");
	Schema::table('file_pickup', function ($table) {
	    $table->dropColumn('child_id');
	});
	DB::statement("drop view if exists closed_rpt");
	DB::statement("drop view if exists tracking_rpt");
	DB::commit();
    }

}
