<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexOnReaderTimeOnTrack extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();

	Schema::table('track', function ($table) {
	    $table->index(['reader_time']);
	});

	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	Schema::table('track', function ($table) {
	    $table->dropIndex(['reader_time']);
	});
	DB::commit();
    }

}
