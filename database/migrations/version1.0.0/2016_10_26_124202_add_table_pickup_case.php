<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePickupCase extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	DB::beginTransaction();
	Schema::table('file_pickup', function ($table) {
	    $table->dropColumn('file_case_id');
	});
	Schema::create('pickup_case', function (Blueprint $table) {
	    $table->bigIncrements('pickup_case_id');
	    $table->bigInteger('file_pickup_id');
	    $table->integer('file_case_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	    $table->index(['file_case_id']);
	    $table->index(['file_pickup_id']);
	    $table->foreign('file_pickup_id')->references('file_pickup_id')->on('file_pickup')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('file_case_id')->references('file_case_id')->on('file_case')->onDelete('restrict')->onUpdate('cascade');
	});
	DB::statement("COMMENT ON TABLE pickup_case IS 'table to store the cases for picking a file'");
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	DB::beginTransaction();
	Schema::table('file_pickup', function (Blueprint $table) {
	    $table->integer('file_case_id');
	    $table->index(['file_case_id']); //index for the foreign keys
	    $table->foreign('file_case_id')->references('file_case_id')->on('file_case')->onDelete('restrict')->onUpdate('cascade');
	});
	Schema::drop('pickup_case');
	DB::commit();
    }

}
