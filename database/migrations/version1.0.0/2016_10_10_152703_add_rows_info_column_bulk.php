<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRowsInfoColumnBulk extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	
	DB::beginTransaction();
	
	Schema::table('bulk', function ($table) {
	    $table->integer('rows_imported')->default("0");
	    $table->integer('total_rows')->nullable();
	});
	
	DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	
	DB::beginTransaction();
	
	Schema::table('bulk', function ($table) {
	    $table->dropColumn('rows_imported');
	    $table->dropColumn('total_rows');
	});
	
	DB::commit();
	
    }

}
