<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBulkTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
	
	DB::beginTransaction();

	Schema::table('file', function ($table) {
	    $table->bigInteger('bulk_id')->nullable();
	});
	\DB::statement("comment on column file.bulk_id is 'optional column which shows that this file was uploaded using which bulk import. If this column does not contain any value it means the file was registered without using bulk import.'");

	Schema::create('bulk', function(Blueprint $table) {
	    $table->increments('bulk_id');
	    $table->string('group_name', 200);
	    $table->string('name', 500);
	    $table->string('mime', 150);
	    $table->string('ext', 20);
	    $table->double('size');
	    $table->string('subject', 200)->nullable();
	    $table->integer('priority_id')->default(2);
	    $table->integer('officer_id');
	    $table->char('status', '1')->default("1");
	    $table->text('notice')->nullable();
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE bulk IS 'Table to hold all the bulk imports for file registration'");
	DB::statement("COMMENT ON column bulk.group_name IS 'name of bulk for easy reference in the future'");
	DB::statement("COMMENT ON column bulk.name IS 'store the name of the uploaded file'");
	DB::statement("COMMENT ON column bulk.status IS 'Display the status of the bulk upload, 1 - Pending to be uploaded, 2 - Upload in progress, 3 - Success all files registered/uploaded, 4 - failed to some reasons to upload.'");
	DB::statement("COMMENT ON column bulk.notice IS 'Store any notice about the upload result including if there was any error to display to the user'");

	Schema::create('bulk_officer', function(Blueprint $table) {
	    $table->increments('bulk_officer_id');
	    $table->integer('bulk_id');
	    $table->integer('officer_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE bulk_officer IS 'Table to show officers specified for a certain bulk upload, each file in the bulk will be specified to the officers specified for this bulk'");

	Schema::create('bulk_department', function(Blueprint $table) {
	    $table->increments('bulk_department_id');
	    $table->integer('bulk_id');
	    $table->integer('department_id');
	    $table->timestamp('created_at')->useCurrent();
	    $table->timestamp('updated_at')->nullable();
	});
	DB::statement("COMMENT ON TABLE bulk_department IS 'Table to show departments specified for a certain bulk upload, each file in the bulk will be specified to the departments specified for this bulk'");
	
	Schema::table('file', function(Blueprint $table) {
	    $table->foreign('bulk_id')->references('bulk_id')->on('bulk')->onDelete('cascade')->onUpdate('cascade');
	});
	
	Schema::table('bulk', function(Blueprint $table) {
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	    $table->foreign('priority_id')->references('priority_id')->on('priority')->onDelete('restrict')->onUpdate('cascade');
	});
	
	Schema::table('bulk_officer', function(Blueprint $table) {
	    $table->foreign('bulk_id')->references('bulk_id')->on('bulk')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('officer_id')->references('officer_id')->on('officer')->onDelete('restrict')->onUpdate('cascade');
	});
	
	Schema::table('bulk_department', function(Blueprint $table) {
	    $table->foreign('bulk_id')->references('bulk_id')->on('bulk')->onDelete('cascade')->onUpdate('cascade');
	    $table->foreign('department_id')->references('department_id')->on('department')->onDelete('restrict')->onUpdate('cascade');
	});
	
	DB::commit();
	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
	
	DB::beginTransaction();
	Schema::drop('bulk_department');
	Schema::drop('bulk_officer');
	Schema::drop('bulk');
	DB::commit();
	
    }

}
